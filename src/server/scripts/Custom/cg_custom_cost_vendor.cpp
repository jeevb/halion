/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "CurrencyMgr.h"
#include "GossipMgr.h"
#include "Language.h"

enum GearVendorAction
{
    CUSTOM_COST_VENDOR_BROWSE_ITEMS,
    CUSTOM_COST_VENDOR_COMPLETE_PURCHASE
};

class cg_custom_cost_vendor : public CreatureScript
{
    public:

        cg_custom_cost_vendor() : CreatureScript("cg_custom_cost_vendor") { }

        bool OnGossipHello(Player* player, Creature* creature) override
        {
            WorldSession* session = player->GetSession();

            player->CLEAR_GOSSIP_MENU();

            if (player->IsInCombat())
            {
                session->SendNotification(LANG_YOU_IN_COMBAT);
                return true;
            }

            sCurrencyMgr->UpdateRewardCurrencies(player);
            session->SendListInventory(creature->GetGUID());
            return true;
        }

        bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action) override
        {
            player->CLEAR_GOSSIP_MENU();
            player->CLOSE_GOSSIP_MENU();

            switch (action)
            {
                case CUSTOM_COST_VENDOR_BROWSE_ITEMS:
                {
                    return OnGossipHello(player, creature);
                }
                case CUSTOM_COST_VENDOR_COMPLETE_PURCHASE:
                {
                    if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(sender))
                    {
                        _PurchaseItemForCost(player, creature, itemTemplate);
                        return OnGossipHello(player, creature);
                    }
                    break;
                }
                default:
                    break;
            }

            return _SendErrorMenu(player, creature);
        }

        bool OnBeforePurchaseItem(Player* player, Creature* creature, ItemTemplate const* itemTemplate, VendorItem const* /*vendor_item*/) override
        {
            if (uint32 cost = sCurrencyMgr->GetVendorItemCustomCostId(creature->GetEntry(), itemTemplate->ItemId))
            {
                // Display cost requirements
                ChatHandler(player->GetSession()).PSendSysMessage("You require the following to purchase %s:\n%s",
                                                                 itemTemplate->ItemLink().c_str(),
                                                                 sCurrencyMgr->GetCustomCostDescription(player, cost).c_str());

                if (sCurrencyMgr->PurchaseItemForCost(player, 0, cost, false))
                    _SendPurchaseItemConfirmation(player, creature, itemTemplate);

                return false;
            }

            return true;
        }

        void ModifyItemVendorData(Player* player, Creature* creature, ItemTemplate const* itemTemplate, VendorItem const* /*vendor_item*/, int32& /*price*/, uint32& extendedCost, uint32& leftInStock, uint32& /*display*/) override
        {
            if (uint32 cost = sCurrencyMgr->GetVendorItemCustomCostId(creature->GetEntry(), itemTemplate->ItemId))
            {
                // Override extended cost requirements
                extendedCost = 2; // Free, no gold cost

                // Grey out items that the player cannot afford
                if (!sCurrencyMgr->CanAffordCustomCost(player, cost))
                    leftInStock = 0;
            }
        }

    private:

        void _SendPurchaseItemConfirmation(Player* player, Creature* creature, ItemTemplate const* itemTemplate)
        {
            std::ostringstream buf;
            buf << "You have chosen to purchase the following item:\n\n"
                << GOSSIP_ICON_TEXT_EXTENDED(_GetItemLink(itemTemplate), _GetItemIcon(itemTemplate), 30, -6, -19, "  ") << "\n\n"
                << "Are you sure you want to proceed?\n";

            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, "Yes, I'd like to purchase this item now.", itemTemplate->ItemId, CUSTOM_COST_VENDOR_COMPLETE_PURCHASE);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, "No, I'd like to continue browsing your items.", NULL, CUSTOM_COST_VENDOR_BROWSE_ITEMS);
            SEND_CUSTOM_GOSSIP_MENU(player, creature->GetGUID(), buf.str());
        }

        std::string _GetItemLink(ItemTemplate const* proto)
        {
            std::string itemName = proto->Name1;

            if (itemName.size() > MAX_LINE_CHARS)
            {
                itemName.resize(MAX_LINE_CHARS - 3);
                itemName += "...";
            }

            std::ostringstream buf;
            buf << "|c" << std::hex << ItemQualityDarkColors[proto->Quality] << std::dec
                << "|Hitem:" << proto->ItemId << ":0:0:0:0:0:0:0:0:0:0|h" << itemName << "|h|r";

            return buf.str();
        }

        std::string _GetItemIcon(ItemTemplate const* proto)
        {
            if (ItemDisplayInfoEntry const* displayInfo = sItemDisplayInfoStore.LookupEntry(proto->DisplayInfoID))
                return "ICONS/" + std::string(displayInfo->inventoryIcon);
            else
                return "ICONS/INV_Misc_QuestionMark";
        }

        void _PurchaseItemForCost(Player* player, Creature* creature, ItemTemplate const* itemTemplate)
        {
            uint32 customCost = sCurrencyMgr->GetVendorItemCustomCostId(creature->GetEntry(), itemTemplate->ItemId);

            ItemPosCountVec dest;
            InventoryResult msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, itemTemplate->ItemId, itemTemplate->BuyCount);
            if (msg == EQUIP_ERR_OK)
            {
                if (sCurrencyMgr->PurchaseItemForCost(player, creature->GetEntry(), customCost))
                    if (Item* item = player->StoreNewItem(dest, itemTemplate->ItemId, true, Item::GenerateItemRandomPropertyId(itemTemplate->ItemId)))
                        player->SendNewItem(item, itemTemplate->BuyCount, true, false);
            }
            else
                player->SendEquipError(msg, NULL, NULL);
        }

        bool _SendErrorMenu(Player* player, Creature* creature)
        {
            ChatHandler(player->GetSession()).SendSysMessage("An error has occurred. Please try again.");
            return OnGossipHello(player, creature);
        }
};

void AddSC_cg_custom_cost_vendor()
{
    new cg_custom_cost_vendor();
}