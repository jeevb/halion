/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "GossipMgr.h"
#include "Language.h"
#include "TrackMgr.h"
#include "TransmogrificationDefines.h"

enum GearSlot
{
    GEAR_SLOT_HEAD,
    GEAR_SLOT_NECK,
    GEAR_SLOT_SHOULDERS,
    GEAR_SLOT_BACK,
    GEAR_SLOT_CHEST,
    GEAR_SLOT_BODY,
    GEAR_SLOT_TABARD,
    GEAR_SLOT_WRISTS,
    GEAR_SLOT_HANDS,
    GEAR_SLOT_WAIST,
    GEAR_SLOT_LEGS,
    GEAR_SLOT_FEET,
    GEAR_SLOT_FINGER,
    GEAR_SLOT_TRINKET,
    GEAR_SLOT_MAINHAND,
    GEAR_SLOT_OFFHAND,
    GEAR_SLOT_RANGED,

    MAX_GEAR_SLOT
};

struct GearSlotInfo
{
    GearSlotInfo(EquipmentSlots _eSlot, std::string const& _name, std::string const& _icon) :
        eSlot(_eSlot), name(_name), icon(_icon) { }

    EquipmentSlots  eSlot;
    std::string     name;
    std::string     icon;
};

const GearSlotInfo GearSlotInfoStore[MAX_GEAR_SLOT] =
{
    GearSlotInfo(EQUIPMENT_SLOT_HEAD,        "Head",         "Icons/inv_helmet_03"              ),
    GearSlotInfo(EQUIPMENT_SLOT_NECK,        "Neck",         "Icons/inv_jewelry_amulet_05"      ),
    GearSlotInfo(EQUIPMENT_SLOT_SHOULDERS,   "Shoulders",    "Icons/inv_shoulder_28"            ),
    GearSlotInfo(EQUIPMENT_SLOT_BACK,        "Back",         "Icons/inv_misc_cape_18"           ),
    GearSlotInfo(EQUIPMENT_SLOT_CHEST,       "Chest",        "Icons/inv_chest_chain_07"         ),
    GearSlotInfo(EQUIPMENT_SLOT_BODY,        "Shirt",        "Icons/inv_shirt_01"               ),
    GearSlotInfo(EQUIPMENT_SLOT_TABARD,      "Tabard",       "Icons/inv_shirt_guildtabard_01"   ),
    GearSlotInfo(EQUIPMENT_SLOT_WRISTS,      "Wrists",       "Icons/inv_bracer_06"              ),
    GearSlotInfo(EQUIPMENT_SLOT_HANDS,       "Hands",        "Icons/inv_gauntlets_05"           ),
    GearSlotInfo(EQUIPMENT_SLOT_WAIST,       "Waist",        "Icons/inv_belt_03"                ),
    GearSlotInfo(EQUIPMENT_SLOT_LEGS,        "Legs",         "Icons/inv_pants_02"               ),
    GearSlotInfo(EQUIPMENT_SLOT_FEET,        "Feet",         "Icons/inv_boots_01"               ),
    GearSlotInfo(EQUIPMENT_SLOT_FINGER1,     "Finger",       "Icons/inv_jewelry_ring_03"        ),
    GearSlotInfo(EQUIPMENT_SLOT_TRINKET1,    "Trinket",      "Icons/inv_jewelry_talisman_05"    ),
    GearSlotInfo(EQUIPMENT_SLOT_MAINHAND,    "Main-hand",    "Icons/inv_throwingaxe_06"         ),
    GearSlotInfo(EQUIPMENT_SLOT_OFFHAND,     "Off-hand",     "Icons/inv_misc_book_11"           ),
    GearSlotInfo(EQUIPMENT_SLOT_RANGED,      "Ranged/Relic", "Icons/inv_jewelry_talisman_06"    )
};

enum GearVendorAction
{
    GEAR_VENDOR_MAIN_MENU,
    GEAR_VENDOR_PURCHASE_ITEM,
    GEAR_VENDOR_COMPLETE_PURCHASE
};

enum GearVendorGossipMessage
{
    GEAR_VENDOR_GOSSIP_MESSAGE_MAIN = 1000009
};

class cg_gear_vendor : public CreatureScript
{
    private:

        typedef std::map<Player*, uint32> Storage;
        Storage m_Storage;

    public:

        cg_gear_vendor() : CreatureScript("cg_gear_vendor") { }

        void OnLogout(Player* player) override
        {
            // Clear player data on logout
            m_Storage.erase(player);
        }

        bool OnGossipHello(Player* player, Creature* creature) override
        {
            player->CLEAR_GOSSIP_MENU();

            m_Storage.erase(player);

            if (player->IsInCombat())
            {
                player->GetSession()->SendNotification(LANG_YOU_IN_COMBAT);
                return true;
            }

            for (uint8 i = GEAR_SLOT_HEAD; i < MAX_GEAR_SLOT; ++i)
            {
                GearSlotInfo const& info = GearSlotInfoStore[i];
                if (_ShowEquipmentSlot(player, creature, info.eSlot))
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD,
                                            GOSSIP_ICON_TEXT(info.name, info.icon),
                                            info.eSlot,
                                            GEAR_VENDOR_PURCHASE_ITEM);
            }

            player->SEND_GOSSIP_MENU(GEAR_VENDOR_GOSSIP_MESSAGE_MAIN, creature->GetGUID());
            return true;
        }

        bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action) override
        {
            player->CLEAR_GOSSIP_MENU();
            player->CLOSE_GOSSIP_MENU();

            switch (action)
            {
                case GEAR_VENDOR_MAIN_MENU:
                    return OnGossipHello(player, creature);
                case GEAR_VENDOR_PURCHASE_ITEM:
                {
                    if (!_IsValidGearVendorSlot(sender))
                       break;

                    m_Storage[player] = sender;
                    player->GetSession()->SendListInventory(creature->GetGUID());
                    return true;
                }
                case GEAR_VENDOR_COMPLETE_PURCHASE:
                {
                    if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(sender))
                    {
                        _PurchaseItemForCost(player, creature, itemTemplate);
                        return OnGossipHello(player, creature);
                    }
                    break;
                }
                default:
                    break;
            }

            return _SendErrorMenu(player, creature);
        }

        bool ShouldDisplayVendorItem(Player* player, Creature* vendor, ItemTemplate const* itemTemplate, VendorItem const* /*vendor_item*/) override
        {
            uint32 eSlot = m_Storage[player];
            if (!_IsValidGearVendorSlot(eSlot))
                return _SendErrorMenu(player, vendor);

            return _IsUsableGearItem(player, itemTemplate, eSlot);
        }

        bool OnBeforePurchaseItem(Player* player, Creature* creature, ItemTemplate const* itemTemplate, VendorItem const* /*vendor_item*/) override
        {
            WorldSession* session = player->GetSession();

            if (uint32 cost = sCurrencyMgr->GetVendorItemCustomCostId(creature->GetEntry(), itemTemplate->ItemId))
            {
                // Display cost requirements
                ChatHandler(session).PSendSysMessage("You require the following to purchase %s:\n%s",
                                                     itemTemplate->ItemLink().c_str(),
                                                     sCurrencyMgr->GetCustomCostDescription(player, cost).c_str());

                if (sCurrencyMgr->PurchaseItemForCost(player, 0, cost, false))
                    _SendPurchaseItemConfirmation(player, creature, itemTemplate);

                return false;
            }

            return true;
        }

        void OnPurchasedItem(Player* player, Creature* vendor, Item* /*item*/, VendorItem const* /*vendor_item*/) override
        {
            OnGossipHello(player, vendor);
        }

        void ModifyItemVendorData(Player* player, Creature* creature, ItemTemplate const* itemTemplate, VendorItem const* /*vendor_item*/, int32& /*price*/, uint32& extendedCost, uint32& leftInStock, uint32& /*display*/) override
        {
            if (uint32 cost = sCurrencyMgr->GetVendorItemCustomCostId(creature->GetEntry(), itemTemplate->ItemId))
            {
                // Override extended cost requirements
                extendedCost = 2; // Free, no gold cost

                // Grey out items that the player cannot afford
                if (!sCurrencyMgr->CanAffordCustomCost(player, cost))
                    leftInStock = 0;
            }
        }

    private:

        bool _ShowEquipmentSlot(Player* player, Creature* creature, uint32 eSlot)
        {
            if (VendorItemData const* items = creature->GetVendorItems(player))
                for (auto const& itr : items->m_items)
                    if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(itr->item))
                        if (_IsUsableGearItem(player, itemTemplate, eSlot))
                            return true;

            return false;
        }

        bool _IsUsableGearItem(Player* player, ItemTemplate const* itemTemplate, uint32 equipmentSlot)
        {
            const static uint32 item_weapon_skills[MAX_ITEM_SUBCLASS_WEAPON] =
            {
                SKILL_AXES,     SKILL_2H_AXES,  SKILL_BOWS,          SKILL_GUNS,      SKILL_MACES,
                SKILL_2H_MACES, SKILL_POLEARMS, SKILL_SWORDS,        SKILL_2H_SWORDS, 0,
                SKILL_STAVES,   0,              0,                   SKILL_FIST_WEAPONS,   0,
                SKILL_DAGGERS,  SKILL_THROWN,   SKILL_ASSASSINATION, SKILL_CROSSBOWS, SKILL_WANDS,
                SKILL_FISHING
            }; // Copied from function Item::GetSkill()

            const static uint32 item_armor_skills[MAX_ITEM_SUBCLASS_ARMOR] =
            {
                0, SKILL_CLOTH, SKILL_LEATHER, SKILL_MAIL, SKILL_PLATE_MAIL, 0, SKILL_SHIELD, 0, 0, 0, 0
            }; // Copied from function Item::GetSkill()

            bool hasSkill = true;
            switch (itemTemplate->Class) {
                case ITEM_CLASS_WEAPON:
                    if (item_weapon_skills[itemTemplate->SubClass])
                        hasSkill = player->GetSkillValue(item_weapon_skills[itemTemplate->SubClass]) != 0;
                    break;
                case ITEM_CLASS_ARMOR:
                    if (item_armor_skills[itemTemplate->SubClass])
                        hasSkill = player->GetSkillValue(item_armor_skills[itemTemplate->SubClass]) != 0;
                    break;
                default: // Gear vendor should not have any other item classes...
                    return false;
            }

            return player->CanEquipInSlot(itemTemplate, equipmentSlot) && hasSkill;
        }

        void _SendPurchaseItemConfirmation(Player* player, Creature* creature, ItemTemplate const* itemTemplate)
        {
            std::ostringstream buf;
            buf << "You have chosen to purchase the following item:\n\n"
                << GOSSIP_ICON_TEXT_EXTENDED(_GetItemLink(itemTemplate), _GetItemIcon(itemTemplate), 30, -6, -19, "  ") << "\n\n"
                << "Are you sure you want to proceed?\n";

            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, "Yes, I'd like to purchase this item now.", itemTemplate->ItemId, GEAR_VENDOR_COMPLETE_PURCHASE);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, "No, I'd like to continue browsing your items.", m_Storage[player], GEAR_VENDOR_PURCHASE_ITEM);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "No, I'd like to return to the main menu.", 0, GEAR_VENDOR_MAIN_MENU);
            SEND_CUSTOM_GOSSIP_MENU(player, creature->GetGUID(), buf.str());
        }

        std::string _GetItemLink(ItemTemplate const* proto)
        {
            std::string itemName = proto->Name1;

            if (itemName.size() > MAX_LINE_CHARS)
            {
                itemName.resize(MAX_LINE_CHARS - 3);
                itemName += "...";
            }

            std::ostringstream buf;
            buf << "|c" << std::hex << ItemQualityDarkColors[proto->Quality] << std::dec
                << "|Hitem:" << proto->ItemId << ":0:0:0:0:0:0:0:0:0:0|h" << itemName << "|h|r";

            return buf.str();
        }

        std::string _GetItemIcon(ItemTemplate const* proto)
        {
            if (ItemDisplayInfoEntry const* displayInfo = sItemDisplayInfoStore.LookupEntry(proto->DisplayInfoID))
                return "ICONS/" + std::string(displayInfo->inventoryIcon);
            else
                return "ICONS/INV_Misc_QuestionMark";
        }

        void _PurchaseItemForCost(Player* player, Creature* creature, ItemTemplate const* itemTemplate)
        {
            uint32 customCost = sCurrencyMgr->GetVendorItemCustomCostId(creature->GetEntry(), itemTemplate->ItemId);

            ItemPosCountVec dest;
            InventoryResult msg = player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, itemTemplate->ItemId, itemTemplate->BuyCount);
            if (msg == EQUIP_ERR_OK)
            {
                if (sCurrencyMgr->PurchaseItemForCost(player, creature->GetEntry(), customCost))
                    if (Item* item = player->StoreNewItem(dest, itemTemplate->ItemId, true, Item::GenerateItemRandomPropertyId(itemTemplate->ItemId)))
                        player->SendNewItem(item, itemTemplate->BuyCount, true, false);
            }
            else
                player->SendEquipError(msg, NULL, NULL);
        }

        bool _SendErrorMenu(Player* player, Creature* creature)
        {
            ChatHandler(player->GetSession()).SendSysMessage("An error has occurred. Please try again.");
            return OnGossipHello(player, creature);
        }

        bool _IsValidGearVendorSlot(uint32 eSlot) const
        {
            for (uint8 i = GEAR_SLOT_HEAD; i < MAX_GEAR_SLOT; ++i)
            {
                GearSlotInfo const& info = GearSlotInfoStore[i];
                if (info.eSlot == EquipmentSlots(eSlot))
                    return true;
            }

            return false;
        }
};

void AddSC_cg_gear_vendor()
{
    new cg_gear_vendor();
}