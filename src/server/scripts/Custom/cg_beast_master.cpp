/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "GossipMgr.h"
#include "Language.h"
#include "NotificationMgr.h"
#include "Pet.h"

enum PetClass
{
    PET_CLASS_CUNNING,
    PET_CLASS_FEROCITY,
    PET_CLASS_TENACITY,
    PET_CLASS_EXOTIC,

    MAX_PET_CLASS
};

enum PetSubClass
{
    // Cunning Pets
    PET_SUBCLASS_BAT,
    PET_SUBCLASS_BIRD_OF_PREY,
    PET_SUBCLASS_DRAGONHAWK,
    PET_SUBCLASS_NETHER_RAY,
    PET_SUBCLASS_RAVAGER,
    PET_SUBCLASS_SERPENT,
    PET_SUBCLASS_SPIDER,
    PET_SUBCLASS_SPOREBAT,
    PET_SUBCLASS_WIND_SERPENT,
    // Ferocity Pets
    PET_SUBCLASS_CARRION_BIRD,
    PET_SUBCLASS_CAT,
    PET_SUBCLASS_HYENA,
    PET_SUBCLASS_MOTH,
    PET_SUBCLASS_RAPTOR,
    PET_SUBCLASS_TALLSTRIDER,
    PET_SUBCLASS_WASP,
    PET_SUBCLASS_WOLF,
    // Tenacity Pets
    PET_SUBCLASS_BEAR,
    PET_SUBCLASS_BOAR,
    PET_SUBCLASS_CRAB,
    PET_SUBCLASS_CROCOLISK,
    PET_SUBCLASS_GORILLA,
    PET_SUBCLASS_SCORPID,
    PET_SUBCLASS_TURTLE,
    PET_SUBCLASS_WARP_STALKER,
    // Exotic Pets
    PET_SUBCLASS_CHIMAERA,
    PET_SUBCLASS_SILITHID,
    PET_SUBCLASS_CORE_HOUND,
    PET_SUBCLASS_DEVILSAUR,
    PET_SUBCLASS_SPIRIT_BEAST,
    PET_SUBCLASS_RHINO,
    PET_SUBCLASS_WORM,

    MAX_PET_SUBCLASS
};

struct OptionDescription
{
    OptionDescription(std::string const& _name, std::string const& _icon) :
        name(_name), icon(_icon) { }

    std::string name;
    std::string icon;
};

OptionDescription PetClassStore[MAX_PET_CLASS] =
{
    OptionDescription("Cunning Pets",   "Icons/ability_hunter_animalhandler"),
    OptionDescription("Ferocity Pets",  "Icons/ability_druid_primaltenacity"),
    OptionDescription("Tenacity Pets",  "Icons/spell_nature_shamanRage"),
    OptionDescription("Exotic Pets",    "Icons/ability_hunter_beastmastery")
};

OptionDescription PetSubClassStore[MAX_PET_SUBCLASS] =
{
    OptionDescription("Bats",           "Icons/ability_hunter_pet_bat"),
    OptionDescription("Bird of Prey",   "Icons/ability_hunter_pet_owl"),
    OptionDescription("Dragonhawks",    "Icons/ability_hunter_pet_dragonhawk"),
    OptionDescription("Nether Rays",    "Icons/ability_hunter_pet_netherray"),
    OptionDescription("Ravagers",       "Icons/ability_hunter_pet_ravager"),
    OptionDescription("Serpents",       "Icons/spell_nature_guardianward"),
    OptionDescription("Spiders",        "Icons/ability_hunter_pet_spider"),
    OptionDescription("Sporebats",      "Icons/ability_hunter_pet_sporebat"),
    OptionDescription("Wind Serpents",  "Icons/ability_hunter_pet_windserpent"),
    OptionDescription("Carrion Birds",  "Icons/ability_hunter_pet_vulture"),
    OptionDescription("Cats",           "Icons/ability_hunter_pet_cat"),
    OptionDescription("Hyenas",         "Icons/ability_hunter_pet_hyena"),
    OptionDescription("Moths",          "Icons/ability_hunter_pet_moth"),
    OptionDescription("Raptors",        "Icons/ability_hunter_pet_raptor"),
    OptionDescription("Tallstriders",   "Icons/ability_hunter_pet_tallstrider"),
    OptionDescription("Wasps",          "Icons/ability_hunter_pet_wasp"),
    OptionDescription("Wolves",         "Icons/ability_hunter_pet_wolf"),
    OptionDescription("Bears",          "Icons/ability_hunter_pet_bear"),
    OptionDescription("Boars",          "Icons/ability_hunter_pet_boar"),
    OptionDescription("Crabs",          "Icons/ability_hunter_pet_crab"),
    OptionDescription("Crocolisks",     "Icons/ability_hunter_pet_crocolisk"),
    OptionDescription("Gorillas",       "Icons/ability_hunter_pet_gorilla"),
    OptionDescription("Scorpids",       "Icons/ability_hunter_pet_scorpid"),
    OptionDescription("Turtles",        "Icons/ability_hunter_pet_turtle"),
    OptionDescription("Warp Stalkers",  "Icons/ability_hunter_pet_warpstalker"),
    OptionDescription("Chimaerae",      "Icons/ability_hunter_pet_chimera"),
    OptionDescription("Silithids",      "Icons/ability_hunter_pet_silithid"),
    OptionDescription("Core Hounds",    "Icons/ability_hunter_pet_corehound"),
    OptionDescription("Devilsaurs",     "Icons/ability_hunter_pet_devilsaur"),
    OptionDescription("Spirit Beasts",  "Icons/ability_druid_primalprecision"),
    OptionDescription("Rhinos",         "Icons/ability_hunter_pet_rhino"),
    OptionDescription("Worms",          "Icons/ability_hunter_pet_worm")
};

enum BeastMasterAction
{
    BEAST_MASTER_MAIN_MENU,
    BEAST_MASTER_PICK_NEW_PET,
    BEAST_MASTER_RESET_TALENTS,
    BEAST_MASTER_STABLE,
    BEAST_MASTER_BROWSE_GOODS,
    BEAST_MASTER_SHOW_PET_SUBCLASS,
    BEAST_MASTER_SHOW_PETS,
    BEAST_MASTER_TAME_PET
};

enum BeastMasterGossipMessage
{
    BEAST_MASTER_GOSSIP_MESSAGE_MAIN    = 1000017,
    BEAST_MASTER_GOSSIP_PET_CLASS       = 1000018,
    BEAST_MASTER_GOSSIP_PET_SUBCLASS    = 1000019,
    BEAST_MASTER_GOSSIP_PETS            = 1000020
};

class cg_beast_master : public CreatureScript
{
    private:

        struct PetData
        {
            uint32 entry;
            uint8 petClass;
            uint8 petSubClass;
            std::string description;
        };

        typedef std::vector<PetData> Pets;
        Pets m_pets;

    public:

        cg_beast_master() : CreatureScript("cg_beast_master")
        {
            m_pets.clear();

            PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_TAMEABLE_PETS);
            if (PreparedQueryResult result = WorldDatabase.Query(stmt))
            {
                do
                {
                    Field* fields = result->Fetch();

                    PetData m_petData;
                    m_petData.entry         = fields[0].GetUInt32();
                    m_petData.petClass      = fields[1].GetUInt8();
                    m_petData.petSubClass   = fields[2].GetUInt8();
                    m_petData.description   = fields[3].GetString();

                    m_pets.push_back(m_petData);
                } while (result->NextRow());
            }
        }

        bool OnGossipHello(Player* player, Creature* creature)
        {
            player->CLEAR_GOSSIP_MENU();

            WorldSession* session = player->GetSession();

            if (player->getClass() != CLASS_HUNTER)
            {
                session->SendNotification("Only hunters can tame pets");
                return true;
            }

            if (player->IsInCombat())
            {
                session->SendNotification(LANG_YOU_IN_COMBAT);
                return true;
            }

            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "I'd like to tame a new pet.", 0, BEAST_MASTER_PICK_NEW_PET);
            player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_INTERACT_1, "I'd like to reset my pet's talents.", 0, BEAST_MASTER_RESET_TALENTS, "Are you sure you want to reset your pet's talents?", 0, false);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "I'd like to access my stabled pets.", 0, BEAST_MASTER_STABLE);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, GOSSIP_TEXT_BROWSE_GOODS, 0, BEAST_MASTER_BROWSE_GOODS);
            player->SEND_GOSSIP_MENU(BEAST_MASTER_GOSSIP_MESSAGE_MAIN, creature->GetGUID());
            return true;
        }

        bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
        {
            player->CLEAR_GOSSIP_MENU();
            player->CLOSE_GOSSIP_MENU();

            WorldSession* session = player->GetSession();

            switch (action)
            {
                case BEAST_MASTER_MAIN_MENU:
                    return OnGossipHello(player, creature);
                case BEAST_MASTER_PICK_NEW_PET:
                {
                    for (uint8 i = PET_CLASS_CUNNING; i < MAX_PET_CLASS; ++i)
                    {
                        if (i == PET_CLASS_EXOTIC && !player->CanTameExoticPets())
                            continue;

                        OptionDescription const& desc = PetClassStore[i];
                        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GOSSIP_ICON_TEXT(desc.name, desc.icon), i, BEAST_MASTER_SHOW_PET_SUBCLASS);
                    }

                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GOSSIP_ICON_TEXT_RETURN_TO_MAIN_MENU, 0, BEAST_MASTER_MAIN_MENU);
                    player->SEND_GOSSIP_MENU(BEAST_MASTER_GOSSIP_PET_CLASS, creature->GetGUID());
                    break;
                }
                case BEAST_MASTER_RESET_TALENTS:
                {
                    Pet* pet = player->GetPet();
                    if (!pet)
                    {
                        session->SendNotification("You do not have a pet");
                        return OnGossipHello(player, creature);
                    }

                    if (pet->getPetType() != HUNTER_PET)
                    {
                        session->SendNotification("Invalid pet");
                        return OnGossipHello(player, creature);
                    }

                    player->ResetPetTalents();
                    session->SendAreaTriggerMessage("Pet talents reset");

                    return OnGossipHello(player, creature);
                }
                case BEAST_MASTER_STABLE:
                {
                    session->SendStablePet(creature->GetGUID());
                    break;
                }
                case BEAST_MASTER_BROWSE_GOODS:
                {
                    session->SendListInventory(creature->GetGUID());
                    break;
                }
                case BEAST_MASTER_SHOW_PET_SUBCLASS:
                {
                    for (uint8 i = PET_SUBCLASS_BAT; i < MAX_PET_SUBCLASS; ++i)
                    {
                        OptionDescription const& desc = PetSubClassStore[i];

                        for (auto const& itr : m_pets)
                        {
                            if (i != itr.petSubClass)
                                continue;

                            if (sender == itr.petClass)
                                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GOSSIP_ICON_TEXT(desc.name, desc.icon), i, BEAST_MASTER_SHOW_PETS);

                            break;
                        }
                    }

                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GOSSIP_ICON_TEXT_RETURN("Return to the pet selection menu"), 0, BEAST_MASTER_PICK_NEW_PET);
                    player->SEND_GOSSIP_MENU(BEAST_MASTER_GOSSIP_PET_SUBCLASS, creature->GetGUID());
                    break;
                }
                case BEAST_MASTER_SHOW_PETS:
                {
                    for (auto const& itr : m_pets)
                    {
                        if (sender == itr.petSubClass)
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TRAINER, itr.description, itr.entry, BEAST_MASTER_TAME_PET);
                    }

                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the pet selection menu", 0, BEAST_MASTER_PICK_NEW_PET);
                    player->SEND_GOSSIP_MENU(BEAST_MASTER_GOSSIP_PETS, creature->GetGUID());
                    break;
                }
                case BEAST_MASTER_TAME_PET:
                {
                    if (!sender)
                    {
                        ChatHandler(session).PSendSysMessage("An error has occurred. Please try again.");
                        return OnGossipHello(player, creature);
                    }

                    CreatePet(player, creature, sender);
                    break;
                }
                default:
                    break;
            }

            return true;
        }

        void CreatePet(Player* player, Creature* creature, uint32 entry)
        {
            WorldSession* session = player->GetSession();

            // This should never happen, but check it anyway...
            if (player->getClass() != CLASS_HUNTER)
            {
                session->SendNotification("Only hunters can tame pets");
                return;
            }

            if (player->GetPetGUID())
            {
                session->SendNotification("You already have an active pet");
                return;
            }

            if (!sObjectMgr->GetCreatureTemplate(entry) ||
                !sObjectMgr->GetCreatureTemplate(entry)->IsTameable(player->CanTameExoticPets()))
            {
                session->SendNotification("Creature is not tameable");
                return;
            }

            Position pos = player->GetNearPosition(PET_FOLLOW_DIST, PET_FOLLOW_ANGLE);
            Creature* creatureTarget = creature->SummonCreature(entry, pos.GetPositionX(), pos.GetPositionY(), pos.GetPositionZ(), pos.GetOrientation(), TEMPSUMMON_CORPSE_TIMED_DESPAWN, 500);
            if (!creatureTarget)
                return;

            // Everything looks OK, create new pet
            Pet* pet = player->CreateTamedPetFrom(creatureTarget, 0);
            if (!pet)
                return;

            creatureTarget->setDeathState(JUST_DIED);
            creatureTarget->RemoveCorpse();
            creatureTarget->SetHealth(0); // just for nice GM-mode view

            pet->SetGuidValue(UNIT_FIELD_CREATEDBY, player->GetGUID());
            pet->SetUInt32Value(UNIT_FIELD_FACTIONTEMPLATE, player->getFaction());

            // prepare visual effect for levelup
            pet->SetUInt32Value(UNIT_FIELD_LEVEL, player->getLevel()-1);

            pet->GetCharmInfo()->SetPetNumber(sObjectMgr->GeneratePetNumber(), true);

            pet->GetMap()->AddToMap(pet->ToCreature());

            // visual effect for levelup
            pet->SetUInt32Value(UNIT_FIELD_LEVEL, player->getLevel());

            // init and update pet stats
            if (!pet->InitStatsForLevel(player->getLevel()))
            {
                TC_LOG_ERROR("misc", "InitStatsForLevel() in cg_gear_vendor::CreatePet() failed! Pet deleted.");
                delete pet;
                return;
            }

            // set health/happiness to full
            pet->SetFullHealth();
            pet->SetPower(POWER_HAPPINESS, pet->GetMaxPower(POWER_HAPPINESS));

            player->SetMinion(pet, true);

            pet->InitPetCreateSpells();
            pet->InitTalentForLevel();
            pet->SavePetToDB(PET_SAVE_AS_CURRENT);
            player->PetSpellInitialize();

            session->SendAreaTriggerMessage("Pet tamed");
        }
};

void AddSC_cg_beast_master()
{
    new cg_beast_master();
}