/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "Object.h"

#define CHECK_BOUNDARY_TIMER 3000
#define MAX_BOUNDARY 120.0f

enum GuardEvents
{
    // Priest
    EVENT_SMITE = 1,
    EVENT_SHADOW_WORD_PAIN,
    EVENT_FEAR,
    EVENT_HEAL_NEARBY,

    // Mage
    EVENT_COUNTERSPELL,
    EVENT_FROSTBOLT,
    EVENT_CHAINS_OF_ICE,
    EVENT_FLAMESTRIKE,

    // Rogue
    EVENT_EVISCERATE,
    EVENT_SHADOW_STEP,
    EVENT_DEADLY_POISON,
    EVENT_ENVENOMED_DAGGER_THROW,

    // Warrior
    EVENT_REND,
    EVENT_CHARGE_KNOCKBACK,
    EVENT_CLEAVE,
    EVENT_MORTAL_STRIKE,
    EVENT_ENRAGE,
};

enum GuardSpells
{
    SPELL_RESISTANT_SKIN            = 72723,

    // Priest
    SPELL_SMITE                     = 61923,
    SPELL_SHADOW_WORD_PAIN          = 59864,
    SPELL_FEAR                      = 39176,
    SPELL_RENEW                     = 60004,

    // Mage
    SPELL_COUNTERSPELL              = 31596,
    SPELL_FROSTBOLT                 = 72166,
    SPELL_CHAINS_OF_ICE             = 72121,
    SPELL_FLAMESTRIKE               = 72169,

    // Rogue
    SPELL_STEALTH                   = 1784,
    SPELL_EVISCERATE                = 71933,
    SPELL_SHADOW_STEP               = 72326,
    SPELL_DEADLY_POISON             = 72329,
    SPELL_ENVENOMED_DAGGER_THROW    = 72333,
    SPELL_KIDNEY_SHOT               = 72335,

    // Warrior
    SPELL_REND                      = 54703,
    SPELL_CHARGE                    = 55530,
    SPELL_CLEAVE                    = 70361,
    SPELL_MORTAL_STRIKE             = 44268,
    SPELL_ENRAGE                    = 42705
};

struct CGPvPMallGuardAI: public ScriptedAI
{
    CGPvPMallGuardAI(Creature* creature) : ScriptedAI(creature), checkBoundaryTimer(CHECK_BOUNDARY_TIMER) { }

    void Reset()
    {
        DoCast(me, SPELL_RESISTANT_SKIN);
        me->SetFullHealth();
        events.Reset();
    }

    bool IsWithinBoundary(uint32 diff)
    {
        checkBoundaryTimer.Update(diff);
        if (checkBoundaryTimer.Passed())
        {
            checkBoundaryTimer.Reset(CHECK_BOUNDARY_TIMER);
            if (me->GetDistance(me->GetHomePosition()) > MAX_BOUNDARY)
                return false;
        }

        return true;
    }

    Player* DoInterruptPlayerCast(uint32 spellId)
    {
        // Find nearby players...
        std::list<Player*> playerList;
        Trinity::AnyPlayerInObjectRangeCheck check(me, 30.0f, true);
        Trinity::PlayerListSearcher<Trinity::AnyPlayerInObjectRangeCheck> searcher(me, playerList, check);
        me->VisitNearbyWorldObject(30.0f, searcher);

        // Find someone to counterspell.
        for (auto const& player : playerList)
        {
            if (!player->IsFriendlyTo(me) &&
                player->IsWithinLOSInMap(me) &&
                player->IsNonMeleeSpellCast(false) &&
                urand(0, 1))
            {
                me->InterruptNonMeleeSpells(false);
                DoCast(player, spellId);
                return player;
            }
        }

        return nullptr;
    }

    bool DoChecksOnUpdateAI(uint32 diff)
    {
        if (!UpdateVictim())
            return false;

        if (!IsWithinBoundary(diff))
        {
            DoResetThreat();
            EnterEvadeMode();
            return false;
        }

        return true;
    }

    protected:

        EventMap events;
        TimeTracker checkBoundaryTimer;
};

class cg_pvp_mall_guard_priest : public CreatureScript
{
    public:

        cg_pvp_mall_guard_priest() : CreatureScript("cg_pvp_mall_guard_priest") { }

        CreatureAI* GetAI(Creature* creature) const
        {
            return new cg_pvp_mall_guard_priestAI(creature);
        }

        struct cg_pvp_mall_guard_priestAI: public CGPvPMallGuardAI
        {
            cg_pvp_mall_guard_priestAI(Creature* creature) : CGPvPMallGuardAI(creature) { }

            void EnterCombat(Unit* /*who*/)
            {
                events.ScheduleEvent(EVENT_SMITE, urand(500, 1000));
                events.ScheduleEvent(EVENT_SHADOW_WORD_PAIN, urand(500, 1000));
                events.ScheduleEvent(EVENT_FEAR, urand(500, 1000));
                events.ScheduleEvent(EVENT_HEAL_NEARBY, urand(500, 1000));
            }

            void UpdateAI(uint32 diff)
            {
                if (!DoChecksOnUpdateAI(diff))
                    return;

                events.Update(diff);

                while (uint32 eventId = events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_SMITE:
                            DoCastVictim(SPELL_SMITE);
                            events.ScheduleEvent(EVENT_SMITE, urand(1500, 2000));
                            return;
                        case EVENT_SHADOW_WORD_PAIN:
                            DoCastVictim(SPELL_SHADOW_WORD_PAIN);
                            events.ScheduleEvent(EVENT_SHADOW_WORD_PAIN, urand(8000, 10000));
                            return;
                        case EVENT_FEAR:
                            events.ScheduleEvent(EVENT_FEAR, DoInterruptPlayerCast(SPELL_FEAR) ? urand(25000, 30000) : urand(500, 1000));
                            return;
                        case EVENT_HEAL_NEARBY:
                            if (Unit* target = DoSelectLowestHpFriendly(40, 5000))
                            {
                                if (!target->HasAura(SPELL_RENEW))
                                    DoCast(target, SPELL_RENEW);

                                events.ScheduleEvent(EVENT_HEAL_NEARBY, urand(1500, 2000));
                            }
                            else
                                events.ScheduleEvent(EVENT_HEAL_NEARBY, urand(500, 1000));
                            return;
                    }
                }

                DoMeleeAttackIfReady();
            }
        };
};

class cg_pvp_mall_guard_mage : public CreatureScript
{
    public:

        cg_pvp_mall_guard_mage() : CreatureScript("cg_pvp_mall_guard_mage") { }

        CreatureAI* GetAI(Creature* creature) const
        {
            return new cg_pvp_mall_guard_mageAI(creature);
        }

        struct cg_pvp_mall_guard_mageAI: public CGPvPMallGuardAI
        {
            cg_pvp_mall_guard_mageAI(Creature* creature) : CGPvPMallGuardAI(creature) { }

            void EnterCombat(Unit* /*who*/)
            {
                events.ScheduleEvent(EVENT_COUNTERSPELL, urand(500, 1000));
                events.ScheduleEvent(EVENT_FROSTBOLT, urand(500, 1000));
                events.ScheduleEvent(EVENT_CHAINS_OF_ICE, urand(4000, 8000));
                events.ScheduleEvent(EVENT_FLAMESTRIKE, urand(10000, 12000));
            }

            void UpdateAI(uint32 diff)
            {
                if (!DoChecksOnUpdateAI(diff))
                    return;

                events.Update(diff);

                while (uint32 eventId = events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_COUNTERSPELL:
                            events.ScheduleEvent(EVENT_COUNTERSPELL, DoInterruptPlayerCast(SPELL_COUNTERSPELL) ? urand(24000, 30000) : urand(500, 1000));
                            return;
                        case EVENT_FROSTBOLT:
                            DoCastVictim(SPELL_FROSTBOLT);
                            events.ScheduleEvent(EVENT_FROSTBOLT, urand(1500, 2000));
                            return;
                        case EVENT_CHAINS_OF_ICE:
                            DoCastVictim(SPELL_CHAINS_OF_ICE);
                            events.ScheduleEvent(EVENT_CHAINS_OF_ICE, urand(4000, 8000));
                            return;
                        case EVENT_FLAMESTRIKE:
                            DoCast(SPELL_FLAMESTRIKE);
                            events.ScheduleEvent(EVENT_FLAMESTRIKE, urand(10000, 12000));
                            return;
                    }
                }

                DoMeleeAttackIfReady();
            }
        };
};

class cg_pvp_mall_guard_rogue : public CreatureScript
{
    public:

        cg_pvp_mall_guard_rogue() : CreatureScript("cg_pvp_mall_guard_rogue") { }

        CreatureAI* GetAI(Creature* creature) const
        {
            return new cg_pvp_mall_guard_rogueAI(creature);
        }

        struct cg_pvp_mall_guard_rogueAI: public CGPvPMallGuardAI
        {
            cg_pvp_mall_guard_rogueAI(Creature* creature) : CGPvPMallGuardAI(creature) { }

            void Reset()
            {
                CGPvPMallGuardAI::Reset();
                DoCast(me, SPELL_STEALTH);
            }

            void EnterCombat(Unit* /*who*/)
            {
                events.ScheduleEvent(EVENT_EVISCERATE, urand(500, 1000));
                events.ScheduleEvent(EVENT_DEADLY_POISON, urand(500, 1000));
                events.ScheduleEvent(EVENT_SHADOW_STEP, urand(500, 1000));
                events.ScheduleEvent(EVENT_ENVENOMED_DAGGER_THROW, urand(4000, 8000));
            }

            void UpdateAI(uint32 diff)
            {
                if (!DoChecksOnUpdateAI(diff))
                    return;

                events.Update(diff);

                while (uint32 eventId = events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_EVISCERATE:
                            DoCastVictim(SPELL_EVISCERATE);
                            events.ScheduleEvent(EVENT_EVISCERATE, urand(4000, 8000));
                            return;
                        case EVENT_DEADLY_POISON:
                            DoCastVictim(SPELL_DEADLY_POISON);
                            events.ScheduleEvent(EVENT_DEADLY_POISON, urand(1500, 2000));
                            return;
                        case EVENT_SHADOW_STEP:
                            if (Player* target = DoInterruptPlayerCast(SPELL_SHADOW_STEP))
                            {
                                DoCast(target, SPELL_KIDNEY_SHOT);
                                events.ScheduleEvent(EVENT_SHADOW_STEP, urand(20000, 22000));
                            }
                            else
                                events.ScheduleEvent(EVENT_SHADOW_STEP, urand(500, 1000));
                            return;
                        case EVENT_ENVENOMED_DAGGER_THROW:
                            DoCastVictim(SPELL_ENVENOMED_DAGGER_THROW);
                            events.ScheduleEvent(EVENT_ENVENOMED_DAGGER_THROW, urand(12000, 15000));
                            return;
                    }
                }

                DoMeleeAttackIfReady();
            }
        };
};

class cg_pvp_mall_guard_warrior : public CreatureScript
{
    public:

        cg_pvp_mall_guard_warrior() : CreatureScript("cg_pvp_mall_guard_warrior") { }

        CreatureAI* GetAI(Creature* creature) const
        {
            return new cg_pvp_mall_guard_warriorAI(creature);
        }

        struct cg_pvp_mall_guard_warriorAI: public CGPvPMallGuardAI
        {
            cg_pvp_mall_guard_warriorAI(Creature* creature) : CGPvPMallGuardAI(creature) { }

            void EnterCombat(Unit* /*who*/)
            {
                events.ScheduleEvent(EVENT_REND, urand(1500, 2000));
                events.ScheduleEvent(EVENT_CHARGE_KNOCKBACK, urand(500, 1000));
                events.ScheduleEvent(EVENT_CLEAVE, urand(500, 1000));
                events.ScheduleEvent(EVENT_MORTAL_STRIKE, urand(500, 1000));
                events.ScheduleEvent(EVENT_ENRAGE, urand(7000, 14000));
            }

            void UpdateAI(uint32 diff)
            {
                if (!DoChecksOnUpdateAI(diff))
                    return;

                events.Update(diff);

                while (uint32 eventId = events.ExecuteEvent())
                {
                    switch (eventId)
                    {
                        case EVENT_CLEAVE:
                            DoCastVictim(SPELL_CLEAVE);
                            events.ScheduleEvent(EVENT_CLEAVE, urand(1500, 3000));
                            break;
                        case EVENT_REND:
                            DoCastVictim(SPELL_REND);
                            events.ScheduleEvent(EVENT_REND, urand(6000, 8000));
                            break;
                        case EVENT_MORTAL_STRIKE:
                            DoCastVictim(SPELL_MORTAL_STRIKE);
                            events.ScheduleEvent(EVENT_MORTAL_STRIKE, urand(5000, 6000));
                            break;
                        case EVENT_CHARGE_KNOCKBACK:
                            events.ScheduleEvent(EVENT_CHARGE_KNOCKBACK, DoInterruptPlayerCast(SPELL_CHARGE) ? urand(24000, 30000) : urand(500, 1000));
                            break;
                        case EVENT_ENRAGE:
                            DoCast(me, SPELL_ENRAGE);
                            events.ScheduleEvent(EVENT_ENRAGE, urand(7000, 14000));
                            break;
                    }
                }

                DoMeleeAttackIfReady();
            }
        };
};

void AddSC_cg_pvp_mall_guards()
{
    new cg_pvp_mall_guard_priest();
    new cg_pvp_mall_guard_mage();
    new cg_pvp_mall_guard_rogue();
    new cg_pvp_mall_guard_warrior();
}