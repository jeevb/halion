/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include <iomanip>
#include "Config.h"
#include "GossipMgr.h"
#include "Language.h"
#include "NotificationMgr.h"
#include "TeleportMgr.h"
#include "TemplateMgr.h"
#include "TrainingMgr.h"

enum LevelingNpcOption
{
    LEVELING_NPC_MAIN_MENU,
    LEVELING_NPC_EXP_CHANGE,
    LEVELING_NPC_EXP_QUERY,
    LEVELING_NPC_LEVEL_UP,
    LEVELING_NPC_BROWSE_GOODS,
    LEVELING_NPC_STARTER_ITEMS
};

enum LevelingNpcEnums
{
    LEVELING_NPC_VENDOR_ID                      = 35507,
    LEVELING_NPC_GOSSIP_MESSAGE_MAIN            = 1000005,
    LEVELING_NPC_GOSSIP_MESSAGE_STARTER_ITEMS   = 1000026
};

class cg_npc_leveling : public CreatureScript
{
    public:

        cg_npc_leveling() : CreatureScript("cg_npc_leveling")
        {
            // Extract all space-delimited level options
            std::stringstream buf(sConfigMgr->GetStringDefault("LevelingNPC.BoostOptions", "60 70 80"));

            uint32 level;
            while (buf >> level)
            {
                m_BoostOptions.insert(uint8(level));
            }
        }

        bool OnGossipHello(Player* player, Creature* creature) override
        {
            player->CLEAR_GOSSIP_MENU();

            if (player->IsInCombat())
            {
                ChatHandler(player->GetSession()).SendSysMessage(LANG_YOU_IN_COMBAT);
                return true;
            }

            std::ostringstream buf;
            buf << std::setprecision(1) << std::fixed << std::showpoint
                << "Specify a new experience rate. You may enter a number between 1.0 and "
                << sWorld->getRate(RATE_XP_MAX_MULT) << ".\n\n"
                << "Current Rate: |cff0000ee" << player->GetExperience()->GetRate() << "|r\n\n";

            if (player->getLevel() < sWorld->getIntConfig(CONFIG_MAX_PLAYER_LEVEL))
                player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_INTERACT_1,
                                                 buf.str(),
                                                 NULL,
                                                 LEVELING_NPC_EXP_CHANGE,
                                                 "",
                                                 0,
                                                 true);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1,
                                    "View a summary of your leveling statistics.",
                                    NULL,
                                    LEVELING_NPC_EXP_QUERY);

            if (!player->HasSavedState(PLAYER_SAVED_STATE_PICKED_TEMPLATE))
            {
                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR,
                                        "Purchase leveling gear.",
                                        NULL,
                                        LEVELING_NPC_BROWSE_GOODS);

                // If player has attained the maximum level and has not yet obtained a
                // starter item set...
                if (player->getLevel() == sWorld->getIntConfig(CONFIG_MAX_PLAYER_LEVEL))
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR,
                                            "Select a starter item set.",
                                            NULL,
                                            LEVELING_NPC_STARTER_ITEMS);
            }

            for (auto const& level : m_BoostOptions)
                AddLevelBoostGossipOption(player, level);

            player->SEND_GOSSIP_MENU(LEVELING_NPC_GOSSIP_MESSAGE_MAIN, creature->GetGUID());
            return true;
        }

        bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action) override
        {
            player->CLEAR_GOSSIP_MENU();
            player->CLOSE_GOSSIP_MENU();

            ChatHandler handler(player->GetSession());

            switch (action)
            {
                case LEVELING_NPC_EXP_QUERY:
                {
                    Experience const* e = player->GetExperience();

                    std::ostringstream buf;
                    buf << "Here is a detailed breakdown of the experience you have gained so far:\n\n"
                        << std::setprecision(1) << std::fixed << std::showpoint
                        // Overall Experience
                        << "|cff0000eeOverall Statistics|r\n"
                        << "    Current rate: " << e->GetRate() << "\n"
                        << "    Total experience: " << e->GetTotalExperience() << "\n"
                        << "    Average rate: " << e->GetAverageRate() <<"\n\n"
                        // Level Experience
                        << "|cff0000eeLevel Statistics|r\n"
                        << "    Current Level: " << uint32(player->getLevel()) << "\n"
                        << "    Experience gained: " << e->GetLevelExperience() << "\n"
                        << "    Average rate: " << e->GetLevelRate() << "\n\n"
                        // Kills Experience
                        << "|cff0000eeKill Statistics|r\n"
                        << "    Units killed: " << e->GetUnitsKilled() << "\n"
                        << "    Experience gained: " << e->GetKillsExperience() << "\n"
                        << "    Average rate: " << e->GetKillsRate() << "\n\n"
                        // Quests Experience
                        << "|cff0000eeQuest Statistics|r\n"
                        << "    Quests completed: " << e->GetQuestsCompleted() << "\n"
                        << "    Experience gained: " << e->GetQuestsExperience() << "\n"
                        << "    Average rate: " << e->GetQuestsRate() << "\n\n"
                        // Exploration Experience
                        << "|cff0000eeExploration Statistics|r\n"
                        << "    Areas explored: " << e->GetAreasExplored() << "\n"
                        << "    Experience gained: " << e->GetExploredExperience() << "\n"
                        << "    Average rate: " << e->GetExploredRate() << "\n";

                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Return to the main menu.", NULL, LEVELING_NPC_MAIN_MENU);
                    SEND_CUSTOM_GOSSIP_MENU(player, creature->GetGUID(), buf.str());
                    return true;
                }
                case LEVELING_NPC_LEVEL_UP:
                {
                    if (!sender)
                    {
                        handler.SendSysMessage("An error has occurred. Please try again.");
                        break;
                    }

                    // Silence achievement broadcasts
                    player->SetSilenceAchievement();

                    player->SetTrack(TRACK_NORMAL_BOOST);
                    sTrain->Boost(player, uint8(sender));
                    sTrack->UpdateNearbyQuestGiverStatus(player);

                    // Teleport Death Knights if still in starting zone
                    if (player->getClass() == CLASS_DEATH_KNIGHT && player->GetMapId() == 609)
                        sTeleport->To(player, TELEPORT_LOC_ACHERUS);

                    // Remove silence achiavement flag
                    player->SetSilenceAchievement(false);

                    break;
                }
                case LEVELING_NPC_BROWSE_GOODS:
                {
                    player->GetSession()->SendListInventory(creature->GetGUID());
                    return true;
                }
                case LEVELING_NPC_STARTER_ITEMS:
                {
                    if (!sender)
                    {
                        SendStarterItemSets(player, creature);
                        return true;
                    }
                    else if (PresetTemplate const* preset = sTemplateMgr->GetPresetTemplate(sender))
                    {
                        // Apply starter item template and save to DB
                        sTemplateMgr->ApplyPresetTemplate(player, preset);
                        player->SetSavedState(PLAYER_SAVED_STATE_PICKED_TEMPLATE);
                        player->SaveToDB();

                        // Show success notification
                        std::ostringstream buf;
                        buf << GOSSIP_ICON_TEXT_NOTIFICATION("|cff1eff00New template applied - |r", sTemplateMgr->GetIcon(preset))
                            << preset->description;
                        sNotify->FlashMessage(player, buf.str());
                    }
                    else
                        handler.SendSysMessage("An error has occurred. Please try again.");

                    break;
                }
                default:
                    break;
            }

            return OnGossipHello(player, creature);
        }

        bool OnGossipSelectCode(Player * player, Creature* creature, uint32 /*sender*/, uint32 action, const char *ccode) override
        {
            player->CLEAR_GOSSIP_MENU();
            player->CLOSE_GOSSIP_MENU();
            ChatHandler handler(player->GetSession());

            switch (action)
            {
                case LEVELING_NPC_EXP_CHANGE:
                {
                    if (!*ccode)
                    {
                        handler.PSendSysMessage("You have entered an invalid rate. Please enter a number between 1.0 and %.1f!",
                                                sWorld->getRate(RATE_XP_MAX_MULT));
                        return OnGossipHello(player, creature);
                    }

                    float rate = atof((char*)ccode);
                    if (!rate ||
                        rate < 1.0f ||
                        rate > sWorld->getRate(RATE_XP_MAX_MULT))
                    {
                        handler.PSendSysMessage("You have entered an invalid rate. Please enter a number between 1.0 and %.1f!",
                                                sWorld->getRate(RATE_XP_MAX_MULT));
                        return OnGossipHello(player, creature);
                    }

                    Experience* e = player->GetExperience();
                    e->SetRate(rate);
                    handler.PSendSysMessage("Your experience rate has been set to: |cff1eff00%.1f|r", e->GetRate());
                    break;
                }
                default:
                    break;
            }

            return OnGossipHello(player, creature);
        }

        void AddLevelBoostGossipOption(Player* player, uint8 level)
        {
            if (player->getLevel() >= level)
                return;

            // No boosts at or under level 60 allowed for Death Knights
            if (player->getClass() == CLASS_DEATH_KNIGHT && level <= 60)
                return;

            std::ostringstream option;
            option << "Boost to Level " << uint32(level);

            std::ostringstream confirm;
            confirm << "Are you sure you want to boost to\n|cff5dfc0aLevel " << uint32(level) << "|r?";

            player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_CHAT,
                                             option.str(),
                                             uint32(level),
                                             LEVELING_NPC_LEVEL_UP,
                                             confirm.str(),
                                             0,
                                             false);
        }

        VendorItemData const* GetVendorItems(Creature const* /*creature*/, Player* /*player*/) const override
        {
            return sObjectMgr->GetNpcVendorItemList(LEVELING_NPC_VENDOR_ID);
        }

        void SendStarterItemSets(Player* player, Creature* creature)
        {
            for (auto const& itr : sTemplateMgr->GetPresetTemplates())
                if (sTemplateMgr->IsValidPresetTemplateForPlayer(player, &itr.second))
                {
                    std::ostringstream buf;
                    buf << "Are you sure you wish to apply the following template:\n\n"
                        << "|cffffce00" << itr.second.description << "|r"
                        << "\n\nNote: You may only select one template per character.";
                    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD,
                                                     GOSSIP_ICON_TEXT(itr.second.description, sTemplateMgr->GetIcon(&itr.second)),
                                                     itr.first,
                                                     LEVELING_NPC_STARTER_ITEMS,
                                                     buf.str(),
                                                     0,
                                                     false);
                }

            player->SEND_GOSSIP_MENU(LEVELING_NPC_GOSSIP_MESSAGE_STARTER_ITEMS, creature->GetGUID());
        }

    private:

        std::set<uint8> m_BoostOptions;
};

void AddSC_cg_npcs_special()
{
    new cg_npc_leveling();
}