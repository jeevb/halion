/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "Config.h"
#include "CurrencyMgr.h"
#include "Language.h"

enum InnkeeperGossipOptions
{
    INNKEEPER_MAIN_MENU,
    INNKEEPER_SET_HOME,
    INNKEEPER_BROWSE_GOODS,
    INNKEEPER_GUILD_PETITION,
    INNKEEPER_GUILD,
    INNKEEPER_GUILD_TABARD,
    INNKEEPER_GUILD_CREATE,
    INNKEEPER_FINANCIAL,
    INNKEEPER_VIEW_REWARD_CURRENCIES,
    INNKEEPER_BANK,
    INNKEEPER_CHAR,
    INNKEEPER_RECUSTOM,
    INNKEEPER_RACE_CHANGE,
    INNKEEPER_FACTION_CHANGE
};

enum InnkeeperGossipText
{
    INNKEEPER_GOSSIP_MESSAGE_MAIN               = 1000022,
    INNKEEPER_GOSSIP_MESSAGE_FINANCIAL_SERVICES = 1000023,
    INNKEEPER_GOSSIP_MESSAGE_GUILD_SERVICES     = 1000024,
    INNKEEPER_GOSSIP_MESSAGE_CHAR_SERVICES      = 1000025,
};

class cg_innkeeper : public CreatureScript
{
    private:

        uint32 recustomCost;
        uint32 raceChangeCost;
        uint32 factionChangeCost;

    public:

        cg_innkeeper() : CreatureScript("cg_innkeeper")
        {
            recustomCost        = sConfigMgr->GetIntDefault("MallInnkeeper.RecustomizationCost", 0);
            raceChangeCost      = sConfigMgr->GetIntDefault("MallInnkeeper.RaceChangeCost", 0);
            factionChangeCost   = sConfigMgr->GetIntDefault("MallInnkeeper.FactionChangeCost", 0);
        }

        bool OnGossipHello(Player* player, Creature* creature)
        {
            player->CLEAR_GOSSIP_MENU();

            if (player->IsInCombat())
            {
                player->GetSession()->SendNotification(LANG_YOU_IN_COMBAT);
                return true;
            }

            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, "Make this my home.", 0, INNKEEPER_SET_HOME);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_VENDOR, "I'd like to browse your goods.", 0, INNKEEPER_BROWSE_GOODS);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "I'd like to view your financial services.", 0, INNKEEPER_FINANCIAL);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "I'd like to view your guild services.", 0, INNKEEPER_GUILD);
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "I'd like to view your character services.", 0, INNKEEPER_CHAR);
            player->SEND_GOSSIP_MENU(INNKEEPER_GOSSIP_MESSAGE_MAIN, creature->GetGUID());
            return true;
        }

        bool OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
        {
            player->CLEAR_GOSSIP_MENU();
            player->CLOSE_GOSSIP_MENU();

            WorldSession* session = player->GetSession();
            ChatHandler handler(session);

            switch (action)
            {
                case INNKEEPER_MAIN_MENU:
                    return OnGossipHello(player, creature);
                case INNKEEPER_SET_HOME:
                    player->SetBindPoint(creature->GetGUID());
                    break;
                case INNKEEPER_BROWSE_GOODS:
                    session->SendListInventory(creature->GetGUID());
                    break;
                case INNKEEPER_FINANCIAL:
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "I'd like to access my bank account.", 0, INNKEEPER_BANK);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "I'd like to view my reward currencies.", 0, INNKEEPER_VIEW_REWARD_CURRENCIES);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Return to the main menu.", 0, 0);
                    player->SEND_GOSSIP_MENU(INNKEEPER_GOSSIP_MESSAGE_FINANCIAL_SERVICES, creature->GetGUID());
                    break;
                case INNKEEPER_BANK:
                    session->SendShowBank(player->GetGUID());
                    break;
                case INNKEEPER_VIEW_REWARD_CURRENCIES:
                    session->HandleDisplayRewardCurrencies();
                    return OnGossipSelect(player, creature, 0, INNKEEPER_FINANCIAL);
                case INNKEEPER_GUILD:
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, "I want to create a guild crest.", 0, INNKEEPER_GUILD_TABARD);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "How do I form a guild?", 0, INNKEEPER_GUILD_PETITION);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Return to the main menu.", 0, 0);
                    player->SEND_GOSSIP_MENU(INNKEEPER_GOSSIP_MESSAGE_GUILD_SERVICES, creature->GetGUID());
                    break;
                case INNKEEPER_GUILD_TABARD:
                    session->SendTabardVendorActivate(creature->GetGUID());
                    break;
                case INNKEEPER_GUILD_PETITION:
                    session->SendPetitionShowList(creature->GetGUID());
                    break;
                case INNKEEPER_CHAR:
                {
                    // Display costs
                    std::stringstream buf;
                    if (recustomCost)
                    {
                        buf << "You will need the following to purchase a |cffffffff[Character Recustomization]|r:\n"
                            << sCurrencyMgr->GetCustomCostDescription(player, recustomCost) << "\n\n";
                    }
                    if (raceChangeCost)
                    {
                        buf << "You will need the following to purchase a |cffffffff[Race Change]|r:\n"
                            << sCurrencyMgr->GetCustomCostDescription(player, raceChangeCost) << "\n\n";
                    }
                    if (factionChangeCost)
                    {
                        buf << "You will need the following to purchase a |cffffffff[Faction Change]|r:\n"
                            << sCurrencyMgr->GetCustomCostDescription(player, factionChangeCost) << "\n\n";
                    }
                    handler.SendSysMessage(buf.str().c_str());

                    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK, "I'd like to recustomize my character.", 0, INNKEEPER_RECUSTOM,
                                                     "Are you sure you want to purchase a\n|cffffce00[Character Recustomization]|r?", 0, false);
                    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK, "I'd like to change my character's race.", 0, INNKEEPER_RACE_CHANGE,
                                                     "Are you sure you want to purchase a\n|cffffce00[Race Change]|r?", 0, false);
                    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TALK, "I'd like to change my character's faction.", 0, INNKEEPER_FACTION_CHANGE,
                                                     "Are you sure you want to purchase a\n|cffffce00[Faction Change]|r?", 0, false);
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Return to the main menu.", 0, 0);
                    player->SEND_GOSSIP_MENU(INNKEEPER_GOSSIP_MESSAGE_CHAR_SERVICES, creature->GetGUID());
                    break;
                }
                case INNKEEPER_RECUSTOM:
                    if (sCurrencyMgr->PurchaseItemForCost(player, creature->GetEntry(), recustomCost))
                    {
                        player->SetAtLoginFlag(AT_LOGIN_CUSTOMIZE);
                        handler.SendSysMessage("Log off and return to the character selection screen to complete the recustomization.");
                    }
                    break;
                case INNKEEPER_RACE_CHANGE:
                    if (sCurrencyMgr->PurchaseItemForCost(player, creature->GetEntry(), raceChangeCost))
                    {
                        player->SetAtLoginFlag(AT_LOGIN_CHANGE_RACE);
                        handler.SendSysMessage("Log off and return to the character selection screen to complete the race change.");
                    }
                    break;
                case INNKEEPER_FACTION_CHANGE:
                    if (sCurrencyMgr->PurchaseItemForCost(player, creature->GetEntry(), factionChangeCost))
                    {
                        player->SetAtLoginFlag(AT_LOGIN_CHANGE_FACTION);
                        handler.SendSysMessage("Log off and return to the character selection screen to complete the faction change.");
                    }
                    break;
                default:
                    break;
            }

            return true;
        }
};

void AddSC_cg_innkeeper()
{
    new cg_innkeeper();
}