/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "World.h"

class cg_duel_reset : public PlayerScript
{
    private:

        struct SpellCooldownInfo
        {
            time_t end;
            uint32 spellId;
        };

        struct SavedPlayerState
        {
            uint32 health;
            uint32 mana;
            std::vector<SpellCooldownInfo> cooldowns;
        };

        typedef std::map<Player*, SavedPlayerState> PlayerStateMap;
        PlayerStateMap m_SavedState;

    public:

        cg_duel_reset() : PlayerScript("cg_duel_reset") { }

        void OnLogout(Player* player) override
        {
            // Clear player data on logout
            m_SavedState.erase(player);
        }

        void OnDuelAccept(Player* target, Player* challenger) override
        {
            _HandleDuelAccept(target);
            _HandleDuelAccept(challenger);
        }

        void OnDuelEnd(Player* winner, Player* loser, DuelCompleteType /*type*/) override
        {
            _RestorePlayerState(winner);
            _RestorePlayerState(loser);
        }

    private:

        void _HandleDuelAccept(Player* player)
        {
            _RemoveExpiringPlayerBuffs(player, 30 * IN_MILLISECONDS);
            _SavePlayerState(player);
            player->ResetAllPowers();
        }

        bool _NotPermAppliedAura(Player* player, uint32 spellId) const
        {
            // We check to see if the spell is an aura (Like Cold Blood, Inner Focus)
            if (Aura *aura = player->GetAura(spellId, player->GetGUID()))
                return !aura->IsPermanent(); // And if it's not permanent (I..e. expires on cast) we track its cooldown.

            return true;
        }

        void _SavePlayerState(Player* player)
        {
            SavedPlayerState& state = m_SavedState[player];

            //  Save player health & mana.
            state.health = player->GetHealth();
            if (player->getPowerType() == POWER_MANA)
                state.mana = player->GetPower(POWER_MANA);
            else
                state.mana = 0;

            // Save cooldowns.
            if (state.cooldowns.size() > 0)
                state.cooldowns.clear();

            time_t curTime = sWorld->GetGameTime();

            for (auto const& itr : player->GetSpellCooldowns())
            {
                SpellInfo const* entry = sSpellMgr->GetSpellInfo(itr.first);
                if (entry &&
                    itr.second.itemid == 0 &&                                       // We don't store or deal with item cooldowns.
                    itr.second.end < curTime + MONTH &&                             // These cooldowns are way too long, TC considers them infinite.
                    itr.second.end > curTime &&                                     // What if for some odd reason it's already expired?
                    entry->RecoveryTime <= 10 * MINUTE * IN_MILLISECONDS &&         // We only reset cooldowns under 10 minutes.
                    entry->CategoryRecoveryTime <= 10 * MINUTE * IN_MILLISECONDS &&
                    _NotPermAppliedAura(player, itr.first))                         // We don't care about auras which are applied and are permanent.
                {
                    SpellCooldownInfo si;
                    si.end = itr.second.end;
                    si.spellId = itr.first;
                    state.cooldowns.push_back(si);

                    player->RemoveSpellCooldown(itr.first, true);
                }
            }
        }

        void _RemoveExpiringPlayerBuffs(Player* player, int32 within)
        {
            // remove auras with duration lower than 30s
            Unit::AuraApplicationMap & auraMap = player->GetAppliedAuras();
            for (auto itr = auraMap.begin(); itr != auraMap.end();)
            {
                AuraApplication* aurApp = itr->second;
                Aura* aura = aurApp->GetBase();
                if (aura->GetId() == 25771 /* Remove FORBERANCE */
                    || (!aura->IsPermanent()
                    && aura->GetDuration() <= within
                    && aurApp->IsPositive()
                    && (!(aura->GetSpellInfo()->Attributes & SPELL_ATTR0_UNAFFECTED_BY_INVULNERABILITY))
                    && (!aura->HasEffectType(SPELL_AURA_MOD_INVISIBILITY))))
                    player->RemoveAura(itr);
                else
                    ++itr;
            }
        }

        void _RestorePlayerState(Player* player)
        {
            auto itr = m_SavedState.find(player);
            // no save state.
            if (itr == m_SavedState.end())
                return;

            SavedPlayerState& state = itr->second;

            // restore health and mana
            player->SetHealth(state.health);
            if (state.mana)
                player->SetPower(POWER_MANA, state.mana);

            // restore cooldowns.
            time_t curTime = sWorld->GetGameTime();

            // Let's populate a set of spellIds we're going to ovewrite and not reset.
            std::map<uint32, uint32> willReset;
            for (auto const& itr : state.cooldowns)
            {
                if (itr.end > curTime)
                    willReset[itr.spellId] = itr.end;
            }

            std::vector<std::pair<SpellInfo const*, uint32>> startCooldowns; // Perma auras (Cold Blood, Inner Focus, ..etc) have to be handled specially.

            SpellCooldowns const cooldowns = player->GetSpellCooldowns();
            SpellCooldowns::const_iterator sitr, next;

            WorldPacket data(SMSG_SPELL_COOLDOWN, 8 + 1 + willReset.size() * 8); // The packet should be at most the size of our cooldowns & reset.
            data << uint64(player->GetGUID());
            data << uint8(0x00);

            for (sitr = cooldowns.begin(); sitr != cooldowns.end(); sitr = next)
            {
                next = sitr;
                ++next;
                SpellInfo const* entry = sSpellMgr->GetSpellInfo(sitr->first);
                if (!entry || !_NotPermAppliedAura(player, sitr->first)) // We don't care about auras which are applied and are permanent.
                    continue;

                auto _wr = willReset.find(sitr->first);
                if ( _wr == willReset.end() &&                                      // If we're not going to re-add the cooldown, then we can reset it if ...
                    entry->RecoveryTime <= 10 * MINUTE * IN_MILLISECONDS &&         // It is under 10 minutes.
                    entry->CategoryRecoveryTime <= 10 * MINUTE * IN_MILLISECONDS)
                {
                    player->RemoveSpellCooldown(sitr->first, true);
                }
            }

            // Re-add cooldowns.
            for (auto const& _wr : willReset)
            {
                if (_NotPermAppliedAura(player, _wr.first))
                {
                    if (SpellInfo const* entry = sSpellMgr->GetSpellInfo(_wr.first))
                        if (entry->GetDuration() == -1)
                            startCooldowns.push_back(std::pair<SpellInfo const*, uint32>(entry, _wr.second));

                    data << uint32(_wr.first);
                    data << uint32((_wr.second - curTime) * IN_MILLISECONDS);
                    player->AddSpellCooldown(_wr.first, 0, _wr.second);
                }
            }

            player->GetSession()->SendPacket(&data);

            // We avoid sending or even consturcting the packet if we don't have to.
            if (startCooldowns.size())
            {
                // For permanent cooldowns, what we have to do is send a reset to the client, then resend the remaining time.
                WorldPacket sc_data(SMSG_SPELL_COOLDOWN, 8 + 1 + startCooldowns.size() * 8); // The packet should be at most the size of our cooldowns & reset.
                sc_data << uint64(player->GetGUID());
                sc_data << uint8(0x00);

                for (auto const& _sc : startCooldowns)
                {
                    player->SendCooldownEvent(_sc.first, 0, NULL, false);
                    data << uint32(_sc.first->Id);
                    data << uint32((_sc.second - curTime) * IN_MILLISECONDS);
                }

                player->GetSession()->SendPacket(&sc_data);
            }

            m_SavedState.erase(player);
        }
};

void AddSC_cg_duel_reset()
{
    new cg_duel_reset();
}