/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "GossipMgr.h"
#include "Language.h"
#include "NotificationMgr.h"
#include "TrainingMgr.h"

enum ProfVendorAction
{
    PROF_VENDOR_MAIN_MENU,
    PROF_VENDOR_LEARN_PROFESSION,
    PROF_VENDOR_BROWSE_GOODS
};

enum ProfVendorGossipMessage
{
    PROF_VENDOR_GOSSIP_MESSAGE_MAIN     = 1000014,
    PROF_VENDOR_GOSSIP_CANNOT_LEARN     = 1000015,
    PROF_VENDOR_GOSSIP_LEARN_SUCCESS    = 1000016
};

class cg_prof_vendor : public CreatureScript
{
    public:

        cg_prof_vendor() : CreatureScript("cg_prof_vendor") { }

        bool OnGossipHello(Player* player, Creature* creature)
        {
            player->CLEAR_GOSSIP_MENU();

            ChatHandler handler(player->GetSession());

            // Sanity checks - player in combat is not allowed to interact with the vendor
            if (player->IsInCombat())
            {
                handler.SendSysMessage(LANG_YOU_IN_COMBAT);
                return true;
            }

            // Automatically learn certain professions on interacting with vendor
            HandleLearnOnInteract(player);

            // Only display these options if the player can learn at least one more profession
            uint32 freeProfs = player->GetFreePrimaryProfessionPoints();
            if (freeProfs)
            {
                for (uint8 prof = PROF_ALCHEMY; prof <= PROF_PRIMARY_MAX; ++prof)
                {
                    ProfessionInfo const& info = ProfessionInfoStore[prof];

                    if (!player->HasSpell(info.spellId))
                    {
                        std::ostringstream buf;
                        buf << "You have chosen to learn the following as your ";
                        switch (freeProfs)
                        {
                            case 1: buf << "second"; break;
                            case 2: buf << "first"; break;
                            default: break;
                        }
                        buf << " profession.\n\n|cffffcc00"
                            << GOSSIP_ICON_TEXT_EXTENDED(info.name, info.icon, 30, -4, -12, "  ")
                            << "|r\n\nDo you wish to proceed?";
                        player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD, GOSSIP_ICON_TEXT(info.name, info.icon),
                                                         prof, PROF_VENDOR_LEARN_PROFESSION, buf.str(), 0, false);
                    }
                }
            }

            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GOSSIP_ICON_TEXT_BROWSE_GOODS, 0, PROF_VENDOR_BROWSE_GOODS);
            player->SEND_GOSSIP_MENU(freeProfs ? PROF_VENDOR_GOSSIP_MESSAGE_MAIN : PROF_VENDOR_GOSSIP_CANNOT_LEARN, creature->GetGUID());
            return true;
        }

        bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
        {
            player->CLEAR_GOSSIP_MENU();
            player->CLOSE_GOSSIP_MENU();

            switch (action)
            {
                case PROF_VENDOR_LEARN_PROFESSION:
                {
                    LearnProfession(player, uint8(sender));

                    // If player can still learn more professions, offer the option to do so
                    if (player->GetFreePrimaryProfessionPoints())
                    {
                        std::ostringstream buf;
                        buf << "You may still learn " << player->GetFreePrimaryProfessionPoints() << " more profession(s)."
                            << " Continue browsing?";
                        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, buf.str(), 0, PROF_VENDOR_MAIN_MENU);
                        player->SEND_GOSSIP_MENU(PROF_VENDOR_GOSSIP_LEARN_SUCCESS, creature->GetGUID());
                    }

                    break;
                }
                case PROF_VENDOR_BROWSE_GOODS:
                {
                    player->GetSession()->SendListInventory(creature->GetGUID());
                    break;
                }
                default:
                    return OnGossipHello(player, creature);
            }

            return true;
        }

        void LearnProfession(Player* player, uint8 prof)
        {
            ChatHandler handler(player->GetSession());

            ProfessionInfo const& info = ProfessionInfoStore[prof];

            if (player->HasSpell(info.spellId))
            {
                handler.PSendSysMessage("You already know %s!", info.name.c_str());
                return;
            }

            if (!player->GetFreePrimaryProfessionPoints())
            {
                handler.PSendSysMessage("You may not learn any more professions!");
                return;
            }

            if (sTrain->LearnProfession(player, info.spellId, info.skillId, info.trainerId))
            {
                std::ostringstream buf;
                buf << GOSSIP_ICON_TEXT_NOTIFICATION("|cff1eff00New profession learned - |r", info.icon) << info.name;
                sNotify->FlashMessage(player, buf.str());
            }
        }

        void HandleLearnOnInteract(Player* player)
        {
            sTrain->LearnProfession(player, PROF_FIRST_AID);
        }
};

void AddSC_cg_prof_vendor()
{
    new cg_prof_vendor;
}