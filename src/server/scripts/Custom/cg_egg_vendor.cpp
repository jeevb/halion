/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "GossipMgr.h"
#include "Language.h"
#include "TrackMgr.h"
#include "TransmogrificationMgr.h"

enum EnchantSlot
{
    ENCHANT_SLOT_HEAD,
    ENCHANT_SLOT_SHOULDERS,
    ENCHANT_SLOT_BACK,
    ENCHANT_SLOT_CHEST,
    ENCHANT_SLOT_WRIST,
    ENCHANT_SLOT_HANDS,
    ENCHANT_SLOT_WAIST,
    ENCHANT_SLOT_LEGS,
    ENCHANT_SLOT_FEET,
    ENCHANT_SLOT_FINGER1,
    ENCHANT_SLOT_FINGER2,
    ENCHANT_SLOT_MAINHAND,
    ENCHANT_SLOT_OFFHAND,
    ENCHANT_SLOT_RANGED,

    MAX_ENCHANT_SLOT
};

struct EnchantSlotInfo
{
    EnchantSlotInfo(EquipmentSlots _eSlot, std::string const& _defaultName, std::string const& _defaultIcon) :
        eSlot(_eSlot), defaultName(_defaultName), defaultIcon(_defaultIcon) { }

    EquipmentSlots  eSlot;
    std::string     defaultName;
    std::string     defaultIcon;
};

const EnchantSlotInfo EnchantSlotInfoStore[MAX_ENCHANT_SLOT] =
{
    EnchantSlotInfo(EQUIPMENT_SLOT_HEAD,        "Head",         "PaperDoll/UI-PaperDoll-Slot-Head"            ),
    EnchantSlotInfo(EQUIPMENT_SLOT_SHOULDERS,   "Shoulders",    "PaperDoll/UI-PaperDoll-Slot-Shoulder"        ),
    EnchantSlotInfo(EQUIPMENT_SLOT_BACK,        "Back",         "PaperDoll/UI-PaperDoll-Slot-Chest"           ),
    EnchantSlotInfo(EQUIPMENT_SLOT_CHEST,       "Chest",        "PaperDoll/UI-PaperDoll-Slot-Chest"           ),
    EnchantSlotInfo(EQUIPMENT_SLOT_WRISTS,      "Wrists",       "PaperDoll/UI-PaperDoll-Slot-Wrists"          ),
    EnchantSlotInfo(EQUIPMENT_SLOT_HANDS,       "Hands",        "PaperDoll/UI-PaperDoll-Slot-Hands"           ),
    EnchantSlotInfo(EQUIPMENT_SLOT_WAIST,       "Waist",        "PaperDoll/UI-PaperDoll-Slot-Waist"           ),
    EnchantSlotInfo(EQUIPMENT_SLOT_LEGS,        "Legs",         "PaperDoll/UI-PaperDoll-Slot-Legs"            ),
    EnchantSlotInfo(EQUIPMENT_SLOT_FEET,        "Feet",         "PaperDoll/UI-PaperDoll-Slot-Feet"            ),
    EnchantSlotInfo(EQUIPMENT_SLOT_FINGER1,     "Finger",       "PaperDoll/UI-PaperDoll-Slot-Finger"          ),
    EnchantSlotInfo(EQUIPMENT_SLOT_FINGER2,     "Finger",       "PaperDoll/UI-PaperDoll-Slot-Finger"          ),
    EnchantSlotInfo(EQUIPMENT_SLOT_MAINHAND,    "Main-hand",    "PaperDoll/UI-PaperDoll-Slot-MainHand"        ),
    EnchantSlotInfo(EQUIPMENT_SLOT_OFFHAND,     "Off-hand",     "PaperDoll/UI-PaperDoll-Slot-SecondaryHand"   ),
    EnchantSlotInfo(EQUIPMENT_SLOT_RANGED,      "Ranged",       "PaperDoll/UI-PaperDoll-Slot-Ranged"          )
};

enum EggVendorAction
{
    EGG_VENDOR_MAIN_MENU,
    EGG_VENDOR_ENCHANT_ITEM,
    EGG_VENDOR_PURCHASE_GLYPH,
    EGG_VENDOR_PURCHASE_GEM,
    EGG_VENDOR_RESET_TALENTS,
    EGG_VENDOR_PRIMARY_MAX,
    EGG_VENDOR_ENCHANT_ITEM_SECONDARY,
    EGG_VENDOR_ENCHANT_ITEM_STANDARD,
    EGG_VENDOR_ENCHANT_ITEM_PROF,
    EGG_VENDOR_PURCHASE_GEM_SECONDARY
};

enum EggVendorGossipMessage
{
    EGG_VENDOR_GOSSIP_MESSAGE_MAIN          = 1000010,
    EGG_VENDOR_GOSSIP_MESSAGE_ENCHANT_ITEMS = 1000011,
    EGG_VENDOR_GOSSIP_MESSAGE_ENCHANT_PROF  = 1000012,
    EGG_VENDOR_GOSSIP_MESSAGE_GEMS          = 1000013
};

struct OptionDescription
{
    OptionDescription() { }
    OptionDescription(std::string const& _name, std::string const& _icon) :
        name(_name), icon(_icon) { }

    std::string name;
    std::string icon;
};

const OptionDescription MainMenuOptions[EGG_VENDOR_PRIMARY_MAX] =
{
    OptionDescription(),
    OptionDescription("Enchant equipped items", "Icons/inv_rod_titanium"),
    OptionDescription("Browse some glyphs", "Icons/inv_glyph_majordruid"),
    OptionDescription("Purchase some gems", "Icons/inv_misc_gem_01"),
    OptionDescription("Reset my talents", "PaperDollInfoFrame/UI-GearManager-LeaveItem-Opaque")
};

const OptionDescription GemOptions[MAX_ITEM_SUBCLASS_GEM] =
{
    OptionDescription("Red Gems", "Icons/inv_jewelcrafting_gem_37"),
    OptionDescription("Blue Gems", "Icons/inv_jewelcrafting_gem_42"),
    OptionDescription("Yellow Gems", "Icons/inv_jewelcrafting_gem_38"),
    OptionDescription("Purple Gems", "Icons/inv_jewelcrafting_gem_40"),
    OptionDescription("Green Gems", "Icons/inv_jewelcrafting_gem_41"),
    OptionDescription("Orange Gems", "Icons/inv_jewelcrafting_gem_39"),
    OptionDescription("Meta Gems", "Icons/inv_jewelcrafting_icediamond_02"),
    OptionDescription("Simple Gems", "Icons/inv_misc_gem_pearl_09"),
    OptionDescription("Prismatic Gems", "Icons/inv_misc_gem_pearl_12")
};

// Hardcoded profession enchant spells
uint32 profEnchantSpellIds[] = {
    // Engineering
    54998, // Hand-Mounted Pyro Rocket
    54999, // Hyperspeed Accelerators
    55016, // Nitro Boosts
    67839, // Mind Amplification Dish
    63765, // Springy Arachnoweave
    55002, // Flexweave Underlay
    63770, // Reticulated Armor Webbing
    54793, // Frag Belt
    // Enchanting
    44645, // Enchant Ring - Assault
    44636, // Enchant Ring - Greater Spellpower
    59636, // Enchant Ring - Stamina
    // Inscription
    61117, // Master's Inscription of the Axe
    61118, // Master's Inscription of the Crag
    61119, // Master's Inscription of the Pinnacle
    61120, // Master's Inscription of the Storm
    // Leatherworking
    57683, // Fur Lining - Attack Power
    57690, // Fur Lining - Stamina
    57691, // Fur Lining - Spell Power
    // Tailoring
    55777, // Swordguard Embroidery
    55642, // Lightweave Embroidery
    55769, // Darkglow Embroidery
    // Blacksmithing
    55641, // Socket Gloves
    55628, // Socket Bracer
};

class cg_egg_vendor : public CreatureScript
{
    private:

        struct PlayerStorage_T
        {
            uint8 opt;
            uint8 eSlot;
            uint8 gemType;
        };

        typedef std::map<Player*, PlayerStorage_T> Storage;
        Storage m_Storage;

    public:

        cg_egg_vendor() : CreatureScript("cg_egg_vendor") { }

        void OnLogout(Player *player)
        {
            // Clear player data on logout
            m_Storage.erase(player);
        }

        bool OnGossipHello(Player* player, Creature* creature) override
        {
            player->CLEAR_GOSSIP_MENU();

            m_Storage.erase(player);

            if (player->IsInCombat())
            {
                player->GetSession()->SendNotification(LANG_YOU_IN_COMBAT);
                return true;
            }

            for (uint8 i = EGG_VENDOR_ENCHANT_ITEM; i < EGG_VENDOR_PRIMARY_MAX; ++i)
            {
                if (_ShowGossipOption(player, creature, i))
                {
                    OptionDescription const& desc = MainMenuOptions[i];
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GOSSIP_ICON_TEXT(desc.name, desc.icon), 0, i);
                }
            }

            player->SEND_GOSSIP_MENU(EGG_VENDOR_GOSSIP_MESSAGE_MAIN, creature->GetGUID());
            return true;
        }

        bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action) override
        {
            player->CLEAR_GOSSIP_MENU();
            player->CLOSE_GOSSIP_MENU();

            switch (action)
            {
                case EGG_VENDOR_ENCHANT_ITEM:
                {
                    _SendEnchantableItemsMenu(player, creature);
                    break;
                }
                case EGG_VENDOR_PURCHASE_GLYPH:
                {
                    m_Storage[player].opt = action;
                    player->GetSession()->SendListInventory(creature->GetGUID());
                    break;
                }
                case EGG_VENDOR_PURCHASE_GEM:
                {
                    _SendGemCategoriesMenu(player, creature);
                    break;
                }
                case EGG_VENDOR_RESET_TALENTS:
                {
                    player->SendTalentWipeConfirm(creature->GetGUID());
                    break;
                }
                case EGG_VENDOR_ENCHANT_ITEM_SECONDARY:
                case EGG_VENDOR_ENCHANT_ITEM_STANDARD:
                {
                    if (!player->GetItemByPos(INVENTORY_SLOT_BAG_0, sender))
                        return _SendErrorMsg(player, creature);

                    _SendItemEnchantMenu(player, creature, sender, action, action == EGG_VENDOR_ENCHANT_ITEM_STANDARD);
                    break;
                }
                case EGG_VENDOR_ENCHANT_ITEM_PROF:
                {
                    PlayerStorage_T const& pStorage = m_Storage[player];
                    Item* target = player->GetItemByPos(INVENTORY_SLOT_BAG_0, pStorage.eSlot);
                    SpellInfo const* proto = sSpellMgr->GetSpellInfo(sender);

                    if (pStorage.opt == EGG_VENDOR_ENCHANT_ITEM_SECONDARY &&
                        _ApplyEnchantToItem(player,
                                            target,
                                            proto))
                    {
                        std::ostringstream buf;
                        buf << "You have successfully enchanted " << target->GetTemplate()->ItemLink()
                            << " with |cff5dfc0a[" << proto->SpellName[0] << "]|r.";
                        ChatHandler(player->GetSession()).SendSysMessage(buf.str().c_str());
                    }

                    return OnGossipSelect(player, creature, 0, EGG_VENDOR_ENCHANT_ITEM);
                }
                case EGG_VENDOR_PURCHASE_GEM_SECONDARY:
                {
                    PlayerStorage_T& pStorage = m_Storage[player];
                    pStorage.opt = action;
                    pStorage.gemType = sender;

                    player->GetSession()->SendListInventory(creature->GetGUID());
                    break;
                }
                default:
                    return OnGossipHello(player, creature);
            }

            return true;
        }

        bool ShouldDisplayVendorItem(Player* player, Creature* /*creature*/, ItemTemplate const* itemTemplate, VendorItem const* /*vendor_item*/) override
        {
            PlayerStorage_T const& pStorage = m_Storage[player];

            uint32 secondaryOption = 0;
            switch (pStorage.opt)
            {
                case EGG_VENDOR_ENCHANT_ITEM_SECONDARY:
                case EGG_VENDOR_ENCHANT_ITEM_STANDARD:
                {
                    secondaryOption = pStorage.eSlot;
                    break;
                }
                case EGG_VENDOR_PURCHASE_GEM_SECONDARY:
                {
                    secondaryOption = pStorage.gemType;
                    break;
                }
                default:
                    break;
            }

            return _IsUsableItemForType(player, itemTemplate, pStorage.opt, secondaryOption);
        }

        bool OnBeforePurchaseItem(Player* player, Creature* creature, ItemTemplate const* itemTemplate, VendorItem const* /*vendor_item*/) override
        {
            PlayerStorage_T& pStorage = m_Storage[player];

            switch (pStorage.opt)
            {
                case EGG_VENDOR_PURCHASE_GLYPH:
                case EGG_VENDOR_PURCHASE_GEM_SECONDARY:
                    return true;
                case EGG_VENDOR_ENCHANT_ITEM_SECONDARY:
                case EGG_VENDOR_ENCHANT_ITEM_STANDARD:
                {
                    Item* target = player->GetItemByPos(INVENTORY_SLOT_BAG_0, pStorage.eSlot);
                    if (_ApplyItemEnhancementToItem(player, target, itemTemplate))
                    {
                        std::ostringstream buf;
                        buf << "You have successfully enchanted " << target->GetTemplate()->ItemLink()
                            << " with " << itemTemplate->ItemLink() << ".";
                        ChatHandler(player->GetSession()).SendSysMessage(buf.str().c_str());
                    }

                    OnGossipSelect(player, creature, 0, EGG_VENDOR_ENCHANT_ITEM);
                    break;
                }
                default:
                    break;
            }

            return false;
        }

    private:

        bool _ShowGossipOption(Player* player, Creature* creature, uint8 option, uint32 secondaryOption = 0)
        {
            // Always show option to reset talents
            if (option == EGG_VENDOR_RESET_TALENTS)
                return true;

            if (VendorItemData const* items = GetVendorItems(creature, player))
                for (auto const& itr : items->m_items)
                    if (ItemTemplate const* itemTemplate = sObjectMgr->GetItemTemplate(itr->item))
                        if (_IsUsableItemForType(player, itemTemplate, option, secondaryOption))
                            return true;

            return _HasValidProfEnchant(player, option, secondaryOption);
        }

        bool _IsUsableItemForType(Player* player, ItemTemplate const* itemTemplate, uint8 option, uint32 secondaryOption)
        {
            if (!itemTemplate)
                return false;

            if (player->CanUseItem(itemTemplate) != EQUIP_ERR_OK)
                return false;

            switch (option)
            {
                case EGG_VENDOR_ENCHANT_ITEM:
                {
                    // Recursively search player's equipped items to find one that can be enchanted
                    // with this item
                    for (uint8 i = ENCHANT_SLOT_HEAD; i < MAX_ENCHANT_SLOT; ++i)
                    {
                        EnchantSlotInfo const& info = EnchantSlotInfoStore[i];
                        if (_ApplyItemEnhancementToItem(player,
                                                        player->GetItemByPos(INVENTORY_SLOT_BAG_0, info.eSlot),
                                                        itemTemplate,
                                                        true))
                            return true;
                    }

                    return false;
                }
                case EGG_VENDOR_PURCHASE_GLYPH:
                    return itemTemplate->Class == ITEM_CLASS_GLYPH;
                case EGG_VENDOR_PURCHASE_GEM:
                case EGG_VENDOR_PURCHASE_GEM_SECONDARY:
                    return itemTemplate->Class == ITEM_CLASS_GEM &&
                        (option != EGG_VENDOR_PURCHASE_GEM_SECONDARY || itemTemplate->SubClass == secondaryOption);
                case EGG_VENDOR_ENCHANT_ITEM_SECONDARY:
                case EGG_VENDOR_ENCHANT_ITEM_STANDARD:
                    return _ApplyItemEnhancementToItem(player,
                                                       player->GetItemByPos(INVENTORY_SLOT_BAG_0, secondaryOption),
                                                       itemTemplate,
                                                       true);
                default:
                    return false;
            }
        }

        bool _ShowProfEnchantMenu(Player* player, Creature* creature)
        {
            PlayerStorage_T const& pStorage = m_Storage[player];

            if (pStorage.opt != EGG_VENDOR_ENCHANT_ITEM_SECONDARY)
                return _SendErrorMsg(player, creature);

            bool haveItemsForProfEnchant = false;
            for (uint32 *spellIdPtr = profEnchantSpellIds; spellIdPtr < profEnchantSpellIds + sizeof(profEnchantSpellIds) / sizeof(uint32); spellIdPtr++)
            {
                if (!player->HasSpell(*spellIdPtr))
                    continue;

                SpellInfo const* proto = sSpellMgr->GetSpellInfo(*spellIdPtr);
                if (_ApplyEnchantToItem(player,
                                        player->GetItemByPos(INVENTORY_SLOT_BAG_0, pStorage.eSlot),
                                        proto,
                                        true))
                {
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_INTERACT_1, std::string(proto->SpellName[0]), proto->Id, EGG_VENDOR_ENCHANT_ITEM_PROF);
                    haveItemsForProfEnchant = true;
                }
            }

            return haveItemsForProfEnchant;
        }

        bool _HasValidProfEnchant(Player* player, uint8 option, uint32 secondaryOption)
        {
            for (uint32 *spellIdPtr = profEnchantSpellIds; spellIdPtr < profEnchantSpellIds + sizeof(profEnchantSpellIds) / sizeof(uint32); spellIdPtr++)
            {
                if (!player->HasSpell(*spellIdPtr))
                    continue;

                SpellInfo const* proto = sSpellMgr->GetSpellInfo(*spellIdPtr);

                switch (option)
                {
                    case EGG_VENDOR_ENCHANT_ITEM:
                    {
                        for (uint8 i = ENCHANT_SLOT_HEAD; i < MAX_ENCHANT_SLOT; ++i)
                        {
                            EnchantSlotInfo const& info = EnchantSlotInfoStore[i];
                            if (_ApplyEnchantToItem(player,
                                                    player->GetItemByPos(INVENTORY_SLOT_BAG_0, info.eSlot),
                                                    proto,
                                                    true))
                                return true;
                        }
                        break;
                    }
                    case EGG_VENDOR_ENCHANT_ITEM_SECONDARY:
                    {
                        if (_ApplyEnchantToItem(player,
                                                player->GetItemByPos(INVENTORY_SLOT_BAG_0, secondaryOption),
                                                proto,
                                                true))
                            return true;

                        break;
                    }
                    default:
                        break;
                }
            }

            return false;
        }

        void _SendEnchantableItemsMenu(Player* player, Creature* creature)
        {
            m_Storage.erase(player);

            for (uint8 i = ENCHANT_SLOT_HEAD; i < MAX_ENCHANT_SLOT; ++i)
            {
                EnchantSlotInfo const& info = EnchantSlotInfoStore[i];
                if (Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, info.eSlot))
                {
                    // Exceptions for items that should not be displayed on gossip menu
                    if (item->GetTemplate()->InventoryType == INVTYPE_RELIC)
                        continue;

                    if (_ShowGossipOption(player, creature, EGG_VENDOR_ENCHANT_ITEM_SECONDARY, info.eSlot))
                    {
                        std::string icon = info.defaultIcon;
                        _UpdateItemIcon(player, item, icon);

                        uint32 enchantId = item->GetEnchantmentId(PERM_ENCHANTMENT_SLOT);
                        if (SpellItemEnchantmentEntry const* itemEnchant = sSpellItemEnchantmentStore.LookupEntry(enchantId))
                        {
                            std::ostringstream buf;
                            buf << item->GetTemplate()->ItemLink()<<"\nis already enchanted with:\n"
                                << "|cff5dfc0a" << itemEnchant->description[0] << "|r\n"
                                << "Are you sure you want to replace this with another?";

                            player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD, GOSSIP_ICON_TEXT(info.defaultName, icon), info.eSlot,
                                                             EGG_VENDOR_ENCHANT_ITEM_SECONDARY, buf.str(), 0, false);
                        }
                        else
                            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GOSSIP_ICON_TEXT(info.defaultName, icon), info.eSlot, EGG_VENDOR_ENCHANT_ITEM_SECONDARY);
                    }
                }
            }

            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GOSSIP_ICON_TEXT_RETURN_TO_MAIN_MENU, 0, EGG_VENDOR_MAIN_MENU);
            player->SEND_GOSSIP_MENU(EGG_VENDOR_GOSSIP_MESSAGE_ENCHANT_ITEMS, creature->GetGUID());
        }

        void _SendGemCategoriesMenu(Player* player, Creature* creature)
        {
            m_Storage.erase(player);

            for (uint8 i = ITEM_SUBCLASS_GEM_RED; i < MAX_ITEM_SUBCLASS_GEM; ++i)
            {
                if (_ShowGossipOption(player, creature, EGG_VENDOR_PURCHASE_GEM_SECONDARY, i))
                {
                    OptionDescription desc = GemOptions[i];
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GOSSIP_ICON_TEXT(desc.name, desc.icon),
                                            i, EGG_VENDOR_PURCHASE_GEM_SECONDARY);
                }
            }

            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GOSSIP_ICON_TEXT_RETURN_TO_MAIN_MENU, 0, EGG_VENDOR_MAIN_MENU);
            player->SEND_GOSSIP_MENU(EGG_VENDOR_GOSSIP_MESSAGE_GEMS, creature->GetGUID());
        }

        void _SendItemEnchantMenu(Player* player, Creature* creature, uint8 eSlot, uint8 option, bool noProf = false)
        {
            PlayerStorage_T& pStorage = m_Storage[player];
            pStorage.opt = option;
            pStorage.eSlot = eSlot;

            if (!noProf && _ShowProfEnchantMenu(player, creature))
            {
                if (_ShowGossipOption(player, creature, EGG_VENDOR_ENCHANT_ITEM_STANDARD, pStorage.eSlot))
                    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_MONEY_BAG, "Show me your other enchants.", pStorage.eSlot, EGG_VENDOR_ENCHANT_ITEM_STANDARD);

                player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TALK, "Return to the main menu.", 0, EGG_VENDOR_MAIN_MENU);
                player->SEND_GOSSIP_MENU(EGG_VENDOR_GOSSIP_MESSAGE_ENCHANT_PROF, creature->GetGUID());
            }
            else
                player->GetSession()->SendListInventory(creature->GetGUID());
        }

        bool _ApplyItemEnhancementToItem(Player* player, Item* item, ItemTemplate const* proto, bool checkOnly = false)
        {
            return _ApplyEnchantToItem(player, item, sSpellMgr->GetSpellInfo(proto->Spells[0].SpellId), checkOnly);
        }

        bool _ApplyEnchantToItem(Player* player, Item* item, SpellInfo const* proto, bool checkOnly = false)
        {
            if (item && proto && (proto->Targets & TARGET_FLAG_ITEM) &&
                _ItemFitsSpellRequirements(item, proto) && _CanApplyEnchant(item, proto))
            {
                if (!checkOnly)
                {
                    SpellCastTargets targets;
                    targets.SetItemTarget(item);

                    Spell* spell = new Spell(player, proto, TRIGGERED_FULL_MASK);

                    spell->InitExplicitTargets(targets);
                    SpellCastResult result = spell->CheckCast(true);
                    if (result != SPELL_CAST_OK)
                    {
                        spell->SendCastResult(result);
                        spell->finish(false);
                        delete spell;

                        return false;
                    }

                    spell->prepare(&targets);
                }

                return true;
            }

            return false;
        }

        bool _ItemFitsSpellRequirements(Item* item, SpellInfo const* proto) const
        {
            return item->IsFitToSpellRequirements(proto) && (proto->BaseLevel ? (item->GetTemplate()->ItemLevel >= proto->BaseLevel) : true);
        }

        bool _CanApplyEnchant(Item* item, SpellInfo const* proto) const
        {
            for (uint8 effIndex = 0; effIndex < MAX_SPELL_EFFECTS; ++effIndex)
            {
                uint32 itemEnchant = 0;
                uint32 enchantId = proto->Effects[effIndex].MiscValue;
                SpellItemEnchantmentEntry const* enchant = sSpellItemEnchantmentStore.LookupEntry(enchantId);

                switch (proto->Effects[effIndex].Effect)
                {
                    case SPELL_EFFECT_ENCHANT_ITEM:
                        itemEnchant = item->GetEnchantmentId(PERM_ENCHANTMENT_SLOT);
                        break;
                    case SPELL_EFFECT_ENCHANT_ITEM_TEMPORARY:
                        itemEnchant = item->GetEnchantmentId(TEMP_ENCHANTMENT_SLOT);
                        break;
                    case SPELL_EFFECT_ENCHANT_ITEM_PRISMATIC:
                        itemEnchant = item->GetEnchantmentId(PRISMATIC_ENCHANTMENT_SLOT);
                        break;
                    default:
                        continue;
                }

                if (enchant && enchant != sSpellItemEnchantmentStore.LookupEntry(itemEnchant))
                    return true;
            }

            return false;
        }

        bool _SendErrorMsg(Player* player, Creature* creature)
        {
            ChatHandler(player->GetSession()).SendSysMessage("An error has occurred. Please try again.");
            return OnGossipHello(player, creature);
        }

        void _UpdateItemIcon(Player* player, Item* item, std::string& icon)
        {
            if (ItemTemplate const* proto = sObjectMgr->GetItemTemplate(sTransmog->GetItemDisplay(player, item)))
                if (ItemDisplayInfoEntry const* displayInfo = sItemDisplayInfoStore.LookupEntry(proto->DisplayInfoID))
                    icon = "ICONS/" + std::string(displayInfo->inventoryIcon);
        }
};

void AddSC_cg_egg_vendor()
{
    new cg_egg_vendor();
}