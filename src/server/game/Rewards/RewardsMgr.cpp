/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "CurrencyMgr.h"
#include "RewardsMgr.h"

RewardsMgr::RewardsMgr()
{
}

void RewardsMgr::Init()
{
    _LoadQuestExtendedRewards();
}

void RewardsMgr::OnRewardQuest(Player* player, uint32 questId)
{
    if (QuestExtendedReward const* reward = _GetQuestExtendedReward(questId))
        // This should always be true since we are adding points. If this is
        // not the case, something is wrong
        ASSERT(sCurrencyMgr->ModifyRewardCurrencies(player,
                                                    static_cast<int32>(reward->votePoints),
                                                    static_cast<int32>(reward->donationPoints),
                                                    TRANSACTION_ENTITY_QUEST,
                                                    questId));
}

void RewardsMgr::_LoadQuestExtendedRewards()
{
    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_QUEST_EXT_REWARDS);
    if (PreparedQueryResult result = WorldDatabase.Query(stmt))
    {
        do {
            Field* fields = result->Fetch();

            uint32 questId = fields[0].GetUInt32();
            if (!sObjectMgr->GetQuestTemplate(questId))
            {
                TC_LOG_ERROR("sql.sql", "Invalid quest ID %u specified in quest_extended_rewards table.", questId);
                continue;
            }

            QuestExtendedReward reward;
            reward.votePoints     = fields[1].GetUInt32();
            reward.donationPoints = fields[2].GetUInt32();

            _questExtendedRewards[questId] = reward;
        } while (result->NextRow());
    }

    TC_LOG_INFO("server.loading", ">> Loaded %u quest extended rewards in %u ms",
                uint32(_questExtendedRewards.size()), GetMSTimeDiffToNow(oldMSTime));
}

QuestExtendedReward const* RewardsMgr::_GetQuestExtendedReward(uint32 questId)
{
    auto itr = _questExtendedRewards.find(questId);
    return itr == _questExtendedRewards.end() ? nullptr : &itr->second;
}