/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_REWARDSMGR_H
#define CG_REWARDSMGR_H

struct QuestExtendedReward
{
    uint32 votePoints;
    uint32 donationPoints;
};

typedef std::map<uint32, QuestExtendedReward> QuestExtendedRewards;

class RewardsMgr
{
    private:

        RewardsMgr();
        ~RewardsMgr() { }

    public:

        static RewardsMgr* instance()
        {
            static RewardsMgr instance;
            return &instance;
        }

        void Init();

        void OnRewardQuest(Player* player, uint32 questId);

    private:

        void _LoadQuestExtendedRewards();

        QuestExtendedReward const* _GetQuestExtendedReward(uint32 questId);

        QuestExtendedRewards _questExtendedRewards;
};

#define sRewardsMgr RewardsMgr::instance()

#endif