/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "AnticheatMgr.h"
#include "Config.h"
#include "MapManager.h"
#include "RBAC.h"
#include "WorldSession.h"

AnticheatMgr::AnticheatMgr()
{
}

AnticheatMgr::~AnticheatMgr()
{
    m_Data.clear();
}

void AnticheatMgr::Init()
{
    m_Enabled = sConfigMgr->GetBoolDefault("Anticheat.Enable", true);
    m_ChecksMask = sConfigMgr->GetIntDefault("Anticheat.ChecksMask", 31);
    m_NotifyInterval = sConfigMgr->GetIntDefault("Anticheat.NotifyInterval", 5) * IN_MILLISECONDS;
}

void AnticheatMgr::Check(Player* player, MovementInfo movementInfo, uint32 opcode)
{
    if (!m_Enabled)
        return;

    // Skip for game masters
    if (!player ||
        player->GetSession()->HasPermission(rbac::RBAC_PERM_SKIP_CHECK_ANTICHEAT))
        return;

    if (!player->IsInFlight() &&
        !player->GetTransport() &&
        !player->GetVehicle())
    {
        _CheckSpeedHack(player, movementInfo, opcode);
        _CheckFlyHack(player, movementInfo, opcode);
        _CheckWaterWalk(player, movementInfo, opcode);
        _CheckJumpHack(player, movementInfo, opcode);
        _CheckTeleportPlaneHack(player, movementInfo, opcode);
        _CheckClimbHack(player, movementInfo, opcode);
    }

    AnticheatData* data = _GetPlayerData(player);
    data->SetLastMovementInfo(movementInfo);
    data->SetLastOpcode(opcode);
}

void AnticheatMgr::InitPlayerData(Player* player)
{
    AnticheatData* data = _GetPlayerData(player);
    ASSERT(data);

    data->InitLastMovementInfo(player->GetPosition());
}

void AnticheatMgr::ClearPlayerData(Player* player)
{
    m_Data.erase(player);
}

AnticheatData* AnticheatMgr::_GetPlayerData(Player* player)
{
    auto itr = m_Data.find(player);

    if (itr == m_Data.end())
        return m_Data[player] = new AnticheatData(player);

    return itr->second;
}

void AnticheatMgr::_CheckSpeedHack(Player* player, MovementInfo movementInfo, uint32 /*opcode*/)
{
    if (!_ShouldCheck(CHEAT_SPEED_HACK))
        return;

    AnticheatData* data = _GetPlayerData(player);

    // We also must check the map because the movementFlag can be modified by the client.
    // If we just check the flag, they could always add that flag and always skip the speed hacking detection.
    // 369 == DEEPRUN TRAM
    if (data->GetLastMovementInfo().HasMovementFlag(MOVEMENTFLAG_ONTRANSPORT) &&
        player->GetMapId() == 369)
        return;

    uint8 moveType = 0;
    // we need to know HOW is the player moving
    // TO-DO: Should we check the incoming movement flags?
    if (player->HasUnitMovementFlag(MOVEMENTFLAG_SWIMMING))
        moveType = MOVE_SWIM;
    else if (player->IsFlying())
        moveType = MOVE_FLIGHT;
    else if (player->HasUnitMovementFlag(MOVEMENTFLAG_WALKING))
        moveType = MOVE_WALK;
    else
        moveType = MOVE_RUN;

    // theoretical speed. add some tolerance with ceil()
    float speedRate = ceil(player->GetSpeed(UnitMoveType(moveType)) + movementInfo.jump.xyspeed);

    // time taken for step.
    uint32 timeDiff = getMSTimeDiff(data->GetLastMovementInfo().time, movementInfo.time);

    // actual speed.
    float dist = movementInfo.pos.GetExactDist2d(&data->GetLastMovementInfo().pos);
    float clientSpeedRate = round(dist * 1000 / float(timeDiff ? timeDiff : 1));

    if (clientSpeedRate > speedRate)
        data->Log(CHEAT_SPEED_HACK);
}

void AnticheatMgr::_CheckFlyHack(Player* player, MovementInfo /*movementInfo*/, uint32 /*opcode*/)
{
    if (!_ShouldCheck(CHEAT_FLY_HACK))
        return;

    AnticheatData* data = _GetPlayerData(player);

    if (!data->GetLastMovementInfo().HasMovementFlag(MOVEMENTFLAG_MASK_MOVING_FLY))
        return;

    if (player->HasAuraType(SPELL_AURA_FLY) ||
        player->HasAuraType(SPELL_AURA_MOD_INCREASE_MOUNTED_FLIGHT_SPEED) ||
        player->HasAuraType(SPELL_AURA_MOD_INCREASE_FLIGHT_SPEED))
        return;

    data->Log(CHEAT_FLY_HACK);
}

void AnticheatMgr::_CheckWaterWalk(Player* player, MovementInfo /*movementInfo*/, uint32 /*opcode*/)
{
    if (!_ShouldCheck(CHEAT_WATER_WALK_HACK))
        return;

    AnticheatData* data = _GetPlayerData(player);

    if (!data->GetLastMovementInfo().HasMovementFlag(MOVEMENTFLAG_WATERWALKING))
        return;

    // ghosts can walk on water
    if (!player->IsAlive())
        return;

    if (player->HasAuraType(SPELL_AURA_FEATHER_FALL) ||
        player->HasAuraType(SPELL_AURA_SAFE_FALL) ||
        player->HasAuraType(SPELL_AURA_WATER_WALK))
        return;

    data->Log(CHEAT_WATER_WALK_HACK);
}

void AnticheatMgr::_CheckJumpHack(Player* player, MovementInfo /*movementInfo*/, uint32 opcode)
{
    if (!_ShouldCheck(CHEAT_JUMP_HACK))
        return;

    AnticheatData* data = _GetPlayerData(player);

    if (data->GetLastOpcode() == MSG_MOVE_JUMP && opcode == MSG_MOVE_JUMP)
        data->Log(CHEAT_JUMP_HACK);
}

void AnticheatMgr::_CheckTeleportPlaneHack(Player* player, MovementInfo movementInfo, uint32 /*opcode*/)
{
    if (!_ShouldCheck(CHEAT_TELEPORT_PLANE_HACK))
        return;

    AnticheatData* data = _GetPlayerData(player);

    if (data->GetLastMovementInfo().pos.GetPositionZ() != 0 ||
        movementInfo.pos.GetPositionZ() != 0)
        return;

    if (movementInfo.HasMovementFlag(MOVEMENTFLAG_FALLING))
        return;

    float x, y, z;
    player->GetPosition(x, y, z);
    float ground_Z = player->GetMap()->GetHeight(x, y, z);

    // we are not really walking there
    if (fabs(ground_Z - z) > 1.0f)
        data->Log(CHEAT_TELEPORT_PLANE_HACK);
}

void AnticheatMgr::_CheckClimbHack(Player* player, MovementInfo movementInfo, uint32 opcode)
{
    if (!_ShouldCheck(CHEAT_CLIMB_HACK))
        return;

    AnticheatData* data = _GetPlayerData(player);

    if (opcode != MSG_MOVE_HEARTBEAT || data->GetLastOpcode() != MSG_MOVE_HEARTBEAT)
        return;

    // in this case we don't care if they are "legal" flags, they are handled in another parts of the Anticheat Manager.
    if (player->IsInWater() ||
        player->IsFlying() ||
        player->IsFalling())
        return;

    Position pos = player->GetPosition();

    float deltaZ = fabs(pos.GetPositionZ() - movementInfo.pos.GetPositionZ());
    float deltaXY = movementInfo.pos.GetExactDist2d(&pos);

    float angle = Position::NormalizeOrientation(tan(deltaZ/deltaXY));

    if (angle > MAX_LEGAL_CLIMB_ANGLE)
        data->Log(CHEAT_CLIMB_HACK);
}