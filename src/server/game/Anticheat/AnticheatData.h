/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_ACDATA_H
#define CG_ACDATA_H

#include "AnticheatDefines.h"

struct CheatTypeStorage
{
    CheatTypeStorage() : lastLogTime(0), logCount(0) { }

    uint32 lastLogTime;
    uint32 logCount;
};

class AnticheatData
{
    public:

        AnticheatData(Player* p);
        ~AnticheatData() { }

        void InitLastMovementInfo(Position pos);

        uint32 GetLastOpcode() const { return m_LastOpcode; }
        void SetLastOpcode(uint32 opcode) { m_LastOpcode = opcode; }

        const MovementInfo& GetLastMovementInfo() const { return m_LastMovementInfo; }
        void SetLastMovementInfo(MovementInfo& movementInfo) { m_LastMovementInfo = movementInfo; }

        void Log(CheatType type);
    private:

        const std::string _GetNameLink() const;

        Player* m_Player;

        uint32 m_LastOpcode;
        MovementInfo m_LastMovementInfo;

        CheatTypeStorage m_Storage[MAX_CHEAT_TYPES];
};

#endif