/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_ACDEFINES_H
#define CG_ACDEFINES_H

#define MAX_LEGAL_CLIMB_ANGLE 1.9f

enum CheatType
{
    CHEAT_SPEED_HACK,
    CHEAT_FLY_HACK,
    CHEAT_WATER_WALK_HACK,
    CHEAT_JUMP_HACK,
    CHEAT_TELEPORT_PLANE_HACK,
    CHEAT_CLIMB_HACK,

    MAX_CHEAT_TYPES
};

inline const std::string GetCheatTypeDesc(CheatType type)
{
    switch (type)
    {
        case CHEAT_SPEED_HACK:              return "speed hack";
        case CHEAT_FLY_HACK:                return "fly hack";
        case CHEAT_WATER_WALK_HACK:         return "water walk hack";
        case CHEAT_JUMP_HACK:               return "jump hack";
        case CHEAT_TELEPORT_PLANE_HACK:     return "teleport plane hack";
        case CHEAT_CLIMB_HACK:              return "climb hack";
        default:                            return "";
    }
}

#endif