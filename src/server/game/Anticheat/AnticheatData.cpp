/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "AnticheatData.h"
#include "AnticheatMgr.h"
#include "Chat.h"
#include "Language.h"
#include "Player.h"
#include "UDPLogger.h"
#include "World.h"
#include "WorldSession.h"

AnticheatData::AnticheatData(Player* p)
{
    ASSERT(p);
    m_Player = p;

    m_LastOpcode = 0;
}

void AnticheatData::InitLastMovementInfo(Position pos)
{
    m_LastMovementInfo.SetMovementFlags(MOVEMENTFLAG_NONE);
    m_LastMovementInfo.time = getMSTime();
    m_LastMovementInfo.pos.Relocate(pos);
}

void AnticheatData::Log(CheatType type)
{
    CheatTypeStorage& storage = m_Storage[type];

    ++storage.logCount;

    uint32 currTime = getMSTime();
    uint32 timeDiff = getMSTimeDiff(storage.lastLogTime, currTime);

    if (timeDiff > sAnticheatMgr->GetNotifyInterval())
    {
        storage.lastLogTime = currTime;

        // Notify GMs in-game
        sWorld->SendGMText(LANG_ANTICHEAT_BROADCAST, GetCheatTypeDesc(type).c_str(),
                           _GetNameLink().c_str(), m_Player->GetGUIDLow(),
                           storage.logCount);

        // Send UDP log
        std::ostringstream buf;
        buf << "[ANTICHEAT]: Detected possible " << GetCheatTypeDesc(type)
            << " by " << m_Player->GetName()
            << " (GUID: " << m_Player->GetGUIDLow() << ") - "
            << storage.logCount << " report(s).";
        UDP_WS_CHEAT(buf.str().c_str());
    }
}

const std::string AnticheatData::_GetNameLink() const
{
    std::string const& name = m_Player->GetName();
    return "|cffffffff|Hplayer:" + name + "|h|Hanticheat:" + name + "|h[" + name + "]|h|r";
}