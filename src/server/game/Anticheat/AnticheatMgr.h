/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_ACMGR_H
#define CG_ACMGR_H

#include "AnticheatData.h"
#include "Player.h"

typedef std::map<Player*, AnticheatData*> ACDataMap;

class AnticheatMgr
{
    private:

        AnticheatMgr();
        ~AnticheatMgr();

    public:

        static AnticheatMgr* instance()
        {
           static AnticheatMgr instance;
           return &instance;
        }

        void Init();

        uint32 GetNotifyInterval() const { return m_NotifyInterval; }

        void InitPlayerData(Player* player);
        void ClearPlayerData(Player* player);

        void Check(Player* player, MovementInfo movementInfo, uint32 opcode);

    private:

        AnticheatData* _GetPlayerData(Player* player);

        bool _ShouldCheck(CheatType type) const { return (m_ChecksMask & (1 << type)) != 0; }

        void _CheckSpeedHack(Player* player, MovementInfo movementInfo, uint32 opcode);
        void _CheckFlyHack(Player* player, MovementInfo movementInfo, uint32 opcode);
        void _CheckWaterWalk(Player* player, MovementInfo movementInfo, uint32 opcode);
        void _CheckJumpHack(Player* player, MovementInfo movementInfo, uint32 opcode);
        void _CheckTeleportPlaneHack(Player* player, MovementInfo movementInfo, uint32 opcode);
        void _CheckClimbHack(Player* player, MovementInfo movementInfo, uint32 opcode);

        ACDataMap m_Data;

        bool m_Enabled;
        uint32 m_ChecksMask;
        uint32 m_NotifyInterval;
};

#define sAnticheatMgr AnticheatMgr::instance()

#endif