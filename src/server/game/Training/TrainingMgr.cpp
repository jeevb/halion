/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "Chat.h"
#include "CurrencyMgr.h"
#include "NotificationMgr.h"
#include "ObjectMgr.h"
#include "Player.h"
#include "TrainingMgr.h"
#include "World.h"

TrainingMgr::TrainingMgr()
{
}

void TrainingMgr::Init()
{
    _LoadAutoItems();
    _LoadAutoTrainers();
}

bool TrainingMgr::IsAutoTrainer(uint32 entry) const
{
    for (auto const& trainer : m_AutoTrainers)
    {
        if (entry == trainer.entry)
            return true;
    }

    return false;
}

void TrainingMgr::Boost(Player* player, uint8 level)
{
    if (!level || player->getLevel() >= level ||
        level > sWorld->getIntConfig(CONFIG_MAX_PLAYER_LEVEL))
        return;

    uint32 totalExpToDesiredLevel = 0;
    for (uint8 i = player->getLevel(); i < level; i++)
        totalExpToDesiredLevel += sObjectMgr->GetXPForLevel(i);

    // Set experience rate to twice the maximum possible experience rate
    Experience* e = player->GetExperience();

    // Temporarily set rate to twice the maximum allowed for the purpose of
    // calculating base experience.
    e->SetRate(sWorld->getRate(RATE_XP_MAX_MULT) * 2.0f);

    // No leveling rewards when boosting
    e->SetCanGetLevelingRewards(false);
    player->GiveXP(totalExpToDesiredLevel - player->GetUInt32Value(PLAYER_XP), nullptr);
    e->SetCanGetLevelingRewards();

    player->InitTalentForLevel();

    std::ostringstream buf;
    buf << "You have been boosted to "
        << "|cff1eff00Level " << uint32(level) << "|r.";
    sNotify->FlashMessage(player, buf.str());

    // Class-specific handling
    switch (player->getClass())
    {
        case CLASS_DEATH_KNIGHT:
        {
            _AbandonAllQuests(player);
            player->CastSpell(player, DEATH_KNIGHT_LEARN_SUMMON_DEATHCHARGER, true); // Summon Deathcharger
            player->SetQuestStatus(DEATH_KNIGHT_QUEST_THE_MIGHT_OF_THE_SCOURGE, QUEST_STATUS_REWARDED); // Auto-cast Dominion Over Acherus
            break;
        }
        default:
            break;
    }

    // Return rate to the maximum possible value
    e->SetRate(sWorld->getRate(RATE_XP_MAX_MULT));
}

void TrainingMgr::AddItems(Player* player, bool onLevelReach /*= false*/)
{
    bool save = false;

    for (auto const& item : m_AutoItems)
    {
        if (item.raceMask && (item.raceMask & player->getRaceMask()) == 0)
            continue;

        if (item.classMask && (item.classMask & player->getClassMask()) == 0)
            continue;

        if (item.trackMask && (item.trackMask & player->getTrackMask()) == 0)
            continue;

        if (item.reqLevel && item.reqLevel > player->getLevel())
            continue;

        if (item.onLevelReach &&
            (!onLevelReach || item.reqLevel != player->getLevel()))
            continue;

        if (_AddItem(player, item.entry, item.count) && !save)
            save = true;
    }

    if (save)
        player->SaveToDB();
}

void TrainingMgr::AddLevelingRewards(Player* player)
{
    int32 vp = 0, dp = 0;
    player->GetExperience()->FillLevelingRewards(vp, dp);

    sCurrencyMgr->ModifyRewardCurrencies(player, vp, dp, TRANSACTION_ENTITY_LEVELING, player->GetGUIDLow(), player->GetName());
}

void TrainingMgr::Learn(Player* player)
{
    bool save = false;

    for (auto const& trainer : m_AutoTrainers)
    {
        if (trainer.raceMask && (trainer.raceMask & player->getRaceMask()) == 0)
            continue;

        if (trainer.classMask && (trainer.classMask & player->getClassMask()) == 0)
            continue;

        if (trainer.trackMask && (trainer.trackMask & player->getTrackMask()) == 0)
            continue;

        if (_Learn(player, trainer.entry) && !save)
            save = true;
    }

    // Learn dual specialization
    if (player->GetSpecsCount() == 1 &&
        player->getLevel() >= sWorld->getIntConfig(CONFIG_MIN_DUALSPEC_LEVEL))
    {
        player->CastSpell(player, DUAL_TALENT_SPEC_SPELL_ONE, true);
        player->CastSpell(player, DUAL_TALENT_SPEC_SPELL_TWO, true);
    }

    // Upgrade all skills to max
    player->UpdateSkillsToMaxSkillsForLevel();

    if (save)
        player->SaveToDB();
}

bool TrainingMgr::LearnProfession(Player* player, uint32 profession)
{
    // Invalid profession entry
    if (profession >= PROF_MAX)
        return false;

    ProfessionInfo const& prof = ProfessionInfoStore[profession];
    return LearnProfession(player, prof.spellId, prof.skillId, prof.trainerId);
}

bool TrainingMgr::LearnProfession(Player* player, uint32 spellId, uint32 skillId, uint32 trainerId)
{
    // Silence all achievement announcements
    player->SetSilenceAchievement();

    player->LearnSpell(spellId, false);
    uint16 maxLevel = player->GetPureMaxSkillValue(skillId);
    player->SetSkill(skillId, player->GetSkillStep(skillId), maxLevel, maxLevel);

    // Un-silence all achievement announcements
    player->SetSilenceAchievement(false);

    return _Learn(player, trainerId);
}

void TrainingMgr::_LoadAutoItems()
{
    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_AUTO_ITEM);
    if (PreparedQueryResult result = WorldDatabase.Query(stmt))
    {
        do {
            Field* fields = result->Fetch();

            AutoItem i;
            i.raceMask      = fields[0].GetUInt32();
            i.classMask     = fields[1].GetUInt32();
            i.trackMask     = fields[2].GetUInt32();
            i.reqLevel      = fields[3].GetUInt8();
            i.entry         = fields[4].GetUInt32();
            i.count         = fields[5].GetUInt32();
            i.onLevelReach  = fields[6].GetBool();

            m_AutoItems.push_back(i);
        } while (result->NextRow());
    }

    TC_LOG_INFO("server.loading", ">> Loaded %u auto item entries in %u ms",
                uint32(m_AutoItems.size()), GetMSTimeDiffToNow(oldMSTime));
}

void TrainingMgr::_LoadAutoTrainers()
{
    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_AUTO_TRAINER);
    if (PreparedQueryResult result = WorldDatabase.Query(stmt))
    {
        do {
            Field* fields = result->Fetch();

            AutoTrainer t;
            t.raceMask  = fields[0].GetUInt32();
            t.classMask = fields[1].GetUInt32();
            t.trackMask = fields[2].GetUInt32();
            t.entry     = fields[3].GetUInt32();
            t.priority  = fields[4].GetUInt8();

            m_AutoTrainers.push_back(t);
        } while (result->NextRow());
    }

    // Sort by priority
    std::sort(m_AutoTrainers.begin(), m_AutoTrainers.end());

    TC_LOG_INFO("server.loading", ">> Loaded %u auto trainer entries in %u ms",
                uint32(m_AutoTrainers.size()), GetMSTimeDiffToNow(oldMSTime));
}

bool TrainingMgr::_AddItem(Player* player, uint32 itemId, uint32 count)
{
    if (player->HasItemCount(itemId, count, true))
        return false;

    if (!player->StoreNewItemInBestSlots(itemId, count))
        player->SendItemRetrievalMail(itemId, count);

    return true;
}

bool TrainingMgr::_Learn(Player* player, uint32 trainer)
{
    TrainerSpellData const* data = sObjectMgr->GetNpcTrainerSpells(trainer);

    if (!data)
        return false;

    uint32 totalLearnedSpells = 0;
    for (uint8 i = 0; i < MAX_LEARN_TRIES; ++i)
    {
        uint32 learnedSpells = 0;
        for (auto const& itr : data->spellList)
        {
            TrainerSpell const* tSpell = &itr.second;

            // Make sure that the trainer spell is actually valid for the player
            bool valid = true;
            for (uint8 i = 0; i < MAX_SPELL_EFFECTS; ++i)
            {
                if (!tSpell->learnedSpell[i])
                    continue;

                if (!player->IsSpellFitByClassAndRace(tSpell->learnedSpell[i]))
                {
                    valid = false;
                    break;
                }
            }

            if (!valid)
                continue;

            TrainerSpellState state = player->GetTrainerSpellState(tSpell);
            if (state == TRAINER_SPELL_GREEN)
            {
                if (tSpell->IsCastable())
                    player->CastSpell(player, tSpell->spell, true);
                else
                    player->LearnSpell(tSpell->spell, false);

                learnedSpells++;
            }
        }

        if (!learnedSpells)
            break;

        totalLearnedSpells += learnedSpells;
    }

    return totalLearnedSpells > 0;
}

// Copied from WorldSession::HandleQuestLogRemoveQuest()
void TrainingMgr::_AbandonAllQuests(Player* player)
{
    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    for (uint8 slot = 0; slot < MAX_QUEST_LOG_SIZE; ++slot)
    {
        if (uint32 questId = player->GetQuestSlotQuestId(slot))
        {
            if (Quest const* quest = sObjectMgr->GetQuestTemplate(questId))
            {
                if (quest->HasSpecialFlag(QUEST_SPECIAL_FLAGS_TIMED))
                    player->RemoveTimedQuest(questId);

                if (quest->HasFlag(QUEST_FLAGS_FLAGS_PVP))
                {
                    player->pvpInfo.IsHostile = player->pvpInfo.IsInHostileArea || player->HasPvPForcingQuest();
                    player->UpdatePvPState();
                }
            }

            player->TakeQuestSourceItem(questId, true); // remove quest src item from player
            player->RemoveActiveQuest(questId);
            player->RemoveTimedAchievement(ACHIEVEMENT_TIMED_TYPE_QUEST, questId);

            if (sWorld->getBoolConfig(CONFIG_QUEST_ENABLE_QUEST_TRACKER)) // check if Quest Tracker is enabled
            {
                // prepare Quest Tracker data
                PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_UPD_QUEST_TRACK_ABANDON_TIME);
                stmt->setUInt32(0, questId);
                stmt->setUInt32(1, player->GetGUIDLow());
                trans->Append(stmt);
            }
        }

        player->SetQuestSlot(slot, 0);
        player->UpdateAchievementCriteria(ACHIEVEMENT_CRITERIA_TYPE_QUEST_ABANDONED, 1);
    }

    CharacterDatabase.CommitTransaction(trans);
}