/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_TRAINMGR_H
#define CG_TRAINMGR_H

struct AutoItem
{
    uint32 raceMask;
    uint32 classMask;
    uint32 trackMask;
    uint8 reqLevel;
    uint32 entry;
    uint32 count;
    bool onLevelReach;
};

struct AutoTrainer
{
    uint32 raceMask;
    uint32 classMask;
    uint32 trackMask;
    uint32 entry;
    uint8 priority;

    bool operator< (AutoTrainer const& other) const
    {
        return priority < other.priority;
    }
};

typedef std::vector<AutoItem> AutoItems;
typedef std::vector<AutoTrainer> AutoTrainers;

#define MAX_LEARN_TRIES 50

enum Profession
{
    PROF_ALCHEMY,
    PROF_BLACKSMITHING,
    PROF_ENCHANTING,
    PROF_ENGINEERING,
    PROF_HERBALISM,
    PROF_LEATHERWORKING,
    PROF_TAILORING,
    PROF_MINING,
    PROF_SKINNING,
    PROF_JEWELCRAFTING,
    PROF_INSCRIPTION,
    PROF_FIRST_AID,

    PROF_MAX
};

#define PROF_PRIMARY_MAX 10

enum ProfessionSpell
{
    GRANDMASTER_ALCHEMY         = 51304,
    GRANDMASTER_BLACKSMITHING   = 51300,
    GRANDMASTER_ENCHANTING      = 51313,
    GRANDMASTER_ENGINEERING     = 51306,
    GRANDMASTER_HERBALISM       = 50300,
    GRANDMASTER_LEATHERWORKING  = 51302,
    GRANDMASTER_TAILORING       = 51309,
    GRANDMASTER_MINING          = 50310,
    GRANDMASTER_SKINNING        = 50305,
    GRANDMASTER_JEWELCRAFTING   = 51311,
    GRANDMASTER_INSCRIPTION     = 45363,
    GRANDMASTER_FIRST_AID       = 50299,
};

enum ProfessionTrainer
{
    TRAINER_ALCHEMY        = 28703,
    TRAINER_BLACKSMITHING  = 28694,
    TRAINER_ENCHANTING     = 28693,
    TRAINER_ENGINEERING    = 28697,
    TRAINER_HERBALISM      = 28704,
    TRAINER_LEATHERWORKING = 28700,
    TRAINER_TAILORING      = 28699,
    TRAINER_MINING         = 28698,
    TRAINER_SKINNING       = 28696,
    TRAINER_JEWELCRAFTING  = 28701,
    TRAINER_INSCRIPTION    = 28702,
    TRAINER_FIRST_AID      = 28706
};

struct ProfessionInfo
{
    ProfessionInfo(std::string const& _name, std::string const& _icon, const uint32 _spellId, const uint32 _skillId, const uint32 _trainerId) :
        name(_name), icon(_icon), spellId(_spellId), skillId(_skillId), trainerId(_trainerId) { }

    std::string name;
    std::string icon;
    uint32      spellId;
    uint32      skillId;
    uint32      trainerId;
};

const ProfessionInfo ProfessionInfoStore[PROF_MAX] =
{
    ProfessionInfo("Alchemy",           "Icons/trade_alchemy",                  GRANDMASTER_ALCHEMY,        SKILL_ALCHEMY,          TRAINER_ALCHEMY),
    ProfessionInfo("Blacksmithing",     "Icons/trade_blacksmithing",            GRANDMASTER_BLACKSMITHING,  SKILL_BLACKSMITHING,    TRAINER_BLACKSMITHING),
    ProfessionInfo("Enchanting",        "Icons/trade_engraving",                GRANDMASTER_ENCHANTING,     SKILL_ENCHANTING,       TRAINER_ENCHANTING),
    ProfessionInfo("Engineering",       "Icons/trade_engineering",              GRANDMASTER_ENGINEERING,    SKILL_ENGINEERING,      TRAINER_ENGINEERING),
    ProfessionInfo("Herbalism",         "Icons/spell_nature_naturetouchgrow",   GRANDMASTER_HERBALISM,      SKILL_HERBALISM,        TRAINER_HERBALISM),
    ProfessionInfo("Leatherworking",    "Icons/inv_misc_armorkit_17",           GRANDMASTER_LEATHERWORKING, SKILL_LEATHERWORKING,   TRAINER_LEATHERWORKING),
    ProfessionInfo("Tailoring",         "Icons/trade_tailoring",                GRANDMASTER_TAILORING,      SKILL_TAILORING,        TRAINER_TAILORING),
    ProfessionInfo("Mining",            "Icons/trade_mining",                   GRANDMASTER_MINING,         SKILL_MINING,           TRAINER_MINING),
    ProfessionInfo("Skinning",          "Icons/inv_misc_pelt_wolf_01",          GRANDMASTER_SKINNING,       SKILL_SKINNING,         TRAINER_SKINNING),
    ProfessionInfo("Jewelcrafting",     "Icons/inv_misc_gem_01",                GRANDMASTER_JEWELCRAFTING,  SKILL_JEWELCRAFTING,    TRAINER_JEWELCRAFTING),
    ProfessionInfo("Inscription",       "Icons/inv_inscription_tradeskill01",   GRANDMASTER_INSCRIPTION,    SKILL_INSCRIPTION,      TRAINER_INSCRIPTION),
    ProfessionInfo("First Aid",         "Icons/spell_holy_sealofsacrifice",     GRANDMASTER_FIRST_AID,      SKILL_FIRST_AID,        TRAINER_FIRST_AID)
};

enum TrainingEnums
{
    DEATH_KNIGHT_LEARN_SUMMON_DEATHCHARGER      = 52382,
    DEATH_KNIGHT_QUEST_THE_MIGHT_OF_THE_SCOURGE = 12657,
    DUAL_TALENT_SPEC_SPELL_ONE                  = 63624,
    DUAL_TALENT_SPEC_SPELL_TWO                  = 63680
};

class TrainingMgr
{
    private:
        TrainingMgr();
        ~TrainingMgr() { }

    public:

        static TrainingMgr* instance()
        {
            static TrainingMgr instance;
            return &instance;
        }

        void Init();

        bool IsAutoTrainer(uint32 entry) const;
        void Boost(Player* player, uint8 level);
        void AddLevelingRewards(Player* player);
        void AddItems(Player* player, bool onLevelReach = false);
        void Learn(Player* player);

        // Helpers to learn professions
        bool LearnProfession(Player* player, uint32 profession);
        bool LearnProfession(Player* player, uint32 spellId, uint32 skillId, uint32 trainerId);

    private:

        void _LoadAutoItems();
        void _LoadAutoTrainers();

        bool _AddItem(Player* player, uint32 itemId, uint32 count);
        bool _Learn(Player* player, uint32 trainer);

        void _AbandonAllQuests(Player* player);

        AutoItems m_AutoItems;
        AutoTrainers m_AutoTrainers;
};

#define sTrain TrainingMgr::instance()

#endif