/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "Player.h"
#include "ScriptMgr.h"

Experience::Experience(Player* player) :
    m_Player(player),
    m_Rate(1.0f),
    m_TempRate(0.0f),
    m_TotalExp(0),
    m_BaseTotalExp(0.0f),
    m_LevelExp(0),
    m_BaseLevelExp(0.0f),
    m_UnitsKilled(0),
    m_KillsExp(0),
    m_BaseKillsExp(0.0f),
    m_QuestsCompleted(0),
    m_QuestsExp(0),
    m_BaseQuestsExp(0.0f),
    m_AreasExplored(0),
    m_ExploredExp(0),
    m_BaseExploredExp(0.0f),
    m_Updated(false),
    m_CanGetLevelingRewards(true)
{
    // Make sure the the player object exists
    ASSERT(m_Player);
}

void Experience::LoadDataFromFields(Field* fields)
{
    uint8 idx = 0;

    m_Rate              = fields[idx++].GetFloat();
    m_TotalExp          = fields[idx++].GetUInt32();
    m_BaseTotalExp      = fields[idx++].GetFloat();
    m_LevelExp          = fields[idx++].GetUInt32();
    m_BaseLevelExp      = fields[idx++].GetFloat();
    m_UnitsKilled       = fields[idx++].GetUInt32();
    m_KillsExp          = fields[idx++].GetUInt32();
    m_BaseKillsExp      = fields[idx++].GetFloat();
    m_QuestsCompleted   = fields[idx++].GetUInt32();
    m_QuestsExp         = fields[idx++].GetUInt32();
    m_BaseQuestsExp     = fields[idx++].GetFloat();
    m_AreasExplored     = fields[idx++].GetUInt32();
    m_ExploredExp       = fields[idx++].GetUInt32();
    m_BaseExploredExp   = fields[idx].GetFloat();
}

void Experience::SaveToDB(SQLTransaction& trans)
{
    if (!m_Updated)
        return;

    _SaveData(trans);

    // Values have been updated, remove flag
    m_Updated = false;
}

void Experience::SetRate(float rate)
{
    m_Rate = rate;

    // Flag values to be written to database
    if (!m_Updated)
        m_Updated = true;
}

void Experience::Add(uint32 gain, Unit* victim, ExperienceType type, float group_rate)
{
    // Make sure the the player object still exists
    ASSERT(m_Player);

    // Apply bonuses to gain from scripts
    sScriptMgr->OnGivePlayerXP(m_Player, gain, victim);

    if (!gain)
        return;

    // Calculate total experience gains (+ RAF or Rested Bonus)
    bool recruitAFriend = m_Player->GetsRecruitAFriendBonus(true);
    uint32 bonus = GetBonusExp(victim, gain, recruitAFriend);
    uint32 totalGain = gain + bonus;

    uint32 nextLvlExp = m_Player->GetUInt32Value(PLAYER_NEXT_LEVEL_XP);
    uint32 curExp = m_Player->GetUInt32Value(PLAYER_XP);
    uint32 newExp = curExp + totalGain;

    // Award levels if experience milestones are met
    uint8 level = m_Player->getLevel();
    bool newLevel = false;
    while (newExp >= nextLvlExp && level < sWorld->getIntConfig(CONFIG_MAX_PLAYER_LEVEL))
    {
        newExp -= nextLvlExp;

        m_Player->GiveLevel(level + 1);

        if (!newLevel)
            newLevel = true;

        level = m_Player->getLevel();
        nextLvlExp = m_Player->GetUInt32Value(PLAYER_NEXT_LEVEL_XP);
    }

    // Set new experience
    m_Player->SetUInt32Value(PLAYER_XP, newExp);

    // Log gains and send information to client
    _Log(type, totalGain, newLevel ? newExp : totalGain, newLevel);
    m_Player->SendLogXPGain(gain, victim, bonus, recruitAFriend, group_rate);
}

uint32 Experience::GetBonusExp(Unit* victim, uint32 gain, bool recruitAFriend)
{
    uint32 bonus = 0;

    // RaF does NOT stack with rested experience
    if (recruitAFriend)
        bonus = 2 * gain; // gain + bonus must add up to 3 * gain for RaF; calculation for quests done client-side
    else
        bonus = victim ? m_Player->GetXPRestBonus(gain) : 0; // XP resting bonus

    return bonus;
}

void Experience::FillLevelingRewards(int32& votePoints, int32& /*donationPoints*/)
{
    if (!m_CanGetLevelingRewards)
        return;

    float avgRate = GetAverageRate();
    votePoints = (m_Player->getLevel() / 3) * (avgRate > (sWorld->getRate(RATE_XP_MAX_MULT) + 0.1f) ? 0 : pow(std::max(avgRate, 1.0f), -0.667f));
}

void Experience::_Log(ExperienceType type, uint32 totalGain, uint32 levelGain, bool newLevel)
{
    m_TotalExp += totalGain;

    float baseGain = float(totalGain) / (m_TempRate == 0.0f ? m_Rate : m_TempRate);
    m_BaseTotalExp += baseGain;

    // Reset level experience if new levels were gained
    if (newLevel)
    {
        m_LevelExp = 0;
        m_BaseLevelExp = 0.0f;
    }

    m_LevelExp += levelGain;
    m_BaseLevelExp += float(levelGain) / (m_TempRate == 0.0f ? m_Rate : m_TempRate);

    switch (type)
    {
        case EXPERIENCE_TYPE_KILL:
        {
            m_UnitsKilled++;
            m_KillsExp += totalGain;
            m_BaseKillsExp += baseGain;
            break;
        }
        case EXPERIENCE_TYPE_QUEST:
        {
            m_QuestsCompleted++;
            m_QuestsExp += totalGain;
            m_BaseQuestsExp += baseGain;
            break;
        }
        case EXPERIENCE_TYPE_EXPLORE:
        {
            m_AreasExplored++;
            m_ExploredExp += totalGain;
            m_BaseExploredExp += baseGain;
            break;
        }
        default:
            break;
    }

    // Flag values to be written to database
    if (!m_Updated)
        m_Updated = true;
}

void Experience::_SaveData(SQLTransaction& trans)
{
    uint8 idx = 0;

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_REP_EXP_DATA);
    stmt->setUInt32 (idx++, m_Player->GetGUIDLow());
    stmt->setFloat  (idx++, m_Rate);
    stmt->setUInt32 (idx++, m_TotalExp);
    stmt->setFloat  (idx++, m_BaseTotalExp);
    stmt->setUInt32 (idx++, m_LevelExp);
    stmt->setFloat  (idx++, m_BaseLevelExp);
    stmt->setUInt32 (idx++, m_UnitsKilled);
    stmt->setUInt32 (idx++, m_KillsExp);
    stmt->setFloat  (idx++, m_BaseKillsExp);
    stmt->setUInt32 (idx++, m_QuestsCompleted);
    stmt->setUInt32 (idx++, m_QuestsExp);
    stmt->setFloat  (idx++, m_BaseQuestsExp);
    stmt->setUInt32 (idx++, m_AreasExplored);
    stmt->setUInt32 (idx++, m_ExploredExp);
    stmt->setFloat  (idx, m_BaseExploredExp);

    trans->Append(stmt);
}