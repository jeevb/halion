/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_EXPERIENCE_H
#define CG_EXPERIENCE_H

class Player;

enum ExperienceType
{
    EXPERIENCE_TYPE_NULL,
    EXPERIENCE_TYPE_KILL,
    EXPERIENCE_TYPE_QUEST,
    EXPERIENCE_TYPE_EXPLORE,

    MAX_EXPERIENCE_TYPE
};

class Experience
{
    public:

        Experience(Player* player);
        ~Experience() { }

        void LoadDataFromFields(Field* fields);
        void SaveToDB(SQLTransaction& trans);

        // Experience Rate
        float GetRate() const { return m_Rate; }
        void SetRate(float rate);
        void SetTempRate(float rate = 0.0f) { m_TempRate = rate; }

        // Experience Statistics
        uint32 GetTotalExperience() const { return m_TotalExp; }
        float GetAverageRate() const { return m_BaseTotalExp > 0.0f ? (m_TotalExp / m_BaseTotalExp) : 0.0f; }

        uint32 GetLevelExperience() const { return m_LevelExp; }
        float GetLevelRate() const { return m_BaseLevelExp > 0.0f ? (m_LevelExp / m_BaseLevelExp) : 0.0f; }

        uint32 GetUnitsKilled() const { return m_UnitsKilled; }
        uint32 GetKillsExperience() const { return m_KillsExp; }
        float GetKillsRate() const { return m_BaseKillsExp > 0.0f ? (m_KillsExp / m_BaseKillsExp) : 0.0f; }

        uint32 GetQuestsCompleted() const { return m_QuestsCompleted; }
        uint32 GetQuestsExperience() const { return m_QuestsExp; }
        float GetQuestsRate() const { return m_BaseQuestsExp > 0.0f ? (m_QuestsExp / m_BaseQuestsExp) : 0.0f; }

        uint32 GetAreasExplored() const { return m_AreasExplored; }
        uint32 GetExploredExperience() const { return m_ExploredExp; }
        float GetExploredRate() const { return m_BaseExploredExp > 0.0f ? (m_ExploredExp / m_BaseExploredExp) : 0.0f; }

        // Experience Gains
        void Add(uint32 gain, Unit* victim, ExperienceType type, float group_rate);
        uint32 GetBonusExp(Unit* victim, uint32 gain, bool recruitAFriend);

        // Leveling Rewards
        void SetCanGetLevelingRewards(bool apply = true) { m_CanGetLevelingRewards = apply; }
        bool CanGetLevelingRewards() const { return m_CanGetLevelingRewards; }
        void FillLevelingRewards(int32& votePoints, int32& donationPoints);

    private:

        // Log values
        void _Log(ExperienceType type, uint32 totalGain, uint32 levelGain, bool newLevel);

        // Write values to database
        void _SaveData(SQLTransaction& trans);

        Player* m_Player;

        float m_Rate;
        float m_TempRate;

        uint32 m_TotalExp;
        float m_BaseTotalExp;

        uint32 m_LevelExp;
        float m_BaseLevelExp;

        uint32 m_UnitsKilled;
        uint32 m_KillsExp;
        float m_BaseKillsExp;

        uint32 m_QuestsCompleted;
        uint32 m_QuestsExp;
        float m_BaseQuestsExp;

        uint32 m_AreasExplored;
        uint32 m_ExploredExp;
        float m_BaseExploredExp;

        // Flag to indicate that values have been updated and need to
        // be written to the database
        bool m_Updated;

        // Flag to determine if player can receive leveling rewards
        bool m_CanGetLevelingRewards;
};

#endif