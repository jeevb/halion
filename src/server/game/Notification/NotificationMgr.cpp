/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "Chat.h"
#include "NotificationMgr.h"
#include "Player.h"
#include "World.h"
#include "WorldPacket.h"

NotificationMgr::NotificationMgr()
{
}

void NotificationMgr::FlashMessage(Player* player, uint32 entry, ...)
{
    const char* format = player->GetSession()->GetTrinityString(entry);
    va_list args;
    char str [MAX_QUERY_LEN];
    va_start(args, entry);
    vsnprintf(str, MAX_QUERY_LEN, format, args);
    va_end(args);
    FlashMessage(player, str);
}

void NotificationMgr::FlashMessage(Player* player, std::string const& text)
{
    if (!player || text.empty())
        return;

    WorldPacket data;
    ChatHandler::BuildChatPacket(data, CHAT_MSG_RAID_BOSS_WHISPER,
                                 LANG_UNIVERSAL, player->GetGUID(),
                                 player->GetGUID(), text, 0);
    player->SendDirectMessage(&data);
}

void NotificationMgr::PvPMessage(Player* player, PvPMsgType type, const char* format, ...)
{
    va_list args;
    va_start(args, format);
    char msg[MAX_QUERY_LEN];
    vsnprintf(msg, MAX_QUERY_LEN, format, args);

    std::string msg2 = "|cfff2583e[PvP]:|r ";
    msg2 += msg;

    WorldPacket data;

    switch (type)
    {
        case PVP_MSG_SELF:
        {
            ChatHandler::BuildChatPacket(data, CHAT_MSG_SYSTEM, LANG_UNIVERSAL, NULL, NULL, msg2.c_str());
            player->GetSession()->SendPacket(&data);
            break;
        }
        case PVP_MSG_AREA:
        {
            ChatHandler::BuildChatPacket(data, CHAT_MSG_SYSTEM, LANG_UNIVERSAL, NULL, NULL, msg2.c_str());
            player->SendMessageToSet(&data, true);
            break;
        }
        case PVP_MSG_AREA_EXCEPT_SELF:
        {
            ChatHandler::BuildChatPacket(data, CHAT_MSG_SYSTEM, LANG_UNIVERSAL, NULL, NULL, msg2.c_str());
            player->SendMessageToSet(&data, false);
            break;
        }
        case PVP_MSG_WORLD:
        {
            ChatHandler::BuildChatPacket(data, CHAT_MSG_SYSTEM, LANG_UNIVERSAL, NULL, NULL, msg2.c_str());
            sWorld->SendGlobalMessage(&data);
            break;
        }
        case PVP_MSG_WORLD_EXCEPT_SELF:
        {
            ChatHandler::BuildChatPacket(data, CHAT_MSG_SYSTEM, LANG_UNIVERSAL, NULL, NULL, msg2.c_str());
            sWorld->SendGlobalMessage(&data, player->GetSession());
            break;
        }
        case PVP_MSG_SELF_RAID_WARN:
        {
            ChatHandler::BuildChatPacket(data, CHAT_MSG_RAID_WARNING, LANG_UNIVERSAL, NULL, NULL, msg2.c_str());
            player->GetSession()->SendPacket(&data);
            break;
        }
        case PVP_MSG_WORLD_RAID_WARN:
        {
            ChatHandler::BuildChatPacket(data, CHAT_MSG_RAID_WARNING, LANG_UNIVERSAL, NULL, NULL, msg2.c_str());
            sWorld->SendGlobalMessage(&data);
            break;
        }
        case PVP_MSG_WORLD_RAID_WARN_EXCEPT_SELF:
        {
            ChatHandler::BuildChatPacket(data, CHAT_MSG_RAID_WARNING, LANG_UNIVERSAL, NULL, NULL, msg2.c_str());
            sWorld->SendGlobalMessage(&data, player->GetSession());
            break;
        }
    }

    va_end(args);
}