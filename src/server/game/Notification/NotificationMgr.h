/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_NOTIFYMGR_H
#define CG_NOTIFYMGR_H

enum PvPMsgType
{
    PVP_MSG_SELF,
    PVP_MSG_AREA,
    PVP_MSG_AREA_EXCEPT_SELF,
    PVP_MSG_WORLD,
    PVP_MSG_WORLD_EXCEPT_SELF,
    PVP_MSG_SELF_RAID_WARN,
    PVP_MSG_WORLD_RAID_WARN,
    PVP_MSG_WORLD_RAID_WARN_EXCEPT_SELF
};

class NotificationMgr
{
    private:
        NotificationMgr();
        ~NotificationMgr() { }

    public:

        static NotificationMgr* instance()
        {
            static NotificationMgr instance;
            return &instance;
        }

        void Init() { }

        // Player notifications
        void FlashMessage(Player* player, uint32 entry, ...);
        void FlashMessage(Player* player, std::string const& text);
        void PvPMessage(Player* player, PvPMsgType type, const char* format, ...);
};

#define sNotify NotificationMgr::instance()

#endif