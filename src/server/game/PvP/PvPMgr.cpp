/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "PvPMgr.h"
#include "SpellAuras.h"

PvPMgr::PvPMgr()
{
}

void PvPMgr::SetNoPvP(Player* player)
{
    // Cast immunity aura
    player->CastSpell(player, SPELL_BLUE_DRAGON_IMMUNITY, true);

    // Set correct duration
    if (Aura* aur = player->GetAura(SPELL_BLUE_DRAGON_IMMUNITY, player->GetGUID()))
        aur->SetDuration(IMMUNITY_TIMER);
}

void PvPMgr::RemoveNoPvP(Player* player)
{
    player->RemoveAura(SPELL_BLUE_DRAGON_IMMUNITY, player->GetGUID());
}

bool PvPMgr::HasNoPvP(Player const* player) const
{
    return player->HasAura(SPELL_BLUE_DRAGON_IMMUNITY, player->GetGUID());
}

bool PvPMgr::HasNoPvP(Unit const* unit) const
{
    Player const* player = unit->GetAffectingPlayer();
    return player ? HasNoPvP(player) : false;
}

bool PvPMgr::CanPvPAssist(Player const* player, Player const* target) const
{
    // Non-PvP case
    if (!player || !target || player == target)
        return true;

    // Either party is disallowed from participating in PvP
    if (HasNoPvP(player) || HasNoPvP(target))
        return false;

    // Cross-faction case outside of dungeons
    if (!player->GetMap()->IsDungeon() && !player->IsInSameTeamAs(target))
        return false;

    return true;
}

bool PvPMgr::CanPvPAssist(Unit const* unit, Unit const* target) const
{
    return CanPvPAssist(unit->GetAffectingPlayer(), target->GetAffectingPlayer());
}

bool PvPMgr::CanPvPAttack(Player const* player, Player const* target) const
{
    // Non-PvP case
    if (!player || !target)
        return true;

    // Either party is disallowed from participating in PvP
    if (HasNoPvP(player) || HasNoPvP(target))
        return false;

    return true;
}

bool PvPMgr::CanPvPAttack(Unit const* unit, Unit const* target) const
{
    return CanPvPAttack(unit->GetAffectingPlayer(), target->GetAffectingPlayer());
}