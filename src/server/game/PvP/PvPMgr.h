/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_PVPMGR_H
#define CG_PVPMGR_H

enum PvPEnums
{
    SPELL_BLUE_DRAGON_IMMUNITY  = 18747,
    IMMUNITY_TIMER              = 1 * HOUR * IN_MILLISECONDS
};

class PvPMgr
{
    private:
        PvPMgr();
        ~PvPMgr() { }

    public:

        static PvPMgr* instance()
        {
            static PvPMgr instance;
            return &instance;
        }

        void Init();

        void SetNoPvP(Player* player);
        void RemoveNoPvP(Player* player);

        bool HasNoPvP(Player const* player) const;
        bool HasNoPvP(Unit const* unit) const;

        bool CanPvPAssist(Player const* player, Player const* target) const;
        bool CanPvPAssist(Unit const* unit, Unit const* target) const;

        bool CanPvPAttack(Player const* player, Player const* target) const;
        bool CanPvPAttack(Unit const* unit, Unit const* target) const;
};

#define sPvPMgr PvPMgr::instance()

#endif