/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "PvPMgr.h"
#include "ScriptMgr.h"
#include "SpellScript.h"

// 18747 - Blue Dragon Immunity
class PvPImmunityFlag : public SpellScriptLoader
{
    public:

        PvPImmunityFlag() : SpellScriptLoader("pvp_immunity_flag") { }

        class PvPImmunityFlag_AuraScript : public AuraScript
        {
            PrepareAuraScript(PvPImmunityFlag_AuraScript);

            bool Validate(SpellInfo const* /*spell*/) override
            {
                if (!sSpellMgr->GetSpellInfo(SPELL_BLUE_DRAGON_IMMUNITY))
                    return false;

                return true;
            }

            void OnAuraStateChanged(AuraEffect const* /*aurEff*/, AuraEffectHandleModes /*mode*/)
            {
                if (Unit* target = GetTarget())
                    if (Player* player = target->GetAffectingPlayer())
                    {
                        uint32 newZone, newArea, newSector;
                        player->GetZoneAreaAndSectorId(newZone, newArea, newSector);
                        player->UpdateZone(newZone, newArea, newSector);
                    }
            }

            void Register() override
            {
                 AfterEffectApply += AuraEffectApplyFn(PvPImmunityFlag_AuraScript::OnAuraStateChanged, EFFECT_0, SPELL_AURA_MECHANIC_IMMUNITY_MASK, AURA_EFFECT_HANDLE_REAL);
                 AfterEffectRemove += AuraEffectRemoveFn(PvPImmunityFlag_AuraScript::OnAuraStateChanged, EFFECT_0, SPELL_AURA_MECHANIC_IMMUNITY_MASK, AURA_EFFECT_HANDLE_REAL);
            }
        };

        AuraScript* GetAuraScript() const override
        {
            return new PvPImmunityFlag_AuraScript();
        }
};

void PvPMgr::Init()
{
    new PvPImmunityFlag();
}