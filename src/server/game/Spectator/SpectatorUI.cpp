/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include <boost/algorithm/string/join.hpp>

#include "ArenaTeamMgr.h"
#include "BattlegroundMgr.h"
#include "GossipMgr.h"
#include "Language.h"
#include "Player.h"
#include "ScriptedGossip.h"
#include "SpectatorMgr.h"
#include "SpectatorUI.h"
#include "WorldSession.h"

SpectatorUI::SpectatorUI() : CreatureScript(SPECTATOR_UI)
{
}

bool SpectatorUI::OnGossipHello(Player* player, Creature* creature)
{
    player->CLEAR_GOSSIP_MENU();

    if (player->IsInCombat())
    {
        player->GetSession()->SendNotification(LANG_YOU_IN_COMBAT);
        return true;
    }

    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GOSSIP_ICON_TEXT("Spectate 2v2 games", "Icons/achievement_arena_2v2_7"), 0, SPECTATE_ARENA_GAMES_2V2);
    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GOSSIP_ICON_TEXT("Spectate 3v3 games", "Icons/achievement_arena_3v3_7"), 0, SPECTATE_ARENA_GAMES_3V3);
    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD, GOSSIP_ICON_TEXT("Spectate 5v5 games", "Icons/achievement_arena_5v5_7"), 0, SPECTATE_ARENA_GAMES_5V5);
    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD, GOSSIP_ICON_TEXT("Spectate a player", "Icons/achievement_featsofstrength_gladiator_05"),
                                     0, SPECTATE_PLAYER, "", 0, true);
    player->SEND_GOSSIP_MENU(SPECTATE_GOSSIP_MESSAGE_MAIN, creature->GetGUID());
    return true;
}

bool SpectatorUI::OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
{
    player->CLEAR_GOSSIP_MENU();
    player->CLOSE_GOSSIP_MENU();

    switch (action)
    {
        case SPECTATE_ARENA_GAMES_2V2:
        case SPECTATE_ARENA_GAMES_3V3:
        case SPECTATE_ARENA_GAMES_5V5:
            return ShowGames(player, creature, action);
        case SPECTATE_SEND_TO_SPECTATE:
        {
            Player* target = sObjectMgr->GetPlayerByLowGUID(sender);
            if (sSpectate->SendSpectatorToPlayer(player, target))
                return true;

            break;
        }
        default:
            break;
    }

    return OnGossipHello(player, creature);
}

bool SpectatorUI::OnGossipSelectCode(Player* player, Creature* creature, uint32 /*sender*/, uint32 action, const char* code)
{
    player->CLEAR_GOSSIP_MENU();
    player->CLOSE_GOSSIP_MENU();

    switch (action)
    {
        case SPECTATE_PLAYER:
        {
            Player* target = sObjectAccessor->FindPlayerByName(std::string(code));
            if (sSpectate->SendSpectatorToPlayer(player, target))
                return true;

            break;
        }
        default:
            break;
    }

    return OnGossipHello(player, creature);
}

bool SpectatorUI::ShowGames(Player* player, Creature* creature, uint32 action)
{
    uint8 gamesCount = 0;
    for (uint32 i = BATTLEGROUND_TYPE_NONE; i < MAX_BATTLEGROUND_TYPE_ID; ++i)
    {
        if (!sBattlegroundMgr->IsArenaType(BattlegroundTypeId(i)))
            continue;

        BattlegroundContainer bgs = sBattlegroundMgr->GetBattlegroundsByType(BattlegroundTypeId(i));
        for (BattlegroundContainer::iterator itr = bgs.begin();
             gamesCount < MAX_GAMES_PER_CATEGORY && itr != bgs.end(); ++itr)
        {
            Battleground* arena = itr->second;

            if (arena->GetArenaType() == GetArenaTypeForAction(action) &&
                arena->GetPlayersSize())
            {
                bool valid = arena->GetStatus() == STATUS_IN_PROGRESS;
                player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_BATTLE,
                                                 GetGameString(arena, valid),
                                                 valid ? GetFirstPlayerGuid(arena) : 0,
                                                 valid ? SPECTATE_SEND_TO_SPECTATE : SpectateOption(action),
                                                 valid ? "" : "This game has not yet commenced. Please refresh the page and try again.",
                                                 0,
                                                 false);
                gamesCount++;
            }
        }
    }

    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Refresh this page.", 0, action);
    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_CHAT, "Return to the main menu.", 0, SPECTATE_MAIN_MENU);
    player->SEND_GOSSIP_MENU(gamesCount ? SPECTATE_GOSSIP_MESSAGE_GOT_GAMES : SPECTATE_GOSSIP_MESSAGE_NO_GAMES,
                             creature->GetGUID());
    return true;
}

uint32 SpectatorUI::GetFirstPlayerGuid(Battleground* arena) const
{
    for (auto const& itr : arena->GetPlayers())
        if (Player* player = ObjectAccessor::FindPlayer(itr.first))
            if (!player->IsSpectator())
                return player->GetGUIDLow();

    return 0;
}

uint8 SpectatorUI::GetArenaTypeForAction(uint32 action) const
{
    switch (action)
    {
        case SPECTATE_ARENA_GAMES_2V2:   return ARENA_TYPE_2v2;
        case SPECTATE_ARENA_GAMES_3V3:   return ARENA_TYPE_3v3;
        case SPECTATE_ARENA_GAMES_5V5:   return ARENA_TYPE_5v5;
        default:                         return 0;
    }
}

std::string SpectatorUI::GetGameString(Battleground* arena, bool valid) const
{
    std::vector<std::string> teamMembers[BG_TEAMS_COUNT];
    for (auto const& itr : arena->GetPlayers())
    {
        if (Player* player = ObjectAccessor::FindPlayer(itr.first))
        {
            if (player->IsSpectator())
                continue;

            uint32 team = itr.second.Team;
            teamMembers[team == HORDE].push_back(GetSpecName(player) + GetClassName(player));
        }
    }

    // Arena name
    std::ostringstream buf;
    buf << "|cff" << (valid ? "000000" : "7e7e7e")
        << arena->GetName()
        << " (" << (arena->isRated() ? "Rated" : "Skirmish") << ")|r";

    // Show team information only if the match has started
    if (valid)
    {
        for (uint8 i = 0; i < BG_TEAMS_COUNT; ++i)
        {
            buf << "\n" << GetTeamString(arena, i) << "  "
                << boost::algorithm::join(teamMembers[i], ", ");
        }
    }

    return buf.str();
}

std::string SpectatorUI::GetTeamString(Battleground* arena, uint8 teamIdx) const
{
    std::ostringstream buf;
    if (!arena->isRated())
        buf << "|cff0000ff[" << uint32(++teamIdx) << "]|r";
    else if (ArenaTeam* arenaTeam = sArenaTeamMgr->GetArenaTeamById(arena->GetArenaTeamIdByIndex(teamIdx)))
        buf << "|cff0000ff[" << arenaTeam->GetRating() << "]|r";

    return buf.str();
}

std::string SpectatorUI::GetSpecName(Player* player) const
{
    uint32 const* talentTabIds = GetTalentTabPages(player->getClass());

    uint8 mainTalentTabPoints       = 0;
    std::string mainTalentTabName   = "";

    // Get talent information
    for (uint8 i = 0; i < MAX_TALENT_TABS; ++i)
    {
        TalentTabEntry const* talentTabInfo = sTalentTabStore.LookupEntry(talentTabIds[i]);

        if (!talentTabInfo)
            continue;

        uint8 pointsInTab = 0;
        for (uint32 talentId = 0; talentId < sTalentStore.GetNumRows(); ++talentId)
        {
            TalentEntry const* talentInfo = sTalentStore.LookupEntry(talentId);

            if (!talentInfo)
                continue;

            // skip another tab talents
            if (talentInfo->TalentTab != talentTabInfo->TalentTabID)
                continue;

            // find points spent on a talent and add to point count for talent tab
            for (int8 rank = MAX_TALENT_RANK-1; rank >= 0; --rank)
            {
                if (talentInfo->RankID[rank] && player->HasTalent(talentInfo->RankID[rank], player->GetActiveSpec()))
                {
                    pointsInTab += rank + 1;
                    break;
                }
            }
        }

        if (pointsInTab && pointsInTab > mainTalentTabPoints)
        {
            mainTalentTabPoints = pointsInTab;
            mainTalentTabName   = talentTabInfo->name[0];
        }
    }

    if      (mainTalentTabName == "Arms")           return "Arms ";
    else if (mainTalentTabName == "Fury")           return "Fury ";
    else if (mainTalentTabName == "Protection")     return "Prot ";
    else if (mainTalentTabName == "Affliction")     return "Aff ";
    else if (mainTalentTabName == "Demonology")     return "Demo ";
    else if (mainTalentTabName == "Destruction")    return "Destro ";
    else if (mainTalentTabName == "Elemental")      return "Ele ";
    else if (mainTalentTabName == "Enhancement")    return "Enh ";
    else if (mainTalentTabName == "Restoration")    return "R";
    else if (mainTalentTabName == "Assassination")  return "Assa ";
    else if (mainTalentTabName == "Combat")         return "Comb ";
    else if (mainTalentTabName == "Subtlety")       return "Sub ";
    else if (mainTalentTabName == "Discipline")     return "Disc ";
    else if (mainTalentTabName == "Holy")           return "H";
    else if (mainTalentTabName == "Shadow")         return "S";
    else if (mainTalentTabName == "Retribution")    return "Ret ";
    else if (mainTalentTabName == "Arcane")         return "Arc ";
    else if (mainTalentTabName == "Fire")           return "Fire ";
    else if (mainTalentTabName == "Frost")          return "Frost ";
    else if (mainTalentTabName == "Beast Mastery")  return "BM ";
    else if (mainTalentTabName == "Marksmanship")   return "MM ";
    else if (mainTalentTabName == "Survival")       return "Surv ";
    else if (mainTalentTabName == "Balance")        return "Bal ";
    else if (mainTalentTabName == "Feral Combat")   return "Feral ";
    else if (mainTalentTabName == "Blood")          return "Blood ";
    else if (mainTalentTabName == "Unholy")         return "UH ";

    return "Uns. ";
}

std::string SpectatorUI::GetClassName(Player* player) const
{
    switch (player->getClass())
    {
        case CLASS_WARRIOR:         return "Warr";      break;
        case CLASS_PALADIN:         return "Pal";       break;
        case CLASS_HUNTER:          return "Hunter";    break;
        case CLASS_ROGUE:           return "Rogue";     break;
        case CLASS_PRIEST:          return "Priest";    break;
        case CLASS_DEATH_KNIGHT:    return "DK";        break;
        case CLASS_SHAMAN:          return "Sham";      break;
        case CLASS_MAGE:            return "Mage";      break;
        case CLASS_WARLOCK:         return "Lock";      break;
        case CLASS_DRUID:           return "Druid";     break;
        default:                    return "Unk";       break;
    }
}