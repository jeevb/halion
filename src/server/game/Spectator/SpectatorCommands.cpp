/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "Player.h"
#include "SpectatorCommands.h"
#include "SpectatorMgr.h"

SpectatorCommands::SpectatorCommands() : CommandScript("spectator_commands")
{
}

ChatCommand* SpectatorCommands::GetCommands() const
{
    static ChatCommand spectateCommandTable[] =
    {
        { "player", rbac::RBAC_PERM_COMMAND_SPECTATE_PLAYER,    false,  &HandleSpectateCommand,         "", NULL },
        { "leave",  rbac::RBAC_PERM_COMMAND_SPECTATE_LEAVE,     false,  &HandleSpectateLeaveCommand,    "", NULL },
        { NULL,     0,                                          false,  NULL,                           "", NULL }
    };

    static ChatCommand commandTable[] =
    {
        { "spectate",   rbac::RBAC_PERM_COMMAND_SPECTATE,   false,  NULL,   "", spectateCommandTable },
        { NULL,         0,                                  false,  NULL,   "", NULL }
    };

    return commandTable;
}

bool SpectatorCommands::HandleSpectateCommand(ChatHandler* handler, const char* args)
{
    Player* target = sObjectAccessor->FindPlayerByName(std::string(args));
    Player* player = handler->GetSession()->GetPlayer();

    if (!sSpectate->SendSpectatorToPlayer(player, target))
    {
        handler->SetSentErrorMessage(true);
        return false;
    }

    return true;
}

bool SpectatorCommands::HandleSpectateLeaveCommand(ChatHandler* handler, const char* /*args*/)
{
    Player* player = handler->GetSession()->GetPlayer();

    if (!player->IsSpectator())
    {
        handler->SendSysMessage("You are not a spectator.");
        handler->SetSentErrorMessage(true);
        return false;
    }

    player->TeleportToBGEntryPoint();
    return true;
 }