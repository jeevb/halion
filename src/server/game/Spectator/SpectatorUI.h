/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_SPECTATEUI_H
#define CG_SPECTATEUI_H

#include "ScriptMgr.h"

#define MAX_GAMES_PER_CATEGORY 20

enum SpectateOption
{
    SPECTATE_MAIN_MENU,
    SPECTATE_ARENA_GAMES_2V2,
    SPECTATE_ARENA_GAMES_3V3,
    SPECTATE_ARENA_GAMES_5V5,
    SPECTATE_PLAYER,
    SPECTATE_SEND_TO_SPECTATE
};

enum SpectateGossipMessage
{
    SPECTATE_GOSSIP_MESSAGE_MAIN        = 1000006,
    SPECTATE_GOSSIP_MESSAGE_GOT_GAMES   = 1000007,
    SPECTATE_GOSSIP_MESSAGE_NO_GAMES    = 1000008
};

class SpectatorUI : public CreatureScript
{
    public:

        SpectatorUI();

        bool OnGossipHello(Player* player, Creature* creature) override;
        bool OnGossipSelect(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction) override;
        bool OnGossipSelectCode(Player* player, Creature* creature, uint32 uiSender, uint32 uiAction, const char* code) override;

        bool ShowGames(Player* player, Creature* creature, uint32 action);

        uint32 GetFirstPlayerGuid(Battleground* arena) const;
        uint8 GetArenaTypeForAction(uint32 action) const;
        std::string GetGameString(Battleground* arena, bool valid) const;
        std::string GetTeamString(Battleground* arena, uint8 teamIdx) const;
        std::string GetSpecName(Player* player) const;
        std::string GetClassName(Player* player) const;
};

#endif