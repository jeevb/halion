/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_SPECTATEMGR_H
#define CG_SPECTATEMGR_H

#define SPECTATOR_UI "spectator_ui"

enum SpectatorEnums
{
    FACTION_NEUTRAL     = 35,
    SPELL_GHOST_VISUAL  = 22650
};

class SpectatorMgr
{
    private:
        SpectatorMgr();
        ~SpectatorMgr() { }

    public:

        static SpectatorMgr* instance()
        {
            static SpectatorMgr instance;
            return &instance;
        }

        void Init() { }

        void SetSpectator(Player* player, bool apply = true);
        bool SendSpectatorToPlayer(Player* player, Player* target);
};

#define sSpectate SpectatorMgr::instance()

#endif