/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "ArenaTeamMgr.h"
#include "Battleground.h"
#include "Language.h"
#include "Player.h"
#include "SpectatorCommands.h"
#include "SpectatorMgr.h"
#include "SpectatorUI.h"
#include "SpellAuraEffects.h"
#include "World.h"

SpectatorMgr::SpectatorMgr()
{
    // Instantiate the Spectator Commands script
    new SpectatorCommands();

    // Instantiate the Spectator UI script
    new SpectatorUI();
}

void SpectatorMgr::SetSpectator(Player* player, bool apply /*= true*/)
{
    if (apply)
    {
        // remember current position as entry point for return at bg end teleportation
        if (!player->GetMap()->IsBattlegroundOrArena())
            player->SetBattlegroundEntryPoint();

        player->SaveRecallPosition();

        player->getHostileRefManager().setOnlineOfflineState(false);
        player->CombatStopWithPets();
        player->setFaction(FACTION_NEUTRAL);

        player->ResetContestedPvP();

        if (player->IsMounted()) // Necessary to "expose" the pets. Pets may not be removed if player is mounted.
            player->RemoveAurasByType(SPELL_AURA_MOUNTED);

        // Remove pets and totems, they show up beside you visible to the arena spectators.
        player->RemoveAllControlled();
        player->UnsummonAllTotems();

        player->SetSpeed(MOVE_RUN, 3.0);

        // Ghost Visual to emulate actual Spectator Mode
        player->CastSpell(player, SPELL_GHOST_VISUAL, true);
    }
    else
    {
        // Remove Ghost Visual
        player->RemoveAurasDueToSpell(SPELL_GHOST_VISUAL, player->GetGUID());

        uint32 newPhase = 0;
        Unit::AuraEffectList const& phases = player->GetAuraEffectsByType(SPELL_AURA_PHASE);
        if (!phases.empty())
            for (Unit::AuraEffectList::const_iterator itr = phases.begin(); itr != phases.end(); ++itr)
                 newPhase |= (*itr)->GetMiscValue();

        if (!newPhase)
            newPhase = PHASEMASK_NORMAL;

        player->SetPhaseMask(newPhase, false);
        player->UpdateSpeed(MOVE_RUN, true);

        player->setFactionForRace(player->getRace());
        player->getHostileRefManager().setOnlineOfflineState(true);
    }

    player->SetSpectator(apply);
    player->UpdateObjectVisibility();
}

bool SpectatorMgr::SendSpectatorToPlayer(Player* player, Player* target)
{
    ChatHandler handler(player->GetSession());

    if (!target)
    {
        handler.SendSysMessage("The specified player could not be found or may be offline.");
        return false;
    }

    if (target == player)
    {
        handler.SendSysMessage("You may not teleport to yourself.");
        return false;
    }

    if (player->IsInCombat())
    {
        handler.SendSysMessage(LANG_YOU_IN_COMBAT);
        return false;
    }

    if (!target)
    {
        handler.SendSysMessage("The specified player could not be found or may be offline.");
        return false;
    }

    if (!player->IsSpectator() && player->GetMap()->IsBattlegroundOrArena())
    {
        handler.SendSysMessage("You may not do that while in a battleground or arena.");
        return false;
    }

    for (uint8 i = BATTLEGROUND_QUEUE_2v2; i < MAX_BATTLEGROUND_QUEUE_TYPES; ++i)
    {
        if (player->InBattlegroundQueueForBattlegroundQueueType(BattlegroundQueueTypeId(i)))
        {
            handler.SendSysMessage("You may not do that while in an arena queue.");
            return false;
        }
    }

    if (!target->GetMap()->IsBattleArena())
    {
        handler.SendSysMessage("The specified player was not found in an arena match.");
        return false;
    }

    if (target->IsSpectator())
    {
        handler.SendSysMessage("You may not spectate other spectators.");
        return false;
    }

    if (player->IsInFlight())
    {
        handler.SendSysMessage("You may not spectate while in flight.");
        return false;
    }

    // search for two teams
    Battleground* bg = target->GetBattleground();
    if (!bg)
    {
        handler.SendSysMessage("An unexpected error has occurred! Please try again.");
        return false;
    }

    if (bg->GetStatus() != STATUS_IN_PROGRESS)
    {
        handler.SendSysMessage("The specified player was not found in an ongoing arena match.");
        return false;
    }

    // Send a instructive message
    handler.SendSysMessage("You are in Spectator Mode. To abandon this battle, use the following command: .spectate leave");

    if (bg->isRated())
    {
        uint32 allianceId = bg->GetArenaTeamIdForTeam(ALLIANCE);
        uint32 hordeId = bg->GetArenaTeamIdForTeam(HORDE);

        if (allianceId && hordeId)
        {
            ArenaTeam* alliance = sArenaTeamMgr->GetArenaTeamById(allianceId);
            ArenaTeam* horde = sArenaTeamMgr->GetArenaTeamById(hordeId);

            if (alliance && horde)
                handler.PSendSysMessage("You are spectating a rated arena match between %s (%u, MMR: %u) and %s (%u, MMR: %u).",
                                         alliance->GetName().c_str(), alliance->GetRating(),
                                         alliance->GetAverageMMR(bg->GetGroupForTeamInArena(ALLIANCE)),
                                         horde->GetName().c_str(), horde->GetRating(),
                                         horde->GetAverageMMR(bg->GetGroupForTeamInArena(HORDE)));
        }
    }
    else
        handler.SendSysMessage("You are spectating a skirmish match.");

    // all is well, set bg id
    // when porting out from the bg, it will be reset to 0
    player->SetBattlegroundId(target->GetBattlegroundId(), target->GetBattlegroundTypeId());

    SetSpectator(player);

    // to point to see at target with same orientation
    float x, y, z;
    target->GetContactPoint(player, x, y, z);

    player->TeleportTo(target->GetMapId(), x, y, z, player->GetAngle(target), TELE_TO_GM_MODE);
    player->SetPhaseMask(target->GetPhaseMask(), true);

    return true;
}