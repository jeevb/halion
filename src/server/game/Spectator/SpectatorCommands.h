/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_SPECTATECMD_H
#define CG_SPECTATECMD_H

#include "Chat.h"
#include "ScriptMgr.h"

class SpectatorCommands : public CommandScript
{
    public:

        SpectatorCommands();

        ChatCommand* GetCommands() const;

        static bool HandleSpectateCommand(ChatHandler* handler, const char* args);
        static bool HandleSpectateLeaveCommand(ChatHandler* handler, const char* /*args*/);
};

#endif