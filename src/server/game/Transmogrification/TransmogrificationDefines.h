/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_TMOGDEFINES_H
#define CG_TMOGDEFINES_H

#define TRANSMOGRIFICATION_UI                   "transmogrification_ui"
#define TRANSMOGRIFICATION_NORMAL_VENDOR_ID     10000000
#define TRANSMOGRIFICATION_PVP_VENDOR_ID        10000001

enum TransmogrificationSlot
{
    TRANSMOG_SLOT_HEAD,
    TRANSMOG_SLOT_SHOULDERS,
    TRANSMOG_SLOT_BACK,
    TRANSMOG_SLOT_CHEST,
    TRANSMOG_SLOT_SHIRT,
    TRANSMOG_SLOT_TABARD,
    TRANSMOG_SLOT_WRIST,
    TRANSMOG_SLOT_HANDS,
    TRANSMOG_SLOT_WAIST,
    TRANSMOG_SLOT_LEGS,
    TRANSMOG_SLOT_FEET,
    TRANSMOG_SLOT_MAINHAND,
    TRANSMOG_SLOT_OFFHAND,
    TRANSMOG_SLOT_RANGED,

    MAX_TRANSMOG_SLOT
};

enum TransmogrificationResult
{
    TRANSMOG_ERR_OK,
    TRANSMOG_ERR_REVERT_OK,
    TRANSMOG_ERR_INVALID_SLOT,
    TRANSMOG_ERR_MISSING_SRC_ITEM,
    TRANSMOG_ERR_MISSING_DEST_ITEM,
    TRANSMOG_ERR_INVALID_SRC_ITEM,
    TRANSMOG_ERR_INVALID_ITEMS,
    TRANSMOG_ERR_INVALID_SEARCH,
    TRANSMOG_ERR_NO_MATCH,
    TRANSMOG_ERR_INVALID_SET_NAME,
    TRANSMOG_ERR_CANNOT_AFFORD_GOLD_COST,
    TRANSMOG_ERR_CANNOT_AFFORD_CUSTOM_COST
};

struct TransmogrificationSlotInfo
{
    TransmogrificationSlotInfo(EquipmentSlots _eSlot, std::string const& _defaultName, std::string const& _defaultIcon) :
        eSlot(_eSlot), defaultName(_defaultName), defaultIcon(_defaultIcon) { }

    EquipmentSlots  eSlot;
    std::string     defaultName;
    std::string     defaultIcon;
};

const TransmogrificationSlotInfo TransmogrificationSlotInfoStore[MAX_TRANSMOG_SLOT] =
{
    TransmogrificationSlotInfo(EQUIPMENT_SLOT_HEAD,        "Head",         "PaperDoll/UI-PaperDoll-Slot-Head"            ),
    TransmogrificationSlotInfo(EQUIPMENT_SLOT_SHOULDERS,   "Shoulders",    "PaperDoll/UI-PaperDoll-Slot-Shoulder"        ),
    TransmogrificationSlotInfo(EQUIPMENT_SLOT_BACK,        "Back",         "PaperDoll/UI-PaperDoll-Slot-Chest"           ),
    TransmogrificationSlotInfo(EQUIPMENT_SLOT_CHEST,       "Chest",        "PaperDoll/UI-PaperDoll-Slot-Chest"           ),
    TransmogrificationSlotInfo(EQUIPMENT_SLOT_BODY,        "Shirt",        "PaperDoll/UI-PaperDoll-Slot-Shirt"           ),
    TransmogrificationSlotInfo(EQUIPMENT_SLOT_TABARD,      "Tabard",       "PaperDoll/UI-PaperDoll-Slot-Tabard"          ),
    TransmogrificationSlotInfo(EQUIPMENT_SLOT_WRISTS,      "Wrists",       "PaperDoll/UI-PaperDoll-Slot-Wrists"          ),
    TransmogrificationSlotInfo(EQUIPMENT_SLOT_HANDS,       "Hands",        "PaperDoll/UI-PaperDoll-Slot-Hands"           ),
    TransmogrificationSlotInfo(EQUIPMENT_SLOT_WAIST,       "Waist",        "PaperDoll/UI-PaperDoll-Slot-Waist"           ),
    TransmogrificationSlotInfo(EQUIPMENT_SLOT_LEGS,        "Legs",         "PaperDoll/UI-PaperDoll-Slot-Legs"            ),
    TransmogrificationSlotInfo(EQUIPMENT_SLOT_FEET,        "Feet",         "PaperDoll/UI-PaperDoll-Slot-Feet"            ),
    TransmogrificationSlotInfo(EQUIPMENT_SLOT_MAINHAND,    "Main-hand",    "PaperDoll/UI-PaperDoll-Slot-MainHand"        ),
    TransmogrificationSlotInfo(EQUIPMENT_SLOT_OFFHAND,     "Off-hand",     "PaperDoll/UI-PaperDoll-Slot-SecondaryHand"   ),
    TransmogrificationSlotInfo(EQUIPMENT_SLOT_RANGED,      "Ranged",       "PaperDoll/UI-PaperDoll-Slot-Ranged"          )
};

typedef std::map<ObjectGuid, uint32> TransmogrificationMap;
typedef std::set<uint32> TransmogrificationHistory;
typedef std::map<uint32, ObjectGuid> TransmogrificationSkins;
typedef std::map<TransmogrificationSlot, uint32> TransmogrificationSlotMap;

struct TransmogrificationSet
{
    std::string name;
    TransmogrificationSlotMap slotMap;
};

typedef std::map<uint8, TransmogrificationSet> TransmogrificationSets;

#define MAX_TRANSMOG_SETS 10
#define NULL_TRANSMOG_SET_SLOT 255

#define MAX_LINE_CHARS 32
#define MAX_SET_NAME_CHARS 19

#endif