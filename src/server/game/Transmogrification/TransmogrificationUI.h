/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_TMOGUI_H
#define CG_TMOGUI_H

#include "ScriptMgr.h"

enum TransmogAction
{
    TRANSMOG_MAIN_MENU,
    TRANSMOG_IN_SLOT,
    TRANSMOG_VENDOR,
    TRANSMOG_VENDOR_SEARCH,
    TRANSMOG_REVERT,
    TRANSMOG_SETS,
    TRANSMOG_ADD_SET,
    TRANSMOG_ADD_SET_CONFIRM,
    TRANSMOG_MANAGE_SET,
    TRANSMOG_APPLY_SET,
    TRANSMOG_DELETE_SET,
    TRANSMOG_REVERT_ALL,

    TRANSMOG_DEFAULT_SENDER     = 0,
    TRANSMOG_START_BIT          = 8,
    TRANSMOG_START_MASK         = (1 << 8) - 1,
};

enum TransmogGossipMessage
{
    TRANSMOG_GOSSIP_MESSAGE_MAIN    = 1000002,
    TRANSMOG_GOSSIP_MESSAGE_SETS    = 1000003,
    TRANSMOG_GOSSIP_MESSAGE_ADD_SET = 1000004,
};

#define ACTION_WITH_OPT(action, opt) ((opt & TRANSMOG_START_MASK) | (action << TRANSMOG_START_BIT))
#define ACTION(action) ACTION_WITH_OPT(action, 0)

struct TransmogrificationStorage_T
{
    EquipmentSlots eSlot;
    TransmogrificationSkins skins;
    TransmogrificationSlotMap slotMap;
    std::wstring search;
};

typedef std::map<Player*, TransmogrificationStorage_T> TransmogrificationStorage;

class TransmogrificationUI : public CreatureScript
{
    public:

        TransmogrificationUI();

        void OnLogout(Player* player) override;

        bool OnGossipHello(Player* player, Creature* creature) override;
        bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action) override;
        bool OnGossipSelectCode(Player* player, Creature* creature, uint32 sender, uint32 action, const char* code) override;

        VendorItemData const* GetVendorItems(Creature const* creature, Player* player) const override;
        bool ShouldDisplayVendorItem(Player* player, Creature* creature, ItemTemplate const* item_template, VendorItem const* vendor_item) override;
        bool BypassReputationRequirementOnPurchaseItem(Player* player, Creature* creature, ItemTemplate const* item_template, VendorItem const* vendor_item) override;
        bool OnBeforePurchaseItem(Player* player, Creature* creature, ItemTemplate const* item_template, VendorItem const* vendor_item) override;
        void ModifyItemVendorData(Player* player, Creature* creature, ItemTemplate const* item_template, VendorItem const* vendor_item, int32& price, uint32& extendedCost, uint32& leftInStock, uint32& display) override;

    private:

        void _SendTransmogrificationSlotMenu(Player* player, Creature* creature, Item* target, uint32 sender, uint32 action);
        void _SendTransmogrificationSetsMenu(Player* player, Creature* creature);
        void _SendTransmogrificationAddSetMenu(Player* player, Creature* creature);
        void _SendTransmogrificationManageSetMenu(Player* player, Creature* creature, uint8 setId);

        void _AddTransmogSlotMenuItems(Player* player);
        void _AddTransmogrificationSetMenuItems(Player* player);

        TransmogrificationResult _Transmogrify(Player* player, EquipmentSlots targetSlot, ItemTemplate const* item_template);

        bool _NeedShowInventoryList(Player* player, Creature* creature);
        bool _HasMatchingNameOrId(ItemTemplate const* item_template, std::wstring wNamePart) const;

        void _UpdateItemDescription(uint32 itemEntry, std::string& itemName, std::string& iconPath, bool trueColor = false);
        void _UpdateItemDescription(ItemTemplate const* proto, std::string& itemName, std::string& iconPath, bool trueColor = false);
        std::string _GetItemLink(ItemTemplate const* proto, bool trueColor);
        std::string _GetItemIcon(ItemTemplate const* proto);
        std::string _GetRandomSetIcon(TransmogrificationSlotMap const& slotMap);

        TransmogrificationStorage m_Storage;
};

#endif