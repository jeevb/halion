/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include <boost/spirit/include/qi.hpp>

#include "Chat.h"
#include "GossipMgr.h"
#include "Language.h"
#include "Player.h"
#include "ScriptedGossip.h"
#include "TransmogrificationMgr.h"
#include "TransmogrificationUI.h"
#include "WorldSession.h"

TransmogrificationUI::TransmogrificationUI() : CreatureScript(TRANSMOGRIFICATION_UI)
{
}

void TransmogrificationUI::OnLogout(Player* player)
{
    // Clear player data on logout
    m_Storage.erase(player);
}

bool TransmogrificationUI::OnGossipHello(Player* player, Creature* creature)
{
    player->CLEAR_GOSSIP_MENU();

    m_Storage.erase(player);

    if (player->IsInCombat())
    {
        player->GetSession()->SendNotification(LANG_YOU_IN_COMBAT);
        return true;
    }

    _AddTransmogSlotMenuItems(player);

    bool hasTransmogrified = sTransmog->HasEquippedItemWithTransmogrification(player);

    std::ostringstream buf;
    buf << (hasTransmogrified ? "|cff000000" : "|cff7e7e7e")
        << "Revert current transmogrifications" << "|r";
    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD,
                                     GOSSIP_ICON_TEXT(buf.str(), "ICONS/INV_Enchant_Disenchant"),
                                     TRANSMOG_DEFAULT_SENDER,
                                     hasTransmogrified ? ACTION(TRANSMOG_REVERT_ALL) : TRANSMOG_MAIN_MENU,
                                     hasTransmogrified ?
                                        "Are you sure you wish to revert transmogrifications on all currently equipped items?" :
                                        "None of your currently equipped items are transmogrified.",
                                     0,
                                     false);
    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD,
                            GOSSIP_ICON_TEXT("View transmogrification sets", "RAIDFRAME/UI-RAIDFRAME-MAINASSIST"),
                            TRANSMOG_DEFAULT_SENDER,
                            ACTION(TRANSMOG_SETS));

    player->SEND_GOSSIP_MENU(TRANSMOG_GOSSIP_MESSAGE_MAIN, creature->GetGUID());
    return true;
}

bool TransmogrificationUI::OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action)
{
    player->CLEAR_GOSSIP_MENU();
    player->CLOSE_GOSSIP_MENU();

    uint8 transmogAction = action >> TRANSMOG_START_BIT;
    uint8 opt = action & TRANSMOG_START_MASK;

    if (sender == TRANSMOG_DEFAULT_SENDER)
    {
        switch (transmogAction)
        {
            case TRANSMOG_SETS:
            {
                _SendTransmogrificationSetsMenu(player, creature);
                return true;
            }
            case TRANSMOG_ADD_SET:
            {
                _SendTransmogrificationAddSetMenu(player, creature);
                return true;
            }
            case TRANSMOG_MANAGE_SET:
            {
                _SendTransmogrificationManageSetMenu(player, creature, opt);
                return true;
            }
            case TRANSMOG_APPLY_SET:
            {
                sTransmog->ApplyTransmogrificationSet(player, opt);
                break;
            }
            case TRANSMOG_DELETE_SET:
            {
                sTransmog->DeleteTransmogrificationSet(player, opt);
                return OnGossipSelect(player, creature, TRANSMOG_DEFAULT_SENDER, ACTION(TRANSMOG_SETS));
            }
            case TRANSMOG_REVERT_ALL:
            {
                sTransmog->ResetAllEquippedItemDisplays(player);
                sTransmog->SendTransmogrificationResult(player, TRANSMOG_ERR_REVERT_OK);
                break;
            }
            default:
                break;
        }

        return OnGossipHello(player, creature);
    }

    EquipmentSlots eSlot = EquipmentSlots(opt);

    // Sanity checks
    Item* target = player->GetItemByPos(INVENTORY_SLOT_BAG_0, eSlot);
    if (!target || target->GetGUIDLow() != sender)
    {
        sTransmog->SendTransmogrificationResult(player, TRANSMOG_ERR_MISSING_DEST_ITEM);
        return OnGossipHello(player, creature);
    }

    TransmogrificationStorage_T& storage = m_Storage[player];
    storage.eSlot = eSlot;
    storage.search = std::wstring();

    switch (transmogAction)
    {
        case TRANSMOG_REVERT:
        {
            sTransmog->ResetItemDisplay(player, target);
            sTransmog->SendTransmogrificationResult(player, TRANSMOG_ERR_REVERT_OK);
            return OnGossipSelect(player, creature, sender, ACTION_WITH_OPT(TRANSMOG_IN_SLOT, opt));
        }
        case TRANSMOG_VENDOR:
        {
            player->GetSession()->SendListInventory(creature->GetGUID());
            break;
        }
        case TRANSMOG_IN_SLOT:
        {
            _SendTransmogrificationSlotMenu(player, creature, target, action, sender);
            break;
        }
        default:
            break;
    }

    return true;
}

bool TransmogrificationUI::OnGossipSelectCode(Player * player, Creature* creature, uint32 sender, uint32 action, const char *ccode)
{
    player->CLEAR_GOSSIP_MENU();
    player->CLOSE_GOSSIP_MENU();

    uint8 transmogAction = action >> TRANSMOG_START_BIT;
    uint8 opt = action & TRANSMOG_START_MASK;

    if (sender == TRANSMOG_DEFAULT_SENDER)
    {
        switch (transmogAction)
        {
            case TRANSMOG_ADD_SET_CONFIRM:
            {
                std::string setName = std::string(ccode);
                if (setName.empty() || setName.size() > MAX_SET_NAME_CHARS)
                {
                    sTransmog->SendTransmogrificationResult(player, TRANSMOG_ERR_INVALID_SET_NAME);
                    return OnGossipSelect(player, creature, sender, ACTION(TRANSMOG_ADD_SET));
                }

                sTransmog->AddNewTransmogrificationSet(player, setName, m_Storage[player].slotMap);
                return OnGossipSelect(player, creature, TRANSMOG_DEFAULT_SENDER, ACTION(TRANSMOG_SETS));
            }
            default:
                break;
        }

        return OnGossipHello(player, creature);
    }

    EquipmentSlots eSlot = EquipmentSlots(opt);

    // Sanity checks
    Item* target = player->GetItemByPos(INVENTORY_SLOT_BAG_0, eSlot);
    if (!target || target->GetGUIDLow() != sender)
    {
        sTransmog->SendTransmogrificationResult(player, TRANSMOG_ERR_MISSING_DEST_ITEM);
        return OnGossipHello(player, creature);
    }

    std::wstring wNamePart;
    if (!*ccode || !Utf8toWStr((std::string)ccode, wNamePart))
    {
        sTransmog->SendTransmogrificationResult(player, TRANSMOG_ERR_INVALID_SEARCH);
        return OnGossipSelect(player, creature, sender, ACTION_WITH_OPT(TRANSMOG_IN_SLOT, action));
    }
    wstrToLower(wNamePart);

    TransmogrificationStorage_T& storage = m_Storage[player];
    storage.eSlot = eSlot;
    storage.search = wNamePart;

    switch (transmogAction)
    {
        case TRANSMOG_VENDOR_SEARCH:
        {
            if (!_NeedShowInventoryList(player, creature))
            {
                sTransmog->SendTransmogrificationResult(player, TRANSMOG_ERR_NO_MATCH);
                return OnGossipSelect(player, creature, sender, ACTION_WITH_OPT(TRANSMOG_IN_SLOT, action));
            }

            player->GetSession()->SendListInventory(creature->GetGUID());
            break;
        }
        default:
            break;
    }

    return true;
}

VendorItemData const* TransmogrificationUI::GetVendorItems(Creature const* /*creature*/, Player* player) const
{
    return sObjectMgr->GetNpcVendorItemList(sTransmog->GetTransmogrificationVendorId(player));
}

bool TransmogrificationUI::ShouldDisplayVendorItem(Player* player, Creature* /*creature*/, ItemTemplate const* item_template, VendorItem const* /*vendor_item*/)
{
    TransmogrificationStorage_T& storage = m_Storage[player];

    TransmogrificationSkins& skins = storage.skins;
    sTransmog->UpdateAvailableSkins(player, skins);

    if (Item* target = player->GetItemByPos(INVENTORY_SLOT_BAG_0, storage.eSlot))
    {
        return (storage.search.empty() || _HasMatchingNameOrId(item_template, storage.search)) &&
            (sTransmog->AlwaysShowItemForPlayer(player, item_template) || skins.find(item_template->ItemId) != skins.end()) &&
            sTransmog->CanTransmogrifyItemWithItem(player, target, item_template);
    }

    return false;
}

bool TransmogrificationUI::BypassReputationRequirementOnPurchaseItem(Player* player, Creature* /*creature*/, ItemTemplate const* item_template, VendorItem const* /*vendor_item*/)
{
    return sTransmog->AlwaysShowItemForPlayer(player, item_template);
}

bool TransmogrificationUI::OnBeforePurchaseItem(Player* player, Creature* creature, ItemTemplate const* item_template, VendorItem const* /*vendor_item*/)
{
    TransmogrificationStorage_T const& storage = m_Storage[player];

    switch (_Transmogrify(player, storage.eSlot, item_template))
    {
        case TRANSMOG_ERR_CANNOT_AFFORD_GOLD_COST:
        case TRANSMOG_ERR_CANNOT_AFFORD_CUSTOM_COST:
            break;
        default:
            OnGossipHello(player, creature);
            break;
    }

    return false;
}

void TransmogrificationUI::ModifyItemVendorData(Player* player, Creature* /*creature*/, ItemTemplate const* item_template, VendorItem const* /*vendor_item*/, int32& price, uint32& /*extendedCost*/, uint32& leftInStock, uint32& /*display*/)
{
    int32 cost = sTransmog->GetTransmogrificationCost(player, item_template);
    price = std::max(0, cost);

    // Grey out items that the player cannot afford
    if (cost < 0 && !sTransmog->CanAffordCustomCost(player, item_template, uint32(-cost)))
        leftInStock = 0;
}

void TransmogrificationUI::_SendTransmogrificationSlotMenu(Player* player, Creature* creature, Item* target, uint32 action, uint32 sender)
{
    bool showInventory = _NeedShowInventoryList(player, creature);

    std::ostringstream buf;
    buf << (showInventory ? "|cff000000" : "|cff7e7e7e")
        << "Show a list of available skins" << "|r";
    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD,
                                     GOSSIP_ICON_TEXT(buf.str(), "ICONS/inv_helmet_08"),
                                     sender,
                                     showInventory ? ACTION_WITH_OPT(TRANSMOG_VENDOR, action) : ACTION_WITH_OPT(TRANSMOG_IN_SLOT, action),
                                     showInventory ? "" : "You do not have any valid transmogrification skins available for this item.",
                                     0,
                                     false);

    buf.str(std::string());
    buf << (showInventory ? "|cff000000" : "|cff7e7e7e")
        << "Filter skins by name or item ID" << "|r";
    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD,
                                     GOSSIP_ICON_TEXT(buf.str(), "ICONS/INV_Misc_Spyglass_03"),
                                     sender,
                                     showInventory ? ACTION_WITH_OPT(TRANSMOG_VENDOR_SEARCH, action) : ACTION_WITH_OPT(TRANSMOG_IN_SLOT, action),
                                     showInventory ? "" : "You do not have any valid transmogrification skins available for this item.",
                                     0,
                                     showInventory ? true : false);

    bool transmogrified = sTransmog->IsItemTransmogrified(player, target);

    buf.str(std::string());
    buf << (transmogrified ? "|cff000000" : "|cff7e7e7e")
        << "Revert current transmogrification" << "|r";
    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD,
                                     GOSSIP_ICON_TEXT(buf.str(), "ICONS/INV_Enchant_Disenchant"),
                                     sender,
                                     transmogrified ? ACTION_WITH_OPT(TRANSMOG_REVERT, action) : ACTION_WITH_OPT(TRANSMOG_IN_SLOT, action),
                                     transmogrified ?
                                        "Are you sure you wish to revert the transmogrification on this item?" :
                                        "This item is not transmogrified.",
                                     0,
                                     false);

    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD,
                            GOSSIP_ICON_TEXT_RETURN_TO_MAIN_MENU,
                            TRANSMOG_DEFAULT_SENDER,
                            TRANSMOG_MAIN_MENU);


    std::string targetName, targetIcon;
    _UpdateItemDescription(target->GetEntry(), targetName, targetIcon, true);

    buf.str(std::string());
    buf << "You have chosen to transmogrify the following item:\n\n";
    if (sTransmog->IsItemTransmogrified(player, target))
    {
        std::string name, icon;
        _UpdateItemDescription(sTransmog->GetItemDisplay(player, target), name, icon);

        buf << GOSSIP_ICON_TEXT_EXTENDED(targetName, targetIcon, 30, -6, -19, "  ") << "\n\n"
            << "currently transmogrified to\n\n"
            << GOSSIP_ICON_TEXT_EXTENDED(name, icon, 30, -6, -19, "  ") << "\n";
    }
    else
        buf << GOSSIP_ICON_TEXT_EXTENDED(targetName, targetIcon, 30, -6, -7, "  ") << "\n";
    SEND_CUSTOM_GOSSIP_MENU(player, creature->GetGUID(), buf.str());
}

void TransmogrificationUI::_SendTransmogrificationSetsMenu(Player* player, Creature* creature)
{
    _AddTransmogrificationSetMenuItems(player);

    if (sTransmog->HasAvailableTransmogrificationSetSlot(player))
    {
        bool canAddNewSet = sTransmog->HasEquippedItemWithTransmogrification(player);

        std::ostringstream buf;
        buf << (canAddNewSet ? "|cff000000" : "|cff7e7e7e")
            << "Add a new transmogrification set" << "|r";
        player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD,
                                         GOSSIP_ICON_TEXT(buf.str(), "GuildBankFrame/UI-GuildBankFrame-NewTab"),
                                         TRANSMOG_DEFAULT_SENDER,
                                         canAddNewSet ? ACTION(TRANSMOG_ADD_SET) : ACTION(TRANSMOG_SETS),
                                         canAddNewSet ? "" : "None of your currently equipped items are transmogrified.",
                                         0,
                                         false);
    }

    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD,
                            GOSSIP_ICON_TEXT_RETURN_TO_MAIN_MENU,
                            TRANSMOG_DEFAULT_SENDER,
                            TRANSMOG_MAIN_MENU);

    player->SEND_GOSSIP_MENU(TRANSMOG_GOSSIP_MESSAGE_SETS, creature->GetGUID());
}

void TransmogrificationUI::_SendTransmogrificationAddSetMenu(Player* player, Creature* creature)
{
    TransmogrificationSlotMap& slotMap = m_Storage[player].slotMap;
    slotMap.clear();

    for (uint8 slot = TRANSMOG_SLOT_HEAD; slot < MAX_TRANSMOG_SLOT; ++slot)
    {
        TransmogrificationSlotInfo const& info = TransmogrificationSlotInfoStore[slot];

        std::string itemName = "|cff7e7e7e" + info.defaultName + "|r";
        std::string iconPath = info.defaultIcon;

        if (Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, info.eSlot))
            if (sTransmog->IsItemTransmogrified(player, item))
            {
                uint32 itemEntry = sTransmog->GetItemDisplay(player, item);
                if (ItemTemplate const* proto = sObjectMgr->GetItemTemplate(itemEntry))
                {
                    slotMap[TransmogrificationSlot(slot)] = proto->ItemId;
                    _UpdateItemDescription(proto, itemName, iconPath);
                }
            }

        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD,
                                GOSSIP_ICON_TEXT(itemName, iconPath),
                                TRANSMOG_DEFAULT_SENDER,
                                ACTION(TRANSMOG_ADD_SET));
    }

    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD,
                                     GOSSIP_ICON_TEXT("Specify a name and save set", "GuildBankFrame/UI-GuildBankFrame-NewTab"),
                                     TRANSMOG_DEFAULT_SENDER,
                                     ACTION(TRANSMOG_ADD_SET_CONFIRM),
                                     "",
                                     0,
                                     true);
    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD,
                            GOSSIP_ICON_TEXT_RETURN("Back to transmogrification sets"),
                            TRANSMOG_DEFAULT_SENDER,
                            ACTION(TRANSMOG_SETS));

    player->SEND_GOSSIP_MENU(TRANSMOG_GOSSIP_MESSAGE_ADD_SET, creature->GetGUID());
}

void TransmogrificationUI::_SendTransmogrificationManageSetMenu(Player* player, Creature* creature, uint8 setId)
{
    TransmogrificationSet* transmogSet = player->GetTransmogrificationSet(setId);
    if (!transmogSet)
    {
        OnGossipSelect(player, creature, TRANSMOG_DEFAULT_SENDER, ACTION(TRANSMOG_SETS));
        return;
    }

    for (uint8 slot = TRANSMOG_SLOT_HEAD; slot < MAX_TRANSMOG_SLOT; ++slot)
    {
        TransmogrificationSlotInfo const& info = TransmogrificationSlotInfoStore[slot];

        std::string itemName = "|cff7e7e7e" + info.defaultName + "|r";
        std::string iconPath = info.defaultIcon;

        if (uint32 itemId = transmogSet->slotMap[TransmogrificationSlot(slot)])
            _UpdateItemDescription(itemId, itemName, iconPath);

        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD,
                                GOSSIP_ICON_TEXT(itemName, iconPath),
                                TRANSMOG_DEFAULT_SENDER,
                                ACTION_WITH_OPT(TRANSMOG_MANAGE_SET, setId));
    }

    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD,
                                     GOSSIP_ICON_TEXT("Apply this set", "ICONS/Spell_Holy_DivineSpirit"),
                                     TRANSMOG_DEFAULT_SENDER,
                                     ACTION_WITH_OPT(TRANSMOG_APPLY_SET, setId),
                                     "Are you sure you wish to apply this transmogrification set for the following amount?",
                                     sTransmog->GetTransmogrificationSetApplyCost(player, transmogSet),
                                     false);
    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD,
                                     GOSSIP_ICON_TEXT("Delete this set", "PaperDollInfoFrame/UI-GearManager-LeaveItem-Opaque"),
                                     TRANSMOG_DEFAULT_SENDER,
                                     ACTION_WITH_OPT(TRANSMOG_DELETE_SET, setId),
                                     "Are you sure you wish to delete this transmogrification set? This action cannot be undone.",
                                     0,
                                     false);
    player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD,
                            GOSSIP_ICON_TEXT_RETURN("Back to transmogrification sets"),
                            TRANSMOG_DEFAULT_SENDER,
                            ACTION(TRANSMOG_SETS));

    std::ostringstream buf;
    buf << "The following transmogrifications are part of the set named |cff0000ff"
        << transmogSet->name << "|r. You may apply these to your currently"
        << " equipped items, or delete the set.";
   SEND_CUSTOM_GOSSIP_MENU(player, creature->GetGUID(), buf.str());
}

void TransmogrificationUI::_AddTransmogSlotMenuItems(Player* player)
{
    for (uint8 slot = TRANSMOG_SLOT_HEAD; slot < MAX_TRANSMOG_SLOT; ++slot)
    {
        TransmogrificationSlotInfo const& info = TransmogrificationSlotInfoStore[slot];

        Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, info.eSlot);
        bool valid = sTransmog->IsValidItem(player, item);

        std::string itemName = (valid ? "|cff000000" : "|cff7e7e7e") + info.defaultName + "|r";
        std::string iconPath = info.defaultIcon;

        if (valid && sTransmog->IsItemTransmogrified(player, item))
        {
            uint32 itemEntry = sTransmog->GetItemDisplay(player, item);
            _UpdateItemDescription(itemEntry, itemName, iconPath);
        }

        player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD,
                                         GOSSIP_ICON_TEXT(itemName, iconPath),
                                         valid ? item->GetGUIDLow() : uint32(TRANSMOG_DEFAULT_SENDER),
                                         valid ? ACTION_WITH_OPT(TRANSMOG_IN_SLOT, info.eSlot) : TRANSMOG_MAIN_MENU,
                                         valid ? "" : "You do not have a valid transmogrifiable item equipped in that slot.",
                                         0,
                                         false);
    }
}

void TransmogrificationUI::_AddTransmogrificationSetMenuItems(Player* player)
{
    TransmogrificationSets const& transmogSets = player->GetTransmogrificationSets();
    for (auto const& itr : transmogSets)
    {
        player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TABARD,
                                GOSSIP_ICON_TEXT(itr.second.name, _GetRandomSetIcon(itr.second.slotMap)),
                                TRANSMOG_DEFAULT_SENDER,
                                ACTION_WITH_OPT(TRANSMOG_MANAGE_SET, itr.first));
    }
}

TransmogrificationResult TransmogrificationUI::_Transmogrify(Player* player, EquipmentSlots targetSlot, ItemTemplate const* item_template)
{
    TransmogrificationStorage_T const& storage = m_Storage[player];
    TransmogrificationSkins const& skins = storage.skins;

    bool forceUseTemplate = sTransmog->AlwaysShowItemForPlayer(player, item_template);

    auto itr = skins.find(item_template->ItemId);
    if (itr == skins.end() && !forceUseTemplate)
        return TRANSMOG_ERR_MISSING_SRC_ITEM;

    TransmogrificationResult result = forceUseTemplate ?
        sTransmog->Transmogrify(player, targetSlot, item_template) :
        sTransmog->Transmogrify(player, targetSlot, itr->second);

    sTransmog->SendTransmogrificationResult(player, result);
    return result;
}

bool TransmogrificationUI::_NeedShowInventoryList(Player* player, Creature* creature)
{
    VendorItemData const* vItems = GetVendorItems(creature, player);
    for (const auto i : vItems->m_items)
    {
        if (ItemTemplate const* proto = sObjectMgr->GetItemTemplate(i->item))
            if (ShouldDisplayVendorItem(player, creature, proto, i))
                return true;
    }

    return false;
}

bool TransmogrificationUI::_HasMatchingNameOrId(ItemTemplate const* item_template, std::wstring search) const
{
    uint32 searchNum = 0;
    if (boost::spirit::qi::parse(search.begin(), search.end(), searchNum))
        return item_template->ItemId == searchNum;

    return Utf8FitTo(item_template->Name1, search);
}

void TransmogrificationUI::_UpdateItemDescription(uint32 itemEntry, std::string& itemName, std::string& iconPath, bool trueColor /*= false*/)
{
    if (ItemTemplate const* proto = sObjectMgr->GetItemTemplate(itemEntry))
        _UpdateItemDescription(proto, itemName, iconPath, trueColor);
}

void TransmogrificationUI::_UpdateItemDescription(ItemTemplate const* proto, std::string& itemName, std::string& iconPath, bool trueColor /*= false*/)
{
    itemName = _GetItemLink(proto, trueColor);
    iconPath = _GetItemIcon(proto);
}

std::string TransmogrificationUI::_GetItemLink(ItemTemplate const* proto, bool trueColor)
{
    std::string itemName = proto->Name1;

    if (itemName.size() > MAX_LINE_CHARS)
    {
        itemName.resize(MAX_LINE_CHARS - 3);
        itemName += "...";
    }

    std::ostringstream buf;
    if (trueColor)
        buf << "|c" << std::hex << ItemQualityDarkColors[proto->Quality];
    else
        buf << "|cff9d003b";
    buf << "|Hitem:" << proto->ItemId << ":0:0:0:0:0:0:0:0:0:0|h" << itemName << "|h|r";

    return buf.str();
}

std::string TransmogrificationUI::_GetItemIcon(ItemTemplate const* proto)
{
    if (ItemDisplayInfoEntry const* displayInfo = sItemDisplayInfoStore.LookupEntry(proto->DisplayInfoID))
        return "ICONS/" + std::string(displayInfo->inventoryIcon);
    else
        return "ICONS/INV_Misc_QuestionMark";
}

std::string TransmogrificationUI::_GetRandomSetIcon(TransmogrificationSlotMap const& slotMap)
{
    std::vector<ItemTemplate const*> items;
    for (auto const& itr : slotMap)
    {
        if (ItemTemplate const* proto = sObjectMgr->GetItemTemplate(itr.second))
            items.push_back(proto);
    }

    auto randIt = items.begin();
    std::advance(randIt, urand(0, items.size() - 1));
    return _GetItemIcon(*randIt);
}