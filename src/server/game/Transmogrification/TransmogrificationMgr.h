/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_TMOGMGR_H
#define CG_TMOGMGR_H

#include "TransmogrificationDefines.h"

class TransmogrificationMgr
{
    private:

        TransmogrificationMgr();
        ~TransmogrificationMgr() { }

    public:

        static TransmogrificationMgr* instance()
        {
            static TransmogrificationMgr instance;
            return &instance;
        }

        void Init();
        void LoadTransmogrificationVendorItems();

        // Error handling
        void SendTransmogrificationResult(Player* player, TransmogrificationResult result);

        // Handlers for item-specific transmogrifications
        bool HasEquippedItemWithTransmogrification(Player* player) const;
        bool IsItemTransmogrified(Player* player, Item* item) const;
        uint32 GetItemDisplay(Player* player, Item* item) const;
        void SetItemDisplay(Player* player, Item* item, ItemTemplate const* source);
        void ResetItemDisplay(Player* player, TransmogrificationSlot slot);
        void ResetItemDisplay(Player* player, Item* item);
        void ResetAllEquippedItemDisplays(Player* player);

        // Handlers for transmogrification sets
        bool HasAvailableTransmogrificationSetSlot(Player* player) const;
        uint8 GetEmptyTransmogrificationSetSlot(Player* player) const;
        void AddNewTransmogrificationSet(Player* player, std::string const& setName, TransmogrificationSlotMap& slotMap);
        uint32 GetTransmogrificationSetApplyCost(Player* player, uint8 setId);
        uint32 GetTransmogrificationSetApplyCost(Player* player, TransmogrificationSet* transmogSet);
        void ApplyTransmogrificationSet(Player* player, uint8 setId);
        void DeleteTransmogrificationSet(Player* player, uint8 setId);

        void UpdateItem(Player* player, Item* item) const;

        uint32 GetTransmogrificationVendorId(Player* player) const;

        // DB handling
        void SaveTransmogrificationToDB(Player* player, Item* item, ItemTemplate const* source);
        void DeleteTransmogrificationFromDB(Player* player, Item* item);
        void DeleteTransmogrificationFromDB(Player* player, uint32 itemGuidLow);
        void SaveTransmogrificationSetToDB(Player* player, uint8 setId);
        void DeleteTransmogrificationSetFromDB(Player* player, uint8 setId);

        // Transmogrification wrappers
        TransmogrificationResult Transmogrify(Player* player, EquipmentSlots targetSlot, ObjectGuid sourceGuid);
        TransmogrificationResult Transmogrify(Player* player, EquipmentSlots targetSlot, ItemTemplate const* source);

        void UpdateAvailableSkins(Player* player, TransmogrificationSkins& skins) const;
        bool AlwaysShowItemForPlayer(Player* player, ItemTemplate const* proto) const;
        int32 GetTransmogrificationCost(Player* player, ItemTemplate const* proto) const;

        // Transmogrification validity checks
        bool IsValidTransmogrificationSlot(EquipmentSlots slot) const;
        bool CanTransmogrifyItemWithItem(Player* player, ItemTemplate const* target, ItemTemplate const* source) const;
        bool CanTransmogrifyItemWithItem(Player* player, Item* target, ItemTemplate const* source) const;
        bool IsValidItem(Player* player, ItemTemplate const* proto) const;
        bool IsValidItem(Player* player, Item* item) const;
        bool IsRangedWeapon(ItemTemplate const* proto) const;
        bool IsAllowedQuality(uint32 quality) const;

        // Transmogrification sets validity checks
        bool IsValidItemForSlot(ItemTemplate const* proto, TransmogrificationSlot slot) const;
        bool IsValidTransmogrificationSet(TransmogrificationSlotMap& slotMap) const;

        // Custom cost helpers
        uint32 GetCustomCost(Player* player, ItemTemplate const* proto) const;
        bool CanAffordCustomCost(Player* player, ItemTemplate const* proto, uint32 customCostId) const;
        bool PurchaseTransmogrificationForCustomCost(Player* player, ItemTemplate const* proto, uint32 customCostId);

    private:

        void _CleanOrphanedEntries();
        void _LoadTransmogrificationVendorItems(uint32 vendorId);

        // Configuration
        void _LoadTransmogrificationConfig();

        bool _allowPoor;
        bool _allowCommon;
        bool _allowUncommon;
        bool _allowRare;
        bool _allowEpic;
        bool _allowLegendary;
        bool _allowArtifact;
        bool _allowHeirloom;
        bool _allowMixedArmorTypes;
        bool _allowMixedWeaponTypes;
        bool _allowFishingPoles;
        bool _ignoreReqRace;
        bool _ignoreReqClass;
        bool _ignoreReqSkill;
        bool _ignoreReqSpell;
        bool _ignoreReqLevel;
        bool _ignoreReqEvent;
        bool _ignoreReqStats;
};

#define sTransmog TransmogrificationMgr::instance()

#endif