/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "Chat.h"
#include "Config.h"
#include "GameEventMgr.h"
#include "Player.h"
#include "TransmogrificationMgr.h"
#include "TransmogrificationUI.h"

TransmogrificationMgr::TransmogrificationMgr()
{
    // Instantiate the Transmogrification UI script
    new TransmogrificationUI();
}

void TransmogrificationMgr::Init()
{
    _CleanOrphanedEntries();
    _LoadTransmogrificationConfig();
}

void TransmogrificationMgr::LoadTransmogrificationVendorItems()
{
    uint32 oldMSTime = getMSTime();

    _LoadTransmogrificationVendorItems(TRANSMOGRIFICATION_NORMAL_VENDOR_ID);
    _LoadTransmogrificationVendorItems(TRANSMOGRIFICATION_PVP_VENDOR_ID);

    TC_LOG_INFO("server.loading", ">> Loaded transmogrification vendors in %u ms", GetMSTimeDiffToNow(oldMSTime));
}

void TransmogrificationMgr::SendTransmogrificationResult(Player* player, TransmogrificationResult result)
{
    WorldSession* session = player->GetSession();
    ChatHandler handler(session);

    switch (result)
    {
        case TRANSMOG_ERR_OK:
            session->SendAreaTriggerMessage("Item transmogrified");
            break;
        case TRANSMOG_ERR_REVERT_OK:
            session->SendAreaTriggerMessage("Item transmogrification reverted");
            break;
        case TRANSMOG_ERR_INVALID_SLOT:
            session->SendNotification("Invalid equipment slot for transmogrification");
            break;
        case TRANSMOG_ERR_MISSING_SRC_ITEM:
            session->SendNotification("Source item not found for transmogrification");
            break;
        case TRANSMOG_ERR_MISSING_DEST_ITEM:
            session->SendNotification("Target item not found for transmogrification");
            break;
        case TRANSMOG_ERR_INVALID_SRC_ITEM:
            session->SendNotification("Invalid source item for transmogrification");
            break;
        case TRANSMOG_ERR_INVALID_ITEMS:
            session->SendNotification("Invalid items for transmogrification");
            break;
        case TRANSMOG_ERR_INVALID_SEARCH:
            session->SendNotification("Invalid search term");
            handler.SendSysMessage("Please specify a partial name or item ID to filter your available transmogrification skins.");
            break;
        case TRANSMOG_ERR_NO_MATCH:
            session->SendNotification("No item found matching search term");
            break;
        case TRANSMOG_ERR_INVALID_SET_NAME:
            session->SendNotification("Invalid name specified for transmogrification set");
            handler.PSendSysMessage("Please specify a name between 1 and %u characters in length for this transmogrification set.", MAX_SET_NAME_CHARS);
            break;
        case TRANSMOG_ERR_CANNOT_AFFORD_GOLD_COST:
            player->SendEquipError(EQUIP_ERR_NOT_ENOUGH_MONEY, NULL);
            break;
        default:
            break;
    }
}

bool TransmogrificationMgr::HasEquippedItemWithTransmogrification(Player* player) const
{
    for (uint8 i = TRANSMOG_SLOT_HEAD; i < MAX_TRANSMOG_SLOT; ++i)
    {
        TransmogrificationSlotInfo const& info = TransmogrificationSlotInfoStore[i];
        if (Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, info.eSlot))
            if (IsItemTransmogrified(player, item))
                return true;
    }

    return false;
}

bool TransmogrificationMgr::IsItemTransmogrified(Player* player, Item* item) const
{
    // Verify that item belongs to player
    ASSERT(player->OwnsItem(item));

    TransmogrificationMap const& transmogMap = player->GetTransmogrificationMap();
    auto itr = transmogMap.find(item->GetGUID());

    return itr != transmogMap.end();
}

uint32 TransmogrificationMgr::GetItemDisplay(Player* player, Item* item) const
{
    // Verify that item belongs to player
    ASSERT(player->OwnsItem(item));

    TransmogrificationMap const& transmogMap = player->GetTransmogrificationMap();
    auto itr = transmogMap.find(item->GetGUID());
    if (itr == transmogMap.end())
        return item->GetEntry();

    return itr->second;
}

void TransmogrificationMgr::SetItemDisplay(Player* player, Item* item, ItemTemplate const* source)
{
    // Verify that item belongs to player
    ASSERT(player->OwnsItem(item));

    // Transmog
    TransmogrificationMap& transmogMap = player->GetTransmogrificationMap();
    transmogMap[item->GetGUID()] = source->ItemId;
    UpdateItem(player, item);

    // Transmog history
    TransmogrificationHistory& transmogHistory = player->GetTransmogrificationHistory();
    transmogHistory.insert(source->ItemId);

    SaveTransmogrificationToDB(player, item, source);
}

void TransmogrificationMgr::ResetItemDisplay(Player* player, TransmogrificationSlot slot)
{
    TransmogrificationSlotInfo const& info = TransmogrificationSlotInfoStore[slot];
    if (Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, info.eSlot))
        ResetItemDisplay(player, item);
}

void TransmogrificationMgr::ResetItemDisplay(Player* player, Item* item)
{
    // Verify that item belongs to player
    ASSERT(player->OwnsItem(item));

    TransmogrificationMap& transmogMap = player->GetTransmogrificationMap();
    if (transmogMap.erase(item->GetGUID()) != 0)
    {
        UpdateItem(player, item);
        DeleteTransmogrificationFromDB(player, item);
    }
}

void TransmogrificationMgr::ResetAllEquippedItemDisplays(Player* player)
{
    for (uint8 i = TRANSMOG_SLOT_HEAD; i < MAX_TRANSMOG_SLOT; ++i)
    {
        TransmogrificationSlotInfo const& info = TransmogrificationSlotInfoStore[i];
        if (Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, info.eSlot))
            ResetItemDisplay(player, item);
    }
}

bool TransmogrificationMgr::HasAvailableTransmogrificationSetSlot(Player* player) const
{
    return GetEmptyTransmogrificationSetSlot(player) != NULL_TRANSMOG_SET_SLOT;
}

uint8 TransmogrificationMgr::GetEmptyTransmogrificationSetSlot(Player* player) const
{
    TransmogrificationSets const& transmogSets = player->GetTransmogrificationSets();
    for (uint8 i = 0; i < MAX_TRANSMOG_SETS; ++i)
    {
        if (transmogSets.find(i) == transmogSets.end())
            return i;
    }

    return NULL_TRANSMOG_SET_SLOT;
}

void TransmogrificationMgr::AddNewTransmogrificationSet(Player* player, std::string const& setName, TransmogrificationSlotMap& slotMap)
{
    uint8 setId = GetEmptyTransmogrificationSetSlot(player);
    if (setId == NULL_TRANSMOG_SET_SLOT || !IsValidTransmogrificationSet(slotMap))
        return;

    TransmogrificationSet& transmogSet = player->GetNewOrExistingTransmogrificationSet(setId);
    transmogSet.name    = setName;
    transmogSet.slotMap = slotMap;

    SaveTransmogrificationSetToDB(player, setId);
}

uint32 TransmogrificationMgr::GetTransmogrificationSetApplyCost(Player* player, uint8 setId)
{
    return GetTransmogrificationSetApplyCost(player, player->GetTransmogrificationSet(setId));
}

uint32 TransmogrificationMgr::GetTransmogrificationSetApplyCost(Player* player, TransmogrificationSet* transmogSet)
{
    if (!transmogSet)
        return 0;

    uint32 cost = 0;
    for (uint8 i = TRANSMOG_SLOT_HEAD; i < MAX_TRANSMOG_SLOT; ++i)
    {
        TransmogrificationSlotInfo const& info = TransmogrificationSlotInfoStore[i];
        if (Item* target = player->GetItemByPos(INVENTORY_SLOT_BAG_0, info.eSlot))
            if (ItemTemplate const* source = sObjectMgr->GetItemTemplate(transmogSet->slotMap[TransmogrificationSlot(i)]))
                if (CanTransmogrifyItemWithItem(player, target, source))
                    cost += GetTransmogrificationCost(player, source);
    }

    return cost;
}

void TransmogrificationMgr::ApplyTransmogrificationSet(Player* player, uint8 setId)
{
    TransmogrificationSet* transmogSet = player->GetTransmogrificationSet(setId);
    if (!transmogSet)
        return;

    uint32 cost = GetTransmogrificationSetApplyCost(player, transmogSet);
    if (!player->HasEnoughMoney(cost))
        return;

    for (uint8 i = TRANSMOG_SLOT_HEAD; i < MAX_TRANSMOG_SLOT; ++i)
    {
        TransmogrificationSlot slot = TransmogrificationSlot(i);
        uint32 itemId = transmogSet->slotMap[slot];

        if (!itemId)
            ResetItemDisplay(player, slot);
        else if (ItemTemplate const* source = sObjectMgr->GetItemTemplate(itemId))
        {
            TransmogrificationSlotInfo const& info = TransmogrificationSlotInfoStore[i];
            Transmogrify(player, info.eSlot, source);
        }
    }

    player->ModifyMoney(-static_cast<int32>(cost));
}

void TransmogrificationMgr::DeleteTransmogrificationSet(Player* player, uint8 setId)
{
    TransmogrificationSets& transmogSets = player->GetTransmogrificationSets();
    if (transmogSets.erase(setId) != 0)
        DeleteTransmogrificationSetFromDB(player, setId);
}

void TransmogrificationMgr::UpdateItem(Player* player, Item* item) const
{
    // Verify that item belongs to player
    ASSERT(player->OwnsItem(item));

    if (item->IsEquipped())
    {
        player->SetVisibleItemSlot(item->GetSlot(), item);
        if (player->IsInWorld())
            item->SendUpdateToPlayer(player);
    }
}

uint32 TransmogrificationMgr::GetTransmogrificationVendorId(Player* player) const
{
    return sTrack->IsPvP(player) ? TRANSMOGRIFICATION_PVP_VENDOR_ID : TRANSMOGRIFICATION_NORMAL_VENDOR_ID;
}

void TransmogrificationMgr::SaveTransmogrificationToDB(Player* player, Item* item, ItemTemplate const* source)
{
    SQLTransaction trans = CharacterDatabase.BeginTransaction();
    PreparedStatement* stmt;

    // Transmog
    stmt = CharacterDatabase.GetPreparedStatement(CHAR_REP_TRANSMOG);
    stmt->setUInt32(0, item->GetGUIDLow());
    stmt->setUInt32(1, source->ItemId);
    trans->Append(stmt);

    // Transmog history
    TransmogrificationHistory const& transmogHistory = player->GetTransmogrificationHistory();

    std::stringstream history;
    std::copy(transmogHistory.begin(), transmogHistory.end(), std::ostream_iterator<uint32>(history, " "));

    stmt = CharacterDatabase.GetPreparedStatement(CHAR_REP_TRANSMOG_HISTORY);
    stmt->setUInt32(0, player->GetGUIDLow());
    stmt->setString(1, history.str());
    trans->Append(stmt);

    CharacterDatabase.CommitTransaction(trans);
}

void TransmogrificationMgr::DeleteTransmogrificationFromDB(Player* player, Item* item)
{
    DeleteTransmogrificationFromDB(player, item->GetGUIDLow());
}

void TransmogrificationMgr::DeleteTransmogrificationFromDB(Player* player, uint32 itemGuidLow)
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_TRANSMOG);
    stmt->setUInt32(0, player->GetGUIDLow());
    stmt->setUInt32(1, itemGuidLow);
    CharacterDatabase.Execute(stmt);
}

void TransmogrificationMgr::SaveTransmogrificationSetToDB(Player* player, uint8 setId)
{
    if (TransmogrificationSet* transmogSet = player->GetTransmogrificationSet(setId))
    {
        uint8 idx = 0;

        PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_REP_TRANSMOG_SET);
        stmt->setUInt32     (idx++, player->GetGUIDLow());
        stmt->setUInt8      (idx++, setId);
        stmt->setString     (idx++, transmogSet->name);

        for (uint8 slot = TRANSMOG_SLOT_HEAD; slot < MAX_TRANSMOG_SLOT; ++slot)
        {
            stmt->setUInt32 (idx++, transmogSet->slotMap[TransmogrificationSlot(slot)]);
        }

        CharacterDatabase.Execute(stmt);
    }
}

void TransmogrificationMgr::DeleteTransmogrificationSetFromDB(Player* player, uint8 setId)
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_TRANSMOG_SET);
    stmt->setUInt32(0, player->GetGUIDLow());
    stmt->setUInt8(1, setId);
    CharacterDatabase.Execute(stmt);
}

TransmogrificationResult TransmogrificationMgr::Transmogrify(Player* player, EquipmentSlots targetSlot, ObjectGuid sourceGuid)
{
    Item* sourceItem = player->GetItemByGuid(sourceGuid);
    if (!sourceItem)
        return TRANSMOG_ERR_MISSING_SRC_ITEM;

    ItemTemplate const* source = sourceItem->GetTemplate();
    if (!source)
        return TRANSMOG_ERR_INVALID_SRC_ITEM;

    TransmogrificationResult result = Transmogrify(player, targetSlot, source);
    if (result == TRANSMOG_ERR_OK)
    {
        if (source->Bonding == BIND_WHEN_PICKED_UP ||
            source->Bonding == BIND_WHEN_EQUIPED ||
            source->Bonding == BIND_WHEN_USE)
            sourceItem->SetBinding(true);

        sourceItem->SetOwnerGUID(player->GetGUID());
        sourceItem->SetNotRefundable(player);
        sourceItem->ClearSoulboundTradeable(player);
    }

    return result;
}

TransmogrificationResult TransmogrificationMgr::Transmogrify(Player* player, EquipmentSlots targetSlot, ItemTemplate const* source)
{
    if (!IsValidTransmogrificationSlot(targetSlot))
        return TRANSMOG_ERR_INVALID_SLOT;

    Item* target = player->GetItemByPos(INVENTORY_SLOT_BAG_0, targetSlot);
    if (!target)
        return TRANSMOG_ERR_MISSING_DEST_ITEM;

    if (!CanTransmogrifyItemWithItem(player, target, source))
        return TRANSMOG_ERR_INVALID_ITEMS;

    int32 cost = sTransmog->GetTransmogrificationCost(player, source);
    if (cost >= 0)
    {
        if (!player->HasEnoughMoney(cost))
            return TRANSMOG_ERR_CANNOT_AFFORD_GOLD_COST;

        player->ModifyMoney(-cost);
    }
    else if (!PurchaseTransmogrificationForCustomCost(player, source, uint32(-cost)))
    {
        ChatHandler(player->GetSession()).PSendSysMessage("You require the following to transmogrify to %s:\n%s",
                                                          source->ItemLink().c_str(),
                                                          sCurrencyMgr->GetCustomCostDescription(player, uint32(-cost)).c_str());
        return TRANSMOG_ERR_CANNOT_AFFORD_CUSTOM_COST;
    }

    SetItemDisplay(player, target, source);

    target->UpdatePlayedTime(player);
    target->SetOwnerGUID(player->GetGUID());
    target->SetNotRefundable(player);
    target->ClearSoulboundTradeable(player);

    return TRANSMOG_ERR_OK;
}

void TransmogrificationMgr::UpdateAvailableSkins(Player* player, TransmogrificationSkins& skins) const
{
    skins.clear();

    if (!sTrack->CanTransmogrifyWithOwnItems(player))
        return;

    TransmogrificationSkins soulboundSkins;

    for (uint8 i = BANK_SLOT_BAG_START; i < BANK_SLOT_BAG_END; ++i)
        if (Bag* pBag = player->GetBagByPos(i))
            for (uint32 j = 0; j < pBag->GetBagSize(); ++j)
                if (Item* pItem = pBag->GetItemByPos(j))
                {
                    if (pItem->IsSoulBound())
                        soulboundSkins[pItem->GetEntry()] = pItem->GetGUID();
                    else
                        skins[pItem->GetEntry()] = pItem->GetGUID();
                }

    for (int i = BANK_SLOT_ITEM_START; i < BANK_SLOT_BAG_END; ++i)
        if (Item* pItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, i))
        {
            if (pItem->IsSoulBound())
                soulboundSkins[pItem->GetEntry()] = pItem->GetGUID();
            else
                skins[pItem->GetEntry()] = pItem->GetGUID();
        }

    for (uint8 i = INVENTORY_SLOT_BAG_START; i < INVENTORY_SLOT_BAG_END; ++i)
        if (Bag* pBag = player->GetBagByPos(i))
            for (uint32 j = 0; j < pBag->GetBagSize(); ++j)
                if (Item* pItem = pBag->GetItemByPos(j))
                {
                    if (pItem->IsSoulBound())
                        soulboundSkins[pItem->GetEntry()] = pItem->GetGUID();
                    else
                        skins[pItem->GetEntry()] = pItem->GetGUID();
                }

    for (uint8 i = INVENTORY_SLOT_ITEM_START; i < INVENTORY_SLOT_ITEM_END; ++i)
        if (Item* pItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, i))
        {
            if (pItem->IsSoulBound())
                soulboundSkins[pItem->GetEntry()] = pItem->GetGUID();
            else
                skins[pItem->GetEntry()] = pItem->GetGUID();
        }

    // Add soulbound items after everything else to override GUIDs
    for (auto const& itr : soulboundSkins)
    {
        skins[itr.first] = itr.second;
    }
}

bool TransmogrificationMgr::AlwaysShowItemForPlayer(Player* player, ItemTemplate const* proto) const
{
    return player->HasItemInTransmogrificationHistory(proto) || GetCustomCost(player, proto) != 0;
}

int32 TransmogrificationMgr::GetTransmogrificationCost(Player* player, ItemTemplate const* proto) const
{
    if (uint32 customCost = GetCustomCost(player, proto))
        return -static_cast<int32>(customCost);

    int32 basePrice = static_cast<int32>(proto->SellPrice);
    return player->getLevel() >= sWorld->getIntConfig(CONFIG_MAX_PLAYER_LEVEL) ?
        std::max(100000, basePrice) : basePrice;
}

bool TransmogrificationMgr::IsValidTransmogrificationSlot(EquipmentSlots slot) const
{
    for (uint8 i = TRANSMOG_SLOT_HEAD; i < MAX_TRANSMOG_SLOT; ++i)
    {
        TransmogrificationSlotInfo const& info = TransmogrificationSlotInfoStore[i];
        if (info.eSlot == slot)
            return true;
    }

    return false;
}

bool TransmogrificationMgr::CanTransmogrifyItemWithItem(Player* player, ItemTemplate const* target, ItemTemplate const* source) const
{
    if (!target || !source)
        return false;

    if (source->ItemId == target->ItemId)
        return false;

    if (!IsValidItem(player, target) || !IsValidItem(player, source))
        return false;

    // TC doesnt check this? Checked by Inventory type check.
    if (source->Class != target->Class)
        return false;

    if (source->SubClass != target->SubClass && !IsRangedWeapon(target))
    {
        if (source->Class == ITEM_CLASS_ARMOR && !_allowMixedArmorTypes)
            return false;
        if (source->Class == ITEM_CLASS_WEAPON && !_allowMixedWeaponTypes)
            return false;
    }

    if (source->InventoryType != target->InventoryType)
    {
        if (source->Class == ITEM_CLASS_WEAPON &&
            (IsRangedWeapon(target) != IsRangedWeapon(source) ||
            source->InventoryType == INVTYPE_WEAPONMAINHAND ||
            source->InventoryType == INVTYPE_WEAPONOFFHAND))
            return false;
        if (source->Class == ITEM_CLASS_ARMOR &&
            !((source->InventoryType == INVTYPE_CHEST && target->InventoryType == INVTYPE_ROBE) ||
            (source->InventoryType == INVTYPE_ROBE && target->InventoryType == INVTYPE_CHEST)))
            return false;
    }

    return true;
}

bool TransmogrificationMgr::CanTransmogrifyItemWithItem(Player* player, Item* target, ItemTemplate const* source) const
{
    if (target)
    {
        if (GetItemDisplay(player, target) == source->ItemId)
            return false;

        return CanTransmogrifyItemWithItem(player, target->GetTemplate(), source);
    }

    return false;
}

bool TransmogrificationMgr::IsValidItem(Player* player, ItemTemplate const* proto) const
{
    if (!player || !proto)
        return false;

    if (proto->Class != ITEM_CLASS_ARMOR &&
        proto->Class != ITEM_CLASS_WEAPON)
        return false;

    if (proto->InventoryType == INVTYPE_BAG ||
        proto->InventoryType == INVTYPE_RELIC ||
        proto->InventoryType == INVTYPE_FINGER ||
        proto->InventoryType == INVTYPE_TRINKET ||
        proto->InventoryType == INVTYPE_AMMO ||
        proto->InventoryType == INVTYPE_QUIVER)
        return false;

    if (!_allowFishingPoles && proto->Class == ITEM_CLASS_WEAPON && proto->SubClass == ITEM_SUBCLASS_WEAPON_FISHING_POLE)
        return false;

    if (!IsAllowedQuality(proto->Quality))
        return false;

    if ((proto->Flags2 & ITEM_FLAGS_EXTRA_HORDE_ONLY) && player->GetTeam() != HORDE)
        return false;

    if ((proto->Flags2 & ITEM_FLAGS_EXTRA_ALLIANCE_ONLY) && player->GetTeam() != ALLIANCE)
        return false;

    if (!_ignoreReqClass && (proto->AllowableClass & player->getClassMask()) == 0)
        return false;

    if (!_ignoreReqRace && (proto->AllowableRace & player->getRaceMask()) == 0)
        return false;

    if (!_ignoreReqSkill && proto->RequiredSkill != 0)
    {
        if (player->GetSkillValue(proto->RequiredSkill) == 0)
            return false;
        else if (player->GetSkillValue(proto->RequiredSkill) < proto->RequiredSkillRank)
            return false;
    }

    if (!_ignoreReqSpell && proto->RequiredSpell != 0 && !player->HasSpell(proto->RequiredSpell))
        return false;

    if (!_ignoreReqLevel && player->getLevel() < proto->RequiredLevel)
        return false;

    // If World Event is not active, prevent using event dependant items
    if (!_ignoreReqEvent && proto->HolidayId && !IsHolidayActive(HolidayIds(proto->HolidayId)))
        return false;

    if (!_ignoreReqStats)
    {
        if (!proto->RandomProperty && !proto->RandomSuffix)
        {
            bool found = false;
            for (uint8 i = 0; i < proto->StatsCount; ++i)
            {
                if (proto->ItemStat[i].ItemStatValue != 0)
                {
                    found = true;
                    break;
                }
            }
            if (!found)
                return false;
        }
    }

    return true;
}

bool TransmogrificationMgr::IsValidItem(Player* player, Item* item) const
{
    return item && IsValidItem(player, item->GetTemplate());
}

bool TransmogrificationMgr::IsRangedWeapon(ItemTemplate const* proto) const
{
    return proto->Class == ITEM_CLASS_WEAPON &&
        (proto->SubClass == ITEM_SUBCLASS_WEAPON_BOW ||
         proto->SubClass == ITEM_SUBCLASS_WEAPON_GUN ||
         proto->SubClass == ITEM_SUBCLASS_WEAPON_CROSSBOW);
}

bool TransmogrificationMgr::IsAllowedQuality(uint32 quality) const
{
    switch (quality)
    {
        case ITEM_QUALITY_POOR:         return _allowPoor;
        case ITEM_QUALITY_NORMAL:       return _allowCommon;
        case ITEM_QUALITY_UNCOMMON:     return _allowUncommon;
        case ITEM_QUALITY_RARE:         return _allowRare;
        case ITEM_QUALITY_EPIC:         return _allowEpic;
        case ITEM_QUALITY_LEGENDARY:    return _allowLegendary;
        case ITEM_QUALITY_ARTIFACT:     return _allowArtifact;
        case ITEM_QUALITY_HEIRLOOM:     return _allowHeirloom;
        default:                        return false;
    }
}

bool TransmogrificationMgr::IsValidItemForSlot(ItemTemplate const* proto, TransmogrificationSlot slot) const
{
    if (!proto)
        return false;

    // Crude check for validity of item-slot combination to avoid client-side visual bugs
    // proper checks will be performed when applying a set to the equipped armor
    switch (proto->InventoryType)
    {
        case INVTYPE_HEAD:
            return slot == TRANSMOG_SLOT_HEAD;
        case INVTYPE_SHOULDERS:
            return slot == TRANSMOG_SLOT_SHOULDERS;
        case INVTYPE_CLOAK:
            return slot == TRANSMOG_SLOT_BACK;
        case INVTYPE_CHEST:
        case INVTYPE_ROBE:
            return slot == TRANSMOG_SLOT_CHEST;
        case INVTYPE_BODY:
            return slot == TRANSMOG_SLOT_SHIRT;
        case INVTYPE_TABARD:
            return slot == TRANSMOG_SLOT_TABARD;
        case INVTYPE_WRISTS:
            return slot == TRANSMOG_SLOT_WRIST;
        case INVTYPE_HANDS:
            return slot == TRANSMOG_SLOT_HANDS;
        case INVTYPE_WAIST:
            return slot == TRANSMOG_SLOT_WAIST;
        case INVTYPE_LEGS:
            return slot == TRANSMOG_SLOT_LEGS;
        case INVTYPE_FEET:
            return slot == TRANSMOG_SLOT_FEET;
        case INVTYPE_WEAPON:
        case INVTYPE_2HWEAPON:
        case INVTYPE_WEAPONMAINHAND:
        case INVTYPE_WEAPONOFFHAND:
             return slot == TRANSMOG_SLOT_MAINHAND || slot == TRANSMOG_SLOT_OFFHAND;
        case INVTYPE_SHIELD:
        case INVTYPE_HOLDABLE:
            return slot == TRANSMOG_SLOT_OFFHAND;
        case INVTYPE_RANGED:
        case INVTYPE_THROWN:
        case INVTYPE_RANGEDRIGHT:
            return slot == TRANSMOG_SLOT_RANGED;
        default:
            return false;
    }
}

bool TransmogrificationMgr::IsValidTransmogrificationSet(TransmogrificationSlotMap& slotMap) const
{
    for (uint8 i = TRANSMOG_SLOT_HEAD; i < MAX_TRANSMOG_SLOT; ++i)
    {
        TransmogrificationSlot slot = TransmogrificationSlot(i);

        uint32 itemId = slotMap[slot];
        if (itemId && !IsValidItemForSlot(sObjectMgr->GetItemTemplate(itemId), slot))
            return false;
    }

    return true;
}

uint32 TransmogrificationMgr::GetCustomCost(Player* player, ItemTemplate const* proto) const
{
    return sCurrencyMgr->GetVendorItemCustomCostId(GetTransmogrificationVendorId(player), proto->ItemId);
}

bool TransmogrificationMgr::CanAffordCustomCost(Player* player, ItemTemplate const* proto, uint32 customCostId) const
{
    return player->HasItemInTransmogrificationHistory(proto) || sCurrencyMgr->CanAffordCustomCost(player, customCostId);
}

bool TransmogrificationMgr::PurchaseTransmogrificationForCustomCost(Player* player, ItemTemplate const* proto, uint32 customCostId)
{
    return player->HasItemInTransmogrificationHistory(proto) || sCurrencyMgr->PurchaseItemForCost(player, GetTransmogrificationVendorId(player), customCostId);
}

void TransmogrificationMgr::_CleanOrphanedEntries()
{
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_DEL_ORPHANED_TRANSMOGS);
    CharacterDatabase.DirectExecute(stmt);
}

void TransmogrificationMgr::_LoadTransmogrificationVendorItems(uint32 vendorId)
{
    ItemTemplateContainer const* templates = sObjectMgr->GetItemTemplateStore();
    ASSERT(templates);

    // Initialize vector to hold vendor items to be loaded
    VendorItemList items;

    for (auto itr : *templates)
    {
        ItemTemplate const& itemTemplate = itr.second;

        if (itemTemplate.Class != ITEM_CLASS_WEAPON &&
            itemTemplate.Class != ITEM_CLASS_ARMOR)
            continue;

        items.push_back(new VendorItem(itemTemplate.ItemId, 0, 0, 2));
    }

    std::sort(items.begin(), items.end(), Trinity::VendorItemListSortPredicate(vendorId));
    sObjectMgr->SetVendorItems(vendorId, items);
}

void TransmogrificationMgr::_LoadTransmogrificationConfig()
{
    _allowPoor = sConfigMgr->GetBoolDefault("Transmogrification.AllowPoor", true);
    _allowCommon = sConfigMgr->GetBoolDefault("Transmogrification.AllowCommon", true);
    _allowUncommon = sConfigMgr->GetBoolDefault("Transmogrification.AllowUncommon", true);
    _allowRare = sConfigMgr->GetBoolDefault("Transmogrification.AllowRare", true);
    _allowEpic = sConfigMgr->GetBoolDefault("Transmogrification.AllowEpic", true);
    _allowLegendary = sConfigMgr->GetBoolDefault("Transmogrification.AllowLegendary", false);
    _allowArtifact = sConfigMgr->GetBoolDefault("Transmogrification.AllowArtifact", false);
    _allowHeirloom = sConfigMgr->GetBoolDefault("Transmogrification.AllowHeirloom", true);

    _allowMixedArmorTypes = sConfigMgr->GetBoolDefault("Transmogrification.AllowMixedArmorTypes", false);
    _allowMixedWeaponTypes = sConfigMgr->GetBoolDefault("Transmogrification.AllowMixedWeaponTypes", false);
    _allowFishingPoles = sConfigMgr->GetBoolDefault("Transmogrification.AllowFishingPoles", false);

    _ignoreReqRace = sConfigMgr->GetBoolDefault("Transmogrification.IgnoreReqRace", false);
    _ignoreReqClass = sConfigMgr->GetBoolDefault("Transmogrification.IgnoreReqClass", false);
    _ignoreReqSkill = sConfigMgr->GetBoolDefault("Transmogrification.IgnoreReqSkill", false);
    _ignoreReqSpell = sConfigMgr->GetBoolDefault("Transmogrification.IgnoreReqSpell", false);
    _ignoreReqLevel = sConfigMgr->GetBoolDefault("Transmogrification.IgnoreReqLevel", false);
    _ignoreReqEvent = sConfigMgr->GetBoolDefault("Transmogrification.IgnoreReqEvent", false);
    _ignoreReqStats = sConfigMgr->GetBoolDefault("Transmogrification.IgnoreReqStats", true);
}