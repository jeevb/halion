/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "GossipDef.h"
#include "GossipMgr.h"
#include "Player.h"
#include "ScriptMgr.h"
#include "WorldPacket.h"
#include "WorldSession.h"

GossipMgr::GossipMgr() : m_CustomNpcTextId(START_CUSTOM_NPC_TEXT_ID)
{
}

uint32 GossipMgr::GetNewOrExistingNpcTextId(ObjectGuid guid)
{
    auto itr = m_CustomNpcTextIdMap.find(guid);

    // Register a new npc text id if necessary
    if (itr == m_CustomNpcTextIdMap.end())
        return m_CustomNpcTextIdMap[guid] = m_CustomNpcTextId++;

    return itr->second;
}

void GossipMgr::SendCustomGossipMenu(Player* player, ObjectGuid guid, std::string const& text)
{
    uint32 npcTextId = GossipMgr::GetNewOrExistingNpcTextId(guid);
    _SendNpcTextUpdate(player, npcTextId, text);

    player->PlayerTalkClass->SendGossipMenu(npcTextId, guid);
}

void GossipMgr::RegisterPlayerGossipScriptName(const char* name, int32 id)
{
    if (id < 0)
        return;

    for (auto const& scr : m_ScriptNameMap)
    {
        ASSERT(scr.first != name && scr.second != id);
    }

    m_ScriptNameMap[name] = id;
}

bool GossipMgr::IsValidPlayerMenuSenderForPlayer(Player* player, ObjectGuid guid)
{
    return player->GetScriptId() >= 0 && player->GetGUID() == guid;
}

void GossipMgr::SendPlayerGossipMenu(Player* player, const char* scriptName)
{
    auto itr = m_ScriptNameMap.find(scriptName);

    if (itr != m_ScriptNameMap.end())
    {
        player->SetScriptId(itr->second);
        return sScriptMgr->OnPlayerGossipMenuSend(player);
    }
}

std::string GossipMgr::GossipMenuItemWithIcon(std::string const& text,
                                              std::string const& icon,
                                              uint32 iconSize,
                                              int32 horizontalOffset,
                                              int32 verticalOffset,
                                              std::string const& offset)
{
    std::ostringstream os;

    os  << "|TInterface/" << icon << ':'
        << iconSize << ':' << iconSize << ':' << horizontalOffset << ':' << verticalOffset
        << "|t" << offset << text;

    return os.str();
}

void GossipMgr::_SendNpcTextUpdate(Player* player, uint32 npcTextId, std::string const& text)
{
    WorldPacket data(SMSG_NPC_TEXT_UPDATE, 100);

    data << npcTextId;
    for (uint32 i = 0; i < MAX_GOSSIP_TEXT_OPTIONS; ++i)
    {
        data << float(0);
        data << text;
        data << text;
        data << uint32(0);
        data << uint32(0);
        data << uint32(0);
        data << uint32(0);
        data << uint32(0);
        data << uint32(0);
        data << uint32(0);
    }

    player->GetSession()->SendPacket(&data);
}