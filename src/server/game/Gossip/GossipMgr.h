/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_GOSSIPMGR_H
#define CG_GOSSIPMGR_H

enum GossipMgrConstants
{
    START_CUSTOM_NPC_TEXT_ID = 10000000
};

typedef std::map<const char*, int32> ScriptNameMap;
typedef std::map<ObjectGuid, uint32> CustomNpcTextIdMap;

class GossipMgr
{
    private:
        GossipMgr();
        ~GossipMgr() { }

    public:

        static GossipMgr* instance()
        {
            static GossipMgr instance;
            return &instance;
        }

        void Init() { }

        // Custom NPC text
        uint32 GetNewOrExistingNpcTextId(ObjectGuid guid);
        void SendCustomGossipMenu(Player* player, ObjectGuid guid, std::string const& text);

        // Player gossip menu
        void RegisterPlayerGossipScriptName(const char* name, int32 id);
        bool IsValidPlayerMenuSenderForPlayer(Player* player, ObjectGuid guid);
        void SendPlayerGossipMenu(Player* player, const char* scriptName);

        // Gossip Menu Item
        std::string GossipMenuItemWithIcon(std::string const& text,
                                           std::string const& icon,
                                           uint32 iconSize = 30,
                                           int32 horizontalOffset = -18,
                                           int32 verticalOffset = 0,
                                           std::string const& offset = "");

    private:

        void _SendNpcTextUpdate(Player* player, uint32 npcTextId, std::string const& text);

        uint32 m_CustomNpcTextId;
        CustomNpcTextIdMap m_CustomNpcTextIdMap;

        ScriptNameMap m_ScriptNameMap;
};

#define sGossipMgr GossipMgr::instance()

#define SEND_CUSTOM_GOSSIP_MENU(player, guid, text) \
    sGossipMgr->SendCustomGossipMenu(player, guid, text)

#define GOSSIP_ICON_TEXT(text, icon) sGossipMgr->GossipMenuItemWithIcon(text, icon)
#define GOSSIP_ICON_TEXT_EXTENDED(text, icon, iconSize, horizontalOffset, verticalOffset, offset) \
    sGossipMgr->GossipMenuItemWithIcon(text, icon, iconSize, horizontalOffset, verticalOffset, offset)
#define GOSSIP_ICON_TEXT_RETURN(text) GOSSIP_ICON_TEXT(text, "PaperDollInfoFrame/UI-GearManager-Undo.blp")
#define GOSSIP_ICON_TEXT_RETURN_TO_MAIN_MENU GOSSIP_ICON_TEXT_RETURN("Return to the main menu")
#define GOSSIP_ICON_TEXT_BROWSE_GOODS GOSSIP_ICON_TEXT("Browse my wares", "Icons/inv_misc_coin_06.blp")
#define GOSSIP_ICON_TEXT_NOTIFICATION(text, icon) \
    sGossipMgr->GossipMenuItemWithIcon(text, icon, 30, 0, 0, "   ")

#endif