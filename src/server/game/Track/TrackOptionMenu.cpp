/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "Chat.h"
#include "GossipMgr.h"
#include "Player.h"
#include "ScriptedGossip.h"
#include "TeleportMgr.h"
#include "TrackMgr.h"
#include "TrackOptionMenu.h"
#include "TrainingMgr.h"

TrackOptionMenu::TrackOptionMenu() : PlayerGossipScript(TRACK_OPTION_MENU)
{
}

void TrackOptionMenu::OnPlayerGossipMenuSend(Player* player)
{
    player->CLEAR_GOSSIP_MENU();

    bool isAlliance = player->GetTeam() == ALLIANCE;
    std::string pve = GOSSIP_ICON_TEXT("Battle against the Lich King", "Icons\\achievement_boss_lichking.blp");
    std::string pvp = isAlliance ?
        GOSSIP_ICON_TEXT("Battle against the Horde (PvP)", "Icons\\inv_bannerpvp_01.blp") :
        GOSSIP_ICON_TEXT("Battle against the Alliance (PvP)", "Icons\\inv_bannerpvp_02.blp");

    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD, pve, 0, TRACK_OPTION_PVE, "Are you sure you want to pursue the\n|cff5dfc0aNormal Track|r?", 0, false);
    player->ADD_GOSSIP_ITEM_EXTENDED(GOSSIP_ICON_TABARD, pvp, 0, TRACK_OPTION_PVP, "Are you sure you want to pursue the\n|cffffce00PvP Track|r?", 0, false);

    player->SEND_GOSSIP_MENU(isAlliance ? TRACK_MESSAGE_ALLIANCE : TRACK_MESSAGE_HORDE, player->GetGUID());
}

void TrackOptionMenu::OnPlayerGossipSelect(Player* player, uint32 /*sender*/, uint32 action)
{
    player->CLEAR_GOSSIP_MENU();
    player->CLOSE_GOSSIP_MENU();

    ChatHandler handler(player->GetSession());

    // Silence achievement broadcasts
    player->SetSilenceAchievement();

    switch (action)
    {
        case TRACK_OPTION_PVE:
            player->SetTrack(TRACK_NORMAL_LEVELING);
            break;
        case TRACK_OPTION_PVP:
        {
            player->SetTrack(TRACK_PVP_LOCKED);
            sTrain->Boost(player, sWorld->getIntConfig(CONFIG_MAX_PLAYER_LEVEL));

            // Teleport player to mall and set home bind
            uint32 teleportLoc = player->GetTeam() == ALLIANCE ? TELEPORT_LOC_ALLIANCE_VANGUARD : TELEPORT_LOC_HORDE_OUTPOST;
            sTeleport->To(player, teleportLoc);
            sTeleport->SetPlayerHomebind(player, teleportLoc);
            player->SetNoPvP();
            break;
        }
        default:
            break;
    }

    sTrack->UpdateNearbyQuestGiverStatus(player);
    sTrack->InitStartingGold(player);
    sTrain->Learn(player);
    sTrain->AddItems(player, true);

    // Remove silence achiavement flag
    player->SetSilenceAchievement(false);

    // Send notification to players
    handler.PSendSysMessage("|cff1eff00You have chosen to pursue the %s track.|r", action == TRACK_OPTION_PVE ? "normal" : "PvP");
}