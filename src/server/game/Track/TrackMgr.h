/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_TRACKMGR_H
#define CG_TRACKMGR_H

#define TRACK_OPTION_MENU               "track_option_menu"
#define TRACK_CHECK_INTERVAL_INITIAL    5 * IN_MILLISECONDS
#define TRACK_CHECK_INTERVAL            20 * IN_MILLISECONDS

#define FIXED_GOLD_AMOUNT 200000000 // 20,000 gold

enum Track
{
    TRACK_NONE,
    TRACK_NORMAL_LEVELING,
    TRACK_NORMAL_BOOST,
    TRACK_PVP_LOCKED,
    TRACK_PVP_UNLOCKED
};

class TrackMgr
{
    private:
        TrackMgr();
        ~TrackMgr() { }

    public:

        static TrackMgr* instance()
        {
            static TrackMgr instance;
            return &instance;
        }

        void Init();

        // Track Option Menu
        bool SendTrackOptionMenu(Player* player) const;

        // Player Tracks
        bool IsUndecided(Track track) const;
        bool IsUndecided(ObjectGuid guid) const;
        bool IsUndecided(Player* player) const;

        bool IsNormal(Track track) const;
        bool IsNormal(ObjectGuid guid) const;
        bool IsNormal(Player* player) const;

        bool IsPvP(Track track) const;
        bool IsPvP(ObjectGuid guid) const;
        bool IsPvP(Player* player) const;

        bool IsBoosted(Track track) const;
        bool IsBoosted(ObjectGuid guid) const;
        bool IsBoosted(Player* player) const;

        bool IsUnlocked(Track track) const;
        bool IsUnlocked(ObjectGuid guid) const;
        bool IsUnlocked(Player* player) const;

        bool IsLeveling(Track track) const;
        bool IsLeveling(ObjectGuid guid) const;
        bool IsLeveling(Player* player) const;

        // Track Permissions
        bool CanInteract(Track sender, Track recipient, bool allowUnlocked = false) const;
        bool CanInteract(ObjectGuid sender, ObjectGuid recipient, bool allowUnlocked = false) const;
        bool CanInteract(ObjectGuid sender, Player* recipient, bool allowUnlocked = false) const;
        bool CanInteract(Player* sender, ObjectGuid recipient, bool allowUnlocked = false) const;
        bool CanInteract(Player* sender, Player* recipient, bool allowUnlocked = false) const;
        bool CanInteract(Player* player, Creature const* creature) const;

        bool CanParty(Player* sender, Player* recipient) const;
        bool CanEnterDungeonsOrRaids(Player* player) const;
        bool CanGainExp(Player* player) const;
        bool CanDoQuests(Player* player) const;
        bool CanUseAuctionHouse(Player* player) const;
        bool CanGetFirstReachAchievements(Player* player) const;
        bool CanGetFirstKillAchievements(Player* player) const;
        bool CanTransmogrifyWithOwnItems(Player* player) const;
        bool CanGetSellPrice(Player* player) const;
        bool CanRepairForFree(Player* player) const;
        bool CanIncurDurabilityLoss(Player* player) const;
        bool HasFixedGoldAmount(Player* player) const;

        // Track hooks
        void OnSetMoney(Player* player, uint32& value);

        // Helpers
        void UpdateNearbyQuestGiverStatus(Player* player);
        void InitStartingGold(Player* player);
        bool HasTrackMask(Creature const* creature) const;
        uint32 GetCreatureTrackMask(Creature const* creature) const;

    private:

        void _LoadCreatureTrackMasks();

        Track _GetTrackByGuid(ObjectGuid guid) const;

        typedef std::map<uint32, uint32> CreatureTrackMaskMap;
        CreatureTrackMaskMap m_CreatureTrackMaskMap;
};

#define sTrack TrackMgr::instance()

#endif