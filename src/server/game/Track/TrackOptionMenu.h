/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_TRACKOPTMENU_H
#define CG_TRACKOPTMENU_H

#include "ScriptMgr.h"

enum TrackOption
{
    TRACK_OPTION_MAIN_MENU,
    TRACK_OPTION_PVE,
    TRACK_OPTION_PVP
};

enum TrackGossipMessage
{
    TRACK_MESSAGE_ALLIANCE  = 1000000,
    TRACK_MESSAGE_HORDE     = 1000001
};

class TrackOptionMenu : public PlayerGossipScript
{
    public:
        TrackOptionMenu();

        void OnPlayerGossipMenuSend(Player* player) override;
        void OnPlayerGossipSelect(Player* player, uint32 sender, uint32 action) override;
};

#endif