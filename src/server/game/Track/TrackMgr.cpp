/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "GossipMgr.h"
#include "Player.h"
#include "TrackMgr.h"
#include "TrackOptionMenu.h"
#include "WorldPacket.h"
#include "WorldSession.h"

TrackMgr::TrackMgr()
{
    // Instantiate the Track Option Menu script
    new TrackOptionMenu();
}

void TrackMgr::Init()
{
    _LoadCreatureTrackMasks();
}

bool TrackMgr::SendTrackOptionMenu(Player* player) const
{
    if (!player->GetSession()->PlayerLoading())
    {
        sGossipMgr->SendPlayerGossipMenu(player, TRACK_OPTION_MENU);
        return true;
    }

    return false;
}

bool TrackMgr::IsUndecided(Track track) const
{
    return track == TRACK_NONE;
}

bool TrackMgr::IsUndecided(ObjectGuid guid) const
{
    return IsUndecided(_GetTrackByGuid(guid));
}

bool TrackMgr::IsUndecided(Player* player) const
{
    return IsUndecided(player->GetTrack());
}

bool TrackMgr::IsNormal(Track track) const
{
    return track == TRACK_NORMAL_LEVELING || track == TRACK_NORMAL_BOOST;
}

bool TrackMgr::IsNormal(ObjectGuid guid) const
{
    return IsNormal(_GetTrackByGuid(guid));
}

bool TrackMgr::IsNormal(Player* player) const
{
    return IsNormal(player->GetTrack());
}

bool TrackMgr::IsPvP(Track track) const
{
    return track == TRACK_PVP_LOCKED || track == TRACK_PVP_UNLOCKED;
}

bool TrackMgr::IsPvP(ObjectGuid guid) const
{
    return IsPvP(_GetTrackByGuid(guid));
}

bool TrackMgr::IsPvP(Player* player) const
{
    return IsPvP(player->GetTrack());
}

bool TrackMgr::IsBoosted(Track track) const
{
    return IsPvP(track) || track == TRACK_NORMAL_BOOST;
}

bool TrackMgr::IsBoosted(ObjectGuid guid) const
{
    return IsBoosted(_GetTrackByGuid(guid));
}

bool TrackMgr::IsBoosted(Player* player) const
{
    return IsBoosted(player->GetTrack());
}

bool TrackMgr::IsUnlocked(Track track) const
{
    return track == TRACK_PVP_UNLOCKED;
}

bool TrackMgr::IsUnlocked(ObjectGuid guid) const
{
    return IsUnlocked(_GetTrackByGuid(guid));
}

bool TrackMgr::IsUnlocked(Player* player) const
{
    return IsUnlocked(player->GetTrack());
}

bool TrackMgr::IsLeveling(Track track) const
{
    return track == TRACK_NORMAL_LEVELING;
}

bool TrackMgr::IsLeveling(ObjectGuid guid) const
{
    return IsLeveling(_GetTrackByGuid(guid));
}

bool TrackMgr::IsLeveling(Player* player) const
{
    return IsLeveling(player->GetTrack());
}

bool TrackMgr::CanInteract(Track sender, Track recipient, bool allowUnlocked /*= false*/) const
{
    return (allowUnlocked && IsUnlocked(recipient)) ||
        (IsPvP(sender) && IsPvP(recipient)) ||
        (IsNormal(sender) && IsNormal(recipient));
}

bool TrackMgr::CanInteract(ObjectGuid sender, ObjectGuid recipient, bool allowUnlocked /*= false*/) const
{
    return CanInteract(_GetTrackByGuid(sender), _GetTrackByGuid(recipient), allowUnlocked);
}

bool TrackMgr::CanInteract(ObjectGuid sender, Player* recipient, bool allowUnlocked /*= false*/) const
{
    return CanInteract(_GetTrackByGuid(sender), recipient->GetTrack(), allowUnlocked);
}

bool TrackMgr::CanInteract(Player* sender, ObjectGuid recipient, bool allowUnlocked /*= false*/) const
{
    return sender->IsGameMaster() || CanInteract(sender->GetTrack(), _GetTrackByGuid(recipient), allowUnlocked);
}

bool TrackMgr::CanInteract(Player* sender, Player* recipient, bool allowUnlocked /*= false*/) const
{
    return sender->IsGameMaster() || CanInteract(sender->GetTrack(), recipient->GetTrack(), allowUnlocked);
}

bool TrackMgr::CanInteract(Player* player, Creature const* creature) const
{
    if (player->IsGameMaster())
        return true;

    auto itr = m_CreatureTrackMaskMap.find(creature->GetEntry());
    if (itr == m_CreatureTrackMaskMap.end())
        return true;

    return itr->second && (itr->second & player->getTrackMask()) != 0;
}

bool TrackMgr::CanParty(Player* sender, Player* recipient) const
{
    return !IsUndecided(sender) && !IsUndecided(recipient);
}

bool TrackMgr::CanEnterDungeonsOrRaids(Player* player) const
{
    return IsNormal(player) || IsUnlocked(player);
}

bool TrackMgr::CanGainExp(Player* player) const
{
    return !IsUndecided(player);
}

bool TrackMgr::CanDoQuests(Player* player) const
{
    return !IsUndecided(player);
}

bool TrackMgr::CanUseAuctionHouse(Player* player) const
{
    return IsNormal(player);
}

bool TrackMgr::CanGetFirstReachAchievements(Player* player) const
{
    float avgRate = player->GetExperience()->GetAverageRate();
    return IsLeveling(player) && avgRate > 0.0f && avgRate < 1.1f;
}

bool TrackMgr::CanGetFirstKillAchievements(Player* player) const
{
    return IsLeveling(player);
}

bool TrackMgr::CanTransmogrifyWithOwnItems(Player* /*player*/) const
{
    return true;
}

bool TrackMgr::CanGetSellPrice(Player* player) const
{
    return IsNormal(player);
}

bool TrackMgr::CanRepairForFree(Player* player) const
{
    return IsPvP(player);
}

bool TrackMgr::CanIncurDurabilityLoss(Player* player) const
{
    return IsUndecided(player) || IsNormal(player);
}

bool TrackMgr::HasFixedGoldAmount(Player* player) const
{
    return IsPvP(player);
}

void TrackMgr::OnSetMoney(Player* player, uint32& value)
{
    if (HasFixedGoldAmount(player))
        value = FIXED_GOLD_AMOUNT;
}

// Copied from  WorldSession::HandleQuestgiverStatusMultipleQuery()
void TrackMgr::UpdateNearbyQuestGiverStatus(Player* player)
{
    uint32 count = 0;

    WorldPacket data(SMSG_QUESTGIVER_STATUS_MULTIPLE, 4);
    data << uint32(count);                                  // placeholder

    for (auto itr = player->m_clientGUIDs.begin(); itr != player->m_clientGUIDs.end(); ++itr)
    {
        uint32 questStatus = DIALOG_STATUS_NONE;

        if (itr->IsAnyTypeCreature())
        {
            // need also pet quests case support
            Creature* questgiver = sObjectAccessor->GetCreatureOrPetOrVehicle(*player, *itr);
            if (!questgiver || questgiver->IsHostileTo(player))
                continue;
            if (!questgiver->HasFlag(UNIT_NPC_FLAGS, UNIT_NPC_FLAG_QUESTGIVER))
                continue;

            questStatus = player->GetQuestDialogStatus(questgiver);

            data << uint64(questgiver->GetGUID());
            data << uint8(questStatus);
            ++count;
        }
        else if (itr->IsGameObject())
        {
            GameObject* questgiver = player->GetMap()->GetGameObject(*itr);
            if (!questgiver || questgiver->GetGoType() != GAMEOBJECT_TYPE_QUESTGIVER)
                continue;

            questStatus = player->GetQuestDialogStatus(questgiver);

            data << uint64(questgiver->GetGUID());
            data << uint8(questStatus);
            ++count;
        }
    }

    data.put<uint32>(0, count);                             // write real count
    player->SendDirectMessage(&data);
}

void TrackMgr::InitStartingGold(Player* player)
{
    if (HasFixedGoldAmount(player))
        player->SetMoney();
}

bool TrackMgr::HasTrackMask(Creature const* creature) const
{
    auto itr = m_CreatureTrackMaskMap.find(creature->GetEntry());
    return itr != m_CreatureTrackMaskMap.end() && itr->second;
}

uint32 TrackMgr::GetCreatureTrackMask(Creature const* creature) const
{
    auto itr = m_CreatureTrackMaskMap.find(creature->GetEntry());
    return itr == m_CreatureTrackMaskMap.end() ? 0 : itr->second;
}

void TrackMgr::_LoadCreatureTrackMasks()
{
    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_CREATURE_TRACKMASK);
    if (PreparedQueryResult result = WorldDatabase.Query(stmt))
    {
        do {
            Field* fields = result->Fetch();

            // Check for validity of creature entry before adding to map
            uint32 entry = fields[0].GetUInt32();
            if (sObjectMgr->GetCreatureTemplate(entry))
                m_CreatureTrackMaskMap[entry] = fields[1].GetUInt32();
        } while (result->NextRow());
    }

    TC_LOG_INFO("server.loading", ">> Loaded %u creature trackmasks in %u ms",
                uint32(m_CreatureTrackMaskMap.size()), GetMSTimeDiffToNow(oldMSTime));
}

Track TrackMgr::_GetTrackByGuid(ObjectGuid guid) const
{
    // prevent DB access for online player
    if (Player* player = sObjectAccessor->FindPlayer(guid))
        return player->GetTrack();

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_TRACK);
    stmt->setUInt32(0, guid.GetCounter());

    if (PreparedQueryResult result = CharacterDatabase.Query(stmt))
        return Track(result->Fetch()[0].GetUInt8());

    return TRACK_NONE;
}