/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "Battleground.h"
#include "CFBattlegroundMgr.h"
#include "Config.h"
#include "GossipMgr.h"
#include "NotificationMgr.h"
#include "Player.h"
#include "WorldSession.h"

CFBattlegroundMgr::CFBattlegroundMgr() : m_Enabled(false)
{
}

void CFBattlegroundMgr::Init()
{
   m_Enabled = sConfigMgr->GetBoolDefault("Battleground.Crossfaction.Enable", false);
}

void CFBattlegroundMgr::SetValidFakeInfo(Player* player)
{
    std::list<FakeDisplay const*> validInfo;

    // Find the races/morphs that are valid for this class.
    for(uint8 i = FAKE_RACE_HUMAN; i < MAX_FAKE_RACE; ++i)
    {
        FakeDisplay const* morph = &FakeDisplays[i];

        if (morph->team == player->GetTeam() &&
            (player->getGender() == GENDER_MALE ? morph->maleMorph : morph->femaleMorph) &&
            sObjectMgr->GetPlayerInfo(morph->raceId, player->getClass()))
            validInfo.push_back(morph);
    }

    ASSERT(validInfo.size()); // we should always find something, if not, there's something direly wrong.

    std::list<FakeDisplay const*>::const_iterator randIt = validInfo.begin();
    std::advance(randIt, urand(0, validInfo.size() - 1));

    player->SetFakeRace((*randIt)->raceId);
    player->SetFakeDisplay(player->getGender() == GENDER_MALE ? (*randIt)->maleMorph : (*randIt)->femaleMorph);
}

void CFBattlegroundMgr::ClearFakeInfo(Player* player)
{
    player->SetFakeRace(0);
    player->SetFakeDisplay(0);
}

void CFBattlegroundMgr::MorphPlayerIfNeeded(Player* player, bool action/* = true*/)
{
    if (player->IsCrossfactionPlayer() && action)
    {
        uint32 display = player->GetFakeDisplay();
        ASSERT(display);
        player->SetNativeDisplayId(display);
    }
    else
        player->InitDisplayIds();

    player->RestoreDisplayId();
}

void CFBattlegroundMgr::FitPlayerInBattlegroundTeam(Player* player, bool action /*= true*/)
{
    if (action)
    {
        SetValidFakeInfo(player);
        player->setFactionForRace(player->getRace(true));
        MorphPlayerIfNeeded(player);
        player->ScheduleOnUpdateOperation(ON_UPDATE_RECACHE_BG_PLAYERS);

        bool isAlliance = player->GetBGTeam() == ALLIANCE;
        std::string icon = isAlliance ?
            "PvPRankBadges/PvPRankAlliance" :
            "PvPRankBadges/PvPRankHorde";
        sNotify->FlashMessage(player, GOSSIP_ICON_TEXT_EXTENDED("", icon, 60, 0, 0, ""));
        sNotify->PvPMessage(player, PVP_MSG_SELF, "You are playing for the %s.",
                            (isAlliance ? "|cff00afffALLIANCE|r" : "|cffff0000HORDE|r"));
    }
    else
    {
        ClearFakeInfo(player);
        player->setFactionForRace(player->getRace());
        MorphPlayerIfNeeded(player, false);
        player->ScheduleOnUpdateOperation(ON_UPDATE_RECACHE_RECACHED_PLAYERS);
    }
}

void CFBattlegroundMgr::ClearNameQueryRecipients(Player* player)
{
    WorldSession::TrackedNameQueryRecipients& tracked = player->GetSession()->GetTrackedNameQueryRecipients();
    for (auto const& itr : tracked)
    {
        if (Player* recipient = sObjectAccessor->FindPlayer(itr))
            recipient->GetSession()->SendNameQueryOpcode(player->GetGUID());
    }

    tracked.clear();
}

void CFBattlegroundMgr::UpdateBattlegroundPlayersNameQueryData(Player* player, Battleground* bg)
{
    for (auto const& itr : bg->GetPlayers())
    {
        player->GetSession()->SendNameQueryOpcode(itr.first, true);

        Player* bgPlayer = sObjectAccessor->FindPlayer(itr.first);
        if (bgPlayer && bgPlayer != player)
            bgPlayer->GetSession()->SendNameQueryOpcode(player->GetGUID(), true);
    }
}

bool CFBattlegroundMgr::SendBattlegroundChat(Player* player, ChatMsg msgtype, std::string const& message)
{
    // Select distance to broadcast to.
    float distance = msgtype == CHAT_MSG_YELL ?
        sWorld->getFloatConfig(CONFIG_LISTEN_RANGE_YELL) :
        sWorld->getFloatConfig(CONFIG_LISTEN_RANGE_SAY);

    Battleground* bg = player->GetBattleground();
    if (bg && !bg->isArena())
    {
        for (auto const& itr : bg->GetPlayers())
        {
            if (Player* bgPlayer = ObjectAccessor::FindPlayer(itr.first))
            {
                if (player->GetDistance2d(bgPlayer->GetPositionX(), bgPlayer->GetPositionY()) <= distance)
                {
                    WorldPacket data(SMSG_MESSAGECHAT, 200);

                    if (player->GetBGTeam() == bgPlayer->GetBGTeam())
                        BuildPlayerChat(player, &data, msgtype, message, LANG_UNIVERSAL);
                    else if (msgtype != CHAT_MSG_EMOTE)
                        BuildPlayerChat(player, &data, msgtype, message, bgPlayer->GetBGTeam() == ALLIANCE ? LANG_ORCISH : LANG_COMMON);

                    bgPlayer->SendDirectMessage(&data);
                }
            }
        }

        return true;
    }

    return false;
}

void CFBattlegroundMgr::BuildPlayerChat(Player* player, WorldPacket* data, uint8 msgtype, const std::string& text, uint32 language) const
{
    *data << uint8(msgtype);
    *data << uint32(language);
    *data << uint64(player->GetGUID());
    *data << uint32(0);
    *data << uint64(player->GetGUID());
    *data << uint32(text.length() + 1);
    *data << text;
    *data << uint8(player->GetChatTag());
}