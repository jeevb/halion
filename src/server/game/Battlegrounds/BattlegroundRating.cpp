/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "BattlegroundAV.h"
#include "BattlegroundWS.h"
#include "BattlegroundAB.h"
#include "BattlegroundEY.h"
#include "BattlegroundSA.h"
#include "BattlegroundIC.h"

#include "BattlegroundRating.h"
#include "Config.h"
#include "GossipMgr.h"
#include "NotificationMgr.h"
#include "Player.h"

BattlegroundRating::BattlegroundRating() : m_Enabled(false), m_RatingCoeff(0.0f)
{
}

void BattlegroundRating::Init()
{
    m_Enabled = sConfigMgr->GetBoolDefault("Battleground.Rating.Enable", false);
    m_RatingCoeff = sConfigMgr->GetFloatDefault("Battleground.Rating.Coefficient", 16.0f);
}

void BattlegroundRating::HandlePlayerLeaveBattleground(Battleground* bg, Battleground::BattlegroundPlayerMap::const_iterator itr)
{
    if (!m_Enabled ||
        !bg->isBattleground() ||
        bg->GetStatus() < STATUS_WAIT_JOIN ||
        bg->GetStatus() > STATUS_IN_PROGRESS)
        return;

    ObjectGuid guid = itr->first;
    if (Player* player = sObjectAccessor->FindPlayer(guid))
        if (player->IsGameMaster() || player->IsBeingSummonedByGM())
            return;

    int32 playerChange = int32(-m_RatingCoeff);
    int32 newPlayerRating = std::max(static_cast<int32>(itr->second.BGRating) + playerChange, STARTING_BG_RATING);

    SQLTransaction trans = CharacterDatabase.BeginTransaction();
    SetBattlegroundRating(guid, newPlayerRating, trans);
    CharacterDatabase.CommitTransaction(trans);
}

void BattlegroundRating::HandleEndBattleground(Battleground* bg, uint32 winner)
{
    if (!m_Enabled || !bg->isBattleground() || bg->IsEndedPrematurely() || !winner)
        return;

    uint32 allianceTeamRating = bg->GetTeamBgRatingForTeam(ALLIANCE);
    uint32 hordeTeamRating = bg->GetTeamBgRatingForTeam(HORDE);

    uint32 winnerScore = bg->GetTeamScore(bg->GetTeamIndexByTeamId(winner));
    uint32 loserScore = bg->GetTeamScore(bg->GetTeamIndexByTeamId(bg->GetOtherTeam(winner)));

    int32 winnerBonus = 0;
    GetWinnerTeamRatingBonus(bg, winner, winnerBonus);

    float winMargin = winnerScore ? float(winnerScore - loserScore) / float(winnerScore) : 0.0f;

    TopScores ts;
    GetBattlegroundTopScores(bg, ts);

    int32 allianceRatingChange = GetRatingChangeForTeam(allianceTeamRating, hordeTeamRating, winMargin, winnerBonus, winner == ALLIANCE);
    SetBGRatingForTeamPlayers(bg, ALLIANCE, allianceRatingChange, winner == ALLIANCE, ts);
    int32 hordeRatingChange = GetRatingChangeForTeam(hordeTeamRating, allianceTeamRating, winMargin, winnerBonus, winner == HORDE);
    SetBGRatingForTeamPlayers(bg, HORDE, hordeRatingChange, winner == HORDE, ts);

    // Log match to DB
    uint8 idx = 0;
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_BG_MATCH_LOG);
    stmt->setUInt32 (idx++, bg->GetStartTimeStamp());
    stmt->setUInt32 (idx++, bg->GetEndTimeStamp());
    stmt->setUInt32 (idx++, winner);
    stmt->setUInt32 (idx++, winnerScore);
    stmt->setUInt32 (idx++, loserScore);
    stmt->setUInt32 (idx++, allianceTeamRating);
    stmt->setUInt32 (idx++, hordeTeamRating);
    stmt->setInt32  (idx++, allianceRatingChange);
    stmt->setInt32  (idx++, hordeRatingChange);
    CharacterDatabase.Execute(stmt);
}

int32 BattlegroundRating::GetRatingChangeForTeam(uint32 ownRating, uint32 opponentRating, float winMargin, int32 winnerBonus, bool winner) const
{
    float chance = 1.0f / (1.0f + exp(log(10.0f) * (float(opponentRating) - float(ownRating)) / 650.0f));
    float winFactor = winner ? 1.0f : 0.0f;

    return int32(ceilf((winFactor - chance) * m_RatingCoeff * (1 + 0.25f * winMargin)) + winFactor * winnerBonus);
}

void BattlegroundRating::SetBGRatingForTeamPlayers(Battleground* bg, uint32 team, int32 teamChange, bool winner, TopScores const& ts)
{
    SQLTransaction trans = CharacterDatabase.BeginTransaction();

    uint32 bgDuration = bg->GetDuration();
    uint32 bgEndTimeStamp = bg->GetEndTimeStamp();

    for (auto const& itr : bg->GetPlayers())
    {
        if (itr.second.Team != team ||
            !IsEligibleForRatingChange(bgDuration,
                                       itr.second.TotalTime(bgEndTimeStamp),
                                       itr.second.PlayedTime(bgEndTimeStamp),
                                       winner))
            continue;

        int32 playerChange = teamChange;

        // Add rating-dependent multipliers for players in winning teams
        // Multipler decreases linearly from 5x to 1x between 1000 and 2000 rating and levels off at 1x at or above 2000 rating
        playerChange *= (winner && itr.second.BGRating < 2000 ? (-0.005 * itr.second.BGRating + 10.0f) : 1.0f);

        AddIndividualRatingBonus(bg, itr.first, ts, playerChange);
        int32 newPlayerRating = std::max(static_cast<int32>(itr.second.BGRating) + playerChange, STARTING_BG_RATING);
        SetBattlegroundRating(itr.first, newPlayerRating, trans);
    }

    CharacterDatabase.CommitTransaction(trans);
}

void BattlegroundRating::SetBattlegroundRating(ObjectGuid guid, uint32 rating, SQLTransaction& trans)
{
    if (Player* player = sObjectAccessor->FindPlayer(guid))
    {
        if (int32 change = rating - player->GetBattlegroundRating())
        {
            player->SetBattlegroundRating(rating);
            SyncBattlegroundRatingTokens(player);

            std::ostringstream buf;
            buf << "You have " << (change > 0 ? "gained |cff00ff00" : "lost |cffff0000")
                << abs(change) << "|r Battleground Rating. "
                << "Your new rating is "
                << (change > 0 ? "|cff00ff00" : "|cffff0000")
                << rating << "|r!";
            sNotify->PvPMessage(player, PVP_MSG_SELF, buf.str().c_str());

            UpdateKnownBGTitles(player);
        }
    }

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_REP_CHAR_BG_RATING);
    stmt->setUInt32(0, guid.GetCounter());
    stmt->setUInt32(1, rating);
    trans->Append(stmt);
}

bool BattlegroundRating::IsEligibleForRatingChange(uint32 bgDuration, uint32 totalTimeInBG, uint32 playedTimeInBG, bool winner) const
{
    // Players must have played 15% of the total length of the BG or
    // 3 minutes, whichever is more.
    uint32 requiredPlayedTime = std::max(uint32(0.15 * bgDuration), uint32(3 * MINUTE));
    // Winners cannot have been offline for more than 10% of their total time in the BG.
    return winner ?
        playedTimeInBG >= std::max(requiredPlayedTime, uint32(0.9 * totalTimeInBG)) :
        playedTimeInBG >= requiredPlayedTime;
}

void BattlegroundRating::GetBattlegroundTopScores(Battleground* bg, TopScores& ts)
{
    for (auto const& itr : bg->GetPlayerScores())
    {
        if (itr.second->KillingBlows > ts.topKillingBlows) ts.topKillingBlows = itr.second->KillingBlows;
        if (itr.second->HonorableKills > ts.topHonorableKills) ts.topHonorableKills = itr.second->HonorableKills;
        if (itr.second->DamageDone > ts.topDamageDone) ts.topDamageDone = itr.second->DamageDone;
        if (itr.second->HealingDone > ts.topHealingDone) ts.topHealingDone = itr.second->HealingDone;
    }
}

void BattlegroundRating::AddIndividualRatingBonus(Battleground* bg, ObjectGuid guid, TopScores const& ts, int32& change)
{
    BattlegroundScore const* score = bg->GetPlayerScore(guid.GetCounter());
    if (!score)
        return;

    // Top killing blows
    if (score->KillingBlows && score->KillingBlows == ts.topKillingBlows)
        change += 2;
    // Top honorable kills
    if (score->HonorableKills && score->HonorableKills == ts.topHonorableKills)
        change += 1;
    // Top damage done
    if (score->DamageDone && score->DamageDone == ts.topDamageDone)
        change += 2;
    // Top healing done
    if (score->HealingDone && score->HealingDone == ts.topHealingDone)
        change += 2;
    // No deaths
    if (score->Deaths == 0)
        change += 1;

    // Battleground-specific bonuses
    switch (bg->GetTypeID())
    {
        case BATTLEGROUND_AV: // Alterac Valley
        {
            if (uint32 graveyardsAssaulted = score->GetAttr1())
                change += graveyardsAssaulted;
            if (uint32 graveyardsDefended = score->GetAttr2())
                change += graveyardsDefended / 2;
            if (uint32 towersAssaulted = score->GetAttr3())
                change += towersAssaulted;
            if (uint32 towersDefended = score->GetAttr4())
                change += towersDefended / 2;
            if (uint32 minesCaptured = score->GetAttr5())
                change += minesCaptured;
            break;
        }
        case BATTLEGROUND_WS: // Warsong Gulch
        {
            if (uint32 flagsCaptured = score->GetAttr1())
                change += flagsCaptured;
            if (uint32 flagsReturned = score->GetAttr2())
                change += flagsReturned / 2;
            break;
        }
        case BATTLEGROUND_AB: // Arathi Basin
        {
            if (uint32 basesAssaulted = score->GetAttr1())
                change += basesAssaulted;
            if (uint32 basesDefended = score->GetAttr2())
                change += basesDefended / 2;
            break;
        }
        case BATTLEGROUND_EY: // Eye of the Storm
        {
            if (uint32 flagsCaptured = score->GetAttr1())
                change += flagsCaptured;
            break;
        }
        case BATTLEGROUND_SA: // Strand of the Ancients
        {
            if (uint32 demolishersDestroyed = score->GetAttr1())
                change += demolishersDestroyed / 2;
            if (uint32 gatesDestroyed = score->GetAttr2())
                change += gatesDestroyed;
            break;
        }
        case BATTLEGROUND_IC: // Isle of Conquest
        {
            if (uint32 basesAssaulted = score->GetAttr1())
                change += basesAssaulted;
            if (uint32 basesDefended = score->GetAttr2())
                change += basesDefended / 2;
            break;
        }
        default:
            break;
    }
}

void BattlegroundRating::GetWinnerTeamRatingBonus(Battleground* bg, uint32 team, int32& bonus)
{
    // Calculate match length bonuses
    uint32 bgDuration = bg->GetDuration();

    if (bgDuration <= 8 * MINUTE)
        bonus += 3;
    else if (bgDuration <= 12 * MINUTE)
        bonus += 2;
    else if (bgDuration <= 16 * MINUTE)
        bonus += 1;

    // Calculate bonuses for ending match with maximum bases possible
    // Applies to Arathi Basin, Eye of the Storm and Isle of Conquest only
    if (bg->IsAllNodesControlledByTeam(team))
        bonus += 3;

    // Calculate bonus for no deaths in team
    if (!bg->GetPlayerDeathsForTeam(team))
        bonus += 5;
}

void BattlegroundRating::SyncBattlegroundRatingTokens(Player* player)
{
    uint32 rating = player->GetBattlegroundRating();
    uint32 tokenCount = player->GetItemCount(BG_RATING_TOKEN, false);

    if (tokenCount == rating)
        return;

    if (rating > tokenCount)
    {
        player->SetHideAddItem();
        player->AddItem(BG_RATING_TOKEN, rating - tokenCount);
        player->SetHideAddItem(false);
    }
    else
        player->DestroyItemCount(BG_RATING_TOKEN, tokenCount - rating, true, false);
}

void BattlegroundRating::UpdateKnownBGTitles(Player* player)
{
    bool isAlliance = player->GetTeam() == ALLIANCE;
    uint32 rating = player->GetBattlegroundRating();

    // Iterate over all BG ranks and award titles if rating requirements are met
    bool newRankAchieved = false;
    for (uint8 i = BG_RANK_14; i > BG_RANK_00; --i)
    {
        BGRankTitle const& rt = BGRankTitles[i];

        if (rating < rt.rating)
            continue;

        uint64 newTitleId = isAlliance ? rt.alliance : rt.horde;
        if (!player->HasTitle(newTitleId))
        {
            CharTitlesEntry const* titleInfo = sCharTitlesStore.LookupEntry(newTitleId);
            if (!titleInfo)
                continue;

            player->SetTitle(titleInfo);

            if (newRankAchieved)
                continue;

            // Send notification of new title
            std::ostringstream buf;
            buf << GOSSIP_ICON_TEXT_NOTIFICATION("|cff1eff00New rank achieved - |r", rt.icon)
                << titleInfo->nameMale[0];
            sNotify->FlashMessage(player, buf.str());

            // If a title was previously set, update it to reflect the new rank
            if (player->GetUInt32Value(PLAYER_CHOSEN_TITLE))
                player->SetUInt32Value(PLAYER_CHOSEN_TITLE, newTitleId);

            newRankAchieved = true;
        }
    }
}