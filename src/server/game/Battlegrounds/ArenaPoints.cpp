/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "ArenaPoints.h"
#include "ArenaTeam.h"
#include "Language.h"
#include "NotificationMgr.h"
#include "ObjectMgr.h"
#include "World.h"
#include "WorldSession.h"

ArenaPoints::ArenaPoints()
{
}

void ArenaPoints::AwardArenaPoints(Player* player, uint8 teamType, int32 ratingMod)
{
    // Get player's weekly cap
    uint32 cap = _GetWeeklyCap(player);

    // Get player's existing weekly points count
    uint32 weeklyPoints = player->GetWeeklyArenaPoints();

    // Calculate the number of arena points the player can receive
    uint32 arenaPoints = std::min<uint32>(std::max<uint32>(0, cap - weeklyPoints), 48 + 96 * ratingMod / 24.0f);

    // Modifiers for specific arena slots
    switch (teamType)
    {
        case ARENA_TEAM_2v2:
            arenaPoints *= 0.76f;
            break;
        case ARENA_TEAM_3v3:
            arenaPoints *= 0.88f;
            break;
        default:
            break;
    }

    // Modify player's arena points
    player->ModifyArenaPoints(arenaPoints);

    // Modify player's weekly points count
    weeklyPoints += arenaPoints;
    _SetWeeklyArenaPoints(player, weeklyPoints);

    // Notifications
    sNotify->PvPMessage(player, PVP_MSG_SELF, sObjectMgr->GetTrinityStringForDBCLocale(LANG_ARENA_POINTS_GAINED), arenaPoints);
    sNotify->PvPMessage(player, PVP_MSG_SELF, sObjectMgr->GetTrinityStringForDBCLocale(LANG_ARENA_POINTS_CAP), cap - weeklyPoints);
}

void ArenaPoints::ResetWeeklyArenaPoints()
{
    // Reset weekly arena points for all online players
    for (auto const& itr : sWorld->GetAllSessions())
        if (Player* player = itr.second->GetPlayer())
            player->SetWeeklyArenaPoints(0);

    // Reset weekly arena points in database
    CharacterDatabase.Execute(CharacterDatabase.GetPreparedStatement(CHAR_UPD_CHAR_RESET_WEEKLY_ARENA_POINTS));
}

uint32 ArenaPoints::_GetWeeklyCap(Player* player)
{
    // Derive weekly cap from the highest personal rating across all brackets
    uint32 highestRating = DEFAULT_ARENA_TEAM_RATING;

    if (highestRating < player->GetArenaPersonalRating(ARENA_SLOT_2v2))
        highestRating = player->GetArenaPersonalRating(ARENA_SLOT_2v2);

    if (highestRating < player->GetArenaPersonalRating(ARENA_SLOT_3v3))
        highestRating = player->GetArenaPersonalRating(ARENA_SLOT_3v3);

    if (highestRating < player->GetArenaPersonalRating(ARENA_SLOT_5v5))
        highestRating = player->GetArenaPersonalRating(ARENA_SLOT_5v5);

    // 2000 (base cap at or below 1500 rating) + 20 for each rating point over 1500
    return 2000 + 20 * (highestRating - 1500);
}

void ArenaPoints::_SetWeeklyArenaPoints(Player* player, uint32 weeklyArenaPoints)
{
    player->SetWeeklyArenaPoints(weeklyArenaPoints);

    // Save to DB
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_REP_CHAR_WEEKLY_ARENA_POINTS);
    stmt->setUInt32(0, player->GetGUIDLow());
    stmt->setUInt32(1, weeklyArenaPoints);
    CharacterDatabase.Execute(stmt);
}