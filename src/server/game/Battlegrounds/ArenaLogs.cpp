/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "ArenaLogs.h"

ArenaLogs::ArenaLogs() : m_NextLogId(0)
{
}

void ArenaLogs::Init()
{
    // Get last arena match log id from database
    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_SEL_MAX_ARENA_LOG_ID);
    if (PreparedQueryResult result = CharacterDatabase.Query(stmt))
        m_NextLogId = result->Fetch()[0].GetUInt32();
}

uint32 ArenaLogs::GenerateLogId()
{
    return ++m_NextLogId;
}

void ArenaLogs::LogArenaMatchDetails(uint32 logId,
                                     uint8 arenaType,
                                     uint32 winner,
                                     uint32 loser,
                                     uint32 elapsedTime,
                                     uint32 winnerRating,
                                     int32 winnerChange,
                                     uint32 loserRating,
                                     int32 loserChange,
                                     uint32 winnerMMR,
                                     int32 winnerMMRChange,
                                     uint32 loserMMR,
                                     int32 loserMMRChange,
                                     uint32 seasonId,
                                     SQLTransaction& trans)
{
    uint8 idx = 0;

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_ARENA_MATCH_DETAILS);

    stmt->setUInt32 (idx++, logId);
    stmt->setUInt32 (idx++, arenaType);
    stmt->setUInt32 (idx++, winner);
    stmt->setUInt32 (idx++, loser);
    stmt->setUInt32 (idx++, elapsedTime);
    stmt->setUInt32 (idx++, winnerRating);
    stmt->setInt32  (idx++, winnerChange);
    stmt->setUInt32 (idx++, loserRating);
    stmt->setInt32  (idx++, loserChange);
    stmt->setUInt32 (idx++, winnerMMR);
    stmt->setInt32  (idx++, winnerMMRChange);
    stmt->setUInt32 (idx++, loserMMR);
    stmt->setInt32  (idx++, loserMMRChange);
    stmt->setUInt32 (idx++, seasonId);

    trans->Append(stmt);
}

void ArenaLogs::LogArenaMatchPlayerScore(uint32 logId,
                                         uint32 guid,
                                         uint32 arenaTeamId,
                                         std::string const& ipAddr,
                                         uint32 damage,
                                         uint32 healing,
                                         uint32 killingBlows,
                                         uint32 deaths,
                                         SQLTransaction& trans)
{
    uint8 idx = 0;

    PreparedStatement* stmt = CharacterDatabase.GetPreparedStatement(CHAR_INS_ARENA_MATCH_PLAYER_SCORE);

    stmt->setUInt32 (idx++, logId);
    stmt->setUInt32 (idx++, guid);
    stmt->setUInt32 (idx++, arenaTeamId);
    stmt->setString (idx++, ipAddr);
    stmt->setUInt32 (idx++, damage);
    stmt->setUInt32 (idx++, healing);
    stmt->setUInt32 (idx++, killingBlows);
    stmt->setUInt32 (idx++, deaths);

    trans->Append(stmt);
}