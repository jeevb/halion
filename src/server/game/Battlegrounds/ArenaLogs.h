/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

 #ifndef CG_ARENALOGS_H
 #define CG_ARENALOGS_H

class ArenaLogs
{
    private:
        ArenaLogs();
        ~ArenaLogs() { }

    public:

        static ArenaLogs* instance()
        {
            static ArenaLogs instance;
            return &instance;
        }

        void Init();

        uint32 GenerateLogId();

        // Logging methods
        void LogArenaMatchDetails(uint32 logId,
                                  uint8 arenaType,
                                  uint32 winner,
                                  uint32 loser,
                                  uint32 elapsedTime,
                                  uint32 winnerRating,
                                  int32 winnerChange,
                                  uint32 loserRating,
                                  int32 loserChange,
                                  uint32 winnerMMR,
                                  int32 winnerMMRChange,
                                  uint32 loserMMR,
                                  int32 loserMMRChange,
                                  uint32 seasonId,
                                  SQLTransaction& trans);
        void LogArenaMatchPlayerScore(uint32 logId,
                                      uint32 guid,
                                      uint32 arenaTeamId,
                                      std::string const& ipAddr,
                                      uint32 damage,
                                      uint32 healing,
                                      uint32 killingBlows,
                                      uint32 deaths,
                                      SQLTransaction& trans);

    private:

        uint32 m_NextLogId;
 };

 #define sArenaLogs ArenaLogs::instance()

 #endif