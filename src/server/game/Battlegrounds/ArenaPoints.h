/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_ARENAPOINTS_H
#define CG_ARENAPOINTS_H

#define DEFAULT_ARENA_TEAM_RATING 1500

class ArenaPoints
{
    private:
        ArenaPoints();
        ~ArenaPoints() { }

    public:

        static ArenaPoints* instance()
        {
            static ArenaPoints instance;
            return &instance;
        }

        void Init() { }

        void AwardArenaPoints(Player* player, uint8 teamType, int32 ratingMod);
        void ResetWeeklyArenaPoints();

    private:

        uint32 _GetWeeklyCap(Player* player);
        void _SetWeeklyArenaPoints(Player* player, uint32 weeklyArenaPoints);
};

#define sArenaPoints ArenaPoints::instance()

#endif