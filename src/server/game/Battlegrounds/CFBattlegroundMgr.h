/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_CFBGMGR_H
#define CG_CFBGMGR_H

 enum FakeRaces
 {
     FAKE_RACE_HUMAN,
     FAKE_RACE_ORC,
     FAKE_RACE_DWARF,
     FAKE_RACE_NIGHTELF,
     FAKE_RACE_UNDEAD_PLAYER,
     FAKE_RACE_TAUREN,
     FAKE_RACE_GNOME,
     FAKE_RACE_TROLL,
     FAKE_RACE_BLOODELF,
     FAKE_RACE_DRAENEI,

     MAX_FAKE_RACE
 };

 struct FakeDisplay
 {
     uint8 raceId;
     uint32 team;
     uint32 maleMorph;
     uint32 femaleMorph;

     FakeDisplay(const uint8 _raceId, const uint32 _team, const uint32 _maleMorph, const uint32 _femaleMorph) :
         raceId(_raceId), team(_team), maleMorph(_maleMorph), femaleMorph(_femaleMorph) { }
 };

 const FakeDisplay FakeDisplays[MAX_FAKE_RACE] =
 {
     FakeDisplay(RACE_HUMAN, HORDE, 19723, 19724),
     FakeDisplay(RACE_ORC, ALLIANCE, 0, 20316),
     FakeDisplay(RACE_DWARF, HORDE, 20317, 0),
     FakeDisplay(RACE_NIGHTELF, HORDE, 20318, 20318),             // NightElf female morph copied from male (for druids)
     FakeDisplay(RACE_UNDEAD_PLAYER, ALLIANCE, 0, 0),
     FakeDisplay(RACE_TAUREN, ALLIANCE, 20585, 20584),
     FakeDisplay(RACE_GNOME, HORDE, 20580, 20581),
     FakeDisplay(RACE_TROLL, ALLIANCE, 20321, 0),
     FakeDisplay(RACE_BLOODELF, ALLIANCE, 20578, 20579),
     FakeDisplay(RACE_DRAENEI, HORDE, 20323, 20323)               // Draenei male morph copied from female (for shamans)
 };

class CFBattlegroundMgr
{
    private:
        CFBattlegroundMgr();
        ~CFBattlegroundMgr() { }

    public:

        static CFBattlegroundMgr* instance()
        {
            static CFBattlegroundMgr instance;
            return &instance;
        }

        void Init();

        bool IsEnabled() const { return m_Enabled; }

        void SetValidFakeInfo(Player* player);
        void ClearFakeInfo(Player* player);
        void MorphPlayerIfNeeded(Player* player, bool action = true);
        void FitPlayerInBattlegroundTeam(Player* player, bool action = true);

        void ClearNameQueryRecipients(Player* player);
        void UpdateBattlegroundPlayersNameQueryData(Player* player, Battleground* bg);

        bool SendBattlegroundChat(Player* player, ChatMsg msgtype, std::string const& message);
        void BuildPlayerChat(Player* player, WorldPacket* data, uint8 msgtype, const std::string& text, uint32 language) const;

    private:

        bool m_Enabled;
};

#define sCFBattlegroundMgr CFBattlegroundMgr::instance()

#endif