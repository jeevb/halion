/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_BGRATING_H
#define CG_BGRATING_H

#include "Battleground.h"

#define STARTING_BG_RATING  1000
#define BG_RATING_TOKEN     37711

struct TopScores
{
    TopScores() : topKillingBlows(0), topHonorableKills(0), topDamageDone(0), topHealingDone(0) { }

    uint32 topKillingBlows;
    uint32 topHonorableKills;
    uint32 topDamageDone;
    uint32 topHealingDone;
};

enum BattlegroundRank
{
    BG_RANK_00,
    BG_RANK_01,
    BG_RANK_02,
    BG_RANK_03,
    BG_RANK_04,
    BG_RANK_05,
    BG_RANK_06,
    BG_RANK_07,
    BG_RANK_08,
    BG_RANK_09,
    BG_RANK_10,
    BG_RANK_11,
    BG_RANK_12,
    BG_RANK_13,
    BG_RANK_14,

    MAX_BG_RANK
};

enum BattlegroundTitle
{
    BG_TITLE_NONE,
    BG_TITLE_PRIVATE,
    BG_TITLE_CORPORAL,
    BG_TITLE_SERGEANT_A,
    BG_TITLE_MASTER_SERGEANT,
    BG_TITLE_SERGEANT_MAJOR,
    BG_TITLE_KNIGHT,
    BG_TITLE_KNIGHT_LIEUTENANT,
    BG_TITLE_KNIGHT_CAPTAIN,
    BG_TITLE_KNIGHT_CHAMPION,
    BG_TITLE_LIEUTENANT_COMMANDER,
    BG_TITLE_COMMANDER,
    BG_TITLE_MARSHAL,
    BG_TITLE_FIELD_MARSHAL,
    BG_TITLE_GRAND_MARSHAL,
    BG_TITLE_SCOUT,
    BG_TITLE_GRUNT,
    BG_TITLE_SERGEANT_H,
    BG_TITLE_SENIOR_SERGEANT,
    BG_TITLE_FIRST_SERGEANT,
    BG_TITLE_STONE_GUARD,
    BG_TITLE_BLOOD_GUARD,
    BG_TITLE_LEGIONNAIRE,
    BG_TITLE_CENTURION,
    BG_TITLE_CHAMPION,
    BG_TITLE_LIEUTENANT_GENERAL,
    BG_TITLE_GENERAL,
    BG_TITLE_WARLORD,
    BG_TITLE_HIGH_WARLORD
};

struct BGRankTitle
{
    std::string icon;
    uint32      rating;
    uint64      alliance;
    uint64      horde;

    BGRankTitle(std::string const& _icon, uint32 _rating, uint64 _alliance, uint64 _horde) :
        icon(_icon), rating(_rating), alliance(_alliance), horde(_horde) { }
};

const BGRankTitle BGRankTitles[MAX_BG_RANK] =
{
    BGRankTitle("",                         0,      BG_TITLE_NONE,                 BG_TITLE_NONE),
    BGRankTitle("PvPRankBadges/PvPRank01",  1250,   BG_TITLE_PRIVATE,              BG_TITLE_SCOUT),
    BGRankTitle("PvPRankBadges/PvPRank02",  1500,   BG_TITLE_CORPORAL,             BG_TITLE_GRUNT),
    BGRankTitle("PvPRankBadges/PvPRank03",  1750,   BG_TITLE_SERGEANT_A,           BG_TITLE_SERGEANT_H),
    BGRankTitle("PvPRankBadges/PvPRank04",  2000,   BG_TITLE_MASTER_SERGEANT,      BG_TITLE_SENIOR_SERGEANT),
    BGRankTitle("PvPRankBadges/PvPRank05",  2250,   BG_TITLE_SERGEANT_MAJOR,       BG_TITLE_FIRST_SERGEANT),
    BGRankTitle("PvPRankBadges/PvPRank06",  2500,   BG_TITLE_KNIGHT,               BG_TITLE_STONE_GUARD),
    BGRankTitle("PvPRankBadges/PvPRank07",  2750,   BG_TITLE_KNIGHT_LIEUTENANT,    BG_TITLE_BLOOD_GUARD),
    BGRankTitle("PvPRankBadges/PvPRank08",  3000,   BG_TITLE_KNIGHT_CAPTAIN,       BG_TITLE_LEGIONNAIRE),
    BGRankTitle("PvPRankBadges/PvPRank09",  3250,   BG_TITLE_KNIGHT_CHAMPION,      BG_TITLE_CENTURION),
    BGRankTitle("PvPRankBadges/PvPRank10",  3500,   BG_TITLE_LIEUTENANT_COMMANDER, BG_TITLE_CHAMPION),
    BGRankTitle("PvPRankBadges/PvPRank11",  3750,   BG_TITLE_COMMANDER,            BG_TITLE_LIEUTENANT_GENERAL),
    BGRankTitle("PvPRankBadges/PvPRank12",  4000,   BG_TITLE_MARSHAL,              BG_TITLE_GENERAL),
    BGRankTitle("PvPRankBadges/PvPRank13",  4250,   BG_TITLE_FIELD_MARSHAL,        BG_TITLE_WARLORD),
    BGRankTitle("PvPRankBadges/PvPRank14",  4500,   BG_TITLE_GRAND_MARSHAL,        BG_TITLE_HIGH_WARLORD)
};

class BattlegroundRating
{
    private:
        BattlegroundRating();
        ~BattlegroundRating() { }

    public:

        static BattlegroundRating* instance()
        {
            static BattlegroundRating instance;
            return &instance;
        }

        void Init();

        void HandlePlayerLeaveBattleground(Battleground* bg, Battleground::BattlegroundPlayerMap::const_iterator itr);
        void HandleEndBattleground(Battleground* bg, uint32 winner);

        int32 GetRatingChangeForTeam(uint32 ownRating, uint32 opponentRating, float winMargin, int32 winnerBonus, bool winner) const;
        void SetBGRatingForTeamPlayers(Battleground* bg, uint32 team, int32 teamChange, bool winner, TopScores const& ts);
        void SetBattlegroundRating(ObjectGuid guid, uint32 rating, SQLTransaction& trans);
        bool IsEligibleForRatingChange(uint32 bgDuration, uint32 totalTimeInBG, uint32 playedTimeInBG, bool winner) const;
        void GetBattlegroundTopScores(Battleground* bg, TopScores& ts);
        void AddIndividualRatingBonus(Battleground* bg, ObjectGuid guid, TopScores const& ts, int32& change);
        void GetWinnerTeamRatingBonus(Battleground* bg, uint32 team, int32& bonus);

        void SyncBattlegroundRatingTokens(Player* player);
        void UpdateKnownBGTitles(Player* player);

    private:

        bool m_Enabled;
        float m_RatingCoeff;
};

#define sBgRating BattlegroundRating::instance()

#endif