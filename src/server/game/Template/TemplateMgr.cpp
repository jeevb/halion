/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "TemplateMgr.h"

TemplateMgr::TemplateMgr()
{
}

void TemplateMgr::Init()
{
    _LoadPresetTemplates();
    _LoadEquipmentTemplates();
}

PresetTemplate const* TemplateMgr::GetPresetTemplate(uint32 id) const
{
    auto itr = _PresetTemplateStore.find(id);
    return itr == _PresetTemplateStore.end() ? nullptr : &itr->second;
}

std::string TemplateMgr::GetIcon(uint32 id) const
{
    return GetIcon(GetPresetTemplate(id));
}

std::string TemplateMgr::GetIcon(PresetTemplate const* preset) const
{
    if (preset)
        if (EquipmentTemplate const* eTemplate = GetEquipmentTemplate(preset->equipmentTemplate))
            return _GetRandomSetIcon(eTemplate);

    return std::string();
}

bool TemplateMgr::IsValidPresetTemplateForPlayer(Player const* player, uint32 id) const
{
    if (PresetTemplate const* preset = GetPresetTemplate(id))
        return IsValidPresetTemplateForPlayer(player, preset);

    return false;
}

bool TemplateMgr::IsValidPresetTemplateForPlayer(Player const* player, PresetTemplate const* preset) const
{
    if (preset->raceMask && (preset->raceMask & player->getRaceMask()) == 0)
        return false;

    if (preset->classMask && (preset->classMask & player->getClassMask()) == 0)
        return false;

    if (preset->trackMask && (preset->trackMask & player->getTrackMask()) == 0)
        return false;

    return true;
}

EquipmentTemplate const* TemplateMgr::GetEquipmentTemplate(uint32 id) const
{
    auto itr = _EquipmentTemplateStore.find(id);
    return itr == _EquipmentTemplateStore.end() ? nullptr : &itr->second;
}

void TemplateMgr::ApplyPresetTemplate(Player* player, uint32 id)
{
    return ApplyPresetTemplate(player, GetPresetTemplate(id));
}

void TemplateMgr::ApplyPresetTemplate(Player* player, PresetTemplate const* preset)
{
    if (!preset || !IsValidPresetTemplateForPlayer(player, preset))
        return;

    // Store percentage of player's health/mana to be restored upon applying preset
    float healthPct = player->GetHealthPct();
    float manaPct = player->getPowerType() == POWER_MANA && player->GetMaxPower(POWER_MANA) ? 100.f * player->GetPower(POWER_MANA) / player->GetMaxPower(POWER_MANA) : 0.0f;

    player->SetHideAddItem();
    if (EquipmentTemplate const* eTemplate = GetEquipmentTemplate(preset->equipmentTemplate))
        for (uint8 i = EQUIPMENT_SLOT_HEAD; i < EQUIPMENT_SLOT_END; ++i)
            if (ItemTemplate const* proto = sObjectMgr->GetItemTemplate((*eTemplate)[i]))
                _AddOrEquipItem(player, proto, i);
    player->SetHideAddItem(false);

    // Restore player's health/mana to the above percentage
    player->SetHealth(player->GetMaxHealth() * healthPct / 100.0f);
    player->SetPower(POWER_MANA, player->GetMaxPower(POWER_MANA) * manaPct / 100.0f);
}

void TemplateMgr::SaveEquipmentTemplate(Player const* player)
{
    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_INS_PRESET_EQUIPMENT_TEMPLATE);

    for (uint8 slot = EQUIPMENT_SLOT_HEAD; slot < EQUIPMENT_SLOT_END; ++slot)
    {
        Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, slot);
        stmt->setUInt32(slot, item ? item->GetEntry() : 0);
    }

    WorldDatabase.Execute(stmt);
}

void TemplateMgr::_LoadPresetTemplates()
{
    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_PRESET_TEMPLATES);
    if (PreparedQueryResult result = WorldDatabase.Query(stmt))
    {
        do {
            Field* fields = result->Fetch();

            uint8 idx = 0;

            PresetTemplate& i   = _PresetTemplateStore[fields[idx++].GetUInt32()];
            i.description       = fields[idx++].GetString();
            i.raceMask          = fields[idx++].GetUInt32();
            i.classMask         = fields[idx++].GetUInt32();
            i.trackMask         = fields[idx++].GetUInt32();
            i.equipmentTemplate = fields[idx++].GetUInt32();
        } while (result->NextRow());
    }

    TC_LOG_INFO("server.loading", ">> Loaded %u preset template entries in %u ms",
                uint32(_PresetTemplateStore.size()), GetMSTimeDiffToNow(oldMSTime));
}

void TemplateMgr::_LoadEquipmentTemplates()
{
    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_PRESET_EQUIPMENT_TEMPLATES);
    if (PreparedQueryResult result = WorldDatabase.Query(stmt))
    {
        do {
            Field* fields = result->Fetch();

            EquipmentTemplate& i = _EquipmentTemplateStore[fields[0].GetUInt32()];
            for (uint8 slot = EQUIPMENT_SLOT_HEAD; slot < EQUIPMENT_SLOT_END; ++slot)
            {
                uint32 itemId = fields[slot + 1].GetUInt32();
                i.push_back(sObjectMgr->GetItemTemplate(itemId) ? itemId : 0);
            }
        } while (result->NextRow());
    }

    TC_LOG_INFO("server.loading", ">> Loaded %u preset equipment template entries in %u ms",
                uint32(_EquipmentTemplateStore.size()), GetMSTimeDiffToNow(oldMSTime));
}

void TemplateMgr::_AddOrEquipItem(Player* player, ItemTemplate const* proto, uint8 slot, uint32 count/* = 1*/)
{
    bool canEquipInSlot = player->CanEquipInSlot(proto, slot);

    // Attempt equip item one at a time
    while (canEquipInSlot && count > 0)
    {
        uint16 dest;
        if (player->CanEquipNewItem(slot, dest, proto->ItemId, false) != EQUIP_ERR_OK)
            break;

        player->EquipNewItem(dest, proto->ItemId, true);
        player->AutoUnequipOffhandIfNeed();
        --count;
    }

    // All items equipped
    if (!count)
        return;

    // Attempt to store item
    ItemPosCountVec dest;
    if (player->CanStoreNewItem(NULL_BAG, NULL_SLOT, dest, proto->ItemId, count) != EQUIP_ERR_OK)
    {
        // Mail items that cannot be stored
        player->SendItemRetrievalMail(proto->ItemId, count);
    }
    else if (Item* tItem = player->StoreNewItem(dest, proto->ItemId, true, Item::GenerateItemRandomPropertyId(proto->ItemId)))
    {
        player->SendNewItem(tItem, count, true, false);

        if (proto->Bonding == BIND_WHEN_PICKED_UP ||
            proto->Bonding == BIND_WHEN_EQUIPED ||
            proto->Bonding == BIND_WHEN_USE)
            tItem->SetBinding(true);

        tItem->SetNotRefundable(player);
        tItem->ClearSoulboundTradeable(player);

        if (!canEquipInSlot)
            return;

        // Swap to template item if this is better than currently equipped item
        if (Item* sItem = player->GetItemByPos(INVENTORY_SLOT_BAG_0, slot))
            if (sItem->GetTemplate()->ItemLevel <= tItem->GetTemplate()->ItemLevel)
                player->SwapItem(tItem->GetPos(), sItem->GetPos());
    }
}

std::string TemplateMgr::_GetRandomSetIcon(EquipmentTemplate const* eTemplate) const
{
    std::vector<ItemTemplate const*> items;
    for (auto const& itr : *eTemplate)
        if (ItemTemplate const* proto = sObjectMgr->GetItemTemplate(itr))
            items.push_back(proto);

    auto randIt = items.begin();
    std::advance(randIt, urand(0, items.size() - 1));
    return _GetItemIcon(*randIt);
}

std::string TemplateMgr::_GetItemIcon(ItemTemplate const* proto) const
{
    if (ItemDisplayInfoEntry const* displayInfo = sItemDisplayInfoStore.LookupEntry(proto->DisplayInfoID))
        return "ICONS/" + std::string(displayInfo->inventoryIcon);
    else
        return "ICONS/INV_Misc_QuestionMark";
}