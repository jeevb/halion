/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_TEMPLATEMGR_H
#define CG_TEMPLATEMGR_H

// Presets
struct PresetTemplate
{
    std::string     description;

    uint32          raceMask;
    uint32          classMask;
    uint32          trackMask;

    uint32          equipmentTemplate;
    // uint32          enchantmentTemplate;
    // uint32          talentTemplate;
    // uint32          glyphTemplate;
};
typedef std::unordered_map<uint32, PresetTemplate> PresetTemplateStore;

// Equipment Templates
typedef std::vector<uint32> EquipmentTemplate;
typedef std::unordered_map<uint32, EquipmentTemplate> EquipmentTemplateStore;

class TemplateMgr
{
    private:
        TemplateMgr();
        ~TemplateMgr() { }

    public:

        static TemplateMgr* instance()
        {
            static TemplateMgr instance;
            return &instance;
        }

        void Init();

        PresetTemplate const* GetPresetTemplate(uint32 id) const;
        PresetTemplateStore const& GetPresetTemplates() const { return _PresetTemplateStore; }

        std::string GetIcon(uint32 id) const;
        std::string GetIcon(PresetTemplate const* preset) const;

        bool IsValidPresetTemplateForPlayer(Player const* player, uint32 id) const;
        bool IsValidPresetTemplateForPlayer(Player const* player, PresetTemplate const* preset) const;

        EquipmentTemplate const* GetEquipmentTemplate(uint32 id) const;

        void ApplyPresetTemplate(Player* player, uint32 id);
        void ApplyPresetTemplate(Player* player, PresetTemplate const* preset);

        // Helpers for generating preset templates
        void SaveEquipmentTemplate(Player const* player);

    private:

        void _LoadPresetTemplates();
        void _LoadEquipmentTemplates();

        void _AddOrEquipItem(Player* player, ItemTemplate const* proto, uint8 slot, uint32 count = 1);

        std::string _GetRandomSetIcon(EquipmentTemplate const* eTemplate) const;
        std::string _GetItemIcon(ItemTemplate const* proto) const;

        PresetTemplateStore _PresetTemplateStore;
        EquipmentTemplateStore _EquipmentTemplateStore;
};

#define sTemplateMgr TemplateMgr::instance()

#endif