/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include <boost/algorithm/string/join.hpp>

#include "ArenaTeam.h"
#include "BattlegroundRating.h"
#include "Chat.h"
#include "GossipMgr.h"
#include "WorldSession.h"

CurrencyMgr::CurrencyMgr()
{
}

void CurrencyMgr::Init()
{
    _LoadCustomCosts();
    _LoadCustomNpcVendors();
}

ItemCustomCost const* CurrencyMgr::GetItemCustomCostById(uint32 customCostId) const
{
    if (customCostId)
    {
        // Custom cost exists
        auto itr = m_ItemCustomCostMap.find(customCostId);
        if (itr != m_ItemCustomCostMap.end())
            return &itr->second;
    }

    return nullptr;
}

uint32 CurrencyMgr::GetVendorItemCustomCostId(uint32 entry, uint32 item) const
{
    auto itr = m_CustomNpcVendorItemCostMap.find(std::make_pair(entry, item));
    if (itr != m_CustomNpcVendorItemCostMap.end())
        return itr->second;

    return 0;
}

bool CurrencyMgr::HasItemWithCustomCost(uint32 entry) const
{
    return m_CustomNpcVendorEntries.find(entry) != m_CustomNpcVendorEntries.end();
}

std::string CurrencyMgr::GetCustomCostDescription(Player* player, uint32 customCostId) const
{
    ItemCustomCost const* cost = GetItemCustomCostById(customCostId);
    if (!cost)
        return "";

    std::ostringstream buf;

    if (cost->costHonor)
    {
        buf << GOSSIP_ICON_TEXT_EXTENDED("", player->GetTeam() == ALLIANCE ? "PVPFrame/PVP-Currency-Alliance" : "PVPFrame/PVP-Currency-Horde", 16, 0, 0, " ")
            << "|cffffffff" << cost->costHonor << " |cffffffff[Honor Points]|r\n";
    }

    if (cost->costArena)
    {
        buf << GOSSIP_ICON_TEXT_EXTENDED("", "PVPFrame/PVP-ArenaPoints-Icon", 16, 0, 0, " ")
            << "|cffffffff" << cost->costArena << " [Arena Points]|r\n";
    }

    if (cost->requiredItem && cost->itemCount)
    {
        if (ItemTemplate const* proto = sObjectMgr->GetItemTemplate(cost->requiredItem))
            buf << "|cffffffff" << cost->itemCount << "|r " << proto->ItemLink() << "\n";
    }

    if (cost->required2v2Rating)
    {
        buf << "|cffffffff" << cost->required2v2Rating << "|r Personal and Team 2v2 Rating\n";
    }

    if (cost->required3v3Rating)
    {
        buf << "|cffffffff" << cost->required3v3Rating << "|r Personal and Team 3v3 Rating\n";
    }

    if (cost->required5v5Rating)
    {
        buf << "|cffffffff" << cost->required5v5Rating << "|r Personal and Team 5v5 Rating\n";
    }

    if (cost->requiredBGRating)
    {
        if (ItemTemplate const* proto = sObjectMgr->GetItemTemplate(BG_RATING_TOKEN))
            buf << "|cffffffff" << cost->requiredBGRating << "|r " << proto->ItemLink() << "\n";
    }

    if (cost->costVotePoints)
    {
        buf << "|cffffffff" << cost->costVotePoints << "|r " << _GetVotePointsItemLink() << "\n";
    }

    if (cost->costDonationPoints)
    {
        buf << "|cffffffff" << cost->costDonationPoints << "|r " << _GetDonationPointsItemLink() << "\n";
    }

    return buf.str();
}

bool CurrencyMgr::CanAffordCustomCost(Player* player, uint32 customCostId) const
{
    ItemCustomCost const* cost = GetItemCustomCostById(customCostId);
    if (!cost)
        return true;

    // honor points price
    if (cost->costHonor && player->GetHonorPoints() < cost->costHonor)
        return false;

    // arena points price
    if (cost->costArena && player->GetArenaPoints() < cost->costArena)
        return false;

    // item base price
    if (cost->requiredItem && !player->HasItemCount(cost->requiredItem, cost->itemCount))
        return false;

    // check for personal/team arena rating and/or BG rating requirement
    bool meetsPvPRatingRequirement = (!cost->required2v2Rating && !cost->required3v3Rating && !cost->required5v5Rating && !cost->requiredBGRating) ||
        (cost->required2v2Rating && std::min(player->GetArenaPersonalRating(ARENA_SLOT_2v2), player->GetArenaTeamRating(ARENA_SLOT_2v2)) >= cost->required2v2Rating) ||
        (cost->required3v3Rating && std::min(player->GetArenaPersonalRating(ARENA_SLOT_3v3), player->GetArenaTeamRating(ARENA_SLOT_3v3)) >= cost->required3v3Rating) ||
        (cost->required5v5Rating && std::min(player->GetArenaPersonalRating(ARENA_SLOT_5v5), player->GetArenaTeamRating(ARENA_SLOT_5v5)) >= cost->required5v5Rating) ||
        (cost->requiredBGRating  && player->GetBattlegroundRating() >= cost->requiredBGRating);

    if (!meetsPvPRatingRequirement)
        return false;

    // vote and donation points - using values cached in player class
    if ((cost->costVotePoints && player->GetVotePoints() < cost->costVotePoints) ||
        (cost->costDonationPoints && player->GetDonationPoints() < cost->costDonationPoints))
        return false;

    return true;
}

bool CurrencyMgr::PurchaseItemForCost(Player* player, uint32 vendorId, uint32 customCostId, bool doPurchase/* = true*/)
{
    ItemCustomCost const* cost = GetItemCustomCostById(customCostId);
    if (!cost)
        return true;

    // honor points price
    if (cost->costHonor && player->GetHonorPoints() < cost->costHonor)
    {
        player->SendEquipError(EQUIP_ERR_NOT_ENOUGH_HONOR_POINTS, NULL, NULL);
        return false;
    }

    // arena points price
    if (cost->costArena && player->GetArenaPoints() < cost->costHonor)
    {
        player->SendEquipError(EQUIP_ERR_NOT_ENOUGH_ARENA_POINTS, NULL, NULL);
        return false;
    }

    // item base price
    if (cost->requiredItem && !player->HasItemCount(cost->requiredItem, cost->itemCount))
    {
        player->SendEquipError(EQUIP_ERR_VENDOR_MISSING_TURNINS, NULL, NULL);
        return false;
    }

    // check for personal/team arena rating and/or BG rating requirement
    bool meetsPvPRatingRequirement = (!cost->required2v2Rating && !cost->required3v3Rating && !cost->required5v5Rating && !cost->requiredBGRating) ||
        (cost->required2v2Rating && std::min(player->GetArenaPersonalRating(ARENA_SLOT_2v2), player->GetArenaTeamRating(ARENA_SLOT_2v2)) >= cost->required2v2Rating) ||
        (cost->required3v3Rating && std::min(player->GetArenaPersonalRating(ARENA_SLOT_3v3), player->GetArenaTeamRating(ARENA_SLOT_3v3)) >= cost->required3v3Rating) ||
        (cost->required5v5Rating && std::min(player->GetArenaPersonalRating(ARENA_SLOT_5v5), player->GetArenaTeamRating(ARENA_SLOT_5v5)) >= cost->required5v5Rating) ||
        (cost->requiredBGRating  && player->GetBattlegroundRating() >= cost->requiredBGRating);

    if (!meetsPvPRatingRequirement)
    {
        // probably not the proper equip err
        player -> SendEquipError(EQUIP_ERR_CANT_EQUIP_RANK, NULL, NULL);
        return false;
    }

    // vote points - using values cached in player class
    if ((cost->costVotePoints && player->GetVotePoints() < cost->costVotePoints) ||
        (cost->costDonationPoints && player->GetDonationPoints() < cost->costDonationPoints))
    {
        player->GetSession()->SendNotification("You don't have enough tokens");
        return false;
    }

    if (!doPurchase)
        return true;

    // apply costs
    int32 votePoints = -static_cast<int32>(cost->costVotePoints), donationPoints = -static_cast<int32>(cost->costDonationPoints);
    if (ModifyRewardCurrencies(player, votePoints, donationPoints, TRANSACTION_ENTITY_VENDOR, vendorId))
    {
        player->ModifyHonorPoints(-static_cast<int32>(cost->costHonor));
        player->ModifyArenaPoints(-static_cast<int32>(cost->costArena));
        if (cost->requiredItem)
            player->DestroyItemCount(cost->requiredItem, cost->itemCount, true);

        return true;
    }

    return false;
}

void CurrencyMgr::GetRewardCurrencies(uint32 accountId, uint32& votePoints, uint32& donationPoints)
{
    PreparedStatement* stmt = SiteDatabase.GetPreparedStatement(SITE_SEL_BOTH_POINTS);
    stmt->setUInt32(0, accountId);

    if (PreparedQueryResult result = SiteDatabase.Query(stmt))
    {
        Field* fields   = result->Fetch();
        votePoints      = fields[0].GetUInt32();
        donationPoints  = fields[1].GetUInt32();
    }
}

void CurrencyMgr::GetRewardCurrencies(Player* player, uint32& votePoints, uint32& donationPoints)
{
    uint32 accountId = player->GetSession()->GetAccountId();
    return GetRewardCurrencies(accountId, votePoints, donationPoints);
}

bool CurrencyMgr::ModifyRewardCurrencies(Player* player, int32 votePoints, int32 donationPoints, TransactionEntity entity, uint32 entityId /*= 0*/, std::string const& comment /*= ""*/)
{
    if (!votePoints && !donationPoints)
        return true;

    WorldSession* session = player->GetSession();

    PreparedStatement* stmt = SiteDatabase.GetPreparedStatement(SITE_UPD_BOTH_POINTS);
    uint8 i = 0;
    stmt->setInt32 (i++, votePoints);
    stmt->setInt32 (i++, donationPoints);
    stmt->setUInt32(i++, session->GetAccountId());
    stmt->setInt32 (i++, votePoints);
    stmt->setInt32 (i, donationPoints);

    uint64 affectedRows;
    if (SiteDatabase.DirectExecute(stmt, affectedRows) && affectedRows >= 1)
    {
        TransactionLog* tLog        = new TransactionLog();
        tLog->votePointsChange      = votePoints;
        tLog->donationPointsChange  = donationPoints;
        tLog->entity                = entity;
        tLog->entityId              = entityId;
        tLog->comment               = comment;

        session->HandlePostTransactionActions(tLog);
        return true;
    }

    return false;
}

void CurrencyMgr::InitRewardCurrencies(uint32 accountId)
{
    PreparedStatement* stmt = SiteDatabase.GetPreparedStatement(SITE_INS_INIT_POINTS);
    stmt->setUInt32(0, accountId);
    SiteDatabase.Execute(stmt);
}

void CurrencyMgr::UpdateRewardCurrencies(Player* player)
{
    _DisplayRewardCurrencies(player, true);
}

void CurrencyMgr::HandleDisplayRewardCurrencies(Player* player, PreparedQueryResult result)
{
    if (result)
    {
        Field* fields = result->Fetch();
        _DisplayRewardCurrencies(player, false, fields[0].GetUInt32(), fields[1].GetUInt32());
    }
}

void CurrencyMgr::HandlePostTransactionActions(Player* player, TransactionLog* tLog, PreparedQueryResult result)
{
    if (result)
    {
        Field* fields           = result->Fetch();
        tLog->votePoints        = fields[0].GetUInt32();
        tLog->donationPoints    = fields[1].GetUInt32();
    }

    std::vector<std::string> currencies;
    if (tLog->votePointsChange)
        currencies.push_back(_BuildCurrencyUpdateString(tLog->votePointsChange, tLog->votePoints, _GetVotePointsItemLink()));
    if (tLog->donationPointsChange)
        currencies.push_back(_BuildCurrencyUpdateString(tLog->donationPointsChange, tLog->donationPoints, _GetDonationPointsItemLink()));
    ChatHandler(player->GetSession()).PSendSysMessage("You now have %s.", boost::algorithm::join(currencies, " and ").c_str());

    _LogTransaction(player, tLog);
}

void CurrencyMgr::_LoadCustomCosts()
{
    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_ITEM_CUSTOM_COSTS);
    if (PreparedQueryResult result = WorldDatabase.Query(stmt))
    {
        do {
            Field* fields = result->Fetch();
            uint8 idx = 0;

            ItemCustomCost cost;
            uint32 customCostId     = fields[idx++].GetUInt32();
            cost.costVotePoints     = fields[idx++].GetUInt32();
            cost.costDonationPoints = fields[idx++].GetUInt32();
            cost.costHonor          = fields[idx++].GetUInt32();
            cost.costArena          = fields[idx++].GetUInt32();
            cost.requiredItem       = fields[idx++].GetUInt32();
            cost.itemCount          = fields[idx++].GetUInt32();
            cost.guildLevel         = fields[idx++].GetUInt32();
            cost.guildReputation    = fields[idx++].GetUInt32();
            cost.required1v1Rating  = fields[idx++].GetUInt32();
            cost.required2v2Rating  = fields[idx++].GetUInt32();
            cost.required3v3Rating  = fields[idx++].GetUInt32();
            cost.required5v5Rating  = fields[idx++].GetUInt32();
            cost.requiredBGRating   = fields[idx].GetUInt32();

            m_ItemCustomCostMap[customCostId] = cost;
        } while (result->NextRow());
    }

    TC_LOG_INFO("server.loading", ">> Loaded %u item custom costs in %u ms",
                uint32(m_ItemCustomCostMap.size()), GetMSTimeDiffToNow(oldMSTime));
}

void CurrencyMgr::_LoadCustomNpcVendors()
{
    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_CUSTOM_NPC_VENDOR);
    if (PreparedQueryResult result = WorldDatabase.Query(stmt))
    {
        do {
            Field* fields = result->Fetch();
            uint8 idx = 0;

            uint32 entry        = fields[idx++].GetUInt32();
            uint32 item         = fields[idx++].GetUInt32();

            if (!sObjectMgr->GetItemTemplate(item))
            {
                TC_LOG_ERROR("sql.sql", "Item (Entry: %u) is invalid! Skipping...", item);
                continue;
            }

            m_CustomNpcVendorEntries.insert(entry);
            m_CustomNpcVendorItemCostMap[std::make_pair(entry, item)] = fields[idx].GetUInt32();
        } while (result->NextRow());
    }

    TC_LOG_INFO("server.loading", ">> Loaded %u custom npc vendor item costs in %u ms",
                uint32(m_CustomNpcVendorItemCostMap.size()), GetMSTimeDiffToNow(oldMSTime));
}

void CurrencyMgr::_LogTransaction(Player* player, TransactionLog const* tLog)
{
    uint8 idx = 0;

    PreparedStatement* stmt = SiteDatabase.GetPreparedStatement(SITE_INS_TRANSACTION_LOG);
    stmt->setUInt32 (idx++, player->GetSession()->GetAccountId());
    stmt->setInt32  (idx++, tLog->votePointsChange);
    stmt->setUInt32 (idx++, tLog->votePoints);
    stmt->setInt32  (idx++, tLog->donationPointsChange);
    stmt->setUInt32 (idx++, tLog->donationPoints);

    std::string entityStr;
    switch (tLog->entity)
    {
        case TRANSACTION_ENTITY_VENDOR:     entityStr = "vendor";   break;
        case TRANSACTION_ENTITY_SITE:       entityStr = "site";     break;
        case TRANSACTION_ENTITY_GAMEMASTER: entityStr = "gm";       break;
        case TRANSACTION_ENTITY_LEVELING:   entityStr = "leveling"; break;
        case TRANSACTION_ENTITY_QUEST:      entityStr = "quest";    break;
    }

    stmt->setString (idx++, entityStr);
    stmt->setUInt32 (idx++, tLog->entityId);
    stmt->setString (idx, tLog->comment);

    SiteDatabase.Execute(stmt);
}

std::string CurrencyMgr::_GetCurrencyItemLink(std::string const& name, uint32 quality) const
{
    std::ostringstream buf;
    buf << "|c" << std::hex << ItemQualityColors[quality] << "[" << name << "]|r";

    return buf.str();
}

std::string CurrencyMgr::_BuildCurrencyUpdateString(int32 change, uint32 points, std::string const& currencyItemLink) const
{
    std::ostringstream buf;
    buf << "|cff" << (change < 0 ? "ff0000" : "1eff00")
        << points << " (" << (change < 0 ? "" : "+")
        << change << ") " << currencyItemLink;

    return buf.str();
}

void CurrencyMgr::_DisplayRewardCurrencies(Player* player, bool sync/* = false*/, uint32 votePoints/* = 0*/, uint32 donationPoints/* = 0*/)
{
    if (sync)
        GetRewardCurrencies(player, votePoints, donationPoints);

    player->UpdateRewardCurrencies(votePoints, donationPoints);

    std::ostringstream buf;
    buf << "You have " << votePoints << " " << _GetVotePointsItemLink()
        << " and " << donationPoints << " " << _GetDonationPointsItemLink() << ".";
    ChatHandler(player->GetSession()).SendSysMessage(buf.str().c_str());
}

void WorldSession::HandleDisplayRewardCurrencies()
{
    PreparedStatement* stmt = SiteDatabase.GetPreparedStatement(SITE_SEL_BOTH_POINTS);
    stmt->setUInt32(0, _accountId);

    _accountRewardCurrenciesCallback = SiteDatabase.AsyncQuery(stmt);
}

void WorldSession::HandlePostTransactionActions(TransactionLog* tLog)
{
    PreparedStatement* stmt = SiteDatabase.GetPreparedStatement(SITE_SEL_BOTH_POINTS);
    stmt->setUInt32(0, _accountId);

    _transactionCallback.SetParam(tLog);
    _transactionCallback.SetFutureResult(SiteDatabase.AsyncQuery(stmt));
}