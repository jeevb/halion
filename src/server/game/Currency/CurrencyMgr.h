/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_CURRMGR_H
#define CG_CURRMGR_H

#include "Player.h"

#define NAME_VOTE_POINTS "Mark of the Crusader"
#define NAME_DONATION_POINTS "Philanthropist's Token"

struct ItemCustomCost
{
    uint32 costVotePoints;
    uint32 costDonationPoints;
    uint32 costHonor;
    uint32 costArena;
    uint32 requiredItem;
    uint32 itemCount;
    uint32 guildLevel;
    uint32 guildReputation;
    uint32 required1v1Rating;
    uint32 required2v2Rating;
    uint32 required3v3Rating;
    uint32 required5v5Rating;
    uint32 requiredBGRating;
};

typedef std::map<uint32, ItemCustomCost> ItemCustomCostMap;
typedef std::map<std::pair<uint32, uint32>, uint32> CustomNpcVendorItemCostMap;
typedef std::set<uint32> CustomNpcVendorEntries;

enum TransactionEntity
{
    TRANSACTION_ENTITY_VENDOR,
    TRANSACTION_ENTITY_SITE,
    TRANSACTION_ENTITY_GAMEMASTER,
    TRANSACTION_ENTITY_LEVELING,
    TRANSACTION_ENTITY_QUEST
};

struct TransactionLog
{
    int32 votePointsChange;
    uint32 votePoints;

    int32 donationPointsChange;
    uint32 donationPoints;

    TransactionEntity entity;
    uint32 entityId;

    std::string comment;
};

class CurrencyMgr
{
    private:
        CurrencyMgr();
        ~CurrencyMgr() { }

    public:

        static CurrencyMgr* instance()
        {
            static CurrencyMgr instance;
            return &instance;
        }

        void Init();

        ItemCustomCost const* GetItemCustomCostById(uint32 customCostId) const;

        uint32 GetVendorItemCustomCostId(uint32 entry, uint32 item) const;
        bool HasItemWithCustomCost(uint32 entry) const;

        std::string GetCustomCostDescription(Player* player, uint32 customCostId) const;

        bool CanAffordCustomCost(Player* player, uint32 customCostId) const;
        bool PurchaseItemForCost(Player* player, uint32 vendorId, uint32 customCostId, bool doPurchase = true);

        void GetRewardCurrencies(uint32 accountId, uint32& votePoints, uint32& donationPoints);
        void GetRewardCurrencies(Player* player, uint32& votePoints, uint32& donationPoints);

        bool ModifyRewardCurrencies(Player* player, int32 votePoints, int32 donationPoints, TransactionEntity entity, uint32 entityId = 0, std::string const& comment = "");

        // Initialize reward currencies for account
        void InitRewardCurrencies(uint32 accountId);

        // Helper to update reward currencies and display to player
        void UpdateRewardCurrencies(Player* player);

        // Post transactions action callback
        void HandleDisplayRewardCurrencies(Player* player, PreparedQueryResult result);
        void HandlePostTransactionActions(Player* player, TransactionLog* tLog, PreparedQueryResult result);

    private:

        void _LoadCustomCosts();
        void _LoadCustomNpcVendors();

        // Logging
        void _LogTransaction(Player* player, TransactionLog const* tLog);

        // Helpers
        std::string _GetVotePointsItemLink() const { return _GetCurrencyItemLink(NAME_VOTE_POINTS, ITEM_QUALITY_ARTIFACT); }
        std::string _GetDonationPointsItemLink() const { return _GetCurrencyItemLink(NAME_DONATION_POINTS, ITEM_QUALITY_ARTIFACT); }
        std::string _GetCurrencyItemLink(std::string const& name, uint32 quality) const;
        std::string _BuildCurrencyUpdateString(int32 change, uint32 points, std::string const& currencyItemLink) const;
        void _DisplayRewardCurrencies(Player* player, bool sync = false, uint32 votePoints = 0, uint32 donationPoints = 0);

        ItemCustomCostMap m_ItemCustomCostMap;
        CustomNpcVendorItemCostMap m_CustomNpcVendorItemCostMap;
        CustomNpcVendorEntries m_CustomNpcVendorEntries;
};

#define sCurrencyMgr CurrencyMgr::instance()

#endif