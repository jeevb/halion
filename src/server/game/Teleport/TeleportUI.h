/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_TRACKOPTMENU_H
#define CG_TRACKOPTMENU_H

#include "ScriptMgr.h"

enum TeleportGossipMessage
{
    TELEPORT_GOSSIP_MESSAGE_MAIN = 1000021
};

class TeleportUI : public CreatureScript
{
    public:

        TeleportUI();

        bool OnGossipHello(Player* player, Creature* creature) override;
        bool OnGossipSelect(Player* player, Creature* creature, uint32 sender, uint32 action) override;
};

#endif