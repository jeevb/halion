/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "Chat.h"
#include "Language.h"
#include "ScriptedGossip.h"
#include "TeleportMgr.h"
#include "TeleportUI.h"
#include "WorldSession.h"

TeleportUI::TeleportUI() : CreatureScript(TELEPORT_UI)
{
}

bool TeleportUI::OnGossipHello(Player* player, Creature* creature)
{
    player->CLEAR_GOSSIP_MENU();

    if (player->IsInCombat())
    {
        player->GetSession()->SendNotification(LANG_YOU_IN_COMBAT);
        return true;
    }

    for (auto const& itr : sTeleport->GetTeleportMap())
    {
        TeleportLoc const& loc = itr.second;
        if (loc.showOnUI && sTeleport->IsValidTeleLocation(player, loc))
            player->ADD_GOSSIP_ITEM(GOSSIP_ICON_TAXI, "Take me to " + loc.name + ".", 0, itr.first);
    }

    player->SEND_GOSSIP_MENU(TELEPORT_GOSSIP_MESSAGE_MAIN, creature->GetGUID());
    return true;
}

bool TeleportUI::OnGossipSelect(Player* player, Creature* creature, uint32 /*sender*/, uint32 action)
{
    player->CLEAR_GOSSIP_MENU();
    player->CLOSE_GOSSIP_MENU();

    if (!action)
    {
        ChatHandler(player->GetSession()).SendSysMessage("An error has occurred. Please try again.");
        return OnGossipHello(player, creature);
    }

    sTeleport->To(player, action);
    return true;
}