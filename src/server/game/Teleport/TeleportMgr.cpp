/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "Player.h"
#include "TeleportMgr.h"
#include "TeleportUI.h"

TeleportMgr::TeleportMgr()
{
    // Instantiate the Teleport UI script
    new TeleportUI();
}

void TeleportMgr::Init()
{
    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_TELEPORT_LOC);
    if (PreparedQueryResult result = WorldDatabase.Query(stmt))
    {
        do
        {
            Field* fields = result->Fetch();

            uint32 id       = fields[0].GetUInt32();

            TeleportLoc tl;
            tl.x            = fields[1].GetFloat();
            tl.y            = fields[2].GetFloat();
            tl.z            = fields[3].GetFloat();
            tl.o            = fields[4].GetFloat();
            tl.mapId        = fields[5].GetUInt32();
            tl.team         = fields[6].GetUInt32();
            tl.trackMask    = fields[7].GetUInt32();
            tl.showOnUI     = fields[8].GetBool();
            tl.name         = fields[9].GetString();

            if (!MapManager::IsValidMapCoord(tl.mapId, tl.x, tl.y, tl.z, tl.o))
            {
                TC_LOG_ERROR("sql.sql", "Wrong position for Name: %s in `teleport_loc` table, ignoring.", tl.name.c_str());
                continue;
            }

            m_TeleportMap[id] = tl;
        } while (result->NextRow());
    }

    TC_LOG_INFO("server.loading", ">> Loaded %u teleport locations in %u ms",
                uint32(m_TeleportMap.size()), GetMSTimeDiffToNow(oldMSTime));
}

TeleportLoc const* TeleportMgr::GetTeleportLoc(Player* player, uint32 id) const
{
    auto itr = m_TeleportMap.find(id);
    if (itr != m_TeleportMap.end())
    {
        TeleportLoc const& loc = itr->second;
        if (IsValidTeleLocation(player, loc))
            return &loc;
    }

    return nullptr;
}

bool TeleportMgr::IsValidTeleLocation(Player* player, TeleportLoc const& loc) const
{
    if (player->IsGameMaster())
        return true;

    // Check team
    if (loc.team && loc.team != player->GetTeam())
        return false;

    // Check track mask
    if (loc.trackMask && (loc.trackMask & player->getTrackMask()) == 0)
        return false;

    return true;
}

void TeleportMgr::To(Player* player, uint32 id)
{
    if (TeleportLoc const* loc = GetTeleportLoc(player, id))
        player->TeleportTo(loc->mapId, loc->x, loc->y, loc->z, loc->o);
}

void TeleportMgr::SetPlayerHomebind(Player* player, uint32 id)
{
    if (TeleportLoc const* loc = GetTeleportLoc(player, id))
    {
        WorldLocation homeBind(loc->mapId, loc->x, loc->y, loc->z, loc->o);
        uint32 areaId = sMapMgr->GetAreaId(loc->mapId, loc->x, loc->y, loc->z);
        player->SetHomebind(homeBind, areaId);
    }
}