/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_TELEMGR_H
#define CG_TELEMGR_H

#define TELEPORT_UI "teleport_ui"

enum TeleportLocation
{
    TELEPORT_LOC_NONE,
    TELEPORT_LOC_ACHERUS,
    TELEPORT_LOC_ORGRIMMAR,
    TELEPORT_LOC_UNDERCITY,
    TELEPORT_LOC_THUNDER_BLUFF,
    TELEPORT_LOC_SILVERMOON_CITY,
    TELEPORT_LOC_STORMWIND_CITY,
    TELEPORT_LOC_IRONFORGE,
    TELEPORT_LOC_DARNASSUS,
    TELEPORT_LOC_EXODAR,
    TELEPORT_LOC_SHATTRATH_CITY,
    TELEPORT_LOC_DALARAN,
    TELEPORT_LOC_HORDE_OUTPOST,
    TELEPORT_LOC_ALLIANCE_VANGUARD,
    TELEPORT_LOC_MAVENTELL_MARKET,
    TELEPORT_LOC_MAVENTELL_BARRACKS
};

struct TeleportLoc
{
    float       x;
    float       y;
    float       z;
    float       o;
    uint32      mapId;
    uint32      team;
    uint32      trackMask;
    bool        showOnUI;
    std::string name;
};

typedef std::map<uint32, TeleportLoc> TeleportMap;

class TeleportMgr
{
    private:
        TeleportMgr();
        ~TeleportMgr() { }

    public:

        static TeleportMgr* instance()
        {
            static TeleportMgr instance;
            return &instance;
        }

        void Init();

        // Getters
        TeleportMap const& GetTeleportMap() { return m_TeleportMap; }
        TeleportLoc const* GetTeleportLoc(Player* player, uint32 id) const;

        // Teleport Handling
        bool IsValidTeleLocation(Player* player, TeleportLoc const& loc) const;
        void To(Player* player, uint32 id);

        // Helpers
        void SetPlayerHomebind(Player* player, uint32 id);

    private:

        TeleportMap m_TeleportMap;
};

#define sTeleport TeleportMgr::instance()

#endif