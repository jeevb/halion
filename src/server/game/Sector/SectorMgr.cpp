/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "GameEventMgr.h"
#include "Language.h"
#include "NotificationMgr.h"
#include "SectorMgr.h"

SectorMgr::SectorMgr()
{
}

void SectorMgr::Init()
{
    _LoadSectors();
}

Sector const* SectorMgr::GetSector(uint32 id) const
{
    auto itr = _sectorTableEntryStore.find(id);
    return itr == _sectorTableEntryStore.end() ? nullptr : &itr->second;
}

Sectors const* SectorMgr::GetSectorsInArea(uint32 mapId, uint32 areaId) const
{
    auto itr = _areaSectorsMap.find(std::make_pair(mapId, areaId));
    return itr == _areaSectorsMap.end() ? nullptr : &itr->second;
}

uint32 SectorMgr::GetSectorId(uint32 mapId, uint32 areaId, float x, float y) const
{
    if (Sectors const* s = GetSectorsInArea(mapId, areaId))
        for (auto const& sector : *s)
            if (_IsInSector(x, y, sector))
                return sector;

    return 0;
}

uint32 SectorMgr::GetSectorId(WorldObject const* worldObject) const
{
    return GetSectorId(worldObject->GetMapId(), worldObject->GetAreaId(), worldObject->GetPositionX(), worldObject->GetPositionY());
}

void SectorMgr::HandlePlayerEnterSector(Player* player, uint32 sector)
{
    if (Sector const* s = GetSector(sector))
    {
        if (s->flags & SECTOR_FLAG_SANCTUARY)
            sNotify->FlashMessage(player, LANG_SECTOR_SANCTUARY, s->name.c_str());
        else if (s->flags & SECTOR_FLAG_FFA_PVP)
            sNotify->FlashMessage(player, LANG_SECTOR_FFA_PVP, s->name.c_str());
        else
            sNotify->FlashMessage(player, LANG_SECTOR_GENERIC, s->name.c_str());
    }
}

void SectorMgr::_LoadSectors()
{
    uint32 oldMSTime = getMSTime();

    PreparedStatement* stmt = WorldDatabase.GetPreparedStatement(WORLD_SEL_SECTORS);
    if (PreparedQueryResult result = WorldDatabase.Query(stmt))
    {
        do {
            Field* fields = result->Fetch();

            uint32 id       = fields[0].GetUInt32();
            uint32 mapId    = fields[1].GetUInt32();

            Sector s;
            s.flags         = fields[3].GetUInt32();
            s.team          = fields[4].GetUInt32();
            s.event         = fields[5].GetUInt32();
            s.name          = fields[7].GetString();
            boost::geometry::read_wkt("POLYGON(" + fields[6].GetString() + ")", s.bounds);

            _sectorTableEntryStore[id] = s;

            // Add sector to the appropriate areas for fast lookups
            std::vector<uint32> areaIds;

            // Read in pre-defined areas
            std::stringstream buf(result->Fetch()[2].GetString());

            uint32 area;
            while (buf >> area)
                areaIds.push_back(area);

            // Determine affected areas if no pre-defined entries found
            if (areaIds.empty())
                for (auto const& p : s.bounds.outer())
                    areaIds.push_back(sMapMgr->GetAreaId(mapId, p.x(), p.y()));

            for (auto const& a : areaIds)
                _areaSectorsMap[std::make_pair(mapId, a)].insert(id);
        } while (result->NextRow());
    }

    TC_LOG_INFO("server.loading", ">> Loaded %u sectors in %u ms",
                uint32(_sectorTableEntryStore.size()), GetMSTimeDiffToNow(oldMSTime));
}

bool SectorMgr::_IsInSector(float x, float y, uint32 sector) const
{
    if (Sector const* s = GetSector(sector))
    {
        if (s->event && !sGameEventMgr->IsActiveEvent(s->event))
            return false;

        Point p(x, y);
        return boost::geometry::within(p, s->bounds);
    }

    return false;
}