/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_SECTORMGR_H
#define CG_SECTORMGR_H

#include <boost/geometry.hpp>
#include <boost/geometry/geometries/point_xy.hpp>
#include <boost/geometry/geometries/polygon.hpp>

typedef boost::geometry::model::d2::point_xy<float> Point;
typedef boost::geometry::model::polygon<Point> Bounds;

enum SectorFlags
{
    SECTOR_FLAG_SANCTUARY   = 0x00000001,
    SECTOR_FLAG_FFA_PVP     = 0x00000002,
    SECTOR_FLAG_REST_ZONE   = 0x00000004,
    SECTOR_FLAG_CONTESTED   = 0x00000008,
    SECTOR_FLAG_NO_ENV_DMG  = 0x00000010,

    SECTOR_FLAG_MAX         = 0xFFFFFFFF
};

struct Sector
{
    uint32      flags;
    uint32      team;
    uint32      event;
    Bounds      bounds;
    std::string name;
};

typedef std::unordered_map<uint32, Sector> SectorTableEntryStore;

typedef std::set<uint32, std::greater<uint32> > Sectors;
typedef std::map<std::pair<uint32, uint32>, Sectors> AreaSectorsMap;

class SectorMgr
{
    private:
        SectorMgr();
        ~SectorMgr() { }

    public:

        static SectorMgr* instance()
        {
            static SectorMgr instance;
            return &instance;
        }

        void Init();

        Sector const* GetSector(uint32 id) const;
        Sectors const* GetSectorsInArea(uint32 mapId, uint32 areaId) const;

        uint32 GetSectorId(uint32 mapId, uint32 areaId, float x, float y) const;
        uint32 GetSectorId(WorldObject const* worldObject) const;

        void HandlePlayerEnterSector(Player* player, uint32 sector);

    private:

        void _LoadSectors();

        bool _IsInSector(float x, float y, uint32 sector) const;

        SectorTableEntryStore _sectorTableEntryStore;
        AreaSectorsMap _areaSectorsMap;
};

#define sSectorMgr SectorMgr::instance()

#endif