/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef CG_UDPLOG_H
#define CG_UDPLOG_H

#include <boost/asio.hpp>

class UDPLogger
{
    private:
        UDPLogger();
        ~UDPLogger() { }

    public:

        static UDPLogger* instance(boost::asio::io_service* ioService = nullptr)
        {
            static UDPLogger instance;

            if (ioService != nullptr)
                instance._ioService = ioService;

            return &instance;
        }

        void Init();

        bool IsRunning() const { return _ioService != nullptr; }
        bool CanBindToSocket() const;

        void GenerateEndpoint();

        void Log(std::string const& logFilter, std::string const& logType, const char* str, ...);
        void Log(std::string const& text);

    private:
        boost::asio::io_service* _ioService;
        boost::asio::ip::udp::endpoint _endpoint;

        std::string m_udpLogAddr;
        std::string m_udpLogPort;
};

#define sUdp UDPLogger::instance()

// Macros for UDP logs
#define UDP_LOG_MSG(FILTER__, TYPE__, STR__, ...) \
    sUdp->Log(FILTER__, TYPE__, STR__, ##__VA_ARGS__)

#define UDP_WS_INFO(STR__, ...) \
    UDP_LOG_MSG("WORLDSERVER", "INFO", STR__, ##__VA_ARGS__)

#define UDP_WS_CHEAT(STR__, ...) \
    UDP_LOG_MSG("ANTICHEAT", "INFO", STR__, ##__VA_ARGS__)

#define UDP_WS_TICKET_CRITICAL(STR__, ...) \
    UDP_LOG_MSG("TICKET", "CRITICAL", STR__, ##__VA_ARGS__)

#define UDP_WS_TICKET(STR__, ...) \
    UDP_LOG_MSG("TICKET", "INFO", STR__, ##__VA_ARGS__)

#define UDP_WS_ARENA_LOGS_MATCH(STR__, ...) \
    UDP_LOG_MSG("ARENA", "CRITICAL", STR__, ##__VA_ARGS__)

#define UDP_WS_ARENA_LOGS_PLAYER_SCORE(STR__, ...) \
    UDP_LOG_MSG("ARENA", "INFO", STR__, ##__VA_ARGS__)

#endif