/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "Config.h"
#include "Log.h"
#include "UDPLogger.h"

UDPLogger::UDPLogger() : _ioService(nullptr)
{
}

void UDPLogger::Init()
{
    if (!IsRunning())
        return;

    ASSERT(CanBindToSocket());

    m_udpLogPort = sConfigMgr->GetStringDefault("UdpLog.Port", "5050");
    m_udpLogAddr = sConfigMgr->GetStringDefault("UdpLog.Addr", "127.0.0.1");

    GenerateEndpoint();
}

void UDPLogger::GenerateEndpoint()
{
    boost::asio::ip::udp::resolver resolver(*_ioService);
    boost::asio::ip::udp::resolver::query query(boost::asio::ip::udp::v4(),
                                                m_udpLogAddr,
                                                m_udpLogPort);
    _endpoint = *resolver.resolve(query);
}

bool UDPLogger::CanBindToSocket() const
{
    boost::system::error_code e;

    boost::asio::ip::udp::socket socket(*_ioService);
    socket.open(boost::asio::ip::udp::v4(), e);

    return !e;
}

void UDPLogger::Log(std::string const& logFilter, std::string const& logType, const char* str, ...)
{
    if (!IsRunning())
        return;

    va_list ap;
    va_start(ap, str);
    char text[MAX_QUERY_LEN];
    vsnprintf(text, MAX_QUERY_LEN, str, ap);
    va_end(ap);

    std::ostringstream buf;
    buf << logFilter << "|" << logType << "|" << text;
    Log(buf.str());
}

void UDPLogger::Log(std::string const& text)
{
    boost::system::error_code e;

    boost::asio::ip::udp::socket socket(*_ioService);
    socket.open(boost::asio::ip::udp::v4(), e);

    if (e)
    {
        TC_LOG_ERROR("network.udplogger", e.message().c_str());
        return;
    }

    socket.send_to(boost::asio::buffer(text), _endpoint);
}