/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#include "SiteDatabase.h"

void SiteDatabaseConnection::DoPrepareStatements()
{
    if (!m_reconnecting)
        m_stmts.resize(MAX_SITEDATABASE_STATEMENTS);

    PrepareStatement(SITE_INS_INIT_POINTS, "INSERT IGNORE INTO website_siteaccount (account_id, vote_points, donation_points) VALUES (?, 0, 0)", CONNECTION_ASYNC);
    PrepareStatement(SITE_SEL_BOTH_POINTS, "SELECT vote_points, donation_points FROM website_siteaccount WHERE account_id = ?", CONNECTION_BOTH);
    PrepareStatement(SITE_UPD_BOTH_POINTS, "UPDATE website_siteaccount SET vote_points = vote_points + ?, donation_points = donation_points + ? "
                                           "WHERE account_id = ? AND vote_points + ? >= 0 AND donation_points + ? >= 0", CONNECTION_SYNCH);
    PrepareStatement(SITE_INS_TRANSACTION_LOG, "INSERT INTO website_siteaccount_updates (account_id, vote_points_change, vote_points_new, donation_points_change, donation_points_new, entity_type, entity_id, comment) VALUES (?, ?, ?, ?, ?, ?, ?, ?)", CONNECTION_ASYNC);
}