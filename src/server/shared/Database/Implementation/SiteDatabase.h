/*
 * Copyright (C) 2015 Crusade Gaming <http://www.crusadegaming.com/>
 */

#ifndef _SITEDATABASE_H
#define _SITEDATABASE_H

#include "DatabaseWorkerPool.h"
#include "MySQLConnection.h"

class SiteDatabaseConnection : public MySQLConnection
{
    public:
        //- Constructors for sync and async connections
        SiteDatabaseConnection(MySQLConnectionInfo& connInfo) : MySQLConnection(connInfo) { }
        SiteDatabaseConnection(ProducerConsumerQueue<SQLOperation*>* q, MySQLConnectionInfo& connInfo) : MySQLConnection(q, connInfo) { }

        //- Loads database type specific prepared statements
        void DoPrepareStatements() override;
};

typedef DatabaseWorkerPool<SiteDatabaseConnection> SiteDatabaseWorkerPool;

enum SiteDatabaseStatements
{
    /*  Naming standard for defines:
        {DB}_{SEL/INS/UPD/DEL/REP}_{Summary of data changed}
        When updating more than one field, consider looking at the calling function
        name for a suiting suffix.
    */

    SITE_INS_INIT_POINTS,
    SITE_SEL_BOTH_POINTS,
    SITE_UPD_BOTH_POINTS,
    SITE_INS_TRANSACTION_LOG,

    MAX_SITEDATABASE_STATEMENTS
};

#endif
