Please follow the guidelines below on how to properly name an SQL file.

The standard structure is:

YYYY_MM_DD_XX_ZZZZZ_TTTTT.sql

YYYY = Year (ex. 2014)
MM = Month (ex. 11)
DD = Day (ex. 04)
XX = The file number for that day (ex. if this is the second SQL file submitted today, use 01).
ZZZZZ = Database name (ex. world)
TTTTT = table name (ex. creature_template)
