DELETE FROM `rbac_permissions` WHERE `id` IN (1002, 1003, 1004);
INSERT INTO `rbac_permissions` (`id`, `name`)
VALUES
    (1002, 'Command: .spectate'),
    (1003, 'Command: .spectate player'),
    (1004, 'Command: .spectate leave');
