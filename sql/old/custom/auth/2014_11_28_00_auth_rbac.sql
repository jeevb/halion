DELETE FROM `rbac_permissions` WHERE `id` = 1001;
INSERT INTO `rbac_permissions` (`id`, `name`)
VALUES
	(1001, 'Command: .mod xprate');

DELETE FROM `rbac_linked_permissions` WHERE `linkedId` = 1001;
INSERT INTO `rbac_linked_permissions` (`id`, `linkedId`)
VALUES
	(194, 1001);