DELETE FROM `rbac_permissions` WHERE `id` = 1000;
INSERT INTO `rbac_permissions` (`id`, `name`)
VALUES
	(1000, 'Skip anticheat check');

DELETE FROM `rbac_linked_permissions` WHERE `id` = 194 AND `linkedId` = 1000;
INSERT INTO `rbac_linked_permissions` (`id`, `linkedId`)
VALUES
	(194, 1000);