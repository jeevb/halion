DELETE FROM `rbac_permissions` WHERE `id` = 1005;
INSERT INTO `rbac_permissions` (`id`, `name`)
VALUES
    (1005, 'Command: .modify vp');

DELETE FROM `rbac_linked_permissions` WHERE `linkedId` = 1005;
INSERT INTO `rbac_linked_permissions` (`id`, `linkedId`)
VALUES
    (194, 1005);
