# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.40-MariaDB-1~wheezy-log)
# Database: characters
# Generation Time: 2014-11-24 22:02:36 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table character_exp_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `character_exp_data`;

CREATE TABLE `character_exp_data` (
  `guid` int(11) unsigned NOT NULL,
  `rate` float unsigned NOT NULL DEFAULT '1',
  `totalExp` int(11) unsigned NOT NULL DEFAULT '0',
  `baseTotalExp` float unsigned NOT NULL DEFAULT '0',
  `levelExp` int(11) unsigned NOT NULL DEFAULT '0',
  `baseLevelExp` float unsigned NOT NULL DEFAULT '0',
  `unitsKilled` int(11) unsigned NOT NULL DEFAULT '0',
  `killsExp` int(11) unsigned NOT NULL DEFAULT '0',
  `baseKillsExp` float unsigned NOT NULL DEFAULT '0',
  `questsCompleted` int(11) unsigned NOT NULL DEFAULT '0',
  `questsExp` int(11) unsigned NOT NULL DEFAULT '0',
  `baseQuestsExp` float unsigned NOT NULL DEFAULT '0',
  `areasExplored` int(11) unsigned NOT NULL DEFAULT '0',
  `exploredExp` int(11) unsigned NOT NULL DEFAULT '0',
  `baseExploredExp` float unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
