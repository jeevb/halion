# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.40-MariaDB-1~wheezy)
# Database: characters
# Generation Time: 2015-01-30 18:51:45 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table arena_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `arena_log`;

CREATE TABLE `arena_log` (
  `arena_log_id` int(10) unsigned NOT NULL,
  `match_type` tinyint(3) unsigned NOT NULL,
  `winner_id` int(10) unsigned NOT NULL,
  `loser_id` int(10) unsigned NOT NULL,
  `elapsed_time` int(10) unsigned NOT NULL,
  `winner_rating` int(10) unsigned NOT NULL,
  `winner_rating_change` int(10) NOT NULL,
  `loser_rating` int(10) unsigned NOT NULL,
  `loser_rating_change` int(10) NOT NULL,
  `winner_mmr` int(10) unsigned NOT NULL,
  `winner_mmr_change` int(10) NOT NULL,
  `loser_mmr` int(10) unsigned NOT NULL,
  `loser_mmr_change` int(10) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `arena_season_id` int(3) unsigned NOT NULL,
  PRIMARY KEY (`arena_log_id`),
  KEY `arena_season_id` (`arena_season_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
