-- This file is part of Maventell's initial release.
-- Includes work by Hash, Dash, Kaiyn and Slash

SET @GRAVEYARD_ID := 1000;

REPLACE INTO `world_safe_locs_extra` (`id`, `mapId`, `x`, `y`, `z`, `name`) VALUES (@GRAVEYARD_ID, 0, -894.366, 1457.09, 59.7237, 'Maventell Graveyard');