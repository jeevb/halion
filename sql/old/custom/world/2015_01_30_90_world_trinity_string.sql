DELETE FROM `trinity_string` WHERE `entry` BETWEEN 30011 AND 30013;
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc1`, `content_loc2`, `content_loc3`, `content_loc4`, `content_loc5`, `content_loc6`, `content_loc7`, `content_loc8`)
VALUES
    (30013, 'A new week has begun. Your weekly arena points have been reset.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
    (30012, 'You may gain an additional |TInterface/PVPFrame/PVP-ArenaPoints-Icon:16:16:0:0|t|cffffffff %u [Arena Points]|r this week before reaching your cap.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
    (30011, 'You have gained |TInterface/PVPFrame/PVP-ArenaPoints-Icon:16:16:0:0|t|cffffffff %u [Arena Points]|r.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
