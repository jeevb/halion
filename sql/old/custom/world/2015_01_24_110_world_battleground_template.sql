-- Lower minimum required players for AB and EOTS
UPDATE `battleground_template` SET `MinPlayersPerTeam` = 7 WHERE `id` IN (3, 7);

-- Lower minimum required players for RBG
UPDATE `battleground_template` SET `MinPlayersPerTeam` = 5 WHERE `id` = 32;

-- Increase likelihood of WSG popping
UPDATE `battleground_template` SET `Weight` = 3 WHERE `id` = 2;
