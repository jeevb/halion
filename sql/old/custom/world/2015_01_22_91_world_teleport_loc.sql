ALTER TABLE `teleport_loc`
ADD COLUMN `team` int(10) unsigned NOT NULL DEFAULT '0' AFTER `mapId`,
ADD COLUMN `trackMask` int(10) unsigned NOT NULL DEFAULT '0' AFTER `team`,
ADD COLUMN `showOnUI` tinyint(1) unsigned NOT NULL DEFAULT '0' AFTER `trackMask`;
