UPDATE `battleground_template` SET `Weight` = 0 WHERE `id` IN (1, 5, 9, 10, 11, 30);
DELETE FROM `disables` WHERE `sourceType` = 3 AND `entry` IN (1, 5, 9, 10, 11, 30);
INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `params_0`, `params_1`, `comment`)
VALUES
    (3, 1, 0, '', '', ''),
    (3, 5, 0, '', '', ''),
    (3, 9, 0, '', '', ''),
    (3, 10, 0, '', '', ''),
    (3, 11, 0, '', '', ''),
    (3, 30, 0, '', '', '');
