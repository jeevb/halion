UPDATE `npc_trainer` SET `entry` = 1000053 WHERE `entry` = 1000050 AND `spell` IN (34090, 54197);

DELETE FROM `auto_trainer` WHERE `entry` = 1000053;
INSERT INTO `auto_trainer` (`raceMask`, `classMask`, `trackMask`, `entry`, `priority`, `Note`)
VALUES
    (0, 0, 30, 1000053, 1, 'Riding Trainer (Common)');
