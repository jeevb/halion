SET @ENTRY := 500000;

DELETE FROM `item_template` WHERE `entry` BETWEEN @ENTRY+0 AND @ENTRY+19;
INSERT INTO `item_template` (`entry`, `class`, `subclass`, `SoundOverrideSubclass`, `name`, `displayid`, `Quality`, `Flags`, `FlagsExtra`, `BuyCount`, `BuyPrice`, `SellPrice`, `InventoryType`, `AllowableClass`, `AllowableRace`, `ItemLevel`, `RequiredLevel`, `RequiredSkill`, `RequiredSkillRank`, `requiredspell`, `requiredhonorrank`, `RequiredCityRank`, `RequiredReputationFaction`, `RequiredReputationRank`, `maxcount`, `stackable`, `ContainerSlots`, `StatsCount`, `stat_type1`, `stat_value1`, `stat_type2`, `stat_value2`, `stat_type3`, `stat_value3`, `stat_type4`, `stat_value4`, `stat_type5`, `stat_value5`, `stat_type6`, `stat_value6`, `stat_type7`, `stat_value7`, `stat_type8`, `stat_value8`, `stat_type9`, `stat_value9`, `stat_type10`, `stat_value10`, `ScalingStatDistribution`, `ScalingStatValue`, `dmg_min1`, `dmg_max1`, `dmg_type1`, `dmg_min2`, `dmg_max2`, `dmg_type2`, `armor`, `holy_res`, `fire_res`, `nature_res`, `frost_res`, `shadow_res`, `arcane_res`, `delay`, `ammo_type`, `RangedModRange`, `spellid_1`, `spelltrigger_1`, `spellcharges_1`, `spellppmRate_1`, `spellcooldown_1`, `spellcategory_1`, `spellcategorycooldown_1`, `spellid_2`, `spelltrigger_2`, `spellcharges_2`, `spellppmRate_2`, `spellcooldown_2`, `spellcategory_2`, `spellcategorycooldown_2`, `spellid_3`, `spelltrigger_3`, `spellcharges_3`, `spellppmRate_3`, `spellcooldown_3`, `spellcategory_3`, `spellcategorycooldown_3`, `spellid_4`, `spelltrigger_4`, `spellcharges_4`, `spellppmRate_4`, `spellcooldown_4`, `spellcategory_4`, `spellcategorycooldown_4`, `spellid_5`, `spelltrigger_5`, `spellcharges_5`, `spellppmRate_5`, `spellcooldown_5`, `spellcategory_5`, `spellcategorycooldown_5`, `bonding`, `description`, `PageText`, `LanguageID`, `PageMaterial`, `startquest`, `lockid`, `Material`, `sheath`, `RandomProperty`, `RandomSuffix`, `block`, `itemset`, `MaxDurability`, `area`, `Map`, `BagFamily`, `TotemCategory`, `socketColor_1`, `socketContent_1`, `socketColor_2`, `socketContent_2`, `socketColor_3`, `socketContent_3`, `socketBonus`, `GemProperties`, `RequiredDisenchantSkill`, `ArmorDamageModifier`, `duration`, `ItemLimitCategory`, `HolidayId`, `ScriptName`, `DisenchantID`, `FoodType`, `minMoneyLoot`, `maxMoneyLoot`, `flagsCustom`, `VerifiedBuild`) 
VALUES
(@ENTRY+0, 15, 0, -1, 'Warrior: Level 80 DPS Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+1, 15, 0, -1, 'Warrior: Level 80 Tank Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+2, 15, 0, -1, 'Paladin: Level 80 Heal Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+3, 15, 0, -1, 'Paladin: Level 80 DPS Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+4, 15, 0, -1, 'Paladin: Level 80 Tank Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+5, 15, 0, -1, 'Hunter: Level 80 DPS Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+6, 15, 0, -1, 'Rogue: Level 80 DPS Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+7, 15, 0, -1, 'Priest: Level 80 Heal Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+8, 15, 0, -1, 'Priest: Level 80 DPS Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+9, 15, 0, -1, 'Death Knight: Level 80 DPS Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+10, 15, 0, -1, 'Death Knight: Level 80 Tank Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+11, 15, 0, -1, 'Shaman: Level 80 RDPS Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+12, 15, 0, -1, 'Shaman: Level 80 DPS Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+13, 15, 0, -1, 'Shaman: Level 80 Heal Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+14, 15, 0, -1, 'Mage: Level 80 DPS Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+15, 15, 0, -1, 'Warlock: Level 80 DPS Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+16, 15, 0, -1, 'Druid: Level 80 RDPS Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+17, 15, 0, -1, 'Druid: Level 80 DPS Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+18, 15, 0, -1, 'Druid: Level 80 Tank Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1),
(@ENTRY+19, 15, 0, -1, 'Druid: Level 80 Heal Gear', 16190, 2, 4, 0, 1, 0, 0, 0, -1, -1, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 0, 0, 0, 0, -1, 0, -1, 1, '', 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, 1);

DELETE FROM `item_loot_template` WHERE `Entry` BETWEEN @ENTRY+0 AND @ENTRY+19;
INSERT INTO `item_loot_template` (`Entry`, `Item`, `Reference`, `Chance`, `QuestRequired`, `LootMode`, `GroupId`, `MinCount`, `MaxCount`)
VALUES
-- Warrior: Level 80 DPS Gear
(@ENTRY+0, 39448, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+0, 39848, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+0, 39888, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+0, 43945, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+0, 42882, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+0, 39457, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+0, 42836, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+0, 39104, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+0, 42880, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+0, 39481, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+0, 43838, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+0, 43829, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+0, 43924, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+0, 43883, 0, 100, 0, 1, 14, 1, 1), -- 2H Weapon
(@ENTRY+0, 42859, 0, 100, 0, 1, 15, 1, 1), -- 2H Weapon
-- Warrior: Level 80 Tank Gear
(@ENTRY+1, 43846, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+1, 43849, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+1, 43844, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+1, 42827, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+1, 39471, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+1, 43845, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+1, 42884, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+1, 42825, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+1, 43842, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+1, 43248, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+1, 43838, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+1, 43829, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+1, 39675, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+1, 39136, 0, 100, 0, 1, 14, 1, 1), -- 1H Weapon
(@ENTRY+1, 43843, 0, 100, 0, 1, 15, 1, 1), -- Shield
-- Paladin: Level 80 Heal Gear
(@ENTRY+2, 42829, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+2, 43884, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+2, 42876, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+2, 42877, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+2, 43831, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+2, 42881, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+2, 42883, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+2, 42830, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+2, 43943, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+2, 42845, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+2, 38764, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+2, 43836, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+2, 43925, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+2, 29380, 0, 100, 0, 1, 14, 1, 1), -- 1H Weapon
(@ENTRY+2, 42860, 0, 100, 0, 1, 15, 1, 1), -- Shield
(@ENTRY+2, 37574, 0, 100, 0, 1, 16, 1, 1), -- Relic
-- Paladin: Level 80 DPS Gear
(@ENTRY+3, 39448, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+3, 39848, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+3, 39888, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+3, 43945, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+3, 42882, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+3, 39457, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+3, 42836, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+3, 39104, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+3, 42880, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+3, 39481, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+3, 43838, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+3, 43829, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+3, 43924, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+3, 43883, 0, 100, 0, 1, 14, 1, 1), -- 2H Weapon
(@ENTRY+3, 37574, 0, 100, 0, 1, 15, 1, 1), -- Relic
-- Paladin: Level 80 Tank Gear
(@ENTRY+4, 43846, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+4, 43849, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+4, 43844, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+4, 42827, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+4, 39471, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+4, 43845, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+4, 42884, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+4, 42825, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+4, 43842, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+4, 43248, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+4, 43838, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+4, 43829, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+4, 39675, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+4, 39136, 0, 100, 0, 1, 14, 1, 1), -- 1H Weapon
(@ENTRY+4, 43843, 0, 100, 0, 1, 15, 1, 1), -- Shield
(@ENTRY+4, 37574, 0, 100, 0, 1, 16, 1, 1), -- Relic
-- Hunter: Level 80 DPS Gear
(@ENTRY+5, 43937, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+5, 35886, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+5, 42893, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+5, 42813, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+5, 39065, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+5, 39887, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+5, 42891, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+5, 39068, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+5, 39070, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+5, 39480, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+5, 43829, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+5, 43838, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+5, 43889, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+5, 42794, 0, 100, 0, 1, 14, 1, 1), -- 2H Weapon
(@ENTRY+5, 39134, 0, 100, 0, 1, 15, 1, 1), -- Bow
-- Rogue: Level 80 DPS Gear
(@ENTRY+6, 43840, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+6, 35886, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+6, 42805, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+6, 39036, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+6, 43909, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+6, 42867, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+6, 39035, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+6, 39038, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+6, 39040, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+6, 39480, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+6, 43829, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+6, 43838, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+6, 43889, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+6, 39130, 0, 100, 0, 1, 14, 2, 2), -- 1H Daggers
(@ENTRY+6, 43833, 0, 100, 0, 1, 15, 2, 2), -- 1H Swords
-- Priest: Level 80 Heal Gear
(@ENTRY+7, 43859, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+7, 42793, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+7, 43872, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+7, 42849, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+7, 42785, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+7, 39335, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+7, 39814, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+7, 39332, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+7, 39330, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+7, 43903, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+7, 38764, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+7, 43836, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+7, 43861, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+7, 31700, 0, 100, 0, 1, 14, 1, 1), -- 2H Weapon
-- Priest: Level 80 DPS Gear
(@ENTRY+8, 42843, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+8, 43884, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+8, 43862, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+8, 43881, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+8, 43866, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+8, 43863, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+8, 42789, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+8, 38736, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+8, 38741, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+8, 42845, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+8, 38764, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+8, 38765, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+8, 43925, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+8, 39121, 0, 100, 0, 1, 14, 1, 1), -- 2H Weapon
-- Death Knight: Level 80 DPS Gear
(@ENTRY+9, 39448, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+9, 39848, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+9, 39888, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+9, 43945, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+9, 42882, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+9, 39457, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+9, 42836, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+9, 39104, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+9, 42880, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+9, 39481, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+9, 43838, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+9, 43829, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+9, 43924, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+9, 43883, 0, 100, 0, 1, 14, 1, 1), -- 2H Weapon
(@ENTRY+9, 43923, 0, 100, 0, 1, 15, 2, 2), -- 1H Weapons
-- Death Knight: Level 80 Tank Gear
(@ENTRY+10, 43846, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+10, 43849, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+10, 43844, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+10, 42827, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+10, 39471, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+10, 43845, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+10, 42884, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+10, 42825, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+10, 43842, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+10, 43248, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+10, 43838, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+10, 43829, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+10, 39675, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+10, 39115, 0, 100, 0, 1, 14, 1, 1), -- 2H Weapon
-- Shaman: Level 80 RDPS Gear
(@ENTRY+11, 42810, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+11, 43884, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+11, 43830, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+11, 43935, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+11, 39432, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+11, 39045, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+11, 42815, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+11, 42892, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+11, 43976, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+11, 42845, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+11, 38764, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+11, 38765, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+11, 43925, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+11, 43897, 0, 100, 0, 1, 14, 1, 1), -- 1H Weapon
(@ENTRY+11, 42860, 0, 100, 0, 1, 15, 1, 1), -- Shield
(@ENTRY+11, 28066, 0, 100, 0, 1, 16, 1, 1), -- Relic
-- Shaman: Level 80 DPS Gear
(@ENTRY+12, 43937, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+12, 35886, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+12, 42893, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+12, 42813, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+12, 39065, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+12, 39887, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+12, 42891, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+12, 39068, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+12, 39070, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+12, 39480, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+12, 43829, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+12, 43838, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+12, 43889, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+12, 39144, 0, 100, 0, 1, 14, 2, 2), -- 1H Weapons
(@ENTRY+12, 37575, 0, 100, 0, 1, 15, 1, 1), -- Relic
-- Shaman: Level 80 Heal Gear
(@ENTRY+13, 43978, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+13, 43884, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+13, 42820, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+13, 39433, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+13, 42814, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+13, 42890, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+13, 42815, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+13, 42888, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+13, 39444, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+13, 43877, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+13, 38764, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+13, 43836, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+13, 39673, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+13, 43916, 0, 100, 0, 1, 14, 1, 1), -- 1H Weapon
(@ENTRY+13, 39010, 0, 100, 0, 1, 15, 1, 1), -- Shield
(@ENTRY+13, 28066, 0, 100, 0, 1, 16, 1, 1), -- Relic
-- Mage: Level 80 DPS Gear
(@ENTRY+14, 42843, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+14, 43884, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+14, 43862, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+14, 43881, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+14, 43866, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+14, 43863, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+14, 42789, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+14, 38736, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+14, 38741, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+14, 42845, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+14, 38764, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+14, 38765, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+14, 43925, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+14, 39121, 0, 100, 0, 1, 14, 1, 1), -- 2H Weapon
-- Warlock: Level 80 DPS Gear
(@ENTRY+15, 42843, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+15, 43884, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+15, 43862, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+15, 43881, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+15, 43866, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+15, 43863, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+15, 42789, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+15, 38736, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+15, 38741, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+15, 42845, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+15, 38764, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+15, 38765, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+15, 43925, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+15, 39121, 0, 100, 0, 1, 14, 1, 1), -- 2H Weapon
-- Druid: Level 80 RDPS Gear
(@ENTRY+16, 43905, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+16, 43884, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+16, 42800, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+16, 42803, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+16, 43914, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+16, 39027, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+16, 43908, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+16, 42865, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+16, 43910, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+16, 42845, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+16, 38764, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+16, 38765, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+16, 43925, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+16, 39121, 0, 100, 0, 1, 14, 1, 1), -- 2H Weapon
-- Druid: Level 80 DPS Gear
(@ENTRY+17, 43840, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+17, 35886, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+17, 42805, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+17, 39036, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+17, 43909, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+17, 42867, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+17, 39035, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+17, 39038, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+17, 39040, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+17, 39480, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+17, 43829, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+17, 43838, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+17, 43889, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+17, 43885, 0, 100, 0, 1, 14, 1, 1), -- 2H Weapon
-- Druid: Level 80 Tank Gear
(@ENTRY+18, 37463, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+18, 43849, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+18, 42805, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+18, 39036, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+18, 42802, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+18, 39380, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+18, 39383, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+18, 42871, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+18, 43904, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+18, 39480, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+18, 43838, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+18, 43829, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+18, 43889, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+18, 42794, 0, 100, 0, 1, 14, 1, 1), -- 2H Weapon
-- Druid: Level 80 Heal Gear
(@ENTRY+19, 43905, 0, 100, 0, 1, 1, 1, 1), -- Head
(@ENTRY+19, 43884, 0, 100, 0, 1, 2, 1, 1), -- Neck
(@ENTRY+19, 42800, 0, 100, 0, 1, 3, 1, 1), -- Shoulder
(@ENTRY+19, 42803, 0, 100, 0, 1, 4, 1, 1), -- Chest
(@ENTRY+19, 43914, 0, 100, 0, 1, 5, 1, 1), -- Waist
(@ENTRY+19, 39027, 0, 100, 0, 1, 6, 1, 1), -- Legs
(@ENTRY+19, 43908, 0, 100, 0, 1, 7, 1, 1), -- Feet
(@ENTRY+19, 42865, 0, 100, 0, 1, 8, 1, 1), -- Wrist
(@ENTRY+19, 43910, 0, 100, 0, 1, 9, 1, 1), -- Hands
(@ENTRY+19, 42845, 0, 100, 0, 1, 10, 2, 2), -- Rings
(@ENTRY+19, 38764, 0, 100, 0, 1, 11, 1, 1), -- Trinket 1
(@ENTRY+19, 43836, 0, 100, 0, 1, 12, 1, 1), -- Trinket 2
(@ENTRY+19, 43925, 0, 100, 0, 1, 13, 1, 1), -- Back
(@ENTRY+19, 39121, 0, 100, 0, 1, 14, 1, 1); -- 2H Weapon