SET @ENTRY := 1000060;

DELETE FROM `npc_trainer` WHERE `entry` BETWEEN @ENTRY AND @ENTRY+11;
INSERT INTO `npc_trainer` (`entry`, `spell`, `spellcost`, `reqskill`, `reqskillvalue`, `reqlevel`)
VALUES
    (@ENTRY, 23229, 0, 0, 0, 40),
    (@ENTRY+1, 18787, 0, 0, 0, 40),
    (@ENTRY+2, 23338, 0, 0, 0, 40),
    (@ENTRY+3, 23223, 0, 0, 0, 40),
    (@ENTRY+4, 35714, 0, 0, 0, 40),
    (@ENTRY+5, 23250, 0, 0, 0, 40),
    (@ENTRY+6, 23243, 0, 0, 0, 40),
    (@ENTRY+7, 23247, 0, 0, 0, 40),
    (@ENTRY+8, 66846, 0, 0, 0, 40),
    (@ENTRY+9, 33660, 0, 0, 0, 40),
    (@ENTRY+10, 32240, 0, 0, 0, 60),
    (@ENTRY+11, 32243, 0, 0, 0, 60);
