DELETE FROM `game_tele` WHERE `id` IN (1425, 1426, 1427, 1428);
INSERT INTO `game_tele` (`id`, `position_x`, `position_y`, `position_z`, `orientation`, `map`, `name`)
VALUES
    (1428, -496.235, 7488.48, 181.594, 1.54878, 530, 'hqueue'),
    (1427, 5947.4, -6733.83, 159.051, 3.30276, 530, 'aqueue'),
    (1426, -11815.2, -4758.7, 6.53404, 0.245017, 1, 'mall'),
    (1425, -10740, 2442.34, 7.20732, 5.23146, 1, 'gm2');
