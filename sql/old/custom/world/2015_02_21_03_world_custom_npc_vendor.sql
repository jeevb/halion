-- Make orb of the sin'dorei more expensive

SET @CREATURE_TEMPLATE_START := 500100;
SET @ITEM_CUSTOM_COST_START := 50;

REPLACE INTO `custom_npc_vendor` (`entry`, `item`, `customCostId`) VALUES
    (@CREATURE_TEMPLATE_START + 6, 35275, @ITEM_CUSTOM_COST_START + 16);