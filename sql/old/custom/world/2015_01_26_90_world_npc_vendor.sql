DELETE FROM `npc_vendor` WHERE `entry` IN (500019, 500021) AND `item` IN (47131, 47088, 47059, 47188, 47451, 47464, 47432, 47477);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `VerifiedBuild`)
VALUES
-- Add Item Level 258 trinkets to Alliance PVE Gear Vendor
    (500019, 0, 47131, 0, 0, 2605, 1),
    (500019, 0, 47088, 0, 0, 2605, 1),
    (500019, 0, 47059, 0, 0, 2605, 1),
    (500019, 0, 47188, 0, 0, 2605, 1),
    (500019, 0, 47451, 0, 0, 2605, 1),
    (500019, 0, 47464, 0, 0, 2605, 1),
    (500019, 0, 47432, 0, 0, 2605, 1),
    (500019, 0, 47477, 0, 0, 2605, 1),
-- Add Item Level 258 trinkets to Horde PVE Gear Vendor
    (500021, 0, 47131, 0, 0, 2605, 1),
    (500021, 0, 47088, 0, 0, 2605, 1),
    (500021, 0, 47059, 0, 0, 2605, 1),
    (500021, 0, 47188, 0, 0, 2605, 1),
    (500021, 0, 47451, 0, 0, 2605, 1),
    (500021, 0, 47464, 0, 0, 2605, 1),
    (500021, 0, 47432, 0, 0, 2605, 1),
    (500021, 0, 47477, 0, 0, 2605, 1);

DELETE FROM `npc_vendor` WHERE `entry` IN (500033, 500029) AND `item` = 51535;
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `VerifiedBuild`)
VALUES
-- Add Wrathful Gladiator's War Edge to Alliance Wrathful Gear Vendor
    (500033, 0, 51535, 0, 0, 2967, 1),
-- Add Wrathful Gladiator's War Edge to Horde Wrathful Gear Vendor
    (500029, 0, 51535, 0, 0, 2967, 1);
