DELETE FROM `npc_trainer` WHERE `entry` = 1000016;
INSERT INTO `npc_trainer` (`entry`, `spell`, `spellcost`, `reqskill`, `reqskillvalue`, `reqlevel`)
VALUES
    (1000016, 3599, 0, 0, 0, 10),
    (1000016, 5394, 0, 0, 0, 20),
    (1000016, 8071, 0, 0, 0, 4);
