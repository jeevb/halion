-- This file is part of Maventell's initial release.
-- Includes work by Hash, Dash, Kaiyn and Slash

SET @SECTOR_ID := 1;

REPLACE INTO `sector_table_entry` (`id`, `mapId`, `flags`, `team`, `event`, `bounds`, `name`) VALUES
    (@SECTOR_ID, 0, 5, 0, 0, '(-1231.749390 1366.291016,-1062.987671 1374.458130,-1060.765137 1603.760010,-1228.978760 1603.609863)', 'Maventell Market'),
    (@SECTOR_ID + 1, 0, 2, 0, 0, '(-1111.381714 1607.692139,-1111.653687 1649.136963,-1162.841675 1649.492065,-1162.565308 1607.728027)', 'Maventell Barracks'),
    (@SECTOR_ID + 2, 0, 0, 0, 0, '(-1059.086182 1479.005005,-1002.024231 1478.999268,-1000.156799 1367.103760,-1062.430542 1374.338989)', 'The Alliance Vanguard'),
    (@SECTOR_ID + 3, 0, 0, 0, 0, '(-942.664795 1701.815552,-943.595154 1626.633301,-920.356140 1606.732178,-886.228638 1596.960083,-860.041138 1618.405640,-855.628967 1696.342407)', 'The Horde Outpost');