-- Fix object phasing in maventell.

SET @GAMEOBJECT_START := 400000;

UPDATE `gameobject` SET `phasemask` = 1 WHERE `phasemask` != 1 AND `guid` BETWEEN @GAMEOBJECT_START AND @GAMEOBJECT_START + 3495;