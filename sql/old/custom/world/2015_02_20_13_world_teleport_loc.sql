-- This file is part of Maventell's initial release.
-- Includes work by Hash, Dash, Kaiyn and Slash

SET @TELEPORT_LOC_START := 12;

REPLACE INTO `teleport_loc` (`id`, `x`, `y`, `z`, `o`, `mapId`, `team`, `trackMask`, `showOnUI`, `name`) VALUES
    (@TELEPORT_LOC_START, -903, 1655, 48.7, 0.5, 0, 67, 24, 1, 'The Horde Outpost'),
    (@TELEPORT_LOC_START + 1, -1046, 1415, 54.2, 3.14, 0, 469, 24, 1, 'The Alliance Vanguard'),
    (@TELEPORT_LOC_START + 2, -1155, 1473, 54.2, 1, 0, 0, 0, 1, 'Maventell Market'),
    (@TELEPORT_LOC_START + 3, -1126, 1665, 54.2, 4.9, 0, 0, 0, 1, 'Maventell Barracks');