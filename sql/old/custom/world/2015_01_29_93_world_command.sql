DELETE FROM `command` WHERE `permission` = 1005;
INSERT INTO `command` (`name`, `permission`, `help`)
VALUES
    ('modify vp', 1005, 'Syntax: .modify vp $amount $comment\nAdd or remove a specified amount of vote points from the selected player or self.');
