DELETE FROM `trinity_string` WHERE `entry` IN (30020, 30021, 30022);
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc1`, `content_loc2`, `content_loc3`, `content_loc4`, `content_loc5`, `content_loc6`, `content_loc7`, `content_loc8`)
VALUES
	(30022, '|cffe70000%s\n(PvP Area)|r', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(30021, '|cff9ee9ff%s\n(Sanctuary)|r', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(30020, '%s', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);