-- This file is part of Maventell's initial release.
-- Includes work by Hash, Dash, Kaiyn and Slash

SET @CREATURE_TEMPLATE_START := 500100;
SET @ITEM_TEMPLATE_START := 400000;

DELETE FROM `npc_vendor` WHERE `entry` BETWEEN (@CREATURE_TEMPLATE_START) AND (@CREATURE_TEMPLATE_START + 25);
REPLACE INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `VerifiedBuild`)
VALUES
    (@CREATURE_TEMPLATE_START + 1, 0, 49285, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 1, 0, 54860, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 1, 0, 54069, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 1, 0, 51954, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 1, 0, 51955, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 1, 0, 44164, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 1, 0, 44175, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 1, 0, 49286, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 13, 0, 49283, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 13, 0, 23720, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 13, 0, 37719, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 13, 0, 54068, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 13, 0, 32768, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 13, 0, 33809, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 13, 0, 49282, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 13, 0, 50250, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 13, 0, 43599, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 13, 0, 49290, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 13, 0, 49284, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 13, 0, 33184, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 13, 0, 54811, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 34519, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 19450, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 21168, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 32588, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 34493, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 49662, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 23713, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 33993, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 25535, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 12529, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 49287, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 41133, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 38658, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 49663, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 13582, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 32498, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 34518, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 19055, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 19054, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 34425, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 37297, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 34492, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 18964, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 45942, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 37298, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 49665, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 13584, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 38050, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 39656, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 23712, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 49362, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 56806, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 49646, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 13583, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 54847, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 44819, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 39286, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 49693, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 3, 0, 49343, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 4, 0, 20371, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 4, 0, 30360, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 4, 0, 22114, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 4, 0, 46802, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 4, 0, 45180, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 4, 0, 33079, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 18258, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 40195, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 6657, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 44812, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 34068, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 43489, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 31337, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 35275, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 44792, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 20410, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 20409, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 20399, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 20398, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 20397, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 20411, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 20414, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 45853, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 45850, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 45851, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 45852, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 45854, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 19979, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 52201, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 1973, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 37254, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 32782, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 6, 0, 49704, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 44625, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 44627, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 44626, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 44629, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 38308, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 38291, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 13508, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 37431, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 18640, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 43626, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 40110, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 18662, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 71153, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 27388, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 54455, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 35223, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 34498, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 38577, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 46779, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 38233, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 43352, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 46725, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 38266, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 33081, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 44958, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 7, 0, 45047, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 8, 0, 22999, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 8, 0, 38314, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 8, 0, 28788, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 8, 0, 38311, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 8, 0, 36941, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 8, 0, 19160, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 8, 0, 38312, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 8, 0, 23705, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 8, 0, 23709, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 8, 0, 38313, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 8, 0, 38309, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 8, 0, 38310, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 9313, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 9318, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 23770, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 23771, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 25886, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 23769, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 23768, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 41427, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 21747, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 5740, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 9314, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 9317, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 19026, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 9315, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 34850, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 8626, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 8624, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 8625, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 14, 0, 49703, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 2, 0, 34499, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 15, 0, 17782, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 2, 0, 45037, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 15, 0, 17142, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 15, 0, 32824, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 2, 0, 50471, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 2, 0, 33223, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 2, 0, 45063, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 2, 0, 35227, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 15, 0, 13262, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 2, 0, 32566, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 15, 0, 22589, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 15, 0, 22630, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 15, 0, 22631, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 15, 0, 22632, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 15, 0, 22691, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 2, 0, 46780, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 1, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 2, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 3, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 4, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 5, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 6, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 7, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 8, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 9, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 10, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 11, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 12, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 13, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 14, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 15, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 16, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 17, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 18, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 19, 0, 0, 2, 1),
    (@CREATURE_TEMPLATE_START + 16, 0, @ITEM_TEMPLATE_START + 20, 0, 0, 2, 1),
    -- Innkeep
    (@CREATURE_TEMPLATE_START + 9, 0, 33443, 0, 0, 0, 0),
    (@CREATURE_TEMPLATE_START + 9, 0, 33444, 0, 0, 0, 0),
    (@CREATURE_TEMPLATE_START + 9, 0, 33445, 0, 0, 0, 0),
    (@CREATURE_TEMPLATE_START + 9, 0, 33449, 0, 0, 0, 0),
    (@CREATURE_TEMPLATE_START + 9, 0, 33454, 0, 0, 0, 0),
    (@CREATURE_TEMPLATE_START + 9, 0, 35950, 0, 0, 0, 0),
    (@CREATURE_TEMPLATE_START + 9, 0, 35952, 0, 0, 0, 0),
    (@CREATURE_TEMPLATE_START + 9, 0, 35953, 0, 0, 0, 0),
    (@CREATURE_TEMPLATE_START + 9, 0, 35954, 0, 0, 0, 0);