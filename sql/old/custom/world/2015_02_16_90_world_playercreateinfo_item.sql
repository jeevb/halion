DELETE FROM `playercreateinfo_item` WHERE `itemid` IN (2101, 2102);
INSERT INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`)
VALUES
    (11, 3, 2101, -1),
    (10, 3, 2101, -1),
    (8, 3, 2101, -1),
    (4, 3, 2101, -1),
    (2, 3, 2101, -1),
    (6, 3, 2102, -1),
    (3, 3, 2102, -1);
