DELETE FROM `item_custom_cost` WHERE `id` BETWEEN 1 AND 11;
INSERT INTO `item_custom_cost` (`id`, `costVotePoints`, `costDonationPoints`, `costHonor`, `costArena`, `requiredItem`, `itemCount`, `guildLevel`, `guildReputation`, `1v1Rating`, `2v2Rating`, `3v3Rating`, `5v5Rating`, `bgRating`)
VALUES
    (1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
    (2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1700, 1600, 0, 0),
    (3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1750, 1650, 0, 0),
    (4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1800, 1700, 0, 0),
    (5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1850, 1750, 0, 0),
    (6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1900, 1800, 0, 0),
    (7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1950, 1850, 0, 0),
    (8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2000, 1900, 0, 0),
    (9, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2050, 1950, 0, 0),
    (10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2100, 2000, 0, 0),
    (11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2150, 2050, 0, 0);
