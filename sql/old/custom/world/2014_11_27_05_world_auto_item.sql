DELETE FROM `auto_item` WHERE `entry` IN (5175, 5176, 5177, 5178);
INSERT INTO `auto_item` (`raceMask`, `classMask`, `trackMask`, `reqLevel`, `entry`, `count`, `Note`)
VALUES
	(0, 64, 28, 4, 5175, 1, 'Earth Totem'),
	(0, 64, 28, 10, 5176, 1, 'Fire Totem'),
	(0, 64, 28, 20, 5177, 1, 'Water Totem'),
	(0, 64, 28, 30, 5178, 1, 'Air Totem');