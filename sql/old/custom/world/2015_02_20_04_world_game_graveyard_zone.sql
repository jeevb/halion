-- This file is part of Maventell's initial release.
-- Includes work by Hash, Dash, Kaiyn and Slash

SET @GRAVEYARD_ID := 1000;

DELETE FROM `game_graveyard_zone` WHERE `id` = @GRAVEYARD_ID;
REPLACE INTO `game_graveyard_zone` (`id`, `ghost_zone`, `faction`) VALUES
    (@GRAVEYARD_ID, 267, 0),
    (@GRAVEYARD_ID, 130, 0);