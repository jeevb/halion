DELETE FROM `disables` WHERE `sourceType`=3 AND `entry` IN (1, 9, 30);
INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `params_0`, `params_1`, `comment`)
VALUES
(3, 1, 0, "", "", "Disable Alterac Valley"),
(3, 9, 0, "", "", "Disable Strand of the Ancients"),
(3, 30, 0, "", "", "Disable isle of Conquest");

