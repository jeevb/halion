DELETE FROM `playercreateinfo_item` WHERE `itemid` = 4497;
INSERT INTO `playercreateinfo_item` (`race`, `class`, `itemid`, `amount`)
VALUES
    (0, 1, 4497, 4),
    (0, 2, 4497, 4),
    (0, 3, 4497, 3),
    (0, 4, 4497, 4),
    (0, 5, 4497, 4),
    (0, 7, 4497, 4),
    (0, 8, 4497, 4),
    (0, 9, 4497, 4),
    (0, 11, 4497, 4);

UPDATE `item_template` SET `SellPrice` = 0, `bonding` = 1 WHERE `entry` = 4497;
