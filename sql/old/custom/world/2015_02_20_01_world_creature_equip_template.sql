-- This file is part of Maventell's initial release.
-- Includes work by Hash, Dash, Kaiyn and Slash

SET @CREATURE_TEMPLATE_START := 500100;

DELETE FROM `creature_equip_template` WHERE `entry` BETWEEN @CREATURE_TEMPLATE_START + 35 AND @CREATURE_TEMPLATE_START + 42;
INSERT INTO `creature_equip_template` (`entry`, `id`, `itemEntry1`, `itemEntry2`, `itemEntry3`, `VerifiedBuild`)
VALUES
    (@CREATURE_TEMPLATE_START + 35, 1, 5277, 0, 0, 1),
    (@CREATURE_TEMPLATE_START + 36, 1, 2177, 0, 0, 1),
    (@CREATURE_TEMPLATE_START + 37, 1, 2711, 2147, 0, 1),
    (@CREATURE_TEMPLATE_START + 38, 1, 33295, 143, 11021, 1),
    (@CREATURE_TEMPLATE_START + 39, 1, 6618, 0, 0, 1),
    (@CREATURE_TEMPLATE_START + 40, 1, 5277, 0, 0, 1),
    (@CREATURE_TEMPLATE_START + 41, 1, 12297, 12298, 0, 1),
    (@CREATURE_TEMPLATE_START + 42, 1, 30848, 0, 0, 1);