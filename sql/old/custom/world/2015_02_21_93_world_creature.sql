SET @VENDOR_GUID_START := 300098;
SET @VENDOR_ID_START := 500143;

DELETE FROM `creature` WHERE `guid` BETWEEN @VENDOR_GUID_START AND @VENDOR_GUID_START + 3;
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`, `VerifiedBuild`)
VALUES
    (@VENDOR_GUID_START + 3, @VENDOR_ID_START + 3, 571, 0, 0, 1, 1, 0, 1, 8527.21, 727.688, 558.528, 3.87672, 300, 0, 0, 10080, 8814, 0, 0, 0, 0, 0),
    (@VENDOR_GUID_START + 2, @VENDOR_ID_START + 2, 571, 0, 0, 1, 1, 0, 1, 5750.85, 738.005, 653.664, 0.964548, 300, 0, 0, 10080, 8814, 0, 0, 0, 0, 0),
    (@VENDOR_GUID_START + 1, @VENDOR_ID_START + 1, 571, 0, 0, 1, 1, 0, 1, 8504.22, 727.265, 558.528, 5.55901, 300, 0, 0, 10080, 8814, 0, 0, 0, 0, 0),
    (@VENDOR_GUID_START, @VENDOR_ID_START, 571, 0, 0, 1, 1, 0, 1, 5940.19, 511.162, 650.18, 2.75417, 300, 0, 0, 10080, 8814, 0, 0, 0, 0, 0);
