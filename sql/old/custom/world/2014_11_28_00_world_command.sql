DELETE FROM `command` WHERE `permission` = 1001;
INSERT INTO `command` (`name`, `permission`, `help`)
VALUES
	('modify xprate', 1001, 'Syntax: .modify xprate #rate\n\nModifies the experience rate of the targeted player or self.');
