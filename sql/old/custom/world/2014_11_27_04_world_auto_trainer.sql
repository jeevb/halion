DELETE FROM `auto_trainer` WHERE `entry` IN (1000000, 1000001, 1000002, 1000003, 1000004, 1000005, 1000006, 1000007, 1000008, 1000009, 1000010, 1000011, 1000012, 1000013, 1000014, 1000015, 1000016, 1000017, 1000018, 1000019, 1000050, 1000051, 1000052);

INSERT INTO `auto_trainer` (`raceMask`, `classMask`, `trackMask`, `entry`, `priority`, `Note`)
VALUES
	(0, 1, 30, 1000000, 1, 'Warrior Trainer'),
	(0, 2, 30, 1000001, 1, 'Paladin Trainer'),
	(0, 4, 30, 1000002, 1, 'Hunter Trainer'),
	(0, 8, 30, 1000003, 1, 'Rogue Trainer'),
	(0, 16, 30, 1000004, 1, 'Priest Trainer'),
	(0, 32, 30, 1000005, 1, 'Death Knight Trainer'),
	(0, 64, 30, 1000006, 1, 'Shaman Trainer'),
	(0, 128, 30, 1000007, 1, 'Mage Trainer'),
	(0, 256, 30, 1000008, 1, 'Warlock Trainer'),
	(0, 1024, 30, 1000009, 1, 'Druid Trainer'),
	(0, 1, 28, 1000010, 1, 'Warrior Quest Spells'),
	(0, 2, 28, 1000011, 1, 'Paladin Quest Spells'),
	(0, 4, 28, 1000012, 1, 'Hunter Quest Spells'),
	(0, 8, 28, 1000013, 1, 'Rogue Quest Spells'),
	(0, 16, 28, 1000014, 1, 'Priest Quest Spells'),
	(0, 32, 28, 1000015, 1, 'Death Knight Quest Spells'),
	(0, 64, 28, 1000016, 1, 'Shaman Quest Spells'),
	(0, 128, 28, 1000017, 1, 'Mage Quest Spells'),
	(0, 256, 28, 1000018, 1, 'Warlock Quest Spells'),
	(0, 1024, 28, 1000019, 1, 'Druid Quest Spells'),
	(0, 1503, 30, 1000050, 0, 'Riding Trainer'),
	(0, 32, 30, 1000051, 0, 'Riding Trainer (Death Knight)'),
	(0, 0, 30, 1000052, 0, 'Weapons Trainer');
