SET @ENTRY := 500000;

-- Horde mount vendor should sit on a mount
DELETE FROM `creature_template_addon` WHERE `entry`=@ENTRY;
INSERT INTO `creature_template_addon` (`entry`, `mount`)
VALUES
(@ENTRY, 15289);

-- Aethalas' pet should lie on the ground
DELETE FROM `creature_template_addon` WHERE `entry`=@ENTRY+14;
INSERT INTO `creature_template_addon` (`entry`, `bytes1`)
VALUES
(@ENTRY+14, 3);

-- Misha (Rexxar's pet) should sit on the ground
DELETE FROM `creature_template_addon` WHERE `entry`=@ENTRY+45;
INSERT INTO `creature_template_addon` (`entry`, `bytes1`)
VALUES
(@ENTRY+45, 1);