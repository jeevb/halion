SET @ENTRY := 500000;

/*
Horde Battleground Rating Vendor    = @ENTRY+46
Alliance Battleground Rating Vendor = @ENTRY+47
*/

-- Battleground Rating Vendors
DELETE FROM `npc_vendor` WHERE `entry` IN (@ENTRY+46, @ENTRY+47);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `VerifiedBuild`)
VALUES
    (@ENTRY+46, 0, 49890, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49893, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49894, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49895, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49896, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49897, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49905, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49906, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49907, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49949, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49950, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49960, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49967, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49975, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49977, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49978, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49983, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49985, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49989, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49990, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49993, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49994, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49998, 0, 0, 2, 1),
    (@ENTRY+46, 0, 49999, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50000, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50002, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50005, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50008, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50009, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50010, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50014, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50015, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50019, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50023, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50025, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50030, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50032, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50036, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50061, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50062, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50063, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50067, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50069, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50071, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50074, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50170, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50174, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50175, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50180, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50182, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50185, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50186, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50187, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50190, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50195, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50205, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50333, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50343, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50344, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50345, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50346, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50351, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50352, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50353, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50354, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50355, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50356, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50357, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50358, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50359, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50360, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50361, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50362, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50413, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50414, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50416, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50417, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50421, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50424, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50447, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50451, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50452, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50453, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50466, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50467, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50468, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50469, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50470, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50987, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50989, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50991, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50992, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50993, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50994, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50995, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50996, 0, 0, 2, 1),
    (@ENTRY+46, 0, 50997, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51413, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51414, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51415, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51416, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51418, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51419, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51420, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51421, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51422, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51424, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51425, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51426, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51427, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51428, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51430, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51433, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51434, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51435, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51436, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51438, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51458, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51459, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51460, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51461, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51462, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51463, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51464, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51465, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51466, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51467, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51468, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51469, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51470, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51471, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51473, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51474, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51475, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51476, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51477, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51479, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51482, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51483, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51484, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51485, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51486, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51487, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51488, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51489, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51490, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51491, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51492, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51493, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51494, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51495, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51496, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51497, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51498, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51499, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51500, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51502, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51503, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51504, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51505, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51506, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51508, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51509, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51510, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51511, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51512, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51514, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51536, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51537, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51538, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51539, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51540, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51541, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51542, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51543, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51544, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51545, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51816, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51818, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51820, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51821, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51822, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51826, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51831, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51832, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51836, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51842, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51843, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51848, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51849, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51850, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51853, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51855, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51856, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51862, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51863, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51867, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51871, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51872, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51873, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51878, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51879, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51884, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51885, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51888, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51890, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51891, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51894, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51899, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51900, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51901, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51907, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51908, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51912, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51913, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51914, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51915, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51918, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51919, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51920, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51925, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51929, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51930, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51931, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51933, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51934, 0, 0, 2, 1),
    (@ENTRY+46, 0, 51935, 0, 0, 2, 1),

    (@ENTRY+47, 0, 49890, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49893, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49894, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49895, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49896, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49897, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49905, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49906, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49907, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49949, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49950, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49960, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49967, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49975, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49977, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49978, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49983, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49985, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49989, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49990, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49993, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49994, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49998, 0, 0, 2, 1),
    (@ENTRY+47, 0, 49999, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50000, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50002, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50005, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50008, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50009, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50010, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50014, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50015, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50019, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50023, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50025, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50030, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50032, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50036, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50061, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50062, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50063, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50067, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50069, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50071, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50074, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50170, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50174, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50175, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50180, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50182, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50185, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50186, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50187, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50190, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50195, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50205, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50333, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50343, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50344, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50345, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50346, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50351, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50352, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50353, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50354, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50355, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50356, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50357, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50358, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50359, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50360, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50361, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50362, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50413, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50414, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50416, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50417, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50421, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50424, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50447, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50451, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50452, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50453, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50466, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50467, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50468, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50469, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50470, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50987, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50989, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50991, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50992, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50993, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50994, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50995, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50996, 0, 0, 2, 1),
    (@ENTRY+47, 0, 50997, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51413, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51414, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51415, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51416, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51418, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51419, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51420, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51421, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51422, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51424, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51425, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51426, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51427, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51428, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51430, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51433, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51434, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51435, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51436, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51438, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51458, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51459, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51460, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51461, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51462, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51463, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51464, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51465, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51466, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51467, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51468, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51469, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51470, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51471, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51473, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51474, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51475, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51476, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51477, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51479, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51482, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51483, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51484, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51485, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51486, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51487, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51488, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51489, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51490, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51491, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51492, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51493, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51494, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51495, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51496, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51497, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51498, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51499, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51500, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51502, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51503, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51504, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51505, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51506, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51508, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51509, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51510, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51511, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51512, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51514, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51536, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51537, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51538, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51539, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51540, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51541, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51542, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51543, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51544, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51545, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51816, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51818, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51820, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51821, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51822, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51826, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51831, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51832, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51836, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51842, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51843, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51848, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51849, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51850, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51853, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51855, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51856, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51862, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51863, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51867, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51871, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51872, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51873, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51878, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51879, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51884, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51885, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51888, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51890, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51891, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51894, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51899, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51900, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51901, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51907, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51908, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51912, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51913, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51914, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51915, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51918, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51919, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51920, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51925, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51929, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51930, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51931, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51933, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51934, 0, 0, 2, 1),
    (@ENTRY+47, 0, 51935, 0, 0, 2, 1);
