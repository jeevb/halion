SET @ENTRY := 500000;

-- Add gear vendor script to gear vendors
UPDATE `creature_template` SET `scriptName`='cg_gear_vendor' WHERE `entry` IN (@ENTRY+11, @ENTRY+16, @ENTRY+23, @ENTRY+30, @ENTRY+29, @ENTRY+21, @ENTRY+33, @ENTRY+19);


DELETE FROM `npc_vendor` WHERE `entry` IN (@ENTRY, @ENTRY+5);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `ExtendedCost`, `VerifiedBuild`)
VALUES
-- Horde vendor
(@ENTRY, 0, 28936, 2, 1),
(@ENTRY, 1, 29224, 2, 1),
(@ENTRY, 2, 29223, 2, 1),
(@ENTRY, 3, 18796, 2, 1),
(@ENTRY, 4, 18797, 2, 1),
(@ENTRY, 5, 18798, 2, 1),
(@ENTRY, 6, 18794, 2, 1),
(@ENTRY, 7, 18795, 2, 1),
(@ENTRY, 8, 18793, 2, 1),
(@ENTRY, 9, 18788, 2, 1),
(@ENTRY, 10, 18789, 2, 1),
(@ENTRY, 11, 18790, 2, 1),
(@ENTRY, 12, 13334, 2, 1),
(@ENTRY, 13, 47101, 2, 1),
(@ENTRY, 14, 18791, 2, 1),
-- Alliance vendor
(@ENTRY+5, 0, 29745, 2, 1),
(@ENTRY+5, 1, 29746, 2, 1),
(@ENTRY+5, 2, 29747, 2, 1),
(@ENTRY+5, 3, 18785, 2, 1),
(@ENTRY+5, 4, 18786, 2, 1),
(@ENTRY+5, 5, 18787, 2, 1),
(@ENTRY+5, 6, 18772, 2, 1),
(@ENTRY+5, 7, 18773, 2, 1),
(@ENTRY+5, 8, 18774, 2, 1),
(@ENTRY+5, 9, 18776, 2, 1),
(@ENTRY+5, 10, 18777, 2, 1),
(@ENTRY+5, 11, 18778, 2, 1),
(@ENTRY+5, 12, 18766, 2, 1),
(@ENTRY+5, 13, 18767, 2, 1),
(@ENTRY+5, 14, 18902, 2, 1);