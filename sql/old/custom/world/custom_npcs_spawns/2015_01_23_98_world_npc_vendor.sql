DELETE FROM `npc_vendor` WHERE `item` = 43236 AND `entry` IN (500007, 500009, 500028, 500037);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `VerifiedBuild`)
VALUES
    (500007, 0, 43236, 0, 0, 2, 1),
    (500009, 0, 43236, 0, 0, 2, 1),
    (500028, 0, 43236, 0, 0, 2, 1),
    (500037, 0, 43236, 0, 0, 2, 1);
