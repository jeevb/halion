DELETE FROM `item_custom_cost` WHERE `id` BETWEEN 12 AND 14;
INSERT INTO `item_custom_cost` (`id`, `costVotePoints`, `costDonationPoints`, `costHonor`, `costArena`, `requiredItem`, `itemCount`, `guildLevel`, `guildReputation`, `1v1Rating`, `2v2Rating`, `3v3Rating`, `5v5Rating`, `bgRating`)
VALUES
    (12, 0, 0, 31000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2120),
    (13, 0, 0, 47400, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2210),
    (14, 0, 0, 47400, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2600);
