DELETE FROM `world_safe_locs_extra` WHERE `id` IN (10000, 10001);
INSERT INTO `world_safe_locs_extra` (`id`, `mapId`, `x`, `y`, `z`, `name`)
VALUES
(10000, 1, -11873.919922 , -4848.459473, 2.232510, "Tanaris PvP Malls - Horde Side"),
(10001, 1, -11281.249023, -4707.828125, 2.390861, "Tanaris PvP Malls - Alliance Side");

DELETE FROM `game_graveyard_zone` WHERE `id` IN (10000, 10001);
INSERT INTO `game_graveyard_zone` (`id`, `ghost_zone`, `faction`)
VALUES
(10000, 440, 67),
(10001, 440, 469);
