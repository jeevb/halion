SET @ENTRY := 500000;
DELETE FROM `npc_vendor` WHERE `entry` IN (@ENTRY+7, @ENTRY+9, @ENTRY+28, @ENTRY+37);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `incrtime`, `maxcount`, `ExtendedCost`, `VerifiedBuild`)
VALUES
(@ENTRY+7, 0, 33445, 0, 0, 2, 1),
(@ENTRY+7, 0, 35948, 0, 0, 2, 1),
(@ENTRY+7, 0, 35951, 0, 0, 2, 1),
(@ENTRY+7, 0, 40202, 0, 0, 2, 1),
(@ENTRY+9, 0, 33445, 0, 0, 2, 1),
(@ENTRY+9, 0, 35948, 0, 0, 2, 1),
(@ENTRY+9, 0, 35951, 0, 0, 2, 1),
(@ENTRY+9, 0, 40202, 0, 0, 2, 1),
(@ENTRY+28, 0, 33445, 0, 0, 2, 1),
(@ENTRY+28, 0, 35948, 0, 0, 2, 1),
(@ENTRY+28, 0, 35951, 0, 0, 2, 1),
(@ENTRY+28, 0, 40202, 0, 0, 2, 1),
(@ENTRY+37, 0, 33445, 0, 0, 2, 1),
(@ENTRY+37, 0, 35948, 0, 0, 2, 1),
(@ENTRY+37, 0, 35951, 0, 0, 2, 1),
(@ENTRY+37, 0, 40202, 0, 0, 2, 1);