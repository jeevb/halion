DELETE FROM `npc_vendor` WHERE `entry` IN (500002, 500017);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `VerifiedBuild`)
VALUES
    (500002, 0, 35948, 0, 0, 2, 1),
    (500002, 0, 35951, 0, 0, 2, 1),
    (500002, 0, 40202, 0, 0, 2, 1),
    (500017, 0, 35948, 0, 0, 2, 1),
    (500017, 0, 35951, 0, 0, 2, 1),
    (500017, 0, 40202, 0, 0, 2, 1);
