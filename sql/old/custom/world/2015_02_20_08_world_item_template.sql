-- This file is part of Maventell's initial release.
-- Includes work by Hash, Dash, Kaiyn and Slash

SET @ITEM_TEMPLATE_START := 400000;

UPDATE `item_template` SET `BuyCount` = 5 WHERE `entry` IN(18258, 40195, 6657, 44812, 34068, 43489, 9313, 9318, 23770, 23771, 25886, 23769, 23768, 41427, 21747, 5740, 9314, 9317, 19026, 9315, 34850, 44625, 44627, 44626, 44629, 38308, 38291, 13508, 37431, 18640, 43626, 18662, 71153, 27388, 54455, 35223, 34498, 38577, 46779, 38233, 43352, 38266, 45047, 33081, 44958);
UPDATE `item_template` SET `BuyCount` = 1 WHERE `entry` IN(46725, 40110);
UPDATE `item_template` SET `maxcount` = 1 WHERE `entry` IN(40110);

REPLACE INTO item_template (`entry`, `class`, `name`, `displayid`, `Quality`, `Flags`, `ItemLevel`, `delay`, `bonding`, `description`, `Material`) VALUES
    (@ITEM_TEMPLATE_START + 1, 15, 'Hey, I am available outfit', 56915, 4, 4, 1, 0, 1, 'I\'m Single and I know it!', 4),
    (@ITEM_TEMPLATE_START + 2, 15, 'Vain Female Outfit', 56915, 4, 4, 1, 0, 1, 'With looks this pretty you\'ll need even more mirrors!', 4),
    (@ITEM_TEMPLATE_START + 3, 15, 'Belly Button gap YO!', 56915, 4, 4, 1, 0, 1, 'The Belly button gap is to stronk!', 4),
    (@ITEM_TEMPLATE_START + 4, 15, 'The Suit', 56915, 4, 4, 1, 0, 1, 'I\'m better than you!', 4),
    (@ITEM_TEMPLATE_START + 5, 15, 'Stormwind Guard Outfit', 56915, 4, 4, 1, 0, 1, 'Royal Guard of Stormwind', 4),
    (@ITEM_TEMPLATE_START + 6, 15, 'Farmer Outfit', 56915, 4, 4, 1, 0, 1, 'Hello, I am Farmer Ash!', 4),
    (@ITEM_TEMPLATE_START + 7, 15, 'Clothes Salesman Outfit', 56915, 4, 4, 1, 0, 1, 'Wanna buy my gear?', 4),
    (@ITEM_TEMPLATE_START + 8, 15, 'Wizard Outfit', 56915, 4, 4, 1, 0, 1, 'You\'re a wizard, Harrie!', 4),
    (@ITEM_TEMPLATE_START + 9, 15, 'Bunny Outfit', 56915, 4, 4, 1, 0, 1, 'Hop your way to victory!', 4),
    (@ITEM_TEMPLATE_START + 10, 15, 'Ninja Outfit', 56915, 4, 4, 1, 0, 1, 'You cant see me!', 4),
    (@ITEM_TEMPLATE_START + 11, 15, 'Romeo & Juliet Outfit', 56915, 4, 4, 1, 0, 1, 'Love is in the air!', 4),
    (@ITEM_TEMPLATE_START + 12, 15, 'Pirate Outfit', 56915, 4, 4, 1, 0, 1, 'Yarrr me matey, shiver me timbers!', 4),
    (@ITEM_TEMPLATE_START + 13, 15, 'Chef Outfit', 56915, 4, 4, 1, 0, 1, 'Want me to make you a sandwich?', 4),
    (@ITEM_TEMPLATE_START + 14, 15, 'Fisher Outfit', 56915, 4, 4, 1, 0, 1, 'Hey ho captain fisherman!', 4),
    (@ITEM_TEMPLATE_START + 15, 15, 'Alchemist Outfit', 56915, 4, 4, 1, 0, 1, 'Pots, pots and more pots!', 4),
    (@ITEM_TEMPLATE_START + 16, 15, 'Rawr Outfit', 56915, 4, 4, 1, 0, 1, 'Hear me rawr!', 4),
    (@ITEM_TEMPLATE_START + 17, 15, 'Jungle Outfit', 56915, 4, 4, 1, 0, 1, 'AHHAHAHAHAHHH', 4),
    (@ITEM_TEMPLATE_START + 18, 15, 'Rich Man Outfit', 56915, 4, 4, 1, 0, 1, 'I\'ll check the time on my diamond watch', 4),
    (@ITEM_TEMPLATE_START + 19, 15, 'Pre-Hooker Outfit', 56915, 4, 4, 1, 0, 1, 'Pre-Hooker Outfit', 4),
    (@ITEM_TEMPLATE_START + 20, 15, 'Jedi Outfit', 56915, 4, 4, 1, 0, 1, 'Luke, I am your father! Note: contains a sword.', 4);