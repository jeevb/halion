DELETE FROM `auto_item` WHERE `entry` BETWEEN 500000 AND 500019;
INSERT INTO `auto_item` (`raceMask`, `classMask`, `trackMask`, `reqLevel`, `entry`, `count`, `onLevelReach`, `Note`)
VALUES
    (0, 1024, 6, 80, 500017, 1, 1, 'Druid: Level 80 DPS Gear'),
    (0, 1024, 6, 80, 500018, 1, 1, 'Druid: Level 80 Tank Gear'),
    (0, 1024, 6, 80, 500019, 1, 1, 'Druid: Level 80 Heal Gear'),
    (0, 1024, 6, 80, 500016, 1, 1, 'Druid: Level 80 RDPS Gear'),
    (0, 256, 6, 80, 500015, 1, 1, 'Warlock: Level 80 DPS Gear'),
    (0, 128, 6, 80, 500014, 1, 1, 'Mage: Level 80 DPS Gear'),
    (0, 64, 6, 80, 500013, 1, 1, 'Shaman: Level 80 Heal Gear'),
    (0, 64, 6, 80, 500012, 1, 1, 'Shaman: Level 80 DPS Gear'),
    (0, 64, 6, 80, 500011, 1, 1, 'Shaman: Level 80 RDPS Gear'),
    (0, 32, 6, 80, 500010, 1, 1, 'Death Knight: Level 80 Tank Gear'),
    (0, 32, 6, 80, 500009, 1, 1, 'Death Knight: Level 80 DPS Gear'),
    (0, 16, 6, 80, 500008, 1, 1, 'Priest: Level 80 DPS Gear'),
    (0, 16, 6, 80, 500007, 1, 1, 'Priest: Level 80 Heal Gear'),
    (0, 8, 6, 80, 500006, 1, 1, 'Rogue: Level 80 DPS Gear'),
    (0, 4, 6, 80, 500005, 1, 1, 'Hunter: Level 80 DPS Gear'),
    (0, 2, 6, 80, 500004, 1, 1, 'Paladin: Level 80 Tank Gear'),
    (0, 2, 6, 80, 500003, 1, 1, 'Paladin: Level 80 DPS Gear'),
    (0, 2, 6, 80, 500002, 1, 1, 'Paladin: Level 80 Heal Gear'),
    (0, 1, 6, 80, 500001, 1, 1, 'Warrior: Level 80 Tank Gear'),
    (0, 1, 6, 80, 500000, 1, 1, 'Warrior: Level 80 DPS Gear');
