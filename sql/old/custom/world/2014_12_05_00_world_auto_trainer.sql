DELETE FROM `npc_trainer` WHERE `entry` = 1000050;
INSERT INTO `npc_trainer` (`entry`, `spell`, `spellcost`, `reqskill`, `reqskillvalue`, `reqlevel`)
VALUES
	(1000050, 33388, 40000, 762, 0, 20),
	(1000050, 33391, 500000, 762, 75, 40);

DELETE FROM `auto_trainer` WHERE `entry` IN (1000050, 1000051);
INSERT INTO `auto_trainer` (`raceMask`, `classMask`, `trackMask`, `entry`, `priority`, `Note`)
VALUES
	(0, 1503, 30, 1000050, 0, 'Riding Trainer (PvE)'),
	(0, 0, 24, 1000051, 0, 'Riding Trainer (PvP)');
