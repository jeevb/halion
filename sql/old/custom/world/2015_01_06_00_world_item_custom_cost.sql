# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.40-MariaDB-1~wheezy)
# Database: world
# Generation Time: 2015-01-07 05:23:56 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table item_custom_cost
# ------------------------------------------------------------

DROP TABLE IF EXISTS `item_custom_cost`;

CREATE TABLE `item_custom_cost` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `costVotePoints` int(10) unsigned NOT NULL DEFAULT '0',
  `costDonationPoints` int(10) unsigned NOT NULL DEFAULT '0',
  `costHonor` int(10) unsigned NOT NULL DEFAULT '0',
  `costArena` int(10) unsigned NOT NULL DEFAULT '0',
  `requiredItem` int(10) NOT NULL DEFAULT '0',
  `itemCount` int(10) NOT NULL DEFAULT '0',
  `guildLevel` int(10) NOT NULL DEFAULT '0',
  `guildReputation` int(10) NOT NULL DEFAULT '0',
  `1v1Rating` int(10) NOT NULL DEFAULT '0',
  `2v2Rating` int(10) NOT NULL DEFAULT '0',
  `3v3Rating` int(10) NOT NULL DEFAULT '0',
  `5v5Rating` int(10) NOT NULL DEFAULT '0',
  `bgRating` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
