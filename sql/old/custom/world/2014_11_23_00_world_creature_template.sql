-- Onyx Flamecaller (Normal & Heroic)
UPDATE `creature_template` SET `DamageModifier`=7.5 WHERE `entry` IN (39814, 39815);

-- Baltharus the Warborn (Normal)
UPDATE `creature_template` SET `DamageModifier`=35 WHERE `entry`=39899;

-- Baltharus the Warborn (Heroic)
UPDATE `creature_template` SET `DamageModifier`=70 WHERE `entry`=39922;

-- Saviana Ragefire (Normal)
UPDATE `creature_template` SET `DamageModifier`=35 WHERE `entry`=39747;

-- Saviana Ragefire (Heroic)
UPDATE `creature_template` SET `DamageModifier`=70 WHERE `entry`=39823;
