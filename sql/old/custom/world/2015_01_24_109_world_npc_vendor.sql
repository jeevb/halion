DELETE FROM `npc_vendor` WHERE `item` = 50255 AND `entry` IN (31580, 31582, 32509, 35508, 35507, 34885);
INSERT INTO `npc_vendor` (`entry`, `slot`, `item`, `maxcount`, `incrtime`, `ExtendedCost`, `VerifiedBuild`)
VALUES
    (31580, 0, 50255, 0, 0, 2, 0),
    (31582, 0, 50255, 0, 0, 2, 0),
    (32509, 0, 50255, 0, 0, 2, 0),
    (35508, 0, 50255, 0, 0, 2, 0),
    (35507, 0, 50255, 0, 0, 2, 0),
    (34885, 0, 50255, 0, 0, 2, 0);
