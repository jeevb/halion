DELETE FROM `creature_addon` WHERE `guid` IN (400090, 400051, 400013, 400091, 400095, 400098, 400096, 400099);
INSERT INTO `creature_addon` (`guid`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`)
VALUES
    (400090, 5000003, 0, 0, 0, 0, NULL),
    (400051, 5000001, 0, 0, 0, 0, NULL),
    (400013, 5000000, 0, 0, 0, 0, NULL),
    (400091, 5000002, 0, 0, 0, 0, NULL),
    (400095, 5000006, 0, 0, 0, 0, NULL),
    (400098, 5000005, 0, 0, 0, 0, NULL),
    (400096, 5000004, 0, 0, 0, 0, NULL),
    (400099, 5000007, 0, 0, 0, 0, NULL);
