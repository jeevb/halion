DELETE FROM `spell_ranks` WHERE `first_spell_id` IN (12880, 57514, 57518);
INSERT INTO `spell_ranks` (`first_spell_id`, `spell_id`, `rank`) VALUES
(12880, 12880, 1), -- Enrage
(12880, 14201, 2), 
(12880, 14202, 3),
(12880, 14203, 4),
(12880, 14204, 5),
(57514, 57514, 1), -- Improved Defensive Stance
(57514, 57516, 2), 
(57518, 57518, 1), -- Wrecking Crew
(57518, 57519, 2), 
(57518, 57520, 3),
(57518, 57521, 4),
(57518, 57522, 5);