DELETE FROM `gameobject` WHERE `guid` BETWEEN 60155 AND 60158;
INSERT INTO `gameobject` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `position_x`, `position_y`, `position_z`, `orientation`, `rotation0`, `rotation1`, `rotation2`, `rotation3`, `spawntimesecs`, `animprogress`, `state`, `VerifiedBuild`)
VALUES
    (60155, 184633, 571, 0, 0, 1, 1, 5919.97, 692.543, 642.482, 2.67735, 0, 0, -0.333807, 0.942641, 300, 0, 1, 0),
    (60156, 184633, 571, 0, 0, 1, 1, 5925.95, 689.406, 642.88, 2.70501, 0, 0, 0.976269, 0.216561, 300, 0, 1, 0),
    (60157, 184633, 571, 0, 0, 1, 1, 5911.68, 683.148, 643.496, 3.95437, 0, 0, 0.918555, -0.395293, 300, 0, 1, 0),
    (60158, 184633, 571, 0, 0, 1, 1, 5904.2, 672.987, 643.496, 0.769572, 0, 0, 0.375361, 0.926879, 300, 0, 1, 0);
