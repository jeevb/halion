DELETE FROM `trinity_string` WHERE `entry` IN (40000, 40001);
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc1`, `content_loc2`, `content_loc3`, `content_loc4`, `content_loc5`, `content_loc6`, `content_loc7`, `content_loc8`)
VALUES
	(40001, 'Successfully modified vote points for |cffffffff%s|r: Amount = %i.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
	(40000, 'Failed to modify vote points for |cffffffff%s|r:  Amount = %i.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
