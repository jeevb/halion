SET @ENTRY := 1000060;

DELETE FROM `auto_trainer` WHERE `entry` BETWEEN @ENTRY AND @ENTRY+11;
INSERT INTO `auto_trainer` (`raceMask`, `classMask`, `trackMask`, `entry`, `priority`, `Note`)
VALUES
    (1, 1245, 30, @ENTRY, 1, 'Human Ground Mount'),
    (4, 1245, 30, @ENTRY+1, 1, 'Dwarf Ground Mount'),
    (8, 1245, 30, @ENTRY+2, 1, 'Night Elf Ground Mount'),
    (64, 1245, 30, @ENTRY+3, 1, 'Gnome Ground Mount'),
    (1024, 1245, 30, @ENTRY+4, 1, 'Draenei Ground Mount'),
    (2, 1245, 30, @ENTRY+5, 1, 'Orc Ground Mount'),
    (128, 1245, 30, @ENTRY+6, 1, 'Troll Ground Mount'),
    (32, 1245, 30, @ENTRY+7, 1, 'Tauren Ground Mount'),
    (16, 1245, 30, @ENTRY+8, 1, 'Undead Ground Mount'),
    (512, 1245, 30, @ENTRY+9, 1, 'Blood Elf Ground Mount'),
    (1101, 511, 6, @ENTRY+10, 1, 'Alliance Flying Mount'),
    (690, 511, 6, @ENTRY+11, 1, 'Horde Flying Mount');
