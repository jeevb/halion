-- Make bosses in ICC immune to Stuns, Interrupts, Charms,Disarms,fear,snare,banish,Silence
update creature_template set mechanic_immune_mask = 617299803 where rank = 3 and entry IN (SELECT creature_id FROM creature_onkill_reputation WHERE rewOnKillRepFaction1 = 1156);

-- Make ozz on Profession not stunable,Fearable
update creature_template set mechanic_immune_mask = 42548280 where entry = 37697;
update creature_template set mechanic_immune_mask = 42548280 where entry = 37562;

-- Druid Teleport:Moonglade
DELETE FROM npc_trainer WHERE entry IN (4217, 3034);
INSERT INTO npc_trainer (entry, spell) VALUES (4217, 18960);
INSERT INTO npc_trainer (entry, spell) VALUES (3034, 18960);

/* Herb nodes
Give a max count of 8 and min count of 2 for lichbloom nodes */
update gameobject_loot_template set mincount = 2, maxcount = 8 where item = 36905;
