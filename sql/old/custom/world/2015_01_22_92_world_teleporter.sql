DELETE FROM `npc_text` WHERE `ID` = 1000021;
INSERT INTO `npc_text` (`ID`, `text0_0`, `text0_1`, `BroadcastTextID0`, `lang0`, `prob0`, `em0_0`, `em0_1`, `em0_2`, `em0_3`, `em0_4`, `em0_5`, `text1_0`, `text1_1`, `BroadcastTextID1`, `lang1`, `prob1`, `em1_0`, `em1_1`, `em1_2`, `em1_3`, `em1_4`, `em1_5`, `text2_0`, `text2_1`, `BroadcastTextID2`, `lang2`, `prob2`, `em2_0`, `em2_1`, `em2_2`, `em2_3`, `em2_4`, `em2_5`, `text3_0`, `text3_1`, `BroadcastTextID3`, `lang3`, `prob3`, `em3_0`, `em3_1`, `em3_2`, `em3_3`, `em3_4`, `em3_5`, `text4_0`, `text4_1`, `BroadcastTextID4`, `lang4`, `prob4`, `em4_0`, `em4_1`, `em4_2`, `em4_3`, `em4_4`, `em4_5`, `text5_0`, `text5_1`, `BroadcastTextID5`, `lang5`, `prob5`, `em5_0`, `em5_1`, `em5_2`, `em5_3`, `em5_4`, `em5_5`, `text6_0`, `text6_1`, `BroadcastTextID6`, `lang6`, `prob6`, `em6_0`, `em6_1`, `em6_2`, `em6_3`, `em6_4`, `em6_5`, `text7_0`, `text7_1`, `BroadcastTextID7`, `lang7`, `prob7`, `em7_0`, `em7_1`, `em7_2`, `em7_3`, `em7_4`, `em7_5`, `VerifiedBuild`)
VALUES
    (1000021, 'Wooooossshhhh... The abyss awaits...', NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);


DELETE FROM `teleport_loc` WHERE `id` BETWEEN 1 AND 13;
INSERT INTO `teleport_loc` (`id`, `x`, `y`, `z`, `o`, `mapId`, `team`, `trackMask`, `showOnUI`, `name`)
VALUES
    (1, 2353.53, -5665.82, 382.249, 0.596, 0, 0, 0, 0, 'Acherus'),
    (2, 1494.98, -4413.65, 23.0172, 0.060435, 1, 67, 0, 1, 'Orgrimmar'),
    (3, 1559.5, 241.046, -43.1024, 6.2268, 0, 67, 0, 1, 'Undercity'),
    (4, -1277.37, 124.804, 131.287, 5.22274, 1, 67, 0, 1, 'Thunder Bluff'),
    (5, 9487.69, -7279.2, 14.2866, 6.16478, 530, 67, 0, 1, 'Silvermoon City'),
    (6, -8833.38, 628.628, 94.0066, 1.06535, 0, 469, 0, 1, 'Stormwind City'),
    (7, -4898.06, -964.315, 501.447, 2.26224, 0, 469, 0, 1, 'Ironforge'),
    (8, 9949.56, 2284.21, 1341.39, 1.59587, 1, 469, 0, 1, 'Darnassus'),
    (9, -3949.77, -11656.6, -138.638, 2.5761, 530, 469, 0, 1, 'Exodar'),
    (10, -1846.96, 5394.25, -12.428, 2.03256, 530, 0, 0, 1, 'Shattrath City'),
    (11, 5804.15, 624.771, 647.767, 1.64, 571, 0, 0, 1, 'Dalaran'),
    (12, -11833.6, -4789.82, 6.07506, 3.39956, 1, 67, 24, 1, 'The Horde PvP Mall'),
    (13, -11353.9, -4723.1, 6.31849, 6.14609, 1, 469, 24, 1, 'The Alliance PvP Mall');
