-- Herbs
update gameobject_loot_template set mincount = 2, maxcount= 8 where item = 36906;
update gameobject_loot_template set mincount = 2, maxcount= 8 where item = 36901;
update gameobject_loot_template set mincount = 2, maxcount= 8 where item = 36904;
update gameobject_loot_template set mincount = 2, maxcount= 8 where item = 36903;
update gameobject_loot_template set mincount = 2, maxcount= 8 where item = 36907;

-- Mining
update gameobject_loot_template set mincount = 4, maxcount= 8 where item = 189979;
update gameobject_loot_template set mincount = 4, maxcount= 8 where item = 189981;
update gameobject_loot_template set mincount = 2, maxcount= 8 where item = 189978;
update gameobject_loot_template set mincount = 2, maxcount= 8 where item = 189980;
update gameobject_loot_template set mincount = 5, maxcount= 10 where item = 191133;

