SET @ENTRY := 500000;
DELETE FROM `creature_template` WHERE `entry` IN (@ENTRY+49, @ENTRY+50);
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `dmgschool`, `BaseAttackTime`, `RangeAttackTime`, `BaseVariance`, `RangeVariance`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `HealthModifier`, `ManaModifier`, `ArmorModifier`, `DamageModifier`, `ExperienceModifier`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `VerifiedBuild`) 
VALUES
(@ENTRY+49, 0, 0, 0, 0, 0, 22506, 0, 0, 0, 'Crystal Guardian', '', '', 0, 255, 255, 2, 1, 0, 1, 1, 1, 0, 0, 2000, 2000, 1, 3, 8, 0, 2048, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 3, 1, 1000000, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '', 1), -- Alliance
(@ENTRY+50, 0, 0, 0, 0, 0, 22506, 0, 0, 0, 'Crystal Guardian', '', '', 0, 255, 255, 2, 2, 0, 1, 1, 1, 0, 0, 2000, 2000, 1, 3, 8, 0, 2048, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 3, 1, 1000000, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, '', 1); -- Horde

UPDATE `creature_template` SET `unit_flags`=`unit_flags`|4|8, `AIName`='SmartAI' WHERE `entry` IN (@ENTRY+49, @ENTRY+50);

-- SAI
SET @SPELL := 31984;
DELETE FROM `smart_scripts` WHERE `entryorguid` IN (@ENTRY+49, @ENTRY+50);
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`)
VALUES
(@ENTRY+49, 0, 0, 0, 9, 0, 100, 0, 0, 165, 1, 1000, 11, @SPELL, 0, 0, 0, 0, 0, 25, 165, 1, 0, 0, 0, 0, 0, 'Crystal Guardian (Alliance) - On Hostile Player Enter Range - Cast Finger of Death'),
(@ENTRY+50, 0, 0, 0, 9, 0, 100, 0, 0, 165, 1, 1000, 11, @SPELL, 0, 0, 0, 0, 0, 25, 165, 1, 0, 0, 0, 0, 0, 'Crystal Guardian (Horde) - On Hostile Player Enter Range - Cast Finger of Death');

-- Spawns
DELETE FROM `creature` WHERE `id` IN (500049, 500050);
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`, `VerifiedBuild`)
VALUES
(NULL, 500049, 1, 0, 0, 1, 1, 0, 0, -11286, -4731.09, 3.96211, 2.36294, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(NULL, 500049, 1, 0, 0, 1, 1, 0, 0, -11358.7, -4762.08, 6.02417, 1.97416, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(NULL, 500049, 1, 0, 0, 1, 1, 0, 0, -11389, -4801.14, 0.896523, 0.619343, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(NULL, 500049, 1, 0, 0, 1, 1, 0, 0, -11393.8, -4695.27, 6.1404, 4.22825, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(NULL, 500049, 1, 0, 0, 1, 1, 0, 0, -11439.9, -4671.43, 6.77265, 4.54241, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(NULL, 500049, 1, 0, 0, 1, 1, 0, 0, -11508.7, -4630.38, 0.416546, 5.2689, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(NULL, 500049, 1, 0, 0, 1, 1, 0, 0, -11566.2, -4697.71, 0.490008, 6.14855, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(NULL, 500049, 1, 0, 0, 1, 1, 0, 0, -11550.6, -4660.91, 0.333216, 0.140258, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(NULL, 500049, 1, 0, 0, 1, 1, 0, 0, -11315.5, -4678.17, 5.39993, 0.297354, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(NULL, 500050, 1, 0, 0, 1, 1, 0, 0, -11875.5, -4730.02, 5.92134, 0.179585, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(NULL, 500050, 1, 0, 0, 1, 1, 0, 0, -11782.7, -4765.88, 5.32996, 2.54615, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(NULL, 500050, 1, 0, 0, 1, 1, 0, 0, -11795.5, -4715.92, 4.29792, 3.21255, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(NULL, 500050, 1, 0, 0, 1, 1, 0, 0, -11856.9, -4782.45, 6.46542, 5.97714, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(NULL, 500050, 1, 0, 0, 1, 1, 0, 0, -11846.8, -4852.19, 2.74884, 1.52, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(NULL, 500050, 1, 0, 0, 1, 1, 0, 0, -11828.4, -4590.82, 0.551029, 0.607626, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(NULL, 500050, 1, 0, 0, 1, 1, 0, 0, -11933.6, -4627.88, 0.945085, 0.246343, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0),
(NULL, 500050, 1, 0, 0, 1, 1, 0, 0, -11889.9, -4566.33, 0.529932, 4.37204, 300, 0, 0, 1, 0, 0, 0, 0, 0, 0);

-- Remove placeholders
DELETE FROM `gameobject` WHERE `guid` IN (318522, 318629) AND `id`=185491;
