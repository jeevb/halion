SET @VENDOR_ID_START := 500143;

DELETE FROM `creature_equip_template` WHERE `entry` BETWEEN @VENDOR_ID_START AND @VENDOR_ID_START + 3;
INSERT INTO `creature_equip_template` (`entry`, `id`, `itemEntry1`, `itemEntry2`, `itemEntry3`, `VerifiedBuild`)
VALUES
    (@VENDOR_ID_START, 1, 34505, 12747, 0, 1),
    (@VENDOR_ID_START + 1, 1, 34505, 12747, 0, 1),
    (@VENDOR_ID_START + 2, 1, 29405, 6618, 0, 1),
    (@VENDOR_ID_START + 3, 1, 29405, 6618, 0, 1);
