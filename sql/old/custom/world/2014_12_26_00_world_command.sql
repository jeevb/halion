DELETE FROM `command` WHERE `name` = 'spectate';
INSERT INTO `command` (`name`, `permission`, `help`) VALUES ('spectate', 1002, 'Syntax: .spectate $subcommand.\nUse .help spectate');
DELETE FROM `command` WHERE `name` = 'spectate player';
INSERT INTO `command` (`name`, `permission`, `help`) VALUES ('spectate player', 1003, 'Syntax: .spectate player #player\nSpectate the selected player in an ongoing match.');
DELETE FROM `command` WHERE `name` = 'spectate leave';
INSERT INTO `command` (`name`, `permission`, `help`) VALUES ('spectate leave', 1004, 'Syntax: .spectate leave\nLeave spectator mode.');
