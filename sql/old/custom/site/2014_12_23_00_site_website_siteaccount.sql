# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.40-MariaDB-1~wheezy-log)
# Database: site
# Generation Time: 2014-12-24 05:47:26 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table website_siteaccount
# ------------------------------------------------------------

DROP TABLE IF EXISTS `website_siteaccount`;

CREATE TABLE `website_siteaccount` (
  `account_id` int(10) unsigned NOT NULL,
  `vote_points` int(10) unsigned NOT NULL DEFAULT '0',
  `donation_points` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table website_siteaccount_updates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `website_siteaccount_updates`;

CREATE TABLE `website_siteaccount_updates` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(10) unsigned NOT NULL DEFAULT '0',
  `vote_points_old` int(10) unsigned NOT NULL DEFAULT '0',
  `vote_points_new` int(10) unsigned NOT NULL DEFAULT '0',
  `donation_points_old` int(10) unsigned NOT NULL DEFAULT '0',
  `donation_points_new` int(10) unsigned NOT NULL DEFAULT '0',
  `entity_type` enum('vendor','site','gm','leveling') DEFAULT NULL COMMENT 'Type of system awarding or revoking points. Add to the enum list in case new systems come up.',
  `entity_id` int(10) unsigned DEFAULT NULL COMMENT 'Id of who or whatever carried out the transaction, if applicable.',
  `comment` varchar(255) NOT NULL DEFAULT '' COMMENT 'Further information on the transaction.',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `entity_type` (`entity_type`),
  KEY `entity_id` (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
